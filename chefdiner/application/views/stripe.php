	<!DOCTYPE html>
	<html>
		<head>
			<title>Stripe Payment</title>
			<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
<style type="text/css">
    body{
     font-family: 'Poppins', sans-serif;
    }
    .wraper{
        width: 100%;
    }

    .web-app-inner{
        max-width: 360px;
        width:100%;
        display: block;
        margin: 0 auto;
    }

    .web-app-inner .payOption{
        width: 100%;
    }
    .web-app-inner .payOption .payLogo{
        max-width: 120px;
        margin: 0 auto;
        display: block;
    }

    .web-app-inner .payOption .payLogo img{
        max-width: 100%;
    }

    .web-app-inner .payOption h3{
        font-size: 18px;
        font-weight: 500;
        color: #3b160e;
        margin-bottom: 5px;
    }
    .web-app-inner .payOption   p {
        margin: 0;
        font-size: 13px;
        color: #737583;
    }
    .web-app-inner .payOption .payList{
        list-style: none;
        display: block;
        padding: 0;
    }

    .web-app-inner .payOption .payList li{
        text-align: left;
        display: table;
        width: 100%;
        border: solid 1px #ccced6;
        border-radius: 3px;
        position: relative;
        padding: 0 5px;
        margin-bottom: 8px;
    }

    .web-app-inner .payOption .payList li.active{
        border: solid 1px #fa471e;
        background:rgba(254,60,70,0.1);
    }

    .web-app-inner .payOption .payList li.no-brder{
        border:none;
    }
    .web-app-inner .payOption .payList li input[type="radio"]{
        opacity: 0;
        display: inline-block;
        position: absolute;
    }

    
    .web-app-inner .payOption .payList li label{
        margin: 3px 12px 0px 27px;
        display: block;
        cursor: pointer;
        color: #ccced6;
        font: 500 14px/1.9em 'Poppins', sans-serif;
        padding: 3px;
    }
    .web-app-inner .payOption .payList li label .pywith{
        float: left;
    display: block;
    font-size: 13px;
    color: #12131d;
    line-height: 16px;
    }

    .web-app-inner .payOption .payList li label .pywith span{
        color: #737583;
        font-size: 12px;
    }
    .web-app-inner .payOption .payList li label img{
        float: right;
    }
    .web-app-inner .payOption .payList li input[type=radio] ~ label:before {
    content: "";
    display: inline-block;
    position: absolute;
    top: 8px;
    left: 8px;
    height: 15px;
    width: 15px;
    border-radius: 50%;
    border: solid 1px #ccced6;
    }

    .web-app-inner .payOption .payList li input[type=radio] ~ label:before {
        top: 13px;
        left: 8px;
    }
    .web-app-inner .payOption .payList li input[type=radio] ~ label:after {
        content: '';
        width: 13px;
        height: 13px;
        position: absolute;
        background:#fa471e;
        border-top: none;
        border-right: none;
        opacity: 0;
        border-radius: 50%;
        }
    
    .web-app-inner .payOption .payList li input[type=radio] ~ label:after {
        top: 15px;
        left: 10px;
    }

    .web-app-inner .payOption .payList li input[type=radio]:checked ~ label:before {
        border: solid 1px #fa471e;
    }

    .web-app-inner .payOption .payList li input[type=radio]:checked ~ label:after {
        opacity: 1;
    }
    .web-app-inner .payOption .payList li input[type="submit"]{
        border: none;
        background: #fe4046;
        color: #fff;
        text-align: center;
        font-size: 15px;
        line-height: 40px;
        width: 100%;
        cursor: pointer;
        margin-top: 10px;
        font-weight: 600;
        text-transform: uppercase;
        -webkit-appearance: none;
    }



.web-app-inner .payOption .payList li.inputFld{
    padding: 0;
}
.web-app-inner .payOption .payList li.inputFld label {
    color: #12131d;
    display: block;
    font-size: 14px;
    font-weight: 400;
    margin-left: 0;
    font-size: 16px;
}
.web-app-inner .payOption .payList li.inputFld input {
    background-color: #fff;
    border: 1px solid #cdcfd8;
    border-radius: 3px;
    color: #333;
    display: block;
    font-size: 14px;
    height: 32px;
    padding: 4px;
    width: 100%;
    -moz-box-sizing: content-box;
    -webkit-box-sizing: content-box;
}

.web-app-inner .payOption .payList li.inputFld input#cardnumber { max-width: 310px;}
.web-app-inner .payOption .payList li.inputFld input::-webkit-input-placeholder {
  color: #ddd;
}
.web-app-inner .payOption .payList li.inputFld input:-moz-placeholder {
  color: #ddd;
  opacity: 1;
}
.web-app-inner .payOption .payList li.inputFld input::-moz-placeholder {
  color: #ddd;
  opacity: 1;
}
.web-app-inner .payOption .payList li.inputFld input:-ms-input-placeholder {
  color: #ddd;
}
.web-app-inner .payOption .payList li.inputFld input:focus {
  outline: 1px solid #3388dd;
}
.web-app-inner .payOption .payList li #cardnumber {
  background-image: url(../../assets/img/images.png);
  background-position: 5px -117px, 260px -61px;
  background-size: 120px 361px, 120px 361px;
  background-repeat: no-repeat;
  padding-left: 54px;
}
.web-app-inner .payOption .payList li #cardnumber.visa {
  background-position: 5px -159px, 260px -61px;
}
.web-app-inner .payOption .payList li #cardnumber.visa_electron {
  background-position: 5px -201px, 260px -61px;
}
.web-app-inner .payOption .payList li #cardnumber.mastercard {
  background-position:5px -243px, 260px -61px;
}
.web-app-inner .payOption .payList li #cardnumber.maestro {
  background-position: 5px -285px, 260px -61px;
}
.web-app-inner .payOption .payList li #cardnumber.discover {
  background-position: 5px -327px, 260px -61px;
}
.web-app-inner .payOption .payList li #cardnumber.valid.visa {
  background-position:  5px -159px, 260px -61px;
}
.web-app-inner .payOption .payList li #cardnumber.valid.visa_electron {
  background-position:5px -201px, 260px -61px;
}
.web-app-inner .payOption .payList li #cardnumber.valid.mastercard {
  background-position: 5px -243px, 260px -61px;
}
.web-app-inner .payOption .payList li #cardnumber.valid.maestro {
  background-position: 5px -285px, 260px -61px;
}
.web-app-inner .payOption .payList li #cardnumber.valid.discover {
  background-position: 5px -327px, 260px -61px;
}

.hlfFrm {
    float: left;
}

.hlfFrm.wd60  input{
    width:40% !important;
    float: left;
    margin-right: 5px;
    text-align: center;
}

.wd60 {
    width: 55%;
}
.wd40 {
    width: 45%;
}

.Price{
    color: #fe4046;
    margin-left: 7px;
    font-weight: 600;
    margin-top: -6px;
    display: block; 
}

@media (max-width:470px) {
    .web-app-inner .payOption .payList li{
        max-width: 95%;
    }
}
@media (max-width:414px) {
    .web-app-inner .payOption .payList li.inputFld input#card_number {
        max-width: 295px;
    }
}
@media (max-width:375px) {
    .web-app-inner .payOption .payList li.inputFld input#card_number {
        max-width: 291px;
    }
}
@media (max-width:360px) {
    .web-app-inner .payOption .payList li.inputFld input#card_number {
        max-width: 277px;
    }
}
@media (max-width:320px) {
    .web-app-inner .payOption .payList li.inputFld input#card_number {
        max-width: 240px;
    }
}
</style>
		</head>
	<body>
<?php 
// echo "<pre>";
// print_r($alldata);die;
?>
<div class="wraper">
 
<?php
if($currency_code !=''){
	$currency_code= $currency_code;

}else{
	$currency_code='USD';
}
?>
 <div class="web-app-inner">
            <div class="payOption">
                
                <h3>Book-Fill in the payment Details</h3>
                <p>
                    You wont't be charged yet. You wil be charged only once the chef accepts your reservation.
                </p>
                <ul class="payList">
                    <li class="active">
                        <input type="radio" name="payopt" id="stripe" checked>
                        <label for="stripe">
                            <span class="pywith">Pay with Stripe<br><span>Transition fee may apply</span></span> 
                            <img src="<?php echo base_url();?>front/images/stripe.png" alt="">
                        </label>
                    </li>
                    </ul>
                    <form id="payment-form" class="form-inline">
		  				<span class="payment-errors"></span>
                    <ul class="payList">
                    <li class="inputFld no-brder">
                        <label>Card Number</label>
                        <input type="text" size="20" class="form-control" id="cardnumber" data-stripe="number" required placeholder="1234 5678 9012 3456">
                       
                    </li>
                    <li class="inputFld no-brder">
                        <label>Name on Card</label>
                        <input type="text" placeholder="Name" required>
                    </li>
                    <li class="inputFld no-brder">
                        <div class="hlfFrm wd60">
                            <label>Expiration Date</label>
                            <input type="text" size="2" class="form-control stripe-half" data-stripe="exp_month"  placeholder="MM" required>
                             <input type="text" size="2" class="form-control stripe-half" data-stripe="exp_year" placeholder="YY" required>
                        </div>
                        <div class="hlfFrm wd40">
                            <label>CVV</label>
                            <input type="password" class="form-control" data-stripe="cvc" required>
                        </div>
                    </li>
                    <li class="inputFld no-brder">
                        <label>Total Price</label>
                       <span class="Price">USD <?=round($amount,2,PHP_ROUND_HALF_UP);?></span>
                    </li>

                    <input type="hidden" name="booking_id" id="booking_id" value="<?=$bookingid;?>">
			<input type="hidden" name="user_id" id="user_id" value="<?=$userid;?>">
			<input type="hidden" name="total_price" id="total_price" value="<?=round($amount,2,PHP_ROUND_HALF_UP);?>">
			<input type="hidden" name="device" id="device" value="<?=$device;?>">
			<input type="hidden" name="currency_code" id="currency_code" value="<?=$currency_code;?>">
                    <li class="no-brder">
                    <input type="submit" class="submit btn btn-eff btn-stripe" value="Pay Now" id="mypage1">
			<a href="<?php echo base_url() ?>index.php/admin/cancelStripe/<?=$bookingid;?>/<?=$device;?>" class="cancel">Cancel</a>
                        <!-- <input type="submit" value="Submit Booking"> -->
                    </li>
                    
                </ul>
                </form>
            </div>
        </div>


	</div>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script  src="https://js.stripe.com/v2/"></script>
	
	<script>
	
	  Stripe.setPublishableKey('pk_test_IHroD9XKiRrmuBm3WqUCg6y4');
	  $(function() {
	  
	  var $form = $('#payment-form');
	  $form.submit(function(event) {

		// Disable the submit button to prevent repeated clicks:
		$form.find('.submit').prop('disabled', true);
		$form.find('.submit').val('Please wait...');

		// Request a token from Stripe:
		Stripe.card.createToken($form, stripeResponseHandler);
	
		return false;
	  });
	});
	 function stripeResponseHandler(status, response) {

		 
		 if (response.error) {
			alert(response.error.message);
			 location.reload();
			/*$form.find('.submit').prop('disabled', false);
			$form.find('.submit').val('Pay Now');*/
		 } else {

		 	var booking_id= '<?php echo $bookingid?>';
		 	var total_price='<?php echo round($amount)?>';
		 	var user_id='<?php echo $userid?>';
		 	var device='<?php echo $device?>';
		 	//var currency_code='<?php //echo $currency_code?>';
		 	
			$.ajax({
				// url: '<?php //echo base_url('stripe/payment/process');?>',
				url: '<?php echo base_url()?>index.php/admin/stripe',
				cache:false,
				data: {access_token: response.id,amount:total_price,bookingid:booking_id,userid:user_id,
					device:device},
				type: 'POST',
				dataType: 'JSON',
				success: function(response){
					console.log(response);
					console.log(response.data);
					if(response.success)

					window.location.href="<?php echo base_url() ?>index.php/admin/successStripe/"+response.data;
				},
				error: function(error){

					console.log(error);
				}
			});
			//console.log(response.id);
		}
	 }
	</script>

	<script src="<?php echo base_url();?>front/js/jquery.min.js"></script>
    <script>
    $('input:radio').click(function() {
        $('input:radio[name='+$(this).attr('name')+']').parent().removeClass('active');
        $(this).parent().addClass('active');
    });
    </script>

<script src="<?php echo base_url();?>front/js/jquery.creditCardValidator.js"></script>
<script>
    $(function() {
        $('#cardnumber').validateCreditCard(function(result) {
            if(result.card_type == null)
            {
                $('#cardnumber').removeClass();
            }
            else
            {
                $('#cardnumber').addClass(result.card_type.name);
            }
            
            if(!result.valid)
            {
                $('#cardnumber').removeClass("valid");
            }
            else
            {
                $('#cardnumber').addClass("valid");
            }
            
        });
    });
</script>

    </body>
</html>
