<?php
  $this->load->view('admin/header');
?>
<?php
  $this->load->view('admin/sidebar');
?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url();?>index.php/admin/main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> <?php echo $this->lang->line('home'); ?></a> <a href="waiting" class="current">Waiting Booking</a> </div>
    <h1>Booking List <span>(Waiting)</span></h1>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
<hr>
  <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>

       <?php 
        $langu =  $this->session->userdata('site_lang');
          if($langu !=''){
            $langu =  $this->session->userdata('site_lang');
          }else{
            $langu = 'english';
          }
      ?>

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Waiting booking list</h5>
            
          
          </div>
         
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th><?php echo $this->lang->line('serialno') ?></th>
                  <th>Chef Name</th>
                  <th>User Name</th>
                  <th>Booking Date</th>
                  <th>Booking Time</th>
                  <th>Payment Type</th>
                  <th>Payment Status</th>
                  <th>Booking Status</th>
                  <!-- <th>Delete</th> -->
                </tr>
              </thead>
              <tbody>
              <?php 
              if(!empty($waitingBooking)){
                $i=1;
                foreach ($waitingBooking as $key => $value) { 
                  
                  ?>
                <tr class="gradeX" align="center">
                  <td><?php echo $i; ?></td>
                  <td><?php echo ucfirst($value['cheffname'].' '.$value['cheflname']); ?></td>
                  <td><?php echo ucfirst($value['first_name'].' '.$value['last_name']); ?></td>
                  <td><?php echo $value['date']; ?></td>
                  <td><?php echo $value['time']; ?></td>
                  <td><?php echo $value['payment_type']; ?></td>
                  <td><?php echo $value['payment_status']; ?></td>
                  <td><span class="btn btn-warning btn-mini"><?php echo $value['booking_status']; ?></span></td>
                  <!-- <td> <a href="<?php // echo base_url();?>index.php/admin/deleteusers/<?php //echo $value['booknow_id']; ?>" lass="btn btn-success btn-mini"> Delete <i class="icon-pencil"></i></a> </td> -->
                  
                </tr>
          <?php  
            $i++;

              }

              }
              ?>
               
               
              </tbody>
            </table>
          </div>
        </div>
</div>

<!--end-main-container-part-->

<?php
  $this->load->view('admin/footer');
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 
<script type="text/javascript"  src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.uniform.js"></script> 
<script src="<?php echo base_url();?>assets/js/matrix.tables.js"></script> 
<script type="text/javascript">
  
  function confirmDialog(lan) {
   // alert(lan);
    if(lan =='simplified-chinese'){
      return confirm("您确定要删除此记录吗？")
    }else if(lan =='traditional-chinese'){
      return confirm("您確定要刪除此記錄嗎？")
    }else{
      return confirm("Are you sure you want to delete this record?")
    }
    
  }


</script>