<?php
  $this->load->view('admin/header');
?>
<?php
  $this->load->view('admin/sidebar');
?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> <?php echo $this->lang->line('home'); ?></a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb"> <a href="<?php echo base_url(); ?>index.php/admin/main"> <i class="icon-dashboard"></i>  <?php echo $this->lang->line('dashboard'); ?> </a> </li>
        <li class="bg_lg span3"> <a href="<?php echo base_url(); ?>index.php/admin/privacyPolicy"> <i class="icon icon-th-list"></i><span class="label label-important">4</span>  <?php echo $this->lang->line('pages'); ?></a> </li>
        <li class="bg_ly"> <a href="<?php echo base_url(); ?>index.php/admin/users"> <i class="icon-group"></i> <?php echo $this->lang->line('user'); ?> </a> </li>
      <li class="bg_lg span3"> <a href="<?php echo base_url(); ?>index.php/admin/menus"> <i class="icon-th-list"></i> <?php echo $this->lang->line('menu'); ?></a> </li>
        <!-- <li class="bg_ly"> <a href="#"> <i class="icon-inbox"></i><span class="label label-success">101</span> Widgets </a> </li>
          <li class="bg_lo"> <a href="#"> <i class="icon-th"></i> Tables</a> </li>
        <li class="bg_ls"> <a href="#"> <i class="icon-fullscreen"></i> Full width</a> </li>
        <li class="bg_lo span3"> <a href="#"> <i class="icon-th-list"></i> Forms</a> </li>
        <li class="bg_ls"> <a href="#"> <i class="icon-tint"></i> Buttons</a> </li>
        <li class="bg_lb"> <a href="#"> <i class="icon-pencil"></i>Elements</a> </li>
        <li class="bg_lg"> <a href="#"> <i class="icon-calendar"></i> Calendar</a> </li>
        <li class="bg_lr"> <a href="#"> <i class="icon-info-sign"></i> Error</a> </li> -->

      </ul>
    </div>
<!--End-Action boxes-->    
<!--Chart-box-->    
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
          <h5>Site Analytics</h5>
        </div>
        <div class="widget-content" >
          <div class="row-fluid">
            <!-- <div class="span9">
              <div class="chart"></div>
            </div> -->
            <div class="span4">
              
               <ul class="site-stats">
                <li class="bg_lh"><i class="icon-user"></i> <strong><?=count($verifyusers); ?></strong> <small>Total Verified Users</small></li>
                <li class="bg_lh"><i class="icon-user"></i> <strong><?=count($notverifyusers); ?></strong> <small>Total Not Verified Users</small></li>
                <li class="bg_lh"><i class="icon-group"></i> <strong><?=count($totalChef); ?></strong> <small>Total Chefs</small></li>
                <li class="bg_lh"><i class="icon-shopping-cart"></i> <strong><?= count($waitingbooking);?></strong> <small> Total Waiting Booking</small></li>
                <li class="bg_lh"><i class="icon-shopping-cart"></i> <strong><?= count($acceptbooking);?></strong> <small>Total Accepted Booking</small></li>
                <li class="bg_lh"><i class="icon-shopping-cart"></i> <strong><?= count($rejectbooking);?></strong> <small>Total Rejected Booking</small></li>
              </ul>
              
            </div>

            <div class="span8">
<div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
          <h5>Recent Transaction </h5>
        </div>
         <div class="widget-content" >
          <div class="row-fluid">

            <table class="table table-bordered data-table">
              <thead>
                <tr>
                        <th>S No. <i class="fa fa-sort"></i></th>
                        <th>Payment Type <i class="fa fa-sort"></i></th>
                        <th>Transaction Id <i class="fa fa-sort"></i></th>
                        <th>Payment Date <i class="fa fa-sort"></i></th>
                        <th>Amount (USD/INR) <i class="fa fa-sort"></i></th>
                        <th>Payment Status <i class="fa fa-sort"></i></th>
                     
                </tr>
              </thead>
              <tbody>
                  <?php 
              if(!empty($payment)){
                $i=1;
                foreach ($payment as $key => $value) { 

                  if($value['currency_code'] =='USD'){
                    $amt = '$'.$value['payment_gross'];

                  }else{
                    $amt = 'Rs.'.$value['payment_gross'];
                  }
                  
                  ?>
                <tr class="gradeX" align="center">
                  <td><?php echo $i; ?></td>
                  <td><?php echo ucfirst($value['payment_type']); ?></td>
                  <td><?php echo $value['txn_id']; ?></td>
                  <td><?php echo $value['created_at']; ?></td>
                  <td><?php echo $amt; ?></td>
                  <td><?php echo $value['payment_status']; ?></td>
                 
                </tr>
          <?php  
            $i++;

              }

              }
              ?>

              </tbody>
              </table>

          </div>

          </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--end-main-container-part-->

<?php
  $this->load->view('admin/footer');
?>