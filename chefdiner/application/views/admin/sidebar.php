<!--sidebar-menu-->
<div id="sidebar">
<?php 
// Define a default Page
  $pg = isset($page) && $page != '' ?  $page :'admindash'  ;    
 // echo $pg;
?>
<a href="#" class="visible-phone"><i class="icon icon-home"></i> <?php echo $this->lang->line('dashboard'); ?></a>
  <ul>
    <li <?php echo  $pg =='admindash' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>admin/main"><i class="icon icon-home"></i> <span><?php echo $this->lang->line('dashboard'); ?></span></a> </li>
    
    <li <?php if($pg=='privacypolicy' || $pg=='termsconditions' || $pg =='contact' || $pg=='jobs' || $pg=='addjobs'){  echo 'class="submenu active"';}else{ echo 'class="submenu"';} ?>> <a href="#"><i class="icon icon-th-list"></i> <span><?php echo $this->lang->line('pages'); ?></span> <span class="label label-important">4</span></a>
      <ul>
        <li <?php echo  $pg =='privacypolicy' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>admin/privacyPolicy"><?php echo $this->lang->line('privacy'); ?></a></li>
        <li <?php echo  $pg =='termsconditions' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>admin/termsConditions"><?php echo $this->lang->line('termscondition'); ?></a></li>
        <li <?php echo  $pg =='contact' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>admin/contact "><?php echo $this->lang->line('contact'); ?></a></li>
        <li <?php if($pg=='jobs' || $pg=='addjobs'){  echo 'class="active"';}else{ echo 'class=""';}?>><a href="<?php echo base_url();?>admin/jobs "><?php echo $this->lang->line('jobs'); ?></a></li>
      </ul>
    </li>

    <li <?php echo  $pg =='userlist' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>admin/users"><i class="icon-group"></i> <span><?php echo $this->lang->line('user'); ?></span></a> </li>

     <li <?php echo  $pg =='menulist' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>admin/menus"><i class="icon icon-th-list"></i> <span><?php echo $this->lang->line('menu'); ?></span></a> </li>
    
     

     <li <?php if($pg=='waiting' || $pg=='accepted' || $pg =='rejected'){  echo 'class="submenu active"';}else{ echo 'class="submenu"';} ?>> <a href="#"><i class="icon icon-th-list"></i> <span>Booking Details</span></a>
      <ul>
        <li <?php echo  $pg =='waiting' ? 'class="active"' : '' ?>><a href="<?php echo base_url() ?>admin/waiting">Waiting</a></li>
        <li <?php echo  $pg =='accepted' ? 'class="active"' : '' ?>><a href="<?php echo base_url() ?>admin/accepted">Accepted</a></li>
        <li <?php echo  $pg =='rejected' ? 'class="active"' : '' ?>><a href="<?php echo base_url() ?>admin/rejected">Rejected</a></li>
        
      </ul>
    </li>

      <li <?php echo  $pg =='referralpage' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>admin/referral"><i class="icon icon-th-list"></i> <span>Referral Amount</span></a> </li>

      <li <?php echo  $pg =='admincharge' ? 'class="active"' : '' ?>><a href="<?php echo base_url();?>admin/admincharge"><i class="icon icon-th-list"></i> <span>Admin Charges</span></a> </li>

      <!-- <li <?php //echo  $pg =='transation' ? 'class="active"' : '' ?>><a href="<?php //echo base_url();?>index.php/admin/transation"><i class="icon icon-th-list"></i> <span>Transaction Details</span></a> </li> -->
  
  </ul>
</div>
<!--sidebar-menu-->
