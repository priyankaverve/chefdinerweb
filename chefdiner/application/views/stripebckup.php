	<!DOCTYPE html>
	<html>
		<head>
			<title>Stripe Payment</title>
			<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<style type="text/css">
    .wraper{
        width: 100%;
    }

    .web-app-inner{
        max-width: 300px;
        width:100%;
        display: block;
        margin: 0 auto;
    }

     .web-app-inner h3{
        font-size: 20px;
        font-weight: 500;
        color: #3b160e;
        text-align: center;
    }
   .form-group label{
    width: 100px;
    float: left;
    font-size: 14px;
    }
    .form-group label span{
    	font-size: 11px;
    	line-height: 12px;
    }
    .form-group  input[type="text"],
    .form-group  input[type="password"]{
    	border: solid 1px #ccc;
	    border-radius: 3px;
	    font-size: 14px;
	    line-height: 1.6em;
    }
    .form-group  input[type="submit"],
    .form-group  a.cancel{
    	color: #fff;
		font-size: 14px;
		font-weight: 500;
		line-height: 28px;
		border: none;
		cursor: pointer;
		padding: 5px 0;
		width: 140px;
		margin: 0 auto;
    }

    .form-group  input[type="submit"].submit{
    	background: #21a2ec;
    }
    .form-group  a.cancel{
    	background: #c40d06;
    	text-decoration: none;
    	display: inline-block;
    	text-align: center;
    }
    .stripe-half{
    	width:82px;
    }
    .form-group {
	    margin-bottom: 15px;
	    clear: both;
	}
	.web-app-inner .payLogo{
        max-width: 120px;
        margin: 0 auto;
        display: block;
    }
    </style>
		</head>
	<body>
<?php 
// echo "<pre>";
// print_r($alldata);die;
?>
<div class="wraper">
  <div class="web-app-inner">
  
	
	<div class="payLogo">
        <img src="<?php echo base_url();?>front/images/stripe.png" alt="">
    </div>
	<h3>Card Details</h3>
		<form action="/your-charge-code" method="POST" id="payment-form" class="form-inline">
		  <span class="payment-errors"></span>
		   <div class="form-group">
			    <label class="control-label" for="cardnumber">Card Number:</label>    
	            <input type="text" size="20" class="form-control" id="cardnumber" data-stripe="number" required>
           </div>
			<div class="form-group">
                <label class="control-label" for="cardnumber">Expiration <span>(MM/YY)</span>:</label>
                <input type="text" size="2" class="form-control stripe-half" data-stripe="exp_month" required>
                <input type="text" size="2" class="form-control stripe-half" data-stripe="exp_year" required>
             </div>
             <div class="form-group">
               <label class="control-label" for="cardnumber">CVV Number:</label>
               <input type="password" class="form-control" data-stripe="cvc" required>
            </div>
		
			<input type="hidden" name="booking_id" id="booking_id" value="<?=$bookingid;?>">
			<input type="hidden" name="user_id" id="user_id" value="<?=$userid;?>">
			<input type="hidden" name="total_price" id="total_price" value="<?=$amount;?>">
			<input type="hidden" name="device" id="device" value="<?=$device;?>">
			<input type="hidden" name="currency_code" id="currency_code" value="<?=$currency_code;?>">
			<div class="form-group">
			<input type="submit" class="submit btn btn-eff btn-stripe" value="Pay Now">
			<a href="<?php echo base_url() ?>index.php/admin/cancelStripe/<?=$bookingid;?>/<?=$device;?>" class="cancel">Cancel</a>
		  </div>
	</form>
	
	</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

	<script type="text/javascript">
	  Stripe.setPublishableKey('pk_test_IHroD9XKiRrmuBm3WqUCg6y4');
	  $(function() {
	  //	alert('hello');
	  var $form = $('#payment-form');
	  $form.submit(function(event) {
		// Disable the submit button to prevent repeated clicks:
		$form.find('.submit').prop('disabled', true);
		$form.find('.submit').val('Please wait...');

		// Request a token from Stripe:
		Stripe.card.createToken($form, stripeResponseHandler);
		// Prevent the form from being submitted:
		return false;
	  });
	});
	 function stripeResponseHandler(status, response) {
		 
		 if (response.error) {
			alert(response.error.message);
		 } else {

		 	var booking_id= <?php echo $bookingid?>;
		 	var total_price=<?php echo $amount?>;
		 	var user_id=<?php echo $userid?>;
		 	var device='<?php echo $device?>';
		 	//var currency_code=<?php //echo $currency_code?>;
		 	
			$.ajax({
				// url: '<?php //echo base_url('stripe/payment/process');?>',
				url: '<?php echo base_url()?>index.php/admin/stripe',
				data: {access_token: response.id,
					amount:total_price,
					bookingid:booking_id,
					userid:user_id,
					//currency_code:currency_code,
					device,device},
				type: 'POST',
				dataType: 'JSON',
				success: function(response){
					console.log(response);
					console.log(response.data);
					if(response.success)

					window.location.href="<?php echo base_url() ?>index.php/admin/successStripe/"+response.data;
				},
				error: function(error){
					console.log(error);
				}
			});
			console.log(response.id);
		}
	 }
	</script>

    </body>
</html>
