<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/my-kitchen.css">

</head>
<body>
<?php
$this->load->view('front/header'); ?>
<div class="wrapper mykitchenPage">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h1 class="clr-black">Mani's Kitchen <a href="">Preview</a></h1>
                <p class="clr-gray"><span class="clr-red">2</span> steps to become a chef!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="KtchnNav">
                    <ul>
                        <li class="kitchen"><a href="mykitchen">My Kitchen</a></li>
                        <li class="profile"><a href="myprofile">Profile</a></li>
                        <li class="address"><a href="myaddress">Address</a></li>
                        <li class="photos"><a href="myphotos">Photos</a></li>
                        <li class="ktchnmenu active"><a href="mymenus">Menu</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="kithchenForm picUploadPage">
                    <div class="form-group sbmt-row fileUpload">
                        <a href="javascript:;" class="txt-upr addMenu float-left">Add New Menu</a>
                    </div>
                    <div class="row clearFix">
                        <div class="col-lg-7">
                            <div class="menuItemSection">
                            <?php 
                            if(!empty($getmenus)){  
                                foreach ($getmenus as $key => $value) {
                                    if($value['menu_image']){
                                        $menu_image = base_url().'assets/menuImgae/'.$value['menu_image'];

                                    }else{
                                        $menu_image =base_url().'front/images/blank.jpg';
                                    }

                                     ?>
                                    
                                    
                                <div class="menuItemBlock">
                                    <div class="itmeDetail">
                                        <h4 class="clr-red ItmName"><a href="javascript:;" onclick='getsubmenu(<?php echo $value['id'] ?>)' class="clr-red"><?=$value['menu_title']?></a></h4>
                                        <p class="itmPrc clr-gray"><?=$value['currency']?> <?=$value['price_per_person']?> per guest</p>
                                        <div class="srvcType">
                                            <h5>Service Type:-</h5>
                                            <p class="clr-gray"><?=$value['service_type']?></p>
                                        </div>
                                        <div class="itmCusinType srvcType">
                                            <h5>Cuisine type :-</h5>
                                            <p class="clr-gray"><?=$value['cusine']?></p>
                                        </div>
                                    </div>  
                                    <div class="itemIng">
                                        <div class="img">
                                            <img src="<?php echo $menu_image;?>" alt="">
                                        </div>
                                    </div>
                                    <div class="menuItemOpt">
                                        <ul>
                                            <li class="up"><a href="javascript:;">Up</a></li>
                                            <li class="down"><a href="javascript:;">Down</a></li>
                                            <li><a href="javascript:;">Draft</a></li>
                                            <li><a href="javascript:;">Edit</a></li>
                                            <li><a href="javascript:;">Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <?php  }
                                } ?>

                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="menuItemBlock onlyDish">
                                <!-- <div class="itmeDetail">
                                    <h4 class="clr-black ItmName">Gong Bao Chicken</h4>
                                    <p class="itmPrc clr-gray">Main Course</p>
                                </div>  
                                <div class="itemIng">
                                    <div class="img">
                                        <img src="images/chefitem.png" alt="">
                                    </div>
                                </div>
                                <div class="menuItemOpt">
                                    <ul>
                                        <li class="up"><a href="javascript:;">Up</a></li>
                                        <li class="down"><a href="javascript:;">Down</a></li>
                                        <li><a href="javascript:;">Draft</a></li>
                                        <li><a href="javascript:;">Edit</a></li>
                                        <li><a href="javascript:;">Delete</a></li>
                                    </ul>
                                </div> -->
                                <div class="form-group sbmt-row fileUpload">
                                    <a href="javascript:;" class="txt-upr addDish float-right">Add New Dish</a>
                                </div>
                            </div>
                                
                        </div>
                    </div>
                    <div class="form-group sbmt-row">
                        <a href="javascript:;" class="txt-upr sbmt float-left">Complete</a>
                        <a href="#" class="txt-upr prvw float-right">Preview</a>
                    </div>
                    <div class="modal fade menuModel" id="MenuModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2 class="clr-black">Menu Description</h2>
                                        </div>
                                    </div>
                                    <form action="<?php echo base_url() ?>index.php/wwlcome/addmenus" method="post">
                                    <input type="hidden" name="chefid" value="<?php if(isset($getchefdata[0]['user_id'])){ echo $getchefdata[0]['user_id'];}?>">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="clr-gray">Menu Title</label>    
                                                    <input type="text" class="input">
                                                </div>
                                                <div class="form-group menuPrice">
                                                    <label class="clr-gray">Price Per Person</label>    
                                                    <input type="text" class="input float-left" name="price_per_person">
                                                   <div class="flagstrap float-left" id="select_nenu_currency" data-input-name="NewBuyer_country" data-selected-country=""></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group dishImg">
                                                    <label class="clr-red" for="dishImgUpload">Upload Image</label>
                                                    <input type="file" id="dishImgUpload" name="fileToUpload">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <h4 class="clr-gray">Service Type</h4>    
                                                    <ul class="serviceBlck menuService"> 
                                                        <li>
                                                            <input class="radio" type="checkbox" id="FoodDelivery" name="serveType[]" value="Food Delivery">
                                                            <label class="radiobLable" for="FoodDelivery"> Food Delivery</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="PrivateMeal" name="serveType[]" value="Home Private Meal">
                                                            <label class="radiobLable" for="PrivateMeal">Home Private Meal</label>
                                                        </li>
                                                        <li class="mrRight">
                                                            <input class="radio" type="checkbox" id="DailyMeal" name="serveType[]" value="Daily Meal">
                                                            <label class="radiobLable" for="DailyMeal">Daily Meal</label>
                                                        </li>
                                                        <li class="">
                                                            <input class="radio" type="checkbox" id="privateMeal" name="serveType[]" value="Restaurant Private Meal">
                                                            <label class="radiobLable" for="privateMeal">Restaurant Private Meal</label>
                                                        </li>
                                                        <li class="">
                                                            <input class="radio" type="checkbox" id="onsiteCooking" name="serveType[]" value="Chef Onsite Cooking">
                                                            <label class="radiobLable" for="onsiteCooking">Chef Onsite Cooking</label>
                                                        </li>
                                                        <li class="">
                                                            <input class="radio" type="checkbox" id="cookingWorkshop" name="serveType[]" value="Cooking Workshop">
                                                            <label class="radiobLable" for="cookingWorkshop">Cooking Workshop</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="form-group untensilbox">
                                                    <p>
                                                        Please kindly let chef know what tools you can provide at your place.
                                                    </p>
                                                    <ul class="serviceBlck menuService utensilList"> 
                                                        <li>
                                                            <input class="radio" type="checkbox" id="potatoMasher" name="kitchenTool[]" value="Potato masher">
                                                            <label class="radiobLable" for="potatoMasher"> Potato masher</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="measuringCups" name="kitchenTool[]" value="Measuring cups/jug and spoons">
                                                            <label class="radiobLable" for="measuringCups">Measuring cups/jug and spoons</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="chefKnife" name="kitchenTool[]" value="Chef Knife">
                                                            <label class="radiobLable" for="chefKnife">Chef Knife</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="parkingKnife" name="kitchenTool[]" value="Paring Knife">
                                                            <label class="radiobLable" for="parkingKnife">Paring Knife</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="potatoricer" name="kitchenTool[]" value="Potato ricer">
                                                            <label class="radiobLable" for="potatoricer"> Potato ricer</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="ovenMitt" name="kitchenTool[]" value="Oven mitt/glove">
                                                            <label class="radiobLable" for="ovenMitt">Oven mitt/glove</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="serrated" name="kitchenTool[]" value="Serrated/Bread Knife">
                                                            <label class="radiobLable" for="serrated">Serrated/Bread Knife</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="vegetablepeelers" name="kitchenTool[]" value="Vegetable peelers">
                                                            <label class="radiobLable" for="vegetablepeelers">Vegetable peelers</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="sharpening" name="kitchenTool[]" value="Sharpening Steel">
                                                            <label class="radiobLable" for="sharpening">Sharpening Steel</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="garlicpress" name="kitchenTool[]" value="Garlic press">
                                                            <label class="radiobLable" for="garlicpress">Garlic press</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="bbqtools" name="kitchenTool[]" value="BBQ Tools">
                                                            <label class="radiobLable" for="bbqtools">BBQ Tools</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="electricsharpener" name="kitchenTool[]" value="Electric Sharpener" >
                                                            <label class="radiobLable" for="electricsharpener">Electric Sharpener</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="cuttingboard" name="kitchenTool[]" value="Egg separater">
                                                            <label class="radiobLable" for="cuttingboard">Cutting Board</label>
                                                        </li>

                                                        <li>
                                                            <input class="radio" type="checkbox" id="eggseparater" name="kitchenTool[]" value="Egg separater">
                                                            <label class="radiobLable" for="eggseparater">Egg separater</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="choppers" name="kitchenTool[]" value="Choppers">
                                                            <label class="radiobLable" for="choppers">Choppers</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="fishscaler" name="kitchenTool[]" value="Fish scaler">
                                                            <label class="radiobLable" for="choppers">Fish scaler</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="lemonpress" name="kitchenTool[]" value="Lemon press/Lemon squeezer">
                                                            <label class="radiobLable" for="lemonpress">Lemon press/Lemon squeezer</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="icecreamscoop" name="kitchenTool[]" value="Ice cream scoop">
                                                            <label class="radiobLable" for="icecreamscoop">Ice cream scoop</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="peppershakers" name="kitchenTool[]" value="Pepper mill,salt & pepper shakers">
                                                            <label class="radiobLable" for="peppershakers">Pepper mill,salt & pepper shakers</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="tongs" name="kitchenTool[]" value="Tongs">
                                                            <label class="radiobLable" for="tongs"> </label>
                                                        </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="roolingpin" name="kitchenTool[]" value="Rolling pin">
                                                            <label class="radiobLable" for="roolingpin">Rolling pin</label>
                                                        </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="canopener" name="kitchenTool[]" value="Can opener">
                                                            <label class="radiobLable" for="canopener">Can opener</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="meatgrinder" name="kitchenTool[]" value="Meat grinder">
                                                            <label class="radiobLable" for="meatgrinder">Meat grinder</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="saladspinner" name="kitchenTool[]" value="Salad spinner">
                                                            <label class="radiobLable" for="meatgrinder">Salad spinner</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="juicer" name="kitchenTool[]" value="Juicer">
                                                            <label class="radiobLable" for="juicer">Juicer</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="blender" name="kitchenTool[]" value="Blender">
                                                            <label class="radiobLable" for="blender">Blender</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="ricecooker" name="kitchenTool[]" value="Rice Cooker">
                                                            <label class="radiobLable" for="ricecooker">Rice Cooker</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="toaster" name="kitchenTool[]" value="Toaster">
                                                            <label class="radiobLable" for="toaster">Toaster</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="waterpurifier" name="kitchenTool[]" value="Water purifier">
                                                            <label class="radiobLable" for="waterpurifier">Water purifier</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="slowcooker" name="kitchenTool[]" value="Slow cooker">
                                                            <label class="radiobLable" for="slowcooker">Slow cooker</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="sandwichmaker" name="kitchenTool[]" value="Sandwich maker">
                                                            <label class="radiobLable" for="sandwichmaker">Sandwich maker</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="foodprocessor" name="kitchenTool[]" value="Food processor">
                                                            <label class="radiobLable" for="foodprocessor">Food processor</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="microwaveoven" name="kitchenTool[]" value="Microwave oven">
                                                            <label class="radiobLable" for="microwaveoven">Microwave oven</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="breadmaker" name="kitchenTool[]" value="Bread Maker">
                                                            <label class="radiobLable" for="breadmaker">Bread Maker</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="castiron" name="kitchenTool[]" value="Cast Iron Skillet">
                                                            <label class="radiobLable" for="castiron">Cast Iron Skillet</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="dutchoven" name="kitchenTool[]" value="Dutch oven">
                                                            <label class="radiobLable" for="dutchoven">Dutch ovent</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="nonstick" name="kitchenTool[]" value="Non Stick Skillet">
                                                            <label class="radiobLable" for="nonstick">Non Stick Skillet</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="stockpot" name="kitchenTool[]" value="Stock Pot">
                                                            <label class="radiobLable" for="stockpot">Stock Pot</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="grillpan" name="kitchenTool[]" value="Grill Pan">
                                                            <label class="radiobLable" for="grillpan">Grill Pan</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="reemedbaking" name="kitchenTool[]" value="Reemed baking sheets">
                                                            <label class="radiobLable" for="reemedbaking">Reemed baking sheets</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="saucepans" name="kitchenTool[]" value="Saucepans">
                                                            <label class="radiobLable" for="saucepans">Saucepans</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="wok" name="kitchenTool[]" value="Wok">
                                                            <label class="radiobLable" for="wok">Wok</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="familygriddle" name="kitchenTool[]" value="Family Griddle">
                                                            <label class="radiobLable" for="familygriddle">Family Griddle</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="roastinpan" name="kitchenTool[]" value="Roasting Pans">
                                                            <label class="radiobLable" for="roastinpan">Roasting Pans</label>
                                                         </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                   <label>Cuisine Type</label>
                                                   <select id="mainCat" class="input">
                                                       <option value="Hong Kong Style">Hong Kong Style</option>
                                                       <option value="Chinese">Chinese</option>
                                                       <option value="Cantonese">Cantonese</option>
                                                       <option value="Taiwanese">Taiwanese</option>
                                                       <option value="Japanese">Japanese</option>
                                                       <option value="Korean">Korean</option>
                                                       <option value="Thai">Thai</option>
                                                       <option value="Other Asians">Other Asians</option>
                                                       <option value="Italian">Italian</option>
                                                       <option value="French">French</option>
                                                       <option value="Western">Western</option>
                                                       <option value="Middle Eastern / Mediterranean">Middle Eastern / Mediterranean</option>
                                                       <option value="Latin American">Latin American</option>
                                                       <option value="Multinational">Multinational</option>
                                                   </select>
                                                </div>
                                                <div class="form-group cusnSub">
                                                    <label>Cuisine Subcategory</label>
                                                    <div class="input">
                                                        <ul id="subCat" class="serviceBlck">
                                                            <li class="catHeading">
                                                                Choose Subcategory
                                                            </li> 
                                                            <li>
                                                                <input class="radio" type="checkbox" id="Shunde">
                                                                <label class="radiobLable" for="Shunde">Shunde</label>
                                                            </li>
                                                            <li>
                                                                <input class="radio" type="checkbox" id="Beijing">
                                                                <label class="radiobLable" for="Beijing">Beijing</label>
                                                            </li>
                                                            <li class="mrRight">
                                                                <input class="radio" type="checkbox" id="Jingchuanhu">
                                                                <label class="radiobLable" for="Jingchuanhu">Jingchuanhu</label>
                                                            </li>
                                                            <li class="mrRight">
                                                                <input class="radio" type="checkbox" id="Guangxi">
                                                                <label class="radiobLable" for="Guangxi">Guangxi</label>
                                                            </li>
                                                            <li class="mrRight">
                                                                <input class="radio" type="checkbox" id="Northeastern">
                                                                <label class="radiobLable" for="Northeastern">Northeastern</label>
                                                            </li>
                                                            <li class="mrRight">
                                                                <input class="radio" type="checkbox" id="Shandong">
                                                                <label class="radiobLable" for="Shandong">Shandong</label>
                                                            </li>
                                                            <li class="mrRight">
                                                                <input class="radio" type="checkbox" id="Xinjiang">
                                                                <label class="radiobLable" for="Xinjiang">Xinjiang</label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="selectedCatBlock">
                                                    <!-- <div class="form-group">
                                                       <div class="cat-box">
                                                            <h4 class="clr-black">Chinese</h4>
                                                            <ul class="subCatList">
                                                                <li>
                                                                    <span class="float-left txt">Shunde</span> <span class="float-left close">X</span>
                                                                </li>
                                                                <li>
                                                                    <span class="float-left txt">Shunde</span> <span class="float-left close">X</span>
                                                                </li>
                                                            </ul>
                                                       </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <a id="addCat" href="javascript:;" class="float-left addMenuDish">Add</a>
                                                <a href="javascript:;" class="float-right addMenuDish">Save Details</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                    
                    <div class="modal fade dishModal" id="DishModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2 class="clr-black">Dish Description</h2>
                                        </div>
                                    </div>
                                    <form>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="clr-gray">Dish Title</label>    
                                                    <input type="text" class="input">
                                                </div>
                                                <div class="form-group">
                                                    <label>Course Type</label>
                                                    <select id="mainCat" class="input">
                                                        <option>Main Course</option>
                                                        <option>Starter</option>
                                                        <option>Drink</option>
                                                        <option>Sweet</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group dishImg">
                                                    <label class="clr-red" for="dishImgUpload">Upload Image</label>
                                                    <input type="file" id="dishImgUpload">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                               <button class="float-right addMenuDish">Save Details</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php $this->load->view('front/footer'); ?>

<script>
    jQuery(document).ready(function () {
        /*-------------Menu Edit Or Add Popup--------*/
        jQuery(".addMenu").click(function(){
            jQuery("#MenuModal").modal('show');
            jQuery('#select_nenu_currency').attr('data-selected-country','HK');
            jQuery('#select_nenu_currency').flagStrap();
        });

        /*-------------Dish Edit Or Add Popup--------*/
        jQuery(".addDish").click(function(){
            jQuery("#DishModal").modal('show');
            
        });
        
        
        /*-----Utensil Box If user select On Site Cooking-----*/
        $(".untensilbox").hide();
            $("#onsiteCooking").click(function() {
            if($(this).is(":checked")) {
                $(".untensilbox").show();
            } else {
                $(".untensilbox").hide();
            }
        });

        //ADD Menu in Right Block.....
        var catA = {
                "Hong Kong Style":["Hong Kong Style"],
                "Chinese":["All Chinese", "Guangdong", "Chiu Chow","Hakka","Sichuan","Shanghai","Yunnan","Hunan","Taiwan","Shunde","Beijing","Jingchuanhu","Guangxi","Northeastern","Shandong","Xinjiang","Village Food","Fujian","Jiang-Zhe","Guizhou","Shanxi(Shan)","Mongolia","Shanxi(Jin)","Hubei","Huaiyang"],
                "Cantonese":["All Cantonese", "Guangdong", "Chiu Chow","Hakka","Shunde"],
                "Taiwanese":["Taiwanese"],
                "Japanese":["Japanese"],
                "Korean":["Korean"],
                "Thai":["Thai"],
                "Other Asians":["All Asian", "Vietnamese", "Indonesian","Singaporean","Malaysian","Philippines","Indian","Nepalese","Sri Lanka"],
                "Italian":["Italian"],
                "French":["French"],
                "Western":["All Western", "Italian", "French","American","British","Spanish","German","Belgian","Australian","Portuguese","Swiss","Irish","Russian","Dutch","Austrian","Western"],
                "Middle Eastern / Mediterranean":["All Middle Eastern / Mediterranean", "Middle Eastern", "Mediterranean","Turkish","Lebanon","Moroccan","Egyptian","Afrian","Jewish","Greek"],
                "Latin American":["All Latin American", "Mexican", "Cuba","Aargentinian","Peruvian","Brazillian"],
                "Multinational":["Multinational"]
                
            };

        $("#mainCat").html('');
        var mianCatHtml = '';
            $.each(catA, function(key, value){
                mianCatHtml += '<option value="'+key+'">'+key+'</option>';
                //console.log('key------', key);
            });
        $("#mainCat").html(mianCatHtml);    
        //console.log(catA);
        var firstCat = 'Hong Kong Style';
        
        subCat(firstCat);

        function subCat(firstCat) {
            //$("#subCat").html('');
            var subCathtml = '<li class="catHeading">'+firstCat+'</li>';
            for (var i = 0; i < catA[firstCat].length; i++) {
                subCathtml += '<li> <input class="radio" type="checkbox" value="'+catA[firstCat][i]+'" id="Shunde'+i+'">';
                subCathtml += '<label class="radiobLable" for="Shunde'+i+'">'+catA[firstCat][i]+'</label> </li>';
            }
            //console.log(subCathtml);
            $("#subCat").html(subCathtml);
        }

        $("#mainCat").change(function(){
            firstCat = $(this).val();
            subCat(firstCat)
        });

        $(".selectedCatBlock").html('');
        $("#addCat").click(function(){
            var label = $("#subCat").find('.catHeading').text();
            $(".selectedCatBlock .form-group").each(function(){
                if($(this).find('.clr-black').attr('title') == label ) {
                    $(this).remove();
                }
            });
            var catListhtml = '<div class="form-group"><div class="cat-box">';
                catListhtml += '<h4 title="'+label+'" class="clr-black">'+label+'</h4> <ul class="subCatList">';
            $("#subCat .radio").each(function(){
                if($(this).is(':checked')) {
                    var checkedVal = $(this).val();
                    catListhtml += '<li><span class="float-left txt">'+checkedVal+'</span> <span class="float-left close">X</span></li>';
                } 
            });
            catListhtml += '</ul></div></div>';
            //console.log(catListhtml);
            $(catListhtml).appendTo('.selectedCatBlock');
            
            
        });

        /*-----For Added Menus Remove----*/
        $(".selectedCatBlock").on('click', '.close', function(){
            var lenth = $(this).parents('.form-group').find('li').length;
            //console.log('lenth', lenth);
            if (lenth == 1) {
                $(this).parents('.form-group').remove();
            } else {
                $(this).parent('li').remove();
            }
            
        });



    });
</script>

<script type="text/javascript">
    function getsubmenu(val){
        alert(val);
    }

</script>
</body>
</html>