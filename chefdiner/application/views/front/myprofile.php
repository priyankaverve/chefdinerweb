<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/my-kitchen.css">


</head>
<body>

<?php
$this->load->view('front/header');

if(!empty($getchefdata)){
  //  print_r($getchefdata);

   $speakLan = $getchefdata[0]['language_speak'];
   $speakLanArr = explode(",", $speakLan);
  // print_r($serviceArr);die;
    $profile = $getchefdata[0]['profile_pic'];


                           if(!empty($profile)){
                            $profilepic = base_url().'assets/chefprofile/'.$profile;

                           }else{
                            $profilepic ='';
                           }
                           //echo $profilepic;die;
}
?>
<div class="wrapper mykitchenPage">
<div class="container">
    <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h1 class="clr-black"><?php if(isset($getchefdata[0]['kitchen_title'])){ echo $getchefdata[0]['kitchen_title'];}else{
                  echo ucfirst($this->session->userdata('first_name'))."'s Kitchen";
                    }?> <a href="<?php echo base_url() ?>chefdetail">Preview</a></h1>
                <p class="clr-gray"><span class="clr-red">4</span> steps to become a chef!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="KtchnNav">
                    <ul>
                        <li class="kitchen "><a href="mykitchen">My Kitchen</a></li>
                        <li class="profile active"><a href="myprofile">Profile</a></li>
                        <li class="address"><a href="myaddress">Address</a></li>
                        <li class="photos"><a href="myphotos">Photos</a></li>
                        <li class="ktchnmenu"><a href="mymenus">Menu</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="kithchenForm">
                    <form method="post" action="<?php echo base_url()?>saveChefprofile" enctype="multipart/form-data">
                    <div class="leftSec">
                         <input type="hidden" name="chefid" value="<?php if(isset($getchefdata[0]['user_id'])){ echo $getchefdata[0]['user_id'];}?>">
                        <div class="form-group float-left frmHalf clearfix">
                            <h4 class="clr-black">First Name <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">First Name</span></span></h4>
                           <input type="text" class="input" name="chef_fname" placeholder="Your First Name" value="<?php if(isset($getchefdata[0]['chef_fname'])){ echo $getchefdata[0]['chef_fname'];}?>">
                        </div>
                        <div class="form-group float-left frmHalf mrRight">
                            <h4 class="clr-black">Last Name<span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Last Name</span></span></h4>
                            <input type="text" class="input" name="chef_lname" placeholder="Your Last Name" value="<?php if(isset($getchefdata[0]['chef_lname'])){ echo $getchefdata[0]['chef_lname'];}?>">
                        </div>
                       <div class="form-group">
                            <h4 class="clr-black">Bio <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Bio</span></span></h4>
                            <textarea class="" name="chef_bio" placeholder="What do you do when you don't cook?"><?php if(isset($getchefdata[0]['chef_bio'])){ echo $getchefdata[0]['chef_bio']; } ?></textarea>
                        </div>
                        <div class="form-group">
                            <h4 class="clr-black">What language do you prefer for website?</h4>
                            <ul class="serviceBlck prfrLang"> 
                                <li>
                                    <input class="radio" type="radio" id="English" name="prfrSelecter" value="english" <?php if(isset($getchefdata[0]['language']) && $getchefdata[0]['language'] == 'english'){ echo 'checked'; } ?>>
                                    <label class="radiobLable" for="English">English</label>
                                </li>
                                <li>
                                    <input class="radio" type="radio" id="繁體中文" name="prfrSelecter" value="traditional-chinese"  <?php if(isset($getchefdata[0]['language']) && $getchefdata[0]['language'] == 'traditional-chinese'){ echo 'checked'; } ?>>
                                    <label class="radiobLable" for="繁體中文" >繁體中文</label>
                                </li>
                                <li>
                                    <input class="radio" type="radio" id="简体中文" name="prfrSelecter" value="simplified-chinese" <?php if(isset($getchefdata[0]['language']) && $getchefdata[0]['language'] == 'simplified-chinese'){ echo 'checked'; } ?> >
                                    <label class="radiobLable" for="简体中文">简体中文</label>
                                </li>
                            </ul>
                        </div>
                        <div class="form-group">
                            <h4 class="clr-black">What Language do you speak?</h4>
                            <ul class="serviceBlck prfrLang"> 
                                <li>
                                    <input class="radio" type="checkbox" id="sEnglish" name="language_speak[]" value="English" <?php if(!empty($speakLanArr) && in_array('English' , $speakLanArr)){ echo 'checked';}?>>
                                    <label class="radiobLable" for="sEnglish">English</label>
                                </li>
                                <li>
                                    <input class="radio" type="checkbox" id="Cantonese" name="language_speak[]" value="Cantonese" <?php if(!empty($speakLanArr) && in_array('Cantonese' , $speakLanArr)){ echo 'checked';}?> >
                                    <label class="radiobLable" for="Cantonese">Cantonese</label>
                                </li>
                                <li>
                                    <input class="radio" type="checkbox" id="Mandarin" name="language_speak[]" value="Mandarin" <?php if(!empty($speakLanArr) && in_array('Mandarin' , $speakLanArr)){ echo 'checked';}?> >
                                    <label class="radiobLable" for="Mandarin">Mandarin</label>
                                </li>
                                <li>
                                    <input class="radio" type="checkbox" id="Malay" name="language_speak[]" value="Malay" <?php if(!empty($speakLanArr) && in_array('Malay' , $speakLanArr)){ echo 'checked';}?>>
                                    <label class="radiobLable" for="Malay">Malay</label>
                                </li>
                                <li>
                                    <input class="radio" type="checkbox" id="Thai" name="language_speak[]" value="Thai" <?php if(!empty($speakLanArr) && in_array('Thai' , $speakLanArr)){ echo 'checked';}?> >
                                    <label class="radiobLable" for="Thai">Thai</label>
                                </li>
                                <li>
                                    <input class="radio" type="checkbox" id="Fillipino" name="language_speak[]" value="Fillipino" <?php if(!empty($speakLanArr) && in_array('Fillipino' , $speakLanArr)){ echo 'checked';}?> >
                                    <label class="radiobLable" for="Fillipino">Fillipino</label>
                                </li>
                                <li>
                                    <input class="radio" type="checkbox" id="Vietnamese" value="Vietnamese" name="language_speak[]" <?php if(!empty($speakLanArr) && in_array('Vietnamese' , $speakLanArr)){ echo 'checked';}?> >
                                    <label class="radiobLable" for="Vietnamese">Vietnamese</label>
                                </li>
                            </ul>
                        </div>
                        <div class="form-group clearfix">
                            <h4 class="clr-black">Other Language <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Add Other Language</span></span></h4>
                           <input type="text" class="input" placeholder="Add Other Language" name="other_language" value="<?php if(isset($getchefdata[0]['other_language'])){ echo $getchefdata[0]['other_language'];} ?>">
                        </div>
                        </div>

                        <div class="rightSec fileUpload">
                            <div class="ChefImage">

                                <img src="<?=$profilepic; ?>" alt="">
                            </div>
                            <label for="uploadPics">Edit Picture </label>
                            <input type="file" id="uploadPics" name="fileToUpload" class="sbmt float-left">
                        </div>
                        <div class="form-group sbmt-row">
                            <input type="submit" value="Next" class="txt-upr sbmt float-left">
                            <a href="#" class="txt-upr prvw float-right">Preview</a>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>
</body>
</html>