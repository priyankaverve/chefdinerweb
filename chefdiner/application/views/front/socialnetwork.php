<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/my-kitchen.css">

</head>
<body>
<?php
$this->load->view('front/header');
?>
<div class="wrapper myProfileEditPage">
    <div class="container">
    <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
    <?php endif;?>
        
    <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
    <?php endif;?>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="KtchnNav">
                    <ul>
                        <li class="profile"><a href="editpofile">Profile</a></li>
                        <li class="pwreset "><a href="changepassword">Password Change</a></li>
                        <li class="preferences active"><a href="socialnetwork">Preferences</a></li>
                    </ul>
                </div>
            </div>
            <?php 
            if(!empty($getpermission)){
             //   print_r($getpermission);die;
                if($getpermission[0]->email_pro != '0'){
                    /****weeklyNewsletter ***/
                    $checkedweekly = 'checked';
                    $valweekly =  '1';

                }else{
                    $checkedweekly = '';
                    $valweekly =  '0';
                }

                if($getpermission[0]->email_msg != '0'){
                    /****messageNotification ***/
                    $checkmsgnoti = 'checked';
                    $valmsgnoti =  '1';

                }else{
                    $checkmsgnoti = '';
                    $valmsgnoti =  '0';
                }

                if($getpermission[0]->sms_msg != '0'){
                    /****SMS about new messages ***/
                    $checksms_msg = 'checked';
                    $valsms_msg =  '1';

                }else{
                    $checksms_msg = '';
                    $valsms_msg =  '0';
                }

                if($getpermission[0]->mobile_msg != '0'){
                    /****Push notification about new messages****/
                    $checkmobile_msg = 'checked';
                    $valmobile_msg =  '1';

                }else{
                    $checkmobile_msg = '';
                    $valmobile_msg =  '0';
                }

                 if($getpermission[0]->instagram_login != '0'){
                    /****Push notification about new messages****/
                    $checkinstagram_login = 'checked';
                    $valinstagram_login =  '1';

                }else{
                    $checkinstagram_login = '';
                    $valinstagram_login =  '0';
                }


            }

            ?>
<div class="col-lg-9 col-md-9">
    <div class="kithchenForm socialprofile">
        <form action="<?php echo base_url() ?>saveSetting" method="post">
        <input type="hidden" name="permissionid" value="<?php if(isset($getpermission[0]->id)){ echo $getpermission[0]->id;} ?>">
            <div class="leftSec">
                <h2 class="clr-black">Social Networks</h2>
                <p>When connected it allows to login without password using 'Login with Facebook' button.</p>
                <a href="" class="lginFb">Login to ChefDiner via Facebook</a>
                <div class="form-group clearfix">
                    <h4 class="clr-black">Email notifications</h4>
                    <div class="slider checkbox-example float-left">
                        <input type="checkbox"  id="weeklyNewsletter" class="checkbox-toggle checkbox-toggle-pill"  <?=$checkedweekly?> value="<?=$valweekly;?>" name="weeklyNewsletter"/>
                        <label class="checkbox-toggle-label checkbox-toggle-pill-label" for="weeklyNewsletter" >Weekly Newsletter<span class="on">ON</span><span class="off">OFF</span></label> 
                    </div>
                    <div class="slider checkbox-example float-left">
                        <input type="checkbox" id="messageNotification" class="checkbox-toggle checkbox-toggle-pill" <?=$checkmsgnoti;?> value="<?=$valmsgnoti;?>" name="messageNotification" />
                        <label class="checkbox-toggle-label checkbox-toggle-pill-label" for="messageNotification">Message Notification<span class="on">ON</span><span class="off">OFF</span></label> 
                    </div>
                </div>
                <div class="form-group clearfix">
                    <h4 class="clr-black">Mobile notifications</h4>
                    <div class="slider checkbox-example float-left">
                        <input type="checkbox" id="smsaboutnewmessages" class="checkbox-toggle checkbox-toggle-pill" <?=$checksms_msg;?> value="<?=$valsms_msg;?>" name="smsaboutnewmessages" />
                        <label class="checkbox-toggle-label checkbox-toggle-pill-label" for="smsaboutnewmessages">SMS about new messages<span class="on">ON</span><span class="off">OFF</span></label> 
                    </div>
                    <div class="slider checkbox-example float-left">
                        <input type="checkbox" id="pushnotification" class="checkbox-toggle checkbox-toggle-pill" <?=$checkmobile_msg;?> value="<?=$valmobile_msg;?>" name="pushnotification" />
                        <label class="checkbox-toggle-label checkbox-toggle-pill-label" for="pushnotification">Push notification about new messages<span class="on">ON</span><span class="off">OFF</span></label> 
                    </div>
                </div>
                <div class="form-group clearfix">
                    <h4 class="clr-black">Instagram Gallery</h4>
                    <p>Lorem ipsum</p>
                    <!-- <a href="" class="lginFb insta">Login to Insta Account</a> -->
                    <div class="serviceBlck prfrLang instaLogin">
                        <input type="checkbox" id="instaLogn" class="radio" name="instaLogn" <?=$checkinstagram_login;?> value="<?=$valinstagram_login;?>" />
                        <label class="radiobLable" for="instaLogn">Login to insta account</label> 
                    </div>
                </div>
                
                <div class="form-group sbmt-row">
                    <input type="submit" value="Save Details" class="txt-upr sbmt float-left">
                </div>
            </div>
        </form>
    </div>
</div>

         
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>
<script type="text/javascript">
$(document).ready(function() {
    //set initial state.

    $('#weeklyNewsletter').change(function() {
        if($(this).is(":checked")) {
            
           // var returnVal = confirm("Are you sure?");
            $(this).attr("checked",true);
            $('#weeklyNewsletter').val('1');    
        }else{
             $('#weeklyNewsletter').val('0');   
        }
            
    });

    $('#messageNotification').change(function() {
        if($(this).is(":checked")) {
            
           // var returnVal = confirm("Are you sure?");
            $(this).attr("checked",true);
            $('#messageNotification').val('1');    
        }else{
             $('#messageNotification').val('0');   
        }
            
    });

    $('#smsaboutnewmessages').change(function() {
        if($(this).is(":checked")) {
            
           // var returnVal = confirm("Are you sure?");
            $(this).attr("checked",true);
            $('#smsaboutnewmessages').val('1');    
        }else{
             $('#smsaboutnewmessages').val('0');   
        }
            
    });

    $('#pushnotification').change(function() {
        if($(this).is(":checked")) {
            
           // var returnVal = confirm("Are you sure?");
            $(this).attr("checked",true);
            $('#pushnotification').val('1');    
        }else{
             $('#pushnotification').val('0');   
        }
            
    });
    $('#instaLogn').change(function() {
        if($(this).is(":checked")) {
            var url =  '<?php echo base_url(); ?>welcome/instagramlogin';
            
           // var returnVal = confirm("Are you sure?");
            $(this).attr("checked",true);
            $('#instaLogn').val('1');  
            window.location.href = 'https://api.instagram.com/oauth/authorize/?client_id=d385960b553c4b7aa0d3c508b5a6c710&redirect_uri='+url+'&response_type=code&scope=public_content+likes+comments'; //Will take you to Google.  
              // window.location.href = 'https://api.instagram.com/oauth/authorize/?client_id=d385960b553c4b7aa0d3c508b5a6c710&redirect_uri='+url+'&response_type=token&scope=public_content+likes+comments'; //Will take you to Google.  
        }else{
             $('#instaLogn').val('0');   
        }
            
    });
});
</script>
</body>
</html>