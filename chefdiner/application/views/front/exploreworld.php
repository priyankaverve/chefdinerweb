<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title><?php echo $this->lang->line('sitetitle'); ?></title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/explore.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" rel="stylesheet"/>

</head>
<body>
<?php
$this->load->view('front/header');
?>
<div class="wrapper exploreWorld">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="chefEplre">
                    <h1 class="clr-black float-left">What’s Hot</h1>
                    <a href="" class="float-right txt-upr clr-red">Reset Filters</a>
                    <div class="tpfltr">
                        <div class="tpfltrBlk location cntry">
                            <input type="text" placeholder="Country" class="input">
                        </div>
                        <div class="tpfltrBlk clndr">
                            <input type="text" name="date" class="input" placeholder="Date">
                        </div>
                        <div class="tpfltrBlk location distrct">
                            <input type="text" placeholder="District" class="input">
                        </div>
                        <div class="tpfltrBlk guest">
                            <input type="text" placeholder="Location" class="input">
                        </div>
                        <div class="tpfltrsrch">
                            <input type="submit" value="Search">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="viewBox">
                    <a href="javascript:;" class="menuView">Menu View</a>
                    <a href="javascript:;" class="ratingView">Rating View</a>
                </div>
                <div class="filterBlock">
                    <h5 class="FilterTitle clr-black">Map View</h5>
                    <div class="mapImg">
                        <img src="<?php echo base_url();?>front/images/map.jpg" alt="">
                    </div>
                </div>
                <div class="filterBlock">
                    <h5 class="FilterTitle clr-black float-left">Cuisines</h5>
                    <button class="float-right clr-red txt-upr clearBtn" onClick="window.location.reload()">Clear</button>
                    <div class="filterList cusineList">
                        <ul>
                            <li class="listChk">
                                <input type="checkbox" name="" value="" id="Hongkong">
                                <label for="Hongkong">
                                    Hongkong <span>(12)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="" value="" id="Indian">
                                <label for="Indian">
                                    Indian <span>(12)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="" value="" id="Chinese ">
                                <label for="Chinese ">
                                    Chinese  <span>(12)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="" value="" id="Taiwanese">
                                <label for="Taiwanese">
                                    Taiwanese <span>(12)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="" value="" id="Japanese">
                                <label for="Japanese">
                                    Japanese <span>(12)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="" value="" id="Korean">
                                <label for="Korean">
                                    Korean <span>(12)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="" value="" id="Thai">
                                <label for="Thai">
                                    Thai <span>(12)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="" value="" id="Italiyan">
                                <label for="Italiyan">
                                    Italiyan <span>(12)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="" value="" id="French">
                                <label for="French">
                                    French <span>(12)</span>
                                </label>
                            </li>
                        </ul>
                        <span id="loadMore" class="clr-red txt-upr">Show More</span>
                    </div>
                </div>
                <div class="filterBlock">
                    <h5 class="FilterTitle clr-black float-left">Price Per Guest</h5>
                    <button class="float-right clr-red txt-upr clearBtn" onClick="window.location.reload()">Clear</button>
                    <div class="filterList">
                        <div class="priceBox">
                            <input class="value" type="text" id="amount" readonly>
                            <div id="slider-range"></div>
                        </div>
                    </div>
                </div>
                <div class="filterBlock">
                    <h5 class="FilterTitle clr-black float-left">Service type</h5>
                    <button class="float-right clr-red txt-upr clearBtn" onClick="window.location.reload()">Clear</button>
                    <div class="filterList">
                        <ul>
                            <li class="listChk">
                                <input type="checkbox" name="service_type[]" value="<?php echo $this->lang->line('daily_meal'); ?>" id="DailyMeal">
                                <label for="DailyMeal">
                                    <?php echo $this->lang->line('daily_meal'); ?> <span>(<?=count($DailyMeal);?>)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="service_type[]" value="<?php echo $this->lang->line('cooking_workshop'); ?>" id="CookingWorkshop" >
                                <label for="CookingWorkshop">
                                    <?php echo $this->lang->line('cooking_workshop'); ?>  <span>(<?=count($CookingWorkshop);?>)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="service_type[]" value="<?php echo $this->lang->line('food_delivery'); ?>" id="FoodDelivery">
                                <label for="FoodDelivery">
                                   <?php echo $this->lang->line('food_delivery'); ?> <span>(<?=count($FoodDelivery);?>)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="service_type[]" value="<?php echo $this->lang->line('chef_onsite_cooking'); ?>" id="OnsiteCooking" >
                                <label for="OnsiteCooking">
                                    <?php echo $this->lang->line('chef_onsite_cooking'); ?> <span>(<?=count($OnsiteCooking);?>)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="service_type[]" value="<?php echo $this->lang->line('home_private_meal'); ?>" id="HomePrivateMeal" >
                                <label for="HomePrivateMeal">
                                    <?php echo $this->lang->line('home_private_meal'); ?> <span>(<?=count($HomePrivateMeal);?>)</span>
                                </label>
                            </li>
                            <li class="listChk">
                                <input type="checkbox" name="service_type[]" value="<?php echo $this->lang->line('restaurant_private_meal'); ?>" id="RestaurantPrivate">
                                <label for="RestaurantPrivate">
                                    <?php echo $this->lang->line('restaurant_private_meal'); ?> <span>(<?=count($RestaurantPrivate);?>)</span>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="sortBy">
                    <div class="float-left sortTitle clr-black">
                        Sort By:
                    </div>
                    <div class="float-left sortList">
                        <a href="#" onclick="filters('highestRec')"><?php echo $this->lang->line('highest_recommended'); ?></a>
                        <a href="#" onclick="filters('highestBook')"><?php echo $this->lang->line('highest_bookmarked'); ?></a>
                        <a href="#" onclick="filters('hightolow')"><?php echo $this->lang->line('price_htl'); ?></a>
                        <a href="#" onclick="filters('lowtohigh')"><?php echo $this->lang->line('price_lth'); ?></a>
                    </div>
                    <button class="float-right clr-red txt-upr clearBtn" onClick="window.location.reload()">Clear</button>
                </div>
                <div id="filters">
                <?php 
                // echo "<pre>";
                // print_r($whatshostchef);die;
              if(!empty($whatshostchef)){
              foreach ($whatshostchef as $key => $value) {

                    $imgs = $value->allimage;
                    $allimg  = explode("|", $imgs);
                    $allimg  = array_values(array_filter($allimg));
                    $count = count($allimg);
                 //   echo $count;
                    if($count > 4){
                      $remain = $count - 4 .'+';
                      $image1 = $allimg[0];
                      $image2 = $allimg[1];
                      $image3 = $allimg[2];
                      $image4 = $allimg[3];
                     
                  }else{
                      $remain = '';
                      $image1 = base_url().'front/images/blank.jpg';
                      $image2 = base_url().'front/images/blank.jpg';
                      $image3 = base_url().'front/images/blank.jpg';
                      $image4 = base_url().'front/images/blank.jpg';
                      
                  }

                  if(!empty($value->profile_pic)){
                    $profile_pic = $value->profile_pic;
                  }else{
                    $profile_pic = base_url().'front/images/blank.jpg';
                  }

                  if(is_float($value->average_rating)){
                                        $avr_rating = $value->average_rating;

                                        }else{
                                           $avr_rating  = number_format((float)$value->average_rating, 1, '.', '');
                                        }
            
                  ?>

                <div class="flterDataBox">
                    <div class="imgBox">
                        <div class="fullWdth">
                            <img src="<?php echo $image1 ?>" alt="">
                        </div>
                        <div class="smlwdth">
                            <img src="<?php echo $image2 ?>">
                        </div>
                        <div class="smlwdth">
                            <img src="<?php echo $image3 ?>">
                        </div>
                        <div class="smlwdth mrRight">
                            <img src="<?php echo $image4 ?>">
                            <div class="shadowBox">
                                <a href="<?php echo base_url(); ?>chefdetail/<?php echo $value->user_id; ?>" class="text-center"><?=$remain?><br>More</a>
                            </div>
                        </div>
                    </div>
                    <div class="float-left KtchnDetaisl">
                        <div class="chfImg">
                            <div class="img">
                                <img src="<?php echo $profile_pic; ?>" alt="">
                            </div>
                        </div>
                        <div class="Kitchn">
                            <h3 class="clr-black wishListed"><?php echo ucfirst($value->kitchen_title); ?></h3>
                            
                            <div class="float-left ktchnUsrName">
                                <a href="" class="clr-red"><?php echo ucfirst($value->chef_fname); ?></a> from <a href="#" class="clr-red"><?php echo ucfirst($value->chef_country); ?></a>
                            </div>
                            <div class="clearfix reactions">
                                <div class="reactionBox clr-gray">
                                    <span class="imogy yummy float-left"></span>
                                    <span class="counts float-left"><?php echo $value->recommended; ?></span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy avg float-left"></span>
                                    <span class="counts float-left"><?php echo $value->ok; ?></span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy bad float-left"></span>
                                    <span class="counts float-left"><?php echo $value->no_recommended; ?></span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy bkmark float-left"></span>
                                    <span class="counts float-left"><?php echo $value->favouritecount; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="lcCat">
                            <div class="float-left ktchLoc clr-gray">
                                <span class="float-left img"><img src="<?php echo base_url();?>front/images/loc-red.png" alt=""></span>
                                <span class="float-left"><?php echo ucfirst($value->chef_address); ?></span>
                            </div>
                            <div class="float-left ktchLoc itmcusine clr-gray">
                                <span class="float-left"><img src="<?php echo base_url();?>front/images/cusine.png" alt=""></span>
                                <span class="float-left no-brdr mrRight"><?php echo $value->cuisine; ?></span>
                            </div>
                        </div>
                        <div class="srvcType">
                            <span class="float-left img">
                                <img src="<?php echo base_url();?>front/images/platehand.png" alt="">
                            </span>
                            <span class="float-left clr-gray"><?php echo $value->service_type; ?></span>
                        </div>
                    </div> 
                    <div class="float-left rvwSec">
                        <div class="reviewsBox">
                            <div class="starBox">
                                <span class="rateScore"><?=$avr_rating;?></span>
                                <span class="star">
                                    <div class="ratingBox">
                                        <ul>
                                              <?php
                                           $rountRat = round($value->average_rating);
                                           //echo $rountRat;
                                           if($rountRat > 0 && $rountRat <=0.5){
                                          
                                           $valhalf5 = '0.5';
                                           $chkhalf5 = 'checked';

                                           }else{
                                            $valhalf5 = '0.5';
                                            $chkhalf5 = '';
                                           }

                                           if($rountRat > 0.5 && $rountRat <=1){
                                          
                                           $valfull1 = '1.0';
                                           $chkfull1 = 'checked';

                                           }else{
                                            $valfull1 = '1.0';
                                            $chkfull1 = '';
                                           }

                                           if($rountRat > 1 && $rountRat <=1.5){
                                          
                                           $valhalf1 = '1.5';
                                           $chkhalf1 = 'checked';

                                           }else{
                                            $valhalf1 = '1.5';
                                            $chkhalf1 = '';
                                           }
                                           if($rountRat > 1.5 && $rountRat <=2){
                                          
                                           $valfull2 = '2';
                                           $chkfull2 = 'checked';

                                           }else{
                                            $valfull2 = '2';
                                            $chkfull2 = '';
                                           }

                                           if($rountRat > 2 && $rountRat <=2.5){
                                          
                                           $valhalf2 = '2.5';
                                           $chkhalf2 = 'checked';

                                           }else{
                                            $valhalf2 = '2.5';
                                            $chkhalf2 = '';
                                           }

                                           if($rountRat > 2.5 && $rountRat <=3){
                                          
                                           $valfull3 = '3';
                                           $chkfull3 = 'checked';

                                           }else{
                                            $valfull3 = '3';
                                            $chkfull3 = '';
                                           }

                                          if($rountRat > 3 && $rountRat <=3.5){
                                          
                                           $valhalf3 = '3.5';
                                           $chkhalf3 = 'checked';

                                           }else{
                                            $valhalf3 = '3.5';
                                            $chkhalf3 = '';
                                           }
                                           if($rountRat > 3.5 && $rountRat <=4){
                                          
                                           $valfull4 = '4';
                                           $chkfull4 = 'checked';

                                           }else{
                                            $valfull4= '4';
                                            $chkfull4 = '';
                                           }
                                           
                                           if($rountRat > 4 && $rountRat <=4.5){
                                          
                                           $valhalf4 = '4.5';
                                           $chkhalf4 = 'checked';

                                           }else{
                                            $valhalf4= '4.5';
                                            $chkhalf4 = '';
                                           }

                                           if($rountRat > 4.5 && $rountRat <=5){
                                          
                                           $valfull5 = '5';
                                           $chkfull5 = 'checked';

                                           }else{
                                            $valfull5= '5';
                                            $chkfull5 = '';
                                           }
                                           

                                            ?>
                                                <li class="rateStar">
                                                <div class="cstmr-reviews">
                                                        <fieldset class="rating">
                                                            <input type="radio" id="stara<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull5?>" <?=$chkfull5?> disabled="disable" />
                                                            <label class = "full" for="stara<?=$value->id?>" title="Awesome - 5 stars"></label>

                                                            <input type="radio" id="starb<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf4?>" <?=$chkhalf4?> disabled="disable" />
                                                            <label class="half" for="starb<?=$value->id?>" title="Pretty good - 4.5 stars"></label>

                                                            <input type="radio" id="starc<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull4?>" <?=$chkfull4?> disabled="disable"  />
                                                            <label class = "full" for="starc<?=$value->id?>" title="Pretty good - 4 stars"></label>

                                                            <input type="radio" id="stard<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf3?>" <?=$chkhalf3?> disabled="disable" />
                                                            <label class="half" for="stard<?=$value->id?>" title="Meh - 3.5 stars"></label>

                                                            <input type="radio" id="stare<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull3?>" <?=$chkfull3?> disabled="disable"/>
                                                            <label class = "full" for="stare<?=$value->id?>" title="Meh - 3 stars"></label>

                                                            <input type="radio" id="starf<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf2?>" <?=$chkhalf2?> disabled="disable" />
                                                            <label class="half" for="starf<?=$value->id?>" title="Kinda bad - 2.5 stars"></label>

                                                            <input type="radio" id="starg<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull2?>" <?=$chkfull2?> disabled="disable" />
                                                            <label class = "full" for="starg<?=$value->id?>" title="Kinda bad - 2 stars"></label>

                                                            <input type="radio" id="starh<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf1?>" <?=$chkhalf1?> disabled="disable"  />
                                                            <label class="half" for="starh<?=$value->id?>" title="Meh - 1.5 stars"></label>

                                                            <input type="radio" id="stari<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull1?>" <?=$chkfull1?> disabled="disable" />
                                                            <label class = "full" for="stari<?=$value->id?>" title="Sucks big time - 1 star"></label>

                                                            <input type="radio" id="starj<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf5?>" <?=$chkhalf5?> disabled="disable" />
                                                            <label class="half" for="starj<?=$value->id?>" title="Sucks big time - 0.5 stars"></label>
                                                        </fieldset>
                                                    </div>
                                           
                                                </li>
                                        </ul>
                                    </div>
                                    <p><?=$value->totalreview;?> Reviews</p>
                                </span>
                            </div>
                        </div>
                        <div class="wishBox">
                            <span class="text-center">
                                <img src="<?php echo base_url();?>front/images/pricelabel.png" alt=""><br>
                                <strong class="clr-black"><?=$value->currency?><?=$value->price?> to <?=$value->currency?><?=$value->maxprice?></strong><br> Per Guest
                            </span>
                        </div>
                    </div>
                </div>
                <?php 
              }
            } 
               ?> 
               </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 1000,
            values: [ 0, 1000 ],
            slide: function( event, ui ) {
            // $( "#amount" ).val( "HKD" + ui.values[ 0 ] + " - HKD" + ui.values[ 1 ] );
            $( "#amount" ).val(ui.values[ 0 ]+ " " + ui.values[ 1 ] );
            }
        });
        // $( "#amount" ).val( "HKD" + $( "#slider-range" ).slider( "values", 0 ) + " - HKD" + $( "#slider-range" ).slider( "values", 1 ) );
        $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) + " " + $( "#slider-range" ).slider( "values", 1 ) )
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function() {
        var dateInput = $('input[name="date"]'); // Our date input has the name "date"
        dateInput.datepicker({
            format: 'yyyy-mm-dd DD',
            todayHighlight: true,
            autoclose: true,
            startDate: truncateDate(new Date()) // <-- THIS WORKS
        });

        $('#date').datepicker('setStartDate', truncateDate(new Date())); // <-- SO DOES THIS
        });
        function truncateDate(date) {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }

    /*VIEW MENU OR RATING*/
    $(function() {
        $( '.viewBox a' ).on( 'click', function() {
            $( this ).parent().find( 'a.active' ).removeClass( 'active' );
            $( this ).addClass( 'active' );
        });

        $( '.sortList a' ).on( 'click', function() {
            $( this ).parent().find( 'a.active' ).removeClass( 'active' );
            $( this ).addClass( 'active' );
        });
    });

    /*SHOW MORE LIST*/
   function filters(val){
    var userid = '<?php echo $this->session->userdata('user_id')?>';
    // alert(userid);
    // alert(val);

    $.ajax(
      {
        url:"<?php echo base_url(); ?>welcome/getfilters",
        type:"POST",
        data:{userid : userid,filter:val},
        dataType: 'html',
        success:function(data)
        { 
            $('#filters').html(data);
        }
        
      });

    }

    /*var checkbox = $('input[name="service_type[]"]');

    checkbox.change(function(event) {
    var checkbox = event.target;
    if (checkbox.checked) {
     $("input[name=service_type]").each( function (i) {
        var val = [];
     val[i] = $(this).val();
       alert(val);
   });
        //Checkbox has been checked
    } else {
        //Checkbox has been unchecked
    }
});*/

 // function getCheckBoxValues(){
 //    $("input[name='service_type[]']").each( function (i) {
 //       alert('hello');
 //   });
 // }
     var checkBox = [];
    $('.filterBlock input[type=checkbox]').change(function(){
        var checkBox = [];
        $('.filterBlock input[type=checkbox]').each(function(){
            if ($(this).is(':checked')) {
                var thisval = $(this).val();
                //var thisvlaues ='"'+thisval+'"';
              //  console.log(thisvlaues);
                checkBox.push(thisval);
            }
        });
       //var  jsonString = JSON.stringify(checkBox);
        var chekval = checkBox.join(',');
       // console.log("checkBox", chekval);
        var val = 'ServiceType';
        var userid = '<?php echo $this->session->userdata('user_id')?>';

      $.ajax(
      {
        url:"<?php echo base_url(); ?>welcome/getfilters",
        type:"POST",
        data:{userid : userid,filter:val,service_type:chekval},
        dataType: 'html',
        success:function(data)
        { 
            $('#filters').html(data);
        }
        
      });

    });

</script>
</body>
</html>