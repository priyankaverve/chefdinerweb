<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/content.css">
</head>
<body>
<?php
$this->load->view('front/header');
?>
<div class="wrapper contentPage">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contentSec">
                    <h1 class="clr-black"><?=$termscondition[0]['title']?></h1>
                    <div class="contentBlock">
                       <?=$termscondition[0]['content']?>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer'); ?>
</body>
</html>