<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/my-kitchen.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/intlTelInput.css">

</head>
<body>
<?php
$this->load->view('front/header');
?>
<div class="wrapper myProfileEditPage">
    <div class="container">
     <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="KtchnNav">
                    <ul>
                        <li class="profile active"><a href="editpofile">Profile</a></li>
                        <li class="pwreset "><a href="changepassword">Password Change</a></li>
                        <li class="preferences"><a href="socialnetwork">Preferences</a></li>
                    </ul>
                </div>
            </div>
            <?php
            if(isset($getchefdata[0]['profile_pic']) && !empty($getchefdata[0]['profile_pic'])){
            $profile_pic = base_url().'assets/chefprofile/'.$getchefdata[0]['profile_pic'];

            }else{
                $profile_pic = base_url().'front/images/blank.jpg';
            }       
            $speakLan = $getchefdata[0]['language_speak'];
   $speakLanArr = explode(",", $speakLan);
 ?>
            <div class="col-lg-9 col-md-9">
                <div class="kithchenForm editprofile">
                    <form method="post" action="<?php echo base_url()?>EditChefprofile" enctype="multipart/form-data" id="formProfile">
                    <input type="hidden" name="chefid" value="<?php if(isset($getchefdata[0]['user_id'])){ echo $getchefdata[0]['user_id'];}?>">
                        <div class="leftSec">
                            <h2 class="clr-black">Your profile</h2>
                            <div class="form-group">
                                <h4 class="clr-black">Email Address <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Email Address</span></span></h4>
                            <input type="text" name="email" class="input" placeholder="Enter Your Email Address" value="<?php if(isset($getchefdata[0]['email'])){echo $getchefdata[0]['email'];} ?>" disabled>
                            </div>
                            <div class="form-group float-left frmHalf clearfix">
                                <h4 class="clr-black">First Name <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">First Name</span></span></h4>
                            <input type="text" class="input" placeholder="Your First Name"  value="<?php if(isset($getchefdata[0]['chef_fname'])){echo $getchefdata[0]['chef_fname'];} ?>" name="chef_fname">
                            </div>
                            <div class="form-group float-left frmHalf">
                                <h4 class="clr-black">Last Name<span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Last Name</span></span></h4>
                                <input type="text" class="input" placeholder="Your Last Name" value="<?php if(isset($getchefdata[0]['chef_lname'])){echo $getchefdata[0]['chef_lname'];} ?>" name="chef_lname">
                            </div>
                            
                            <div class="form-group">
                                <h4 class="clr-black">Phone Number<span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Contact Number</span></span></h4>
                               <input type="tel" id="phone1" class="input" placeholder="Mobile No." value="<?php if(isset($getchefdata[0]['phone_number'])){ echo $getchefdata[0]['phone_number'];}?>">
                               <input type="hidden" name="countryCode1" id="countryCode1" />
                            <input type="hidden" name="phonefull1" id="phonefull1" />
                            </div>
                            <div class="form-group">
                                <h4 class="clr-black">About <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Bio</span></span></h4>
                                <textarea class="" name="chef_bio" placeholder="Say Somthing about yourself"><?php if(isset($getchefdata[0]['chef_bio'])){ echo $getchefdata[0]['chef_bio'];} ?></textarea>
                            </div>
                            <div class="form-group">
                                <h4 class="clr-black">What language do you prefer for website?</h4>
                                <ul class="serviceBlck prfrLang"> 
                                    <li>
                                        <input class="radio" type="radio" id="English" name="prfrSelecter" value="english" <?php if(isset($getchefdata[0]['language']) && $getchefdata[0]['language'] == 'english'){ echo 'checked'; } ?>>
                                        <label class="radiobLable" for="English">English</label>
                                    </li>
                                    <li>
                                        <input class="radio" type="radio" id="繁體中文" name="prfrSelecter" value="traditional-chinese"  <?php if(isset($getchefdata[0]['language']) && $getchefdata[0]['language'] == 'traditional-chinese'){ echo 'checked'; } ?>>
                                        <label class="radiobLable" for="繁體中文">繁體中文</label>
                                    </li>
                                    <li>
                                        <input class="radio" type="radio" id="简体中文" name="prfrSelecter" value="simplified-chinese" <?php if(isset($getchefdata[0]['language']) && $getchefdata[0]['language'] == 'simplified-chinese'){ echo 'checked'; } ?>>
                                        <label class="radiobLable" for="简体中文">简体中文</label>
                                    </li>
                                </ul>
                            </div>
                            <div class="form-group">
                                <h4 class="clr-black">What Language do you speak?</h4>
                                <ul class="serviceBlck prfrLang"> 
                                    <li>
                                        <input class="radio" type="checkbox" name="language_speak[]" id="sEnglish" value="English" <?php if(!empty($speakLanArr) && in_array('English' , $speakLanArr)){ echo 'checked';}?>>
                                        <label class="radiobLable" for="sEnglish">English</label>
                                    </li>
                                    <li>
                                        <input class="radio" type="checkbox" id="Cantonese" value="Cantonese" <?php if(!empty($speakLanArr) && in_array('Cantonese' , $speakLanArr)){ echo 'checked';}?> >
                                        <label class="radiobLable" for="Cantonese">Cantonese</label>
                                    </li>
                                    <li>
                                        <input class="radio" type="checkbox" name="language_speak[]" id="Mandarin" value="Mandarin" <?php if(!empty($speakLanArr) && in_array('Mandarin' , $speakLanArr)){ echo 'checked';}?> >
                                        <label class="radiobLable" for="Mandarin">Mandarin</label>
                                    </li>
                                    <li>
                                        <input class="radio" type="checkbox" name="language_speak[]" id="Malay" value="Malay" <?php if(!empty($speakLanArr) && in_array('Malay' , $speakLanArr)){ echo 'checked';}?>>
                                        <label class="radiobLable" for="Malay">Malay</label>
                                    </li>
                                    <li>
                                        <input class="radio" type="checkbox" id="Thai" value="Thai" <?php if(!empty($speakLanArr) && in_array('Thai' , $speakLanArr)){ echo 'checked';}?> >
                                        <label class="radiobLable" for="Thai">Thai</label>
                                    </li>
                                    <li>
                                        <input class="radio" type="checkbox" name="language_speak[]" id="Fillipino" value="Fillipino" <?php if(!empty($speakLanArr) && in_array('Fillipino' , $speakLanArr)){ echo 'checked';}?> >
                                        <label class="radiobLable" for="Fillipino">Fillipino</label>
                                    </li>
                                    <li>
                                        <input class="radio" type="checkbox" name="language_speak[]" id="Vietnamese" value="Vietnamese"  <?php if(!empty($speakLanArr) && in_array('Vietnamese' , $speakLanArr)){ echo 'checked';}?> >
                                        <label class="radiobLable" for="Vietnamese">Vietnamese</label>
                                    </li>
                                </ul>
                            </div>
                            <div class="form-group clearfix">
                                <h4 class="clr-black">Other Language <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Add Other Language</span></span></h4>
                            <input type="text" class="input" placeholder="Add Other Language" value="<?php if(isset($getchefdata[0]['other_language'])){ echo $getchefdata[0]['other_language']; } ?>" name="other_language">
                            </div>
                        </div>
                        <div class="rightSec fileUpload">
                            <div class="ChefImage">
                                <img src="<?=$profile_pic;?>" alt="">
                            </div>
                            <label for="uploadPics">Upload Picture </label>
                            <input type="file" id="uploadPics" name="fileToUpload" class="sbmt float-left">
                        </div>
                        <div class="form-group sbmt-row">
                            <input type="submit" value="Save Details" class="txt-upr sbmt float-left">
                        </div>
                    </form>
                </div>
            </div>

         
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>
<script type="text/javascript">

/*****Contact number for myaddress form*****/
$(function(){

      $("#phone1").intlTelInput({
            autoHideDialCode: true,
            autoPlaceholder: true,
            separateDialCode: true,
            nationalMode: true,
            geoIpLookup: function (callback) {
                $.get("http://ipinfo.io", function () {}, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            initialCountry: "hk",
        });

        // get the country data from the plugin
        var countryData = $.fn.intlTelInput.getCountryData(),
          telInput = $("#phone1"),
          addressDropdown = $("#listcountry");

        // init plugin
        telInput.intlTelInput({
          utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.5.0/js/utils.js" // just for formatting/placeholders etc
        });

        // populate the country dropdown
        $.each(countryData, function(i, country) {
          addressDropdown.append($("<option></option>").attr("value", country.iso2).text(country.name));
        });

        // listen to the telephone input for changes
        telInput.on("countrychange", function() {
          var countryCode = telInput.intlTelInput("getSelectedCountryData").iso2;
          addressDropdown.val(countryCode);
        });

        // trigger a fake "change" event now, to trigger an initial sync
        telInput.trigger("countrychange");

        // listen to the address dropdown for changes
        addressDropdown.change(function() {
          var countryCode = $(this).val();
          telInput.intlTelInput("setCountry", countryCode);
        });

        // update the hidden input on submit
        $("#formProfile").submit(countryData,function(i, country) {
          $("#countryCode1").val(telInput.intlTelInput("getSelectedCountryData").name);
        //  console.log($("#countryCode").val(telInput.intlTelInput("getSelectedCountryData").name));
          $("#phonefull1").val('+' + telInput.intlTelInput("getSelectedCountryData").dialCode + $("#phone1").val());      
        });
      });

</script>
</body>
</html>