<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/my-kitchen.css">

</head>
<body>
<?php
$this->load->view('front/header');
if(!empty($getchefdata)){

   $serviceType = $getchefdata[0]['service_type'];
   $serviceArr = explode(",", $serviceType);
  // print_r($serviceArr);die;
}
?>
<div class="wrapper mykitchenPage">
 <div class="container">
 <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
        <div class="row">
            <div class="col-lg-12 col-md-12">
               <h1 class="clr-black"><?php if(isset($getchefdata[0]['kitchen_title'])){ echo $getchefdata[0]['kitchen_title'];}else{
                  echo ucfirst($this->session->userdata('first_name'))."'s Kitchen";
                    }?> <a href="<?php echo base_url() ?>chefdetail">Preview</a></h1>
                <p class="clr-gray">Please kindly complete all <span class="clr-red">5</span> steps to become a chef!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="KtchnNav">
                    <ul>
                        <li class="kitchen active"><a href="mykitchen">My Kitchen</a></li>
                        <li class="profile"><a href="myprofile">Profile</a></li>
                        <li class="address"><a href="myaddress">Address</a></li>
                        <li class="photos"><a href="myphotos">Photos</a></li>
                        <li class="ktchnmenu"><a href="mymenus">Menu</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="kithchenForm">
                    <form method="post" action="<?php echo base_url()?>saveKitchen" >
                    <input type="hidden" name="chefid" value="<?php if(isset($getchefdata[0]['user_id'])){ echo $getchefdata[0]['user_id'];}?>">
                         <div class="form-group">
                           <h4 class="clr-black">Title <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Kitchen Tiltle Here</span></span></h4>
                           <input type="text" class="input" name="kitchen_title" placeholder="Give your event a discriptive title" value="<?php if(isset($getchefdata[0]['kitchen_title'])){ echo $getchefdata[0]['kitchen_title'];}?>">
                         </div>
                         <div class="form-group">
                            <h4 class="clr-black">Meals <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Meals Timing</span></span></h4>
                            <div class="input serviceBlck">
                                <input class="radio" type="checkbox" id="brunch" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] !=''){ echo 'checked';} ?>>
                                <label class="radiobLable" for="brunch"> Brunch</label>
                                <select class="slctTime" name="brunch">
                                    <option value="00:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='00:00'){ echo 'selected';} ?>>00:00</option>
                                    <option value="01:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='01:00'){ echo 'selected';} ?>>01:00</option>
                                    <option value="02:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='02:00'){ echo 'selected';} ?>>02:00</option>
                                    <option value="03:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='03:00'){ echo 'selected';} ?>>03:00</option>
                                    <option value="04:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='04:00'){ echo 'selected';} ?>>04:00</option>
                                    <option value="05:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='05:00'){ echo 'selected';} ?>>05:00</option>
                                    <option value="06:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='06:00'){ echo 'selected';} ?>>06:00</option>
                                    <option value="07:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='07:00'){ echo 'selected';} ?>>07:00</option>
                                    <option value="08:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='08:00'){ echo 'selected';} ?>>08:00</option>
                                    <option value="09:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='09:00'){ echo 'selected';} ?>>09:00</option>
                                    <option value="10:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='10:00'){ echo 'selected';} ?>>10:00</option>
                                    <option value="11:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='11:00'){ echo 'selected';} ?>>11:00</option>
                                    <option value="12:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='12:00'){ echo 'selected';} ?>>12:00</option>
                                    <option value="13:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='13:00'){ echo 'selected';} ?>>13:00</option>
                                    <option value="14:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='14:00'){ echo 'selected';} ?>>14:00</option>
                                    <option value="15:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='15:00'){ echo 'selected';} ?>>15:00</option>
                                    <option value="16:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='16:00'){ echo 'selected';} ?>>16:00</option>
                                    <option value="17:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='17:00'){ echo 'selected';} ?>>17:00</option>
                                    <option value="18:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='18:00'){ echo 'selected';} ?>>18:00</option>
                                    <option value="19:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='19:00'){ echo 'selected';} ?>>19:00</option>
                                    <option value="20:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='20:00'){ echo 'selected';} ?>>20:00</option>
                                    <option value="21:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='21:00'){ echo 'selected';} ?>>21:00</option>
                                    <option value="22:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='22:00'){ echo 'selected';} ?>>22:00</option>
                                    <option value="23:00" <?php if(isset($getchefdata[0]['brunch']) && $getchefdata[0]['brunch'] =='23:00'){ echo 'selected';} ?>>23:00</option>
                                </select>
                            </div>
                            <div class="input serviceBlck">
                                <input class="radio" type="checkbox" id="lunch" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] !=''){ echo 'checked';}?>>
                                <label class="radiobLable" for="lunch"> Lunch</label>
                                <select class="slctTime" name="lunch">
                                    <option value="00:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='00:00'){ echo 'selected';} ?>>00:00</option>
                                    <option value="01:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='01:00'){ echo 'selected';} ?>>01:00</option>
                                    <option value="02:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='02:00'){ echo 'selected';} ?>>02:00</option>
                                    <option value="03:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='03:00'){ echo 'selected';} ?>>03:00</option>
                                    <option value="04:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='04:00'){ echo 'selected';} ?>>04:00</option>
                                    <option value="05:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='05:00'){ echo 'selected';} ?>>05:00</option>
                                    <option value="06:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='06:00'){ echo 'selected';} ?>>06:00</option>
                                    <option value="07:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='07:00'){ echo 'selected';} ?>>07:00</option>
                                    <option value="08:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='08:00'){ echo 'selected';} ?>>08:00</option>
                                    <option value="09:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='09:00'){ echo 'selected';} ?>>09:00</option>
                                    <option value="10:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='10:00'){ echo 'selected';} ?>>10:00</option>
                                    <option value="11:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='11:00'){ echo 'selected';} ?>>11:00</option>
                                    <option value="12:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='12:00'){ echo 'selected';} ?>>12:00</option>
                                    <option value="13:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='13:00'){ echo 'selected';} ?>>13:00</option>
                                    <option value="14:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='14:00'){ echo 'selected';} ?>>14:00</option>
                                    <option value="15:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='15:00'){ echo 'selected';} ?>>15:00</option>
                                    <option value="16:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='16:00'){ echo 'selected';} ?>>16:00</option>
                                    <option value="17:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='17:00'){ echo 'selected';} ?>>17:00</option>
                                    <option value="18:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='18:00'){ echo 'selected';} ?>>18:00</option>
                                    <option value="19:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='19:00'){ echo 'selected';} ?>>19:00</option>
                                    <option value="20:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='20:00'){ echo 'selected';} ?>>20:00</option>
                                    <option value="21:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='21:00'){ echo 'selected';} ?>>21:00</option>
                                    <option value="22:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='22:00'){ echo 'selected';} ?>>22:00</option>
                                    <option value="23:00" <?php if(isset($getchefdata[0]['lunch']) && $getchefdata[0]['lunch'] =='23:00'){ echo 'selected';} ?>>23:00</option>
                                </select>
                            </div>
                            <div class="input serviceBlck">
                                <input class="radio" type="checkbox" id="dinner"  <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] !=''){ echo 'checked';} ?>>
                                <label class="radiobLable" for="dinner"> Dinner</label>
                                <select class="slctTime" name="dinner">
                                    <option value="00:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='00:00'){ echo 'selected';} ?>>00:00</option>
                                    <option value="01:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='01:00'){ echo 'selected';} ?>>01:00</option>
                                    <option value="02:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='02:00'){ echo 'selected';} ?>>02:00</option>
                                    <option value="03:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='03:00'){ echo 'selected';} ?>>03:00</option>
                                    <option value="00:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='04:00'){ echo 'selected';} ?>>04:00</option>
                                    <option value="05:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='05:00'){ echo 'selected';} ?>>05:00</option>
                                    <option value="06:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='06:00'){ echo 'selected';} ?>>06:00</option>
                                    <option value="07:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='07:00'){ echo 'selected';} ?>>07:00</option>
                                    <option value="08:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='08:00'){ echo 'selected';} ?>>08:00</option>
                                    <option value="09:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='09:00'){ echo 'selected';} ?>>09:00</option>
                                    <option value="10:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='10:00'){ echo 'selected';} ?>>10:00</option>
                                    <option value="11:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='11:00'){ echo 'selected';} ?>>11:00</option>
                                    <option value="12:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='12:00'){ echo 'selected';} ?>>12:00</option>
                                    <option value="13:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='13:00'){ echo 'selected';} ?>>13:00</option>
                                    <option value="14:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='14:00'){ echo 'selected';} ?>>14:00</option>
                                    <option value="15:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='15:00'){ echo 'selected';} ?>>15:00</option>
                                    <option value="16:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='16:00'){ echo 'selected';} ?>>16:00</option>
                                    <option value="17:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='17:00'){ echo 'selected';} ?>>17:00</option>
                                    <option value="18:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='18:00'){ echo 'selected';} ?>>18:00</option>
                                    <option value="19:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='19:00'){ echo 'selected';} ?>>19:00</option>
                                    <option value="20:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='20:00'){ echo 'selected';} ?>>20:00</option>
                                    <option value="21:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='21:00'){ echo 'selected';} ?>>21:00</option>
                                    <option value="00:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='22:00'){ echo 'selected';} ?>>22:00</option>
                                    <option value="23:00" <?php if(isset($getchefdata[0]['dinner']) && $getchefdata[0]['dinner'] =='23:00'){ echo 'selected';} ?>>23:00</option>
                                </select>
                            </div>
                        </div>  
                        <div class="form-group">
                            <h4 class="clr-black">Service types <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Type of Services</span></span></h4>
                            <ul class="serviceBlck srvcType"> 
                                <li>
                                    <input class="radio" type="checkbox" id="FoodDelivery" name="serveType[]" value="Food Delivery" <?php if(!empty($serviceArr) && in_array('Food Delivery' , $serviceArr)){ echo 'checked';}?> >
                                    <label class="radiobLable" for="FoodDelivery"> Food Delivery</label>
                                </li>
                                <li>
                                    <input class="radio" type="checkbox" id="PrivateMeal" name="serveType[]" value="Home Private Meal" <?php if(!empty($serviceArr) && in_array('Home Private Meal' , $serviceArr)){ echo 'checked';}?>>
                                    <label class="radiobLable" for="PrivateMeal">Home Private Meal</label>
                                </li>
                                <li class="mrRight">
                                    <input class="radio" type="checkbox" id="DailyMeal" name="serveType[]" value="Daily Meal" <?php if(!empty($serviceArr) && in_array('Daily Meal' , $serviceArr)){ echo 'checked';}?>>
                                    <label class="radiobLable" for="DailyMeal">Daily Meal</label>
                                </li>
                            </ul>
                            <ul class="serviceBlck srvcType"> 
                                <li>
                                    <input class="radio" type="checkbox" id="RestaurantPrivate" name="serveType[]" value="Restaurant Private Meal" <?php if(!empty($serviceArr) && in_array('Restaurant Private Meal' , $serviceArr)){ echo 'checked';}?>>
                                    <label class="radiobLable" for="RestaurantPrivate">Restaurant Private Meal</label>
                                </li>
                                <li>
                                    <input class="radio" type="checkbox" id="OnsiteCooking" name="serveType[]" value="Chef Onsite Cooking" <?php if(!empty($serviceArr) && in_array('Chef Onsite Cooking' , $serviceArr)){ echo 'checked';}?>>
                                    <label class="radiobLable" for="OnsiteCooking">Chef Onsite Cooking</label>
                                </li>
                                <li class="mrRight">
                                    <input class="radio" type="checkbox" id="CookingWorkshop" name="serveType[]" value="Cooking Workshop" <?php if(!empty($serviceArr) && in_array('Cooking Workshop' , $serviceArr)){ echo 'checked';}?>>
                                    <label class="radiobLable" for="CookingWorkshop">Cooking Workshop</label>
                                </li>
                            </ul>
                        </div>
                        <div class="form-group float-left frmHalf clearfix">
                            <h4 class="clr-black">Min. No. of Guests <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Min. No.'s of guest</span></span></h4>
                           <select  class="input" name="from_guest">
                               <option value="1" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='1'){ echo 'selected';} ?> >1</option>
                               <option value="2" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='2'){ echo 'selected';} ?> >2</option>
                               <option value="3" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='3'){ echo 'selected';} ?> >3</option>
                               <option value="4" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='4'){ echo 'selected';} ?> >4</option>
                               <option value="5" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='5'){ echo 'selected';} ?> >5</option>
                               <option value="6" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='6'){ echo 'selected';} ?> >6</option>
                               <option value="7" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='7'){ echo 'selected';} ?> >7</option>
                               <option value="8" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='8'){ echo 'selected';} ?>  >8</option>
                               <option value="9" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='9'){ echo 'selected';} ?>  >9</option>
                               <option value="10" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='10'){ echo 'selected';} ?>  >10</option>
                               <option value="11" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='11'){ echo 'selected';} ?>  >11</option>
                               <option value="12" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='12'){ echo 'selected';} ?>  >12</option>
                               <option value="13" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='13'){ echo 'selected';} ?>  >13</option>
                               <option value="14" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='14'){ echo 'selected';} ?>  >14</option>
                               <option value="15" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='15'){ echo 'selected';} ?>  >15</option>
                               <option value="16" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='16'){ echo 'selected';} ?>  >16</option>
                               <option value="17" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='17'){ echo 'selected';} ?>  >17</option>
                               <option value="18" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='18'){ echo 'selected';} ?>  >18</option>
                               <option value="19" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='19'){ echo 'selected';} ?>  >19</option>
                               <option value="20" <?php if(isset($getchefdata[0]['from_guest']) && $getchefdata[0]['from_guest'] =='20'){ echo 'selected';} ?>  >20</option>
                           </select>
                        </div>
                        <div class="form-group float-left frmHalf mrRight">
                            <h4 class="clr-black">Max. No. of Guests <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Max. No.'s of guest</span></span></h4>
                            <select class="input" name="upto_guest">
                               <option value="1" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='1'){ echo 'selected';} ?> >1</option>
                               <option value="2" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='2'){ echo 'selected';} ?> >2</option>
                               <option value="3" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='3'){ echo 'selected';} ?> >3</option>
                               <option value="4" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='4'){ echo 'selected';} ?> >4</option>
                               <option value="5" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='5'){ echo 'selected';} ?> >5</option>
                               <option value="6" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='6'){ echo 'selected';} ?> >6</option>
                               <option value="7" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='7'){ echo 'selected';} ?> >7</option>
                               <option value="8" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='8'){ echo 'selected';} ?> >8</option>
                               <option value="9" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='9'){ echo 'selected';} ?> >9</option>
                               <option value="10" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='10'){ echo 'selected';} ?> >10</option>
                               <option value="11" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='11'){ echo 'selected';} ?> >11</option>
                               <option value="12" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='12'){ echo 'selected';} ?> >12</option>
                               <option value="13" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='13'){ echo 'selected';} ?> >13</option>
                               <option value="14" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='14'){ echo 'selected';} ?> >14</option>
                               <option value="15" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='15'){ echo 'selected';} ?> >15</option>
                               <option value="16" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='16'){ echo 'selected';} ?> >16</option>
                               <option value="17" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='17'){ echo 'selected';} ?>  >17</option>
                               <option value="18" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='18'){ echo 'selected';} ?>  >18</option>
                               <option value="19" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='19'){ echo 'selected';} ?>  >19</option>
                               <option value="20" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='20'){ echo 'selected';} ?> >20</option>
                               <option value="21" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='21'){ echo 'selected';} ?> >21</option>
                               <option value="22" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='22'){ echo 'selected';} ?> >22</option>
                               <option value="23" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='23'){ echo 'selected';} ?> >23</option>
                               <option value="24" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='24'){ echo 'selected';} ?> >24</option>
                               <option value="25" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='25'){ echo 'selected';} ?> >25</option>
                               <option value="26" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='26'){ echo 'selected';} ?> >26</option>
                               <option value="27" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='27'){ echo 'selected';} ?> >27</option>
                               <option value="28" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='28'){ echo 'selected';} ?> >28</option>
                               <option value="29" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='29'){ echo 'selected';} ?> >29</option>
                               <option value="30" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='30'){ echo 'selected';} ?> >30</option>
                               <option value="31" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='31'){ echo 'selected';} ?> >31</option>
                               <option value="32" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='32'){ echo 'selected';} ?> >32</option>
                               <option value="33" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='33'){ echo 'selected';} ?> >33</option>
                               <option value="34" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='34'){ echo 'selected';} ?> >34</option>
                               <option value="35" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='35'){ echo 'selected';} ?> >35</option>
                               <option value="36" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='36'){ echo 'selected';} ?> >36</option>
                               <option value="37" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='37'){ echo 'selected';} ?> >37</option>
                               <option value="38" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='38'){ echo 'selected';} ?> >38</option>
                               <option value="39" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='39'){ echo 'selected';} ?> >39</option>
                               <option value="40" <?php if(isset($getchefdata[0]['upto_guest']) && $getchefdata[0]['upto_guest'] =='40'){ echo 'selected';} ?> >40</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <h4 class="clr-black">Currency <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">Deafult Currency</span></span></h4>
                            <div class="flagstrap input" id="select_host_currency" data-input-name="NewBuyer_country" data-selected-country=""></div>
                        </div>
                       
                        <div class="form-group">
                            <h4 class="clr-black">About the kitchen <span><img src="<?php echo base_url();?>front/images/tooltip.png" alt=""><span class="tooltiptext">kitchen Description</span></span></h4>
                            <textarea class="" name="kitchen_descrition" placeholder="Please let us know what's unique about your kitchen. Is it the authentic cuisine? Is it the decor? Is it the atmosphere? Is it the host and the stories? Maybe it's all of these? Let your guests know why they must visit you!"><?php if(isset($getchefdata[0]['kitchen_descrition']) && $getchefdata[0]['kitchen_descrition'] !=''){ echo  $getchefdata[0]['kitchen_descrition'];} ?></textarea>
                        </div>
                        <div class="form-group sbmt-row">
                            <input type="submit" value="Next" class="txt-upr sbmt float-left">
                            <a href="#" class="txt-upr prvw float-right">Preview</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>
<script>
    jQuery(document).ready(function () {
        var curr = '<?php echo $getchefdata[0]['currency'] ?>';
        if(curr == ''){
            curr = 'HKD';
        } 
       // alert(curr);
        jQuery('#select_host_currency').attr('data-selected-country',curr);
        jQuery('#select_host_currency').flagStrap();

//        $('#select_nenu_currency option:selected').val(curr);
    });

 
</script>
</body>
</html>