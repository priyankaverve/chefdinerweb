<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/my-kitchen.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/intlTelInput.css">
</head>
<body>
<?php
$this->load->view('front/header');?>
<div class="wrapper mykitchenPage">
    <div class="container">
    <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h1 class="clr-black"><?php if(isset($getchefdata[0]['kitchen_title'])){ echo $getchefdata[0]['kitchen_title'];}else{
                  echo ucfirst($this->session->userdata('first_name'))."'s Kitchen";
                    }?> <a href="<?php echo base_url() ?>chefdetail">Preview</a></h1>
                <p class="clr-gray"><span class="clr-red">3</span> steps to become a chef!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="KtchnNav">
                    <ul>
                        <li class="kitchen"><a href="mykitchen">My Kitchen</a></li>
                        <li class="profile"><a href="myprofile">Profile</a></li>
                        <li class="address active"><a href="myaddress">Address</a></li>
                        <li class="photos"><a href="myphotos">Photos</a></li>
                        <li class="ktchnmenu"><a href="mymenus">Menu</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-5 col-md-5">
                <div class="kithchenForm">
                    <form action="<?php echo base_url(); ?>saveMyadd" method="post" id="form">
                   <input type="hidden" name="chefid" value="<?php if(isset($getchefdata[0]['user_id'])){ echo $getchefdata[0]['user_id'];}?>">
                        <div class="form-group">
                            <h4 class="clr-black">Country</h4>
                           <select  class="input" id="country">
                           <option value="">Select Country</option>
                           <?php 
                           if(!empty($country)){
                            foreach ($country as $key => $value) { ?>
                            <option value="<?=$value['id']?>" <?php if($value['name'] == $getchefdata[0]['chef_country']){ echo 'selected';} ?> ><?=$value['name']?></option>
                         <?php      
                                }
                           }

                           ?>
                           
                           </select>
                        </div>
                        <input type="hidden" name="countryname" id="countryname" value="<?php if(isset($getchefdata[0]['chef_country'])){ echo $getchefdata[0]['chef_country'];}?>">
                        <div class="form-group">
                            <h4 class="clr-black">State</h4>
                            <select  class="input" id="state">
                                <option value="">Select country first</option>
                            </select>
                        </div>
                        <input type="hidden" name="statesname" id="statesname" value="<?php if(isset($getchefdata[0]['chef_state'])){ echo $getchefdata[0]['chef_state'];}?>">
                        <div class="form-group">
                            <h4 class="clr-black">City</h4>
                            <select  class="input" id="city" name="city">
                                <option value="">Select state first</option>
                            </select>
                        </div>

                        <div class="form-group isdCode">
                            <h4 class="clr-black">Phone</h4>
                            <input type="tel" id="phone" class="input" placeholder="Mobile No." value="<?php if(isset($getchefdata[0]['phone_number'])){ echo $getchefdata[0]['phone_number'];}?>">
                            
                            
                            <p class="ntce">Phone Number will be kept privately and we will only use it to inform you about all bookings.</p>
                        </div>

                        <input type="hidden" name="countryCode" id="countryCode" />
                            <input type="hidden" name="phonefull" id="phonefull" />
                        
                        <div class="form-group">
                            <h4 class="clr-black">Address <span><img src="<?= base_url()?>front/images/tooltip.png" alt=""><span class="tooltiptext">Add a Location</span></span></h4>
                            <textarea class="" name="chef_address"><?php if(isset($getchefdata[0]['chef_address'])){ echo $getchefdata[0]['chef_address'];}  ?></textarea>
                            <p class="ntce">Your exact address will be kept privately and only be shared with guests after reservation is confirmed by you</p>
                        </div>

                        <div class="form-group">
                            <h4 class="clr-black">Directions <span><img src="<?= base_url()?>front/images/tooltip.png" alt=""><span class="tooltiptext">Add Directions</span></span></h4>
                            <textarea class="" name="add_direction"><?php if(isset($getchefdata[0]['add_direction'])){ echo $getchefdata[0]['add_direction'];}  ?></textarea>
                            <p class="ntce">Only guests whom you confirm can view this. Please give the instructions how guests can reach your place through public transport or what to mention to driver</p>
                        </div>
                        <div class="form-group">
                            <h4 class="clr-black">House Rules <span><img src="<?= base_url()?>front/images/tooltip.png" alt=""><span class="tooltiptext">House Rules</span></span></h4>
                            <textarea class="" name="house_rules" placeholder="Can your guests bring alcohol, smoke? Do you have pets in a house?"><?php if(isset($getchefdata[0]['house_rules'])){ echo $getchefdata[0]['house_rules'];}  ?></textarea>
                        </div>
                        <div class="form-group sbmt-row">
                            <input type="submit" value="Next" class="txt-upr sbmt float-left">
                            <a href="#" class="txt-upr prvw float-right">Preview</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="addMap" id="map" style="width:100%;height:582px;">
                   <!--  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d28465.496716101916!2d75.77660845000001!3d26.897556899999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1515217357006" width="100%" height="582px" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>

<script>
var lati = <?php echo $getchefdata[0]['chef_latitude'] ?>;
var long = <?php echo $getchefdata[0]['chef_longitude'] ?>;
var add  = '<?php echo $getchefdata[0]['chef_address'] ?>';
if(lati == '' && long ==''){
lati = 22.3964;
long = 114.1095;
}
      // This example displays a marker at the center of Australia.
      // When the user clicks the marker, an info window opens.

      function initMap() {
        var uluru = {lat: lati, lng: long};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: uluru
        });

        var contentString = '<div id="content">'+add+'</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          title: 'Uluru (Ayers Rock)'
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKOx_4-P_cvIm4y7is8ymWL3Gc8CImVX4&callback=initMap">
    </script>
</body>
</html>