<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title><?php echo $this->lang->line('sitetitle'); ?></title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/chef-detail.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/owl.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
</head>
<body>
<?php $this->load->view('front/header');?>
<div class="wrapper chefDetailPage">
    <div class="container">
    <?php 
    // echo '<pre>';
    // print_r($getmenus);die;
    $imgs = $getchefdata[0]->allimage;
    $allimg  = explode("|", $imgs);
    $allimg  = array_values(array_filter($allimg));
    // echo '<pre>';
    // print_r($getchefdata);die;

    $cu = $getchefdata[0]->cuisine;
    $cuisine  = explode(",", $cu);
    // print_r($cuisine);
    // echo count($cuisine); die;
    if(!empty($cuisine)){
    if(count($cuisine) > 0 && count($cuisine) == 1){
         //echo '1';
        $c1 = $cuisine[0];
        $c2 ='';
    }elseif(count($cuisine) > 1 && count($cuisine) == 2){
         //echo '2';
        $c1 = $cuisine[0];
        $c2 = $cuisine[1];
    }elseif(count($cuisine) > 2){
         //echo '2';
        $c1 = $cuisine[0];
        $c2 = $cuisine[1];
    }else{
        //echo '3';
        $c1 = '';
        $c2 = '';
    }
    }else{
        
       $c1 = '';
        $c2 = '';
    }
   
//die;
    ?>
        <div class="row">
            <div class="col-md-12">

            <?php
            if(!empty($getchefdata)){
                    $imgs = $getchefdata[0]->allimage;
                    $allimg  = explode("|", $imgs);
                    $allimg  = array_values(array_filter($allimg));

            }
          //  print_r($allimg);die;
            $count = count($allimg);
            //echo $count;
            if($count > 6){
                $remain = $count - 6 .'+ More Photos';
                $image1 = $allimg[0];
                $image2 = $allimg[1];
                $image3 = $allimg[2];
                $image4 = $allimg[3];
                $image5 = $allimg[4];
                $image6 = $allimg[5];
            }else{
                $remain = '';
                $image1 = base_url().'front/images/blank.jpg';
                $image2 = base_url().'front/images/blank.jpg';
                $image3 = base_url().'front/images/blank.jpg';
                $image4 = base_url().'front/images/blank.jpg';
                $image5 = base_url().'front/images/blank.jpg';
                $image6 = base_url().'front/images/blank.jpg';
            }
            ?>

                <div class="topImages">
                    <div class="imageBox Square">
                        <img src="<?php echo $image1 ?>" alt="">
                    </div>
                    <div class="imageBox Rectangle">
                        <img src="<?php echo $image2 ?>" alt="">
                    </div>
                </div>
                <div class="topImages">
                    <div class="imageBox Rectangle">
                        <img src="<?php echo $image3 ?>" alt="">
                    </div>
                    <div class="imageBox Square">
                        <img src="<?php echo $image4 ?>" alt="">
                    </div>
                </div>
                <div class="topImages mrRight">
                    <div class="imageBox Square">
                        <img src="<?php echo $image5 ?>" alt="">
                    </div>
                    <div class="imageBox Rectangle">
                        <img src="<?php echo $image6 ?>" alt="">
                        <div class="shadowbox">
                            <p><?=$remain?>  </p>
                            <a href="#modal-carousel" data-toggle="modal">View All</a>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modal-carousel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div id="InstaImages" class="carousel slide" data-ride="carousel">

                                    <div class="carousel-inner">
                                    <?php 
                                    $i = 1;
                                    foreach ($allimg as $key => $value) {
                                        if($i == 1){
                                                $c = 'active';
                                        }else{
                                                $c = '';
                                        }
                                      

                                    ?>
                                    <div class="carousel-item <?=$c;?>">
                                        <img src="<?=$value;?>" alt="">
                                        </div>
                                       
                                        <?php $i++; } ?>
                                    </div>
                                    <a class="carousel-control-prev" href="#InstaImages" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#InstaImages" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div><!-- /.modal-body -->
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <div class="KitchnDetails">
                    <div class="float-left">
                        <div class="chfImg">
                            <div class="img">
                                <img src="<?=$getchefdata[0]->profile_pic;?>" alt="">
                            </div>
                        </div>
                        <div class="Kitchn">
                            <h3 class="clr-black"><?=ucfirst($getchefdata[0]->kitchen_title);?></h3>
                            <div class="float-left ktchLoc clr-gray">
                               <span class="float-left"><img src="<?php echo base_url();?>front/images/loc-red.png" alt=""></span>
                               <span class="float-left"><?=ucfirst($getchefdata[0]->chef_city).','.ucfirst($getchefdata[0]->chef_country);?></span>
                            </div>
                            <div class="float-left ktchnUsrName">
                                <a href="" class="clr-red"><?=ucfirst($getchefdata[0]->chef_fname);?></a>
                            </div>
                            <div class="clearfix reactions">
                                <div class="reactionBox clr-gray">
                                    <span class="imogy yummy float-left"></span>
                                    <span class="counts float-left"><?=$getchefdata[0]->recommended;?></span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy avg float-left"></span>
                                    <span class="counts float-left"><?=$getchefdata[0]->ok;?></span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy bad float-left"></span>
                                    <span class="counts float-left"><?=$getchefdata[0]->no_recommended;?></span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy bkmark float-left"></span>
                                    <span class="counts float-left"><?=$getchefdata[0]->favouritecount;?></span>
                                </div>
                            </div>
                            <div class="float-left ktchLoc itmprice clr-gray">
                                <span class="float-left"><img src="<?php echo base_url();?>front/images/pricelabel.png" alt=""></span>
                                <span class="float-left"><?=$getchefdata[0]->currency;?> <?=$getchefdata[0]->price;?> to <?=$getchefdata[0]->currency;?> <?=$getchefdata[0]->maxprice;?></span>
                            </div>
                            <div class="float-left ktchLoc itmcusine clr-gray">
                                <span class="float-left"><img src="<?php echo base_url();?>front/images/cusine.png" alt=""></span>
                                <span class="float-left no-brdr mrRight"><?=$c1 .' , '. $c2?></span>
                            </div>

                        </div>
                        
                    </div>  
                    <?php 
                                     if(is_float($getchefdata[0]->average_rating)){
                                        $avr_rating = $getchefdata[0]->average_rating;

                                        }else{
                                           $avr_rating  = number_format((float)$getchefdata[0]->average_rating, 1, '.', '');
                                        }
                    ?>
                    <div class="float-right">
                        <div class="reviewsBox">
                            <div class="starBox">
                                <div class="float-right rateScore"><?=$avr_rating;?></div>
                                <div class="float-right star">
                                <div class="cstmr-reviews">
                                 <?php
                                           $roundRatchf = round($getchefdata[0]->average_rating);
                                           //echo $rountRat;
                                           if($roundRatchf > 0 && $roundRatchf <=0.5){
                                          
                                           $valhalf5chf = '0.5';
                                           $chkhalf5chf = 'checked';

                                           }else{
                                            $valhalf5chf = '0.5';
                                            $chkhalf5chf = '';
                                           }

                                           if($roundRatchf > 0.5 && $roundRatchf <=1){
                                          
                                           $valfull1chf = '1.0';
                                           $chkfull1chf = 'checked';

                                           }else{
                                            $valfull1chf = '1.0';
                                            $chkfull1chf = '';
                                           }

                                           if($roundRatchf > 1 && $roundRatchf <=1.5){
                                          
                                           $valhalf1chf = '1.5';
                                           $chkhalf1chf = 'checked';

                                           }else{
                                            $valhalf1chf = '1.5';
                                            $chkhalf1chf = '';
                                           }
                                           if($roundRatchf > 1.5 && $roundRatchf <=2){
                                          
                                           $valfull2chf = '2';
                                           $chkfull2chf = 'checked';

                                           }else{
                                            $valfull2chf = '2';
                                            $chkfull2chf = '';
                                           }

                                           if($roundRatchf > 2 && $roundRatchf <=2.5){
                                          
                                           $valhalf2chf = '2.5';
                                           $chkhalf2chf = 'checked';

                                           }else{
                                            $valhalf2chf = '2.5';
                                            $chkhalf2chf = '';
                                           }

                                           if($roundRatchf > 2.5 && $roundRatchf <=3){
                                          
                                           $valfull3chf = '3';
                                           $chkfull3chf = 'checked';

                                           }else{
                                            $valfull3chf = '3';
                                            $chkfull3chf = '';
                                           }

                                          if($roundRatchf > 3 && $roundRatchf <=3.5){
                                          
                                           $valhalf3chf = '3.5';
                                           $chkhalf3chf = 'checked';

                                           }else{
                                            $valhalf3chf = '3.5';
                                            $chkhalf3chf = '';
                                           }
                                           if($roundRatchf > 3.5 && $roundRatchf <=4){
                                          
                                           $valfull4chf = '4';
                                           $chkfull4chf = 'checked';

                                           }else{
                                            $valfull4chf = '4';
                                            $chkfull4chf = '';
                                           }
                                           
                                           if($roundRatchf > 4 && $roundRatchf <=4.5){
                                          
                                           $valhalf4chf = '4.5';
                                           $chkhalf4chf = 'checked';

                                           }else{
                                            $valhalf4chf = '4.5';
                                            $chkhalf4chf = '';
                                           }

                                           if($roundRatchf > 4.5 && $roundRatchf <=5){
                                          
                                           $valfull5chf = '5';
                                           $chkfull5chf = 'checked';

                                           }else{
                                            $valfull5chf = '5';
                                            $chkfull5chf = '';
                                           }
                                           

                                            ?>
                                                          <fieldset class="rating">
                                                            <input type="radio" id="starachf" name="ratingachf" value="<?=$valfull5chf?>" <?=$chkfull5chf?> disabled="disable" />
                                                            <label class = "full" for="starachf" title="Awesome - 5 stars"></label>

                                                            <input type="radio" id="starbchf" name="ratingbchf" value="<?=$valhalf4chf?>" <?=$chkhalf4chf?> disabled="disable" />
                                                            <label class="half" for="starbchf" title="Pretty good - 4.5 stars"></label>

                                                            <input type="radio" id="starcchf" name="ratingcchf" value="<?=$valfull4chf?>" <?=$chkfull4chf?> disabled="disable"  />
                                                            <label class = "full" for="starcchfchf" title="Pretty good - 4 stars"></label>

                                                            <input type="radio" id="stardchf" name="ratingdchf" value="<?=$valhalf3chf?>" <?=$chkhalf3chf?> disabled="disable" />
                                                            <label class="half" for="stardchf" title="Meh - 3.5 stars"></label>

                                                            <input type="radio" id="starechf" name="ratingechf" value="<?=$valfull3chf?>" <?=$chkfull3chf?> disabled="disable"/>
                                                            <label class = "full" for="starechf" title="Meh - 3 stars"></label>

                                                            <input type="radio" id="starfchf" name="ratingfchf" value="<?=$valhalf2chf?>" <?=$chkhalf2chf?> disabled="disable" />
                                                            <label class="half" for="starfchf" title="Kinda bad - 2.5 stars"></label>

                                                            <input type="radio" id="stargchf" name="ratinggchf" value="<?=$valfull2chf?>" <?=$chkfull2chf?> disabled="disable" />
                                                            <label class = "full" for="stargchf" title="Kinda bad - 2 stars"></label>

                                                            <input type="radio" id="starhchf" name="ratinghchf" value="<?=$valhalf1chf?>" <?=$chkhalf1chf?> disabled="disable"  />
                                                            <label class="half" for="starhchf" title="Meh - 1.5 stars"></label>

                                                            <input type="radio" id="starichf" name="ratingichf" value="<?=$valfull1chf?>" <?=$chkfull1chf?> disabled="disable" />
                                                            <label class = "full" for="starichf" title="Sucks big time - 1 star"></label>

                                                            <input type="radio" id="starjchf" name="ratingjchf" value="<?=$valhalf5chf?>" <?=$chkhalf5chf?> disabled="disable" />
                                                            <label class="half" for="starjchf" title="Sucks big time - 0.5 stars"></label>
                                                        </fieldset>
                                                    </div>

                            <p><?=$getchefdata[0]->totalreview;?> Reviews</p></div>
                            </div>
                        </div>
                        <div class="wishBox">
                            <span class="float-right shareImg">
                                <img src="<?php echo base_url();?>front/images/share.png" alt="">
                                <ul class="shareList">
                                    <li>
                                        <a href="javascript:;"><img src="<?php echo base_url();?>front/images/fbsquare.svg" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"><img src="<?php echo base_url();?>front/images/twitsquare.svg" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"><img src="<?php echo base_url();?>front/images/mailsquare.svg" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"><img src="<?php echo base_url();?>front/images/gplussquare.svg" alt=""></a>
                                    </li>
                                </ul>
                            </span>
                            <span class="float-right text-center">
                            <?php 
                            if($wishlistChefcount != 0){
                              $clswishlist = 'wished';
                            }else{
                              $clswishlist = '';
                            }

                            ?>
                                <button class="wishlistbtn <?=$clswishlist;?>" onclick="Wishlist('<?php echo $wishlistChefcount ?>','<?php echo $chef ?>')" ></button>
                                Add to wishlist
                            </span>
                        </div>
                    </div>

                    <div class="srvcType">
                            <span class="float-left">
                                <img src="<?php echo base_url();?>front/images/platehand.png" alt="">
                            </span>
                            <span class="float-left clr-gray"><strong class="clr-black">Service type :</strong> <?=$getchefdata[0]->service_type?></span>
                        </div>
                </div>

                <div class="ktchnMenu">
                    <h4 class="secTitle">
                        Menu
                    </h4>
                    <div class="owl-carousel owl-theme">
                    <?php 

                    if(!empty($getmenus)){

                        $i = 0;
                        foreach ($getmenus as $key => $value) {
                            // echo '<pre>';
                            // print_r($value);
                            

                    
                    ?>
                        <div class="item">
                            <div class="menuItmsec">
                                <h5 class="itmName text-center"> <a href=""><?=$value[0]->menu_title?></a></h5>
                                <?php

                                 foreach ($value as $key => $value1) {
                                     if(!empty($value1->dish_image)){
                                        $dish_image = $value1->dish_image;
                                     }else{
                                        $dish_image = base_url().'front/images/blank.jpg';
                                     }

                                    // print_r($value1);die;

                                ?>
                                <div class="itmbox">
                                    <p class="text-center priceP">
                                        <span class=""><img src="<?php echo base_url();?>front/images/pricelabelgray.png" alt=""></span>
                                        <span class="txt"><?=$value1->menucurrency.$value1->menu_price;?> Per Guest</span>
                                    </p>
                                    <div class="img">
                                        <a href="">
                                            <img src="<?php echo $dish_image ?>" alt="">
                                        </a>
                                    </div>
                                    <p class="text-center startrP clr-black"><?=ucfirst($value1->dish_name);?><br><span class="clr-gray"><?=ucfirst($value1->dish_category);?></span></p>
                                </div>

                                <?php 
                            }

                                ?>
                            
                            </div>
                        </div>
                       <?php }
                       $i++;

                       } ?>
                    </div>
                </div>
                <div class="aboutKthcnBlock">
                     <h4 class="secTitle">About the Kitchen</h4>
                     <ul class="KthcnBlockNacList">
                        <li>
                            <a href="#Overview">Overview</a>
                        </li>
                        <li>
                            <a href="#About-Chef">About Chef</a>
                        </li>
                        <li>
                            <a href="#Details">Details</a>
                        </li>
                        <li>
                            <a href="#Location">Location</a>
                        </li>
                     </ul>

                     <div id="Overview" class="aboutKitchen ktchnsSec scrollSection">
                       <h5 class="subSecTitle">About Kitchen</h5>
                       <p>
                           <?=$getchefdata[0]->kitchen_descrition;?>
                       </p>
                     </div>
                     <div id="About-Chef" class="aboutChef ktchnsSec scrollSection">
                        <h5 class="subSecTitle">About Chef</h5>
                        <div class="chfName">
                            <span class="float-left img">
                                <img src="<?=$getchefdata[0]->profile_pic;?>" alt="">
                            </span>
                            <span class="Detail clr-gray float-left">
                                <strong class="clr-black"><?=ucfirst($getchefdata[0]->chef_fname);?></strong><br>
                                From <?=ucfirst($getchefdata[0]->chef_country);?>
                            </span>
                        </div>
                        <p>
                        <?=ucfirst($getchefdata[0]->chef_bio);?>
                        </p>
                    </div>
                    <div id="Details" class="aboutDetails ktchnsSec scrollSection">
                        <h5 class="subSecTitle">Details</h5>
                        <div class="betailBox chefsname clr-gray">
                            <strong class="clr-black">Chef Name</strong><br>
                            <?=ucfirst($getchefdata[0]->chef_fname);?>
                        </div>
                        <div class="betailBox guest clr-gray">
                            <strong class="clr-black">Gusets</strong><br>
                            Min - <?=$getchefdata[0]->from_guest;?> | Max - <?=$getchefdata[0]->upto_guest;?>
                        </div>
                        <div class="betailBox price clr-gray">
                            <strong class="clr-black">Price</strong><br>
                            <?=$getchefdata[0]->currency.$getchefdata[0]->price;?> to <?=$getchefdata[0]->currency.$getchefdata[0]->maxprice;?>
                        </div>
                        <div class="betailBox ChfLanguage clr-gray">
                            <strong class="clr-black">Language</strong><br>
                            <?=$getchefdata[0]->language_speak?>
                        </div>
                        <div class="betailBox Meals clr-gray">
                            <strong class="clr-black">Meals</strong><br>
                            <?php
                            if(!empty($getchefdata[0]->brunch) && empty($getchefdata[0]->lunch) && empty($getchefdata[0]->dinner)){
                               // echo '1';
                                $brunch = 'Brunch';
                                 $lunch = '';
                                $dinner = '';
                            }elseif(!empty($getchefdata[0]->lunch) && empty($getchefdata[0]->brunch) && empty($getchefdata[0]->dinner)){
                              //  echo '2';
                                $lunch = 'Lunch';
                                $brunch = '';
                                $dinner = '';
                            }elseif(!empty($getchefdata[0]->dinner) && empty($getchefdata[0]->brunch) && empty($getchefdata[0]->lunch)){
                              // echo '3';
                                $dinner = 'Dinner';
                                $brunch = '';
                                $lunch = '';
                            }elseif(!empty($getchefdata[0]->brunch) && !empty($getchefdata[0]->lunch) && empty($getchefdata[0]->dinner)){
                              //echo '4';
                                $brunch = 'Brunch';
                                $lunch = 'Lunch';
                                $dinner = '';
                            }elseif(!empty($getchefdata[0]->brunch) && !empty($getchefdata[0]->dinner) && empty($getchefdata[0]->lunch)){
                              //  echo '5';
                                $brunch = 'Brunch';
                                $lunch = '';
                                $dinner = 'Dinner';
                            }elseif(!empty($getchefdata[0]->lunch) && !empty($getchefdata[0]->dinner) && empty($getchefdata[0]->brunch)){
                              //  echo '6';
                                $lunch = 'Lunch';
                                $brunch = '';
                                $dinner = 'Dinner';
                            }elseif(!empty($getchefdata[0]->brunch) && !empty($getchefdata[0]->lunch) && !empty($getchefdata[0]->dinner)){
                               // echo '7';
                                $brunch = 'Brunch';
                                $lunch = 'Lunch';
                                $dinner = 'Dinner';
                            }else{
                              //  echo '8';
                                $brunch = '';
                                $lunch = '';
                                $dinner = '';
                            }
                            ?>
                            <?=$brunch;?>, <?=$lunch;?> & <?=$dinner;?>
                        </div>
                        <div class="betailBox Cuisinetype clr-gray">
                            <strong class="clr-black">Cuisine type</strong><br>
                            <?=$getchefdata[0]->cuisine;?>
                        </div>
                        <div class="betailBox hseRule clr-gray">
                            <strong class="clr-black">House rules</strong><br>
                            <? //=$getchefdata[0]->house_rules;?>
                            Non-alcoholic drink is welcome. Our house is very friendly to children. Pet should be outside. Smoking outside of the house.
                        </div>
                        <div class="betailBox svcType clr-gray">
                            <strong class="clr-black">Service type</strong><br>
                           <?=$getchefdata[0]->service_type;?>
                        </div>
                    </div>
                    <div id="Location" class="aboutLocation ktchnsSec scrollSection">
                        <h5 class="subSecTitle">Location</h5> 
                        <p><?=$getchefdata[0]->chef_address;?></p>
                        <div class="addMap" id="map" style="width:100%;height:350px;"></div>
                        <script>
                        var lati = <?php echo $getchefdata[0]->chef_latitude; ?>;
                        var long = <?php echo $getchefdata[0]->chef_longitude; ?>;
                        var add  = '<?php echo $getchefdata[0]->chef_address; ?>';
                        if(lati == '' && long ==''){
                        lati = 22.3964;
                        long = 114.1095;
                        }
                              // This example displays a marker at the center of Australia.
                              // When the user clicks the marker, an info window opens.

                              function initMap() {
                                var uluru = {lat: lati, lng: long};
                                var map = new google.maps.Map(document.getElementById('map'), {
                                  zoom: 10,
                                  center: uluru
                                });

                                var contentString = '<div id="content">'+add+'</div>';

                                var infowindow = new google.maps.InfoWindow({
                                  content: contentString
                                });

                                var marker = new google.maps.Marker({
                                  position: uluru,
                                  map: map,
                                  title: 'Uluru (Ayers Rock)'
                                });
                                marker.addListener('click', function() {
                                  infowindow.open(map, marker);
                                });
                              }
                            </script>
                            <script async defer
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKOx_4-P_cvIm4y7is8ymWL3Gc8CImVX4&callback=initMap">
                            </script>
                       
                    </div>
                    <div class="aboutinstagram ktchnsSec">
                        <h5 class="secTitle">Instagram Stories</h5>
                        <?php
                        $finalresult=$getchefdata[0]->access_token;
                        //echo  $finalresult;die;
                        if( $finalresult !=''){
                        $curl = curl_init('https://api.instagram.com/v1/users/self/media/recent/?access_token='.$finalresult);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // RETURN RESULT true
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // VERIFY SSL PEER false
                        $mediaresult = curl_exec($curl);
                        curl_close($curl);
                            
                        $final = json_decode($mediaresult);
                      //  echo '<pre>';
                       // print_r( $final);die;
                        $imagedata = $final->data;
                        foreach ($imagedata as $post=>$val) { 
                       //   print_r($val);
                        //     if(isset($post->carousel_media) && !empty($post->carousel_media)){
            
                        // foreach ($post->carousel_media as $key => $value) {
                            ?>

                        <div class="instagram">
                            <img src="<?php echo $val->images->thumbnail->url ?>" alt="">
                        </div>
                        
                    <?php    
                // }
                // }
            }
                        }else{
                         ?>
                        <div class="instagram">
                        <span>No records found</span>
                        </div>
                     <?php   }
                        ?> 
                        
                        <!-- <div class="instagram">
                            <img src="<?php //echo base_url();?>front/images/insta3.png" alt="">
                        </div>
                        <div class="instagram">
                            <img src="<?php //echo base_url();?>front/images/insta3.png" alt="">
                        </div>
                        <div class="instagram">
                            <img src="<?php //echo base_url();?>front/images/insta3.png" alt="">
                        </div>
                        <div class="instagram mrRight">
                            <img src="<?php //echo base_url();?>front/images/insta1.png" alt="">
                            <div class="shadowBox">
                                <a href="">25+<br>More Photos</a>
                            </div>
                        </div> -->
                    </div>
                    <div class="referEarn ktchnsSec">
                        <h5 class="secTitle">Refer & Earn</h5> 
                        <div class="referBox">
                            <div class="float-left img">
                                <img src="<?php echo base_url();?>front/images/refer.jpg" alt="">
                            </div>
                            <div class="float-left referDetail">
                                <p class="clr-black">Refer a friend and get up to 100 USD.</p>
                                <a href="" class="clr-red">Earn dining credits</a>
                                <div class="share">
                                    <span class="clr-gray">Share:</span>
                                    <ul>
                                        <li><a href="javascript:;"><img src="<?php echo base_url();?>front/images/fbsquare.svg"></a></li>
                                        <li><a href="javascript:;"><img src="<?php echo base_url();?>front/images/twitsquare.svg"></a></li>
                                        <li><a href="javascript:;"><img src="<?php echo base_url();?>front/images/mailsquare.svg"></a></li>
                                        <li><a href="javascript:;"><img src="<?php echo base_url();?>front/images/gplussquare.svg"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ReviewsSec ktchnsSec">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="secTitle RateStars float-left">Reviews</h5> 
                                <div class="float-left">
                                    <div class="cstmr-reviews">
                                      <?php
                                                   $rountRatRR = round($ratingarr['avr_review']);
                                                   //echo $rountRat;
                                                   if($rountRatRR > 0 && $rountRatRR <=0.5){
                                                  
                                                   $valhalf5RR = '0.5';
                                                   $chkhalf5RR = 'checked';

                                                   }else{
                                                    $valhalf5RR = '0.5';
                                                    $chkhalf5RR = '';
                                                   }

                                                   if($rountRatRR > 0.5 && $rountRatRR <=1){
                                                  
                                                   $valfull1RR = '1.0';
                                                   $chkfull1RR = 'checked';

                                                   }else{
                                                    $valfull1RR = '1.0';
                                                    $chkfull1RR = '';
                                                   }

                                                   if($rountRatRR > 1 && $rountRatRR <=1.5){
                                                  
                                                   $valhalf1RR = '1.5';
                                                   $chkhalf1RR = 'checked';

                                                   }else{
                                                    $valhalf1RR = '1.5';
                                                    $chkhalf1RR = '';
                                                   }
                                                   if($rountRatRR > 1.5 && $rountRatRR <=2){
                                                  
                                                   $valfull2RR = '2';
                                                   $chkfull2RR = 'checked';

                                                   }else{
                                                    $valfull2RR = '2';
                                                    $chkfull2RR = '';
                                                   }

                                                   if($rountRatRR > 2 && $rountRatRR <=2.5){
                                                  
                                                   $valhalf2RR = '2.5';
                                                   $chkhalf2RR = 'checked';

                                                   }else{
                                                    $valhalf2RR = '2.5';
                                                    $chkhalf2RR = '';
                                                   }

                                                   if($rountRatRR > 2.5 && $rountRatRR <=3){
                                                  
                                                   $valfull3RR = '3';
                                                   $chkfull3RR = 'checked';

                                                   }else{
                                                    $valfull3RR = '3';
                                                    $chkfull3RR = '';
                                                   }

                                                  if($rountRatRR > 3 && $rountRatRR <=3.5){
                                                  
                                                   $valhalf3RR = '3.5';
                                                   $chkhalf3RR = 'checked';

                                                   }else{
                                                    $valhalf3RR = '3.5';
                                                    $chkhalf3RR = '';
                                                   }
                                                   if($rountRatRR > 3.5 && $rountRatRR <=4){
                                                  
                                                   $valfull4RR = '4';
                                                   $chkfull4RR = 'checked';

                                                   }else{
                                                    $valfull4RR = '4';
                                                    $chkfull4RR = '';
                                                   }
                                                   
                                                   if($rountRatRR > 4 && $rountRatRR <=4.5){
                                                  
                                                   $valhalf4RR = '4.5';
                                                   $chkhalf4RR = 'checked';

                                                   }else{
                                                    $valhalf4RR = '4.5';
                                                    $chkhalf4RR = '';
                                                   }

                                                   if($rountRatRR > 4.5 && $rountRatRR <=5){
                                                  
                                                   $valfull5RR = '5';
                                                   $chkfull5RR = 'checked';

                                                   }else{
                                                    $valfull5RR = '5';
                                                    $chkfull5RR = '';
                                                   }
                                                   

                                                    ?>
                                                                <fieldset class="rating">
                                                                    <input type="radio" id="staraRR" name="ratingRR" value="<?=$valfull5RR?>" <?=$chkfull5RR?> disabled="disable" />
                                                                    <label class = "full" for="staraRR" title="Awesome - 5 stars"></label>

                                                                    <input type="radio" id="starbRR" name="ratingRR" value="<?=$valhalf4RR?>" <?=$chkhalf4RR?> disabled="disable" />
                                                                    <label class="half" for="starbRR" title="Pretty good - 4.5 stars"></label>

                                                                    <input type="radio" id="starcRR" name="ratingRR" value="<?=$valfull4RR?>" <?=$chkfull4RR?> disabled="disable"  />
                                                                    <label class = "full" for="starcRR" title="Pretty good - 4 stars"></label>

                                                                    <input type="radio" id="stardRR" name="ratingRR" value="<?=$valhalf3RR?>" <?=$chkhalf3RR?> disabled="disable" />
                                                                    <label class="half" for="stardRR" title="Meh - 3.5 stars"></label>

                                                                    <input type="radio" id="stareRR" name="ratingRR" value="<?=$valfull3RR?>" <?=$chkfull3RR?> disabled="disable"/>
                                                                    <label class = "full" for="stareRR" title="Meh - 3 stars"></label>

                                                                    <input type="radio" id="starfRR" name="ratingRR" value="<?=$valhalf2RR?>" <?=$chkhalf2RR?> disabled="disable" />
                                                                    <label class="half" for="starfRR" title="Kinda bad - 2.5 stars"></label>

                                                                    <input type="radio" id="stargRR" name="ratingRR" value="<?=$valfull2RR?>" <?=$chkfull2RR?> disabled="disable" />
                                                                    <label class = "full" for="stargRR" title="Kinda bad - 2 stars"></label>

                                                                    <input type="radio" id="starhRR" name="ratingRR" value="<?=$valhalf1RR?>" <?=$chkhalf1RR?> disabled="disable"  />
                                                                    <label class="half" for="starhRR" title="Meh - 1.5 stars"></label>

                                                                    <input type="radio" id="stariRR" name="ratingRR" value="<?=$valfull1RR?>" <?=$chkfull1RR?> disabled="disable" />
                                                                    <label class = "full" for="stariRR" title="Sucks big time - 1 star"></label>

                                                                    <input type="radio" id="starjRR" name="ratingRR" value="<?=$valhalf5RR?>" <?=$chkhalf5RR?> disabled="disable" />
                                                                    <label class="half" for="starjRR" title="Sucks big time - 0.5 stars"></label>
                                                                </fieldset>
                                                            </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="RateStarList">
                                    <div class="float-left txt clr-gray">
                                        Taste
                                    </div>
                                    <div class="float-left stars">
        <?php
                $rountRat = round($ratingarr['avrTaste']);
                //echo $rountRat;
                if($rountRat > 0 && $rountRat <=0.5){

                $valhalf5 = '0.5';
                $chkhalf5 = 'checked';

                }else{
                $valhalf5 = '0.5';
                $chkhalf5 = '';
                }

                if($rountRat > 0.5 && $rountRat <=1){

                $valfull1 = '1.0';
                $chkfull1 = 'checked';

                }else{
                $valfull1 = '1.0';
                $chkfull1 = '';
                }

                if($rountRat > 1 && $rountRat <=1.5){

                $valhalf1 = '1.5';
                $chkhalf1 = 'checked';

                }else{
                $valhalf1 = '1.5';
                $chkhalf1 = '';
                }
                if($rountRat > 1.5 && $rountRat <=2){

                $valfull2 = '2';
                $chkfull2 = 'checked';

                }else{
                $valfull2 = '2';
                $chkfull2 = '';
                }

                if($rountRat > 2 && $rountRat <=2.5){

                $valhalf2 = '2.5';
                $chkhalf2 = 'checked';

                }else{
                $valhalf2 = '2.5';
                $chkhalf2 = '';
                }

                if($rountRat > 2.5 && $rountRat <=3){

                $valfull3 = '3';
                $chkfull3 = 'checked';

                }else{
                $valfull3 = '3';
                $chkfull3 = '';
                }

                if($rountRat > 3 && $rountRat <=3.5){

                $valhalf3 = '3.5';
                $chkhalf3 = 'checked';

                }else{
                $valhalf3 = '3.5';
                $chkhalf3 = '';
                }
                if($rountRat > 3.5 && $rountRat <=4){

                $valfull4 = '4';
                $chkfull4 = 'checked';

                }else{
                $valfull4= '4';
                $chkfull4 = '';
                }

                if($rountRat > 4 && $rountRat <=4.5){

                $valhalf4 = '4.5';
                $chkhalf4 = 'checked';

                }else{
                $valhalf4= '4.5';
                $chkhalf4 = '';
                }

                if($rountRat > 4.5 && $rountRat <=5){

                $valfull5 = '5';
                $chkfull5 = 'checked';

                }else{
                $valfull5= '5';
                $chkfull5 = '';
                }
        ?>
                          <fieldset class="rating">
                            <input type="radio" id="stara" name="ratinga" value="<?=$valfull5?>" <?=$chkfull5?> disabled="disable" />
                            <label class = "full" for="stara" title="Awesome - 5 stars"></label>

                            <input type="radio" id="starb" name="ratingb" value="<?=$valhalf4?>" <?=$chkhalf4?> disabled="disable" />
                            <label class="half" for="starb" title="Pretty good - 4.5 stars"></label>

                            <input type="radio" id="starc" name="ratingc" value="<?=$valfull4?>" <?=$chkfull4?> disabled="disable"  />
                            <label class = "full" for="starc" title="Pretty good - 4 stars"></label>

                            <input type="radio" id="stard" name="ratingd" value="<?=$valhalf3?>" <?=$chkhalf3?> disabled="disable" />
                            <label class="half" for="stard" title="Meh - 3.5 stars"></label>

                            <input type="radio" id="stare" name="ratinge" value="<?=$valfull3?>" <?=$chkfull3?> disabled="disable"/>
                            <label class = "full" for="stare" title="Meh - 3 stars"></label>

                            <input type="radio" id="starf" name="ratingf" value="<?=$valhalf2?>" <?=$chkhalf2?> disabled="disable" />
                            <label class="half" for="starf" title="Kinda bad - 2.5 stars"></label>

                            <input type="radio" id="starg" name="ratingg" value="<?=$valfull2?>" <?=$chkfull2?> disabled="disable" />
                            <label class = "full" for="starg" title="Kinda bad - 2 stars"></label>

                            <input type="radio" id="starh" name="ratingh" value="<?=$valhalf1?>" <?=$chkhalf1?> disabled="disable"  />
                            <label class="half" for="starh" title="Meh - 1.5 stars"></label>

                            <input type="radio" id="stari" name="ratingi" value="<?=$valfull1?>" <?=$chkfull1?> disabled="disable" />
                            <label class = "full" for="stari" title="Sucks big time - 1 star"></label>

                            <input type="radio" id="starj" name="ratingj" value="<?=$valhalf5?>" <?=$chkhalf5?> disabled="disable" />
                            <label class="half" for="starj" title="Sucks big time - 0.5 stars"></label>
                        </fieldset>
                                    </div>
                                </div>
                                <div class="RateStarList">
                                    <div class="float-left txt clr-gray">
                                        Environment
                                    </div>
        <div class="float-left stars">
<?php
                $rountRatEnv = round($ratingarr['avr_environment']);
                //echo $rountRat;
                if($rountRatEnv > 0 && $rountRatEnv <=0.5){

                $valhalf5Env = '0.5';
                $chkhalf5Env = 'checked';

                }else{
                $valhalf5Env = '0.5';
                $chkhalf5Env = '';
                }

                if($rountRatEnv > 0.5 && $rountRatEnv <=1){

                $valfull1Env = '1.0';
                $chkfull1Env = 'checked';

                }else{
                $valfull1Env = '1.0';
                $chkfull1Env = '';
                }

                if($rountRatEnv > 1 && $rountRatEnv <=1.5){

                $valhalf1Env = '1.5';
                $chkhalf1Env = 'checked';

                }else{
                $valhalf1Env = '1.5';
                $chkhalf1Env = '';
                }
                if($rountRatEnv > 1.5 && $rountRatEnv <=2){

                $valfull2Env = '2';
                $chkfull2Env = 'checked';

                }else{
                $valfull2Env = '2';
                $chkfull2Env = '';
                }

                if($rountRatEnv > 2 && $rountRatEnv <=2.5){

                $valhalf2Env = '2.5';
                $chkhalf2Env = 'checked';

                }else{
                $valhalf2Env = '2.5';
                $chkhalf2Env = '';
                }

                if($rountRatEnv > 2.5 && $rountRatEnv <=3){

                $valfull3Env = '3';
                $chkfull3Env = 'checked';

                }else{
                $valfull3Env = '3';
                $chkfull3Env = '';
                }

                if($rountRatEnv > 3 && $rountRatEnv <=3.5){

                $valhalf3Env = '3.5';
                $chkhalf3Env = 'checked';

                }else{
                $valhalf3Env = '3.5';
                $chkhalf3Env = '';
                }
                if($rountRatEnv > 3.5 && $rountRatEnv <=4){

                $valfull4Env = '4';
                $chkfull4Env = 'checked';

                }else{
                $valfull4Env = '4';
                $chkfull4Env = '';
                }

                if($rountRatEnv > 4 && $rountRatEnv <=4.5){

                $valhalf4Env = '4.5';
                $chkhalf4Env = 'checked';

                }else{
                $valhalf4Env = '4.5';
                $chkhalf4Env = '';
                }

                if($rountRatEnv > 4.5 && $rountRatEnv <=5){

                $valfull5Env = '5';
                $chkfull5Env = 'checked';

                }else{
                $valfull5Env = '5';
                $chkfull5Env = '';
                }
        ?>
                        <fieldset class="rating">
                            <input type="radio" id="staraenv" name="ratingaenv" value="<?=$valfull5Env?>" <?=$chkfull5Env?> disabled="disable" />
                            <label class = "full" for="staraenv" title="Awesome - 5 stars"></label>

                            <input type="radio" id="starbenv" name="ratingbenv" value="<?=$valhalf4Env?>" <?=$chkhalf4Env?> disabled="disable" />
                            <label class="half" for="starbenv" title="Pretty good - 4.5 stars"></label>

                            <input type="radio" id="starcenv" name="ratingcenv" value="<?=$valfull4Env?>" <?=$chkfull4Env?> disabled="disable"  />
                            <label class = "full" for="starcenv" title="Pretty good - 4 stars"></label>

                            <input type="radio" id="stardenv" name="ratingdenv" value="<?=$valhalf3Env?>" <?=$chkhalf3Env?> disabled="disable" />
                            <label class="half" for="stardenv" title="Meh - 3.5 stars"></label>

                            <input type="radio" id="stareenv" name="ratingeenv" value="<?=$valfull3Env?>" <?=$chkfull3Env?> disabled="disable"/>
                            <label class = "full" for="stareenv" title="Meh - 3 stars"></label>

                            <input type="radio" id="starfenv" name="ratingfenv" value="<?=$valhalf2Env?>" <?=$chkhalf2Env?> disabled="disable" />
                            <label class="half" for="starfenv" title="Kinda bad - 2.5 stars"></label>

                            <input type="radio" id="stargenv" name="ratinggenv" value="<?=$valfull2Env?>" <?=$chkfull2Env?> disabled="disable" />
                            <label class = "full" for="stargenv" title="Kinda bad - 2 stars"></label>

                            <input type="radio" id="starhenv" name="ratinghenv" value="<?=$valhalf1Env?>" <?=$chkhalf1Env?> disabled="disable"  />
                            <label class="half" for="starhenv" title="Meh - 1.5 stars"></label>

                            <input type="radio" id="starienv" name="ratingienv" value="<?=$valfull1Env?>" <?=$chkfull1Env?> disabled="disable" />
                            <label class = "full" for="starienv" title="Sucks big time - 1 star"></label>

                            <input type="radio" id="starjenv" name="ratingjenv" value="<?=$valhalf5Env?>" <?=$chkhalf5Env?> disabled="disable" />
                            <label class="half" for="starjenv" title="Sucks big time - 0.5 stars"></label>
                        </fieldset>
                                    </div>
                                </div>
                                <div class="RateStarList">
                                    <div class="float-left txt clr-gray">
                                        Service Quality
                                    </div>
                                    <div class="float-left stars">
                                        <?php
                $rountRatSer = round($ratingarr['avr_service_quality']);
                //echo $rountRat;
                if($rountRatSer > 0 && $rountRatSer <=0.5){

                $valhalf5Ser = '0.5';
                $chkhalf5Ser = 'checked';

                }else{
                $valhalf5Ser = '0.5';
                $chkhalf5Ser = '';
                }

                if($rountRatSer > 0.5 && $rountRatSer <=1){

                $valfull1Ser = '1.0';
                $chkfull1Ser = 'checked';

                }else{
                $valfull1Ser = '1.0';
                $chkfull1Ser = '';
                }

                if($rountRatSer > 1 && $rountRatSer <=1.5){

                $valhalf1Ser = '1.5';
                $chkhalf1Ser = 'checked';

                }else{
                $valhalf1Ser = '1.5';
                $chkhalf1Ser = '';
                }
                if($rountRatSer > 1.5 && $rountRatSer <=2){

                $valfull2Ser = '2';
                $chkfull2Ser = 'checked';

                }else{
                $valfull2Ser = '2';
                $chkfull2Ser = '';
                }

                if($rountRatSer > 2 && $rountRatSer <=2.5){

                $valhalf2Ser = '2.5';
                $chkhalf2Ser = 'checked';

                }else{
                $valhalf2Ser = '2.5';
                $chkhalf2Ser = '';
                }

                if($rountRatSer > 2.5 && $rountRatSer <=3){

                $valfull3Ser = '3';
                $chkfull3Ser = 'checked';

                }else{
                $valfull3Ser = '3';
                $chkfull3Ser = '';
                }

                if($rountRatSer > 3 && $rountRatSer <=3.5){

                $valhalf3Ser = '3.5';
                $chkhalf3Ser = 'checked';

                }else{
                $valhalf3Ser = '3.5';
                $chkhalf3Ser = '';
                }
                if($rountRatSer > 3.5 && $rountRatSer <=4){

                $valfull4Ser = '4';
                $chkfull4Ser = 'checked';

                }else{
                $valfull4Ser = '4';
                $chkfull4Ser = '';
                }

                if($rountRatSer > 4 && $rountRatSer <=4.5){

                $valhalf4Ser = '4.5';
                $chkhalf4Ser = 'checked';

                }else{
                $valhalf4Ser = '4.5';
                $chkhalf4Ser = '';
                }

                if($rountRatSer > 4.5 && $rountRatSer <=5){

                $valfull5Ser = '5';
                $chkfull5Ser = 'checked';

                }else{
                $valfull5Ser = '5';
                $chkfull5Ser = '';
                }
        ?>
                          <fieldset class="rating">
                            <input type="radio" id="staraser" name="ratingaser" value="<?=$valfull5Ser?>" <?=$chkfull5Ser?> disabled="disable" />
                            <label class = "full" for="staraser" title="Awesome - 5 stars"></label>

                            <input type="radio" id="starbser" name="ratingbser" value="<?=$valhalf4Ser?>" <?=$chkhalf4Ser?> disabled="disable" />
                            <label class="half" for="starbser" title="Pretty good - 4.5 stars"></label>

                            <input type="radio" id="starcser" name="ratingcser" value="<?=$valfull4Ser?>" <?=$chkfull4Ser?> disabled="disable"  />
                            <label class = "full" for="starcser" title="Pretty good - 4 stars"></label>

                            <input type="radio" id="stardser" name="ratingdser" value="<?=$valhalf3Ser?>" <?=$chkhalf3Ser?> disabled="disable" />
                            <label class="half" for="stardser" title="Meh - 3.5 stars"></label>

                            <input type="radio" id="stareser" name="ratingeser" value="<?=$valfull3Ser?>" <?=$chkfull3Ser?> disabled="disable"/>
                            <label class = "full" for="stareser" title="Meh - 3 stars"></label>

                            <input type="radio" id="starfser" name="ratingfser" value="<?=$valhalf2Ser?>" <?=$chkhalf2Ser?> disabled="disable" />
                            <label class="half" for="starfser" title="Kinda bad - 2.5 stars"></label>

                            <input type="radio" id="stargser" name="ratinggser" value="<?=$valfull2Ser?>" <?=$chkfull2Ser?> disabled="disable" />
                            <label class = "full" for="stargser" title="Kinda bad - 2 stars"></label>

                            <input type="radio" id="starhser" name="ratinghser" value="<?=$valhalf1Ser?>" <?=$chkhalf1Ser?> disabled="disable"  />
                            <label class="half" for="starhser" title="Meh - 1.5 stars"></label>

                            <input type="radio" id="stariser" name="ratingiser" value="<?=$valfull1Ser?>" <?=$chkfull1Ser?> disabled="disable" />
                            <label class = "full" for="stariser" title="Sucks big time - 1 star"></label>

                            <input type="radio" id="starjser" name="ratingjser" value="<?=$valhalf5Ser?>" <?=$chkhalf5Ser?> disabled="disable" />
                            <label class="half" for="starjser" title="Sucks big time - 0.5 stars"></label>
                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="RateStarList">
                                    <div class="float-left txt clr-gray">
                                        Cleanliness
                                    </div>
                                    <div class="float-left stars">
                                       <?php
                $rountRatCln = round($ratingarr['avr_cleanliness']);
                //echo $rountRat;
                if($rountRatCln > 0 && $rountRatCln <=0.5){

                $valhalf5Cln = '0.5';
                $chkhalf5Cln = 'checked';

                }else{
                $valhalf5Cln = '0.5';
                $chkhalf5Cln = '';
                }

                if($rountRatCln > 0.5 && $rountRatCln <=1){

                $valfull1Cln = '1.0';
                $chkfull1Cln = 'checked';

                }else{
                $valfull1Cln = '1.0';
                $chkfull1Cln = '';
                }

                if($rountRatCln > 1 && $rountRatCln <=1.5){

                $valhalf1Cln = '1.5';
                $chkhalf1Cln = 'checked';

                }else{
                $valhalf1Cln = '1.5';
                $chkhalf1Cln = '';
                }
                if($rountRatCln > 1.5 && $rountRatCln <=2){

                $valfull2Cln = '2';
                $chkfull2Cln = 'checked';

                }else{
                $valfull2Cln = '2';
                $chkfull2Cln = '';
                }

                if($rountRatCln > 2 && $rountRatCln <=2.5){

                $valhalf2Cln = '2.5';
                $chkhalf2Cln = 'checked';

                }else{
                $valhalf2Cln = '2.5';
                $chkhalf2Cln = '';
                }

                if($rountRatCln > 2.5 && $rountRatCln <=3){

                $valfull3Cln = '3';
                $chkfull3Cln = 'checked';

                }else{
                $valfull3Cln = '3';
                $chkfull3Cln = '';
                }

                if($rountRatCln > 3 && $rountRatCln <=3.5){

                $valhalf3Cln = '3.5';
                $chkhalf3Cln = 'checked';

                }else{
                $valhalf3Cln = '3.5';
                $chkhalf3Cln = '';
                }
                if($rountRatCln > 3.5 && $rountRatCln <=4){

                $valfull4Cln = '4';
                $chkfull4Cln = 'checked';

                }else{
                $valfull4Cln = '4';
                $chkfull4Cln = '';
                }

                if($rountRatCln > 4 && $rountRatCln <=4.5){

                $valhalf4Cln = '4.5';
                $chkhalf4Cln = 'checked';

                }else{
                $valhalf4Cln = '4.5';
                $chkhalf4Cln = '';
                }

                if($rountRatCln > 4.5 && $rountRatCln <=5){

                $valfull5Cln = '5';
                $chkfull5Cln = 'checked';

                }else{
                $valfull5Cln = '5';
                $chkfull5Cln = '';
                }
        ?>
                          <fieldset class="rating">
                            <input type="radio" id="staracln" name="ratingacln" value="<?=$valfull5Cln?>" <?=$chkfull5Cln?> disabled="disable" />
                            <label class = "full" for="staracln" title="Awesome - 5 stars"></label>

                            <input type="radio" id="starbcln" name="ratingbcln" value="<?=$valhalf4Cln?>" <?=$chkhalf4Cln?> disabled="disable" />
                            <label class="half" for="starbcln" title="Pretty good - 4.5 stars"></label>

                            <input type="radio" id="starccln" name="ratingccln" value="<?=$valfull4Cln?>" <?=$chkfull4Cln?> disabled="disable"  />
                            <label class = "full" for="starccln" title="Pretty good - 4 stars"></label>

                            <input type="radio" id="stardcln" name="ratingdcln" value="<?=$valhalf3Cln?>" <?=$chkhalf3Cln?> disabled="disable" />
                            <label class="half" for="stardcln" title="Meh - 3.5 stars"></label>

                            <input type="radio" id="starecln" name="ratingecln" value="<?=$valfull3Cln?>" <?=$chkfull3Cln?> disabled="disable"/>
                            <label class = "full" for="starecln" title="Meh - 3 stars"></label>

                            <input type="radio" id="starfcln" name="ratingfcln" value="<?=$valhalf2Cln?>" <?=$chkhalf2Cln?> disabled="disable" />
                            <label class="half" for="starfcln" title="Kinda bad - 2.5 stars"></label>

                            <input type="radio" id="stargcln" name="ratinggcln" value="<?=$valfull2Cln?>" <?=$chkfull2Cln?> disabled="disable" />
                            <label class = "full" for="stargcln" title="Kinda bad - 2 stars"></label>

                            <input type="radio" id="starhcln" name="ratinghcln" value="<?=$valhalf1Cln?>" <?=$chkhalf1Cln?> disabled="disable"  />
                            <label class="half" for="starhcln" title="Meh - 1.5 stars"></label>

                            <input type="radio" id="staricln" name="ratingicln" value="<?=$valfull1Cln?>" <?=$chkfull1Cln?> disabled="disable" />
                            <label class = "full" for="staricln" title="Sucks big time - 1 star"></label>

                            <input type="radio" id="starjcln" name="ratingjcln" value="<?=$valhalf5Cln?>" <?=$chkhalf5Cln?> disabled="disable" />
                            <label class="half" for="starjcln" title="Sucks big time - 0.5 stars"></label>
                        </fieldset>
                                    </div>
                                </div>
                                <div class="RateStarList">
                                    <div class="float-left txt clr-gray">
                                        Price
                                    </div>
                                    <div class="float-left stars">
                                          <?php
                $rountRatPri = round($ratingarr['avr_price']);
                //echo $rountRat;
                if($rountRatPri > 0 && $rountRatPri <=0.5){

                $valhalf5Pri = '0.5';
                $chkhalf5Pri = 'checked';

                }else{
                $valhalf5Pri = '0.5';
                $chkhalf5Pri = '';
                }

                if($rountRatPri > 0.5 && $rountRatPri <=1){

                $valfull1Pri = '1.0';
                $chkfull1Pri = 'checked';

                }else{
                $valfull1Pri = '1.0';
                $chkfull1Pri = '';
                }

                if($rountRatPri > 1 && $rountRatPri <=1.5){

                $valhalf1Pri = '1.5';
                $chkhalf1Pri = 'checked';

                }else{
                $valhalf1Pri = '1.5';
                $chkhalf1Pri = '';
                }
                if($rountRatPri > 1.5 && $rountRatPri <=2){

                $valfull2Pri = '2';
                $chkfull2Pri = 'checked';

                }else{
                $valfull2Pri = '2';
                $chkfull2Pri = '';
                }

                if($rountRatPri > 2 && $rountRatPri <=2.5){

                $valhalf2Pri = '2.5';
                $chkhalf2Pri = 'checked';

                }else{
                $valhalf2Pri = '2.5';
                $chkhalf2Pri = '';
                }

                if($rountRatPri > 2.5 && $rountRatPri <=3){

                $valfull3Pri = '3';
                $chkfull3Pri = 'checked';

                }else{
                $valfull3Pri = '3';
                $chkfull3Pri = '';
                }

                if($rountRatPri > 3 && $rountRatPri <=3.5){

                $valhalf3Pri = '3.5';
                $chkhalf3Pri = 'checked';

                }else{
                $valhalf3Pri = '3.5';
                $chkhalf3Pri = '';
                }
                if($rountRatPri > 3.5 && $rountRatPri <=4){

                $valfull4Pri = '4';
                $chkfull4Pri = 'checked';

                }else{
                $valfull4Pri = '4';
                $chkfull4Pri = '';
                }

                if($rountRatPri > 4 && $rountRatPri <=4.5){

                $valhalf4Pri = '4.5';
                $chkhalf4Pri = 'checked';

                }else{
                $valhalf4Pri = '4.5';
                $chkhalf4Pri = '';
                }

                if($rountRatPri > 4.5 && $rountRatPri <=5){

                $valfull5Pri = '5';
                $chkfull5Pri = 'checked';

                }else{
                $valfull5Pri = '5';
                $chkfull5Pri = '';
                }
        ?>
                          <fieldset class="rating">
                            <input type="radio" id="staraPri" name="ratingaPri" value="<?=$valfull5Pri?>" <?=$chkfull5Pri?> disabled="disable" />
                            <label class = "full" for="staraPri" title="Awesome - 5 stars"></label>

                            <input type="radio" id="starbPri" name="ratingbPri" value="<?=$valhalf4Pri?>" <?=$chkhalf4Pri?> disabled="disable" />
                            <label class="half" for="starbPri" title="Pretty good - 4.5 stars"></label>

                            <input type="radio" id="starcPri" name="ratingcPri" value="<?=$valfull4Pri?>" <?=$chkfull4Pri?> disabled="disable"  />
                            <label class = "full" for="starcPri" title="Pretty good - 4 stars"></label>

                            <input type="radio" id="stardPri" name="ratingdPri" value="<?=$valhalf3Pri?>" <?=$chkhalf3Pri?> disabled="disable" />
                            <label class="half" for="stardPri" title="Meh - 3.5 stars"></label>

                            <input type="radio" id="starePri" name="ratingePri" value="<?=$valfull3Pri?>" <?=$chkfull3Pri?> disabled="disable"/>
                            <label class = "full" for="starePri" title="Meh - 3 stars"></label>

                            <input type="radio" id="starfPri" name="ratingfPri" value="<?=$valhalf2Pri?>" <?=$chkhalf2Pri?> disabled="disable" />
                            <label class="half" for="starfPri" title="Kinda bad - 2.5 stars"></label>

                            <input type="radio" id="stargPri" name="ratinggPri" value="<?=$valfull2Pri?>" <?=$chkfull2Pri?> disabled="disable" />
                            <label class = "full" for="stargPri" title="Kinda bad - 2 stars"></label>

                            <input type="radio" id="starhPri" name="ratinghPri" value="<?=$valhalf1Pri?>" <?=$chkhalf1Pri?> disabled="disable"  />
                            <label class="half" for="starhPri" title="Meh - 1.5 stars"></label>

                            <input type="radio" id="stariPri" name="ratingiPri" value="<?=$valfull1Pri?>" <?=$chkfull1Pri?> disabled="disable" />
                            <label class = "full" for="stariPri" title="Sucks big time - 1 star"></label>

                            <input type="radio" id="starjPri" name="ratingjPri" value="<?=$valhalf5Pri?>" <?=$chkhalf5Pri?> disabled="disable" />
                            <label class="half" for="starjPri" title="Sucks big time - 0.5 stars"></label>
                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="reviewsBox">
                                    <div class="starBox">
                                        <span class="float-right">
                                            <span class="rateScore">
                                             <?php 
                                                    if(is_float($ratingarr['avr_review'])){
                                                    $avr_review = $ratingarr['avr_review'];

                                                    }else{
                                                    $avr_review  = number_format((float)$ratingarr['avr_review'], 1, '.', '');
                                                    }
                                                    ?>
                                            <?=$avr_review;?>
                                            </span>
                                            <p> <?=$ratingarr['totalreview'];?> Reviews</p>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="commentBox">
                                <?php if(!empty($getRRlist)){
                                    foreach ($getRRlist as $key => $value) {
                                        if($value->profile_pic !=''){
                                            $proimg = $value->profile_pic;
                                        }else{
                                            $proimg = base_url().'front/images/blank.jpg';
                                        }

                                        if ($value->avr_rating <= '2') {
                                            $emoji = base_url().'front/images/bad.png';
                                        } elseif ($value->avr_rating < '4') {
                                             $emoji = base_url().'front/images/avg.png';
                                        } elseif ($value->avr_rating >= '4') {
                                             $emoji = base_url().'front/images/yummy-large.png';
                                        }
                                       if(is_float($value->avr_rating)){
                                        $avr_rating = $value->avr_rating;

                                        }else{
                                           $avr_rating  = number_format((float)$value->avr_rating, 1, '.', '');
                                        }
                                        //echo number_format((float)$value->avr_rating, 1, '.', '');
                                      
                                    ?>
                                    <div class="float-left">
                                        <div class="chfName">
                                            <span class="float-left img">
                                                <img src="<?=$proimg;?>" alt="No image">
                                            </span>
                                            <span class="Detail clr-gray float-left">
                                                <strong class="clr-red"><?=$value->first_name; ?></strong><br>
                                                <?=$value->newdate; ?>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="float-right">
                                        <div class="reviewsBox">
                                            <div class="starBox">
                                                <span class="float-right">
                                                    <img src="<?php echo $emoji;?>" alt="">
                                                </span>
                                                <span class="float-right">
                                                    <span class="rateScore">

                                                    <?=$avr_rating; ?>
                                                    </span>
                                                </span>
                                                <span class="float-right reviewStars">
                                                     <?php
                $rountRatRev = round($value->avr_rating);
                //echo $rountRat;
                if($rountRatRev > 0 && $rountRatRev <=0.5){

                $valhalf5Rev = '0.5';
                $chkhalf5Rev = 'checked';

                }else{
                $valhalf5Rev = '0.5';
                $chkhalf5Rev = '';
                }

                if($rountRatRev > 0.5 && $rountRatRev <=1){

                $valfull1Rev = '1.0';
                $chkfull1Rev = 'checked';

                }else{
                $valfull1Rev = '1.0';
                $chkfull1Rev = '';
                }

                if($rountRatRev > 1 && $rountRatRev <=1.5){

                $valhalf1Rev = '1.5';
                $chkhalf1Rev = 'checked';

                }else{
                $valhalf1Rev = '1.5';
                $chkhalf1Rev = '';
                }
                if($rountRatRev > 1.5 && $rountRatRev <=2){

                $valfull2Rev = '2';
                $chkfull2Rev = 'checked';

                }else{
                $valfull2Rev = '2';
                $chkfull2Rev = '';
                }

                if($rountRatRev > 2 && $rountRatRev <=2.5){

                $valhalf2Rev = '2.5';
                $chkhalf2Rev = 'checked';

                }else{
                $valhalf2Rev = '2.5';
                $chkhalf2Rev = '';
                }

                if($rountRatRev > 2.5 && $rountRatRev <=3){

                $valfull3Rev = '3';
                $chkfull3Rev = 'checked';

                }else{
                $valfull3Rev = '3';
                $chkfull3Rev = '';
                }

                if($rountRatRev > 3 && $rountRatRev <=3.5){

                $valhalf3Rev = '3.5';
                $chkhalf3Rev = 'checked';

                }else{
                $valhalf3Rev = '3.5';
                $chkhalf3Rev = '';
                }
                if($rountRatRev > 3.5 && $rountRatRev <=4){

                $valfull4Rev = '4';
                $chkfull4Rev = 'checked';

                }else{
                $valfull4Rev = '4';
                $chkfull4Rev = '';
                }

                if($rountRatRev > 4 && $rountRatRev <=4.5){

                $valhalf4Rev = '4.5';
                $chkhalf4Rev = 'checked';

                }else{
                $valhalf4Rev = '4.5';
                $chkhalf4Rev= '';
                }

                if($rountRatRev > 4.5 && $rountRatRev <=5){

                $valfull5Rev = '5';
                $chkfull5Rev = 'checked';

                }else{
                $valfull5Rev = '5';
                $chkfull5Rev = '';
                }
        ?>
                          <fieldset class="rating">
                            <input type="radio" id="stara<?=$value->rr_id;?>" name="ratinga<?=$value->rr_id;?>" value="<?=$valfull5Rev?>" <?=$chkfull5Rev?> disabled="disable" />
                            <label class = "full" for="stara<?=$value->rr_id;?>" title="Awesome - 5 stars"></label>

                            <input type="radio" id="starb<?=$value->rr_id;?>" name="ratingb<?=$value->rr_id;?>" value="<?=$valhalf4Rev?>" <?=$chkhalf4Rev?> disabled="disable" />
                            <label class="half" for="starb<?=$value->rr_id;?>" title="Pretty good - 4.5 stars"></label>

                            <input type="radio" id="starc<?=$value->rr_id;?>" name="ratingc<?=$value->rr_id;?>" value="<?=$valfull4Rev?>" <?=$chkfull4Rev?> disabled="disable"  />
                            <label class = "full" for="starc<?=$value->rr_id;?>" title="Pretty good - 4 stars"></label>

                            <input type="radio" id="stard<?=$value->rr_id;?>" name="ratingd<?=$value->rr_id;?>" value="<?=$valhalf3Rev?>" <?=$chkhalf3Rev?> disabled="disable" />
                            <label class="half" for="stard<?=$value->rr_id;?>" title="Meh - 3.5 stars"></label>

                            <input type="radio" id="stare<?=$value->rr_id;?>" name="ratinge<?=$value->rr_id;?>" value="<?=$valfull3Rev?>" <?=$chkfull3Rev?> disabled="disable"/>
                            <label class = "full" for="stare<?=$value->rr_id;?>" title="Meh - 3 stars"></label>

                            <input type="radio" id="starf<?=$value->rr_id;?>" name="ratingf<?=$value->rr_id;?>" value="<?=$valhalf2Rev?>" <?=$chkhalf2Rev?> disabled="disable" />
                            <label class="half" for="starf<?=$value->rr_id;?>" title="Kinda bad - 2.5 stars"></label>

                            <input type="radio" id="starg<?=$value->rr_id;?>" name="ratingg<?=$value->rr_id;?>" value="<?=$valfull2Rev?>" <?=$chkfull2Rev?> disabled="disable" />
                            <label class = "full" for="starg<?=$value->rr_id;?>" title="Kinda bad - 2 stars"></label>

                            <input type="radio" id="starh<?=$value->rr_id;?>" name="ratingh<?=$value->rr_id;?>" value="<?=$valhalf1Rev?>" <?=$chkhalf1Rev?> disabled="disable"  />
                            <label class="half" for="starh<?=$value->rr_id;?>" title="Meh - 1.5 stars"></label>

                            <input type="radio" id="stari<?=$value->rr_id;?>" name="ratingi<?=$value->rr_id;?>" value="<?=$valfull1Rev?>" <?=$chkfull1Rev?> disabled="disable" />
                            <label class = "full" for="stari<?=$value->rr_id;?>" title="Sucks big time - 1 star"></label>

                            <input type="radio" id="starj<?=$value->rr_id;?>" name="ratingj<?=$value->rr_id;?>" value="<?=$valhalf5Rev?>" <?=$chkhalf5Rev?> disabled="disable" />
                            <label class="half" for="starj<?=$value->rr_id;?>" title="Sucks big time - 0.5 stars"></label>
                        </fieldset>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="commentPra">
                                        <p><?=$value->review; ?></p>
                                    </div>
                                    <div class="cmntImages">
                                    <?php
                                        if(!empty($value->newimages)){
                                            for($i=0;$i<count($value->newimages);$i++){
                                    ?>
                                        <span>
                                            <img src="<?php echo $value->newimages[$i]?>" alt="">
                                        </span>
                                        <?php

                                    }
                                    }
                                        ?>
                                       
                                    </div>
                                    <div class="cmntLikes">
                                        <div class="likes">
                                        <?php
                                        if($this->session->userdata('user_id') !=''){
                                            if($value->likeKey != 'no'){
                                                $img ='like';
                                            }else{
                                                $img ='dislike';
                                            }
                                        ?>
                                           
                                            <button class="float-left likeImg <?=$img?>" onclick="likeDislike('<?=$value->likeKey?>',<?=$value->rr_id?>)">Like</button>
                                            <span class="float-left nmbrs">
                                               <?= $value->likecount;?>
                                            </span>

                                            <?php }else{ ?>
                                             <button class="float-left likeImg dislike"><a style="color:#747684;" href="<?php echo base_url() ?>welcome/likeDislike">Like</a></button>
                                            <span class="float-left nmbrs">
                                               <?= $value->likecount;?>
                                            </span>
                                           
                                            <?php } ?>
                                        </div>
                                    </div>

                                    
                                    <?php } 
                                    } ?>
                                </div>
                               
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="enquiryCalenderBlock">
                    <div class="topImage">
                        <img src="<?php echo base_url();?>front/images/fish.png" alt="">
                    </div>
                    <p>Click on a date to choose the meal date</p>
                    <div class="calenderBlock">
                        <div class="clndrAvlblty text-center">
                            Availability Calender
                        </div>
                        <div id="date"></div>
                    </div>
                    <div class="seatAvl">
                        2 Seats are available
                    </div>
                    <div class="ChfClndrFrm">
                        <form>
                            <input type="text" class="input" placeholder="1">
                            <select class="input">
                                <option value="">Trendy Tradition</option>
                                <option value="">From Tandoor</option>
                                <option value="">South Indian</option>
                            </select>
                            <select id="foodCat" class="input">
                                <option value="Home private meal">Home private meal</option>
                                <option value="Food Delivery">Food Delivery</option>
                                <option value="Restaurant private meal">Restaurant private meal</option>
                                <option value="Chef onsite cooking">Chef onsite cooking</option>
                                <option value="Cooking workshop">Cooking workshop</option>
                                <option class="Daily meal">Daily meal</option>
                            </select>
                            <select id="foodDlvryopt" class="input">
                                <option value="Pick up from chef">Pick up from chef</option>
                                <option value="Deliver to your home">Deliver to your home</option>
                            </select>
                            <a href="javascript:;" class="inputBtn redbg txt-upr text-center clr-white deliveryAddress">Book Now</a>
                            <a href="javascript:;" class="inputBtn blkbg text-center txt-upr clr-red makEnquiry">Make Enquiry</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade menuModel" id="MenuModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="form-group untensilbox">
                    <p>
                        Please kindly let chef know what tools you can provide at your place.
                    </p>
                    <ul class="serviceBlck menuService utensilList"> 
                        <li>
                            <input class="radio" type="checkbox" id="potatoMasher">
                            <label class="radiobLable" for="potatoMasher"> Potato Masher</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="ovenMitt">
                            <label class="radiobLable" for="ovenMitt">Oven Mitt/Glove</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="chefKnife">
                            <label class="radiobLable" for="chefKnife">Chef Knife</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="parkingKnife">
                            <label class="radiobLable" for="parkingKnife">Parking Knife</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="measuringCups">
                            <label class="radiobLable" for="measuringCups"> Measuring Cups/Jug & Spoons</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="serrated">
                            <label class="radiobLable" for="serrated">Serrated/Bread Knife</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="sharpening">
                            <label class="radiobLable" for="sharpening">Sharpening Steel</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="vegetablepeelers">
                            <label class="radiobLable" for="vegetablepeelers">Vegetable Peelers</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="garlicpress">
                            <label class="radiobLable" for="garlicpress">Garlic Press</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="bbqtools">
                            <label class="radiobLable" for="bbqtools">BBQ Tools</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="electricsharpener">
                            <label class="radiobLable" for="electricsharpener">Electric Sharpener</label>
                        </li>
                        <li>
                            <input class="radio" type="checkbox" id="cuttingboard">
                            <label class="radiobLable" for="cuttingboard">Cutting Board</label>
                        </li>
                    </ul>
                    <button class="inputBtn redbg txt-upr clr-white">Continue</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade enquiryModel" id="enquiryModel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="enquiryForm">
                    <div class="frmttl">
                        Make Equiry
                    </div>
                    <form>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="formgroup">
                                    <label>Date</label>
                                    <input type="text" name="date">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="formgroup">
                                    <label>Time</label>
                                    <select>
                                        <option>00:00</option>
                                        <option>01:00</option>
                                        <option>02:00</option>
                                        <option>03:00</option>
                                        <option>04:00</option>
                                        <option>05:00</option>
                                        <option>06:00</option>
                                        <option>07:00</option>
                                        <option>08:00</option>
                                        <option>08:00</option>
                                        <option>10:00</option>
                                        <option>01:00</option>
                                        <option>11:00</option>
                                        <option>12:00</option>
                                        <option>13:00</option>
                                        <option>14:00</option>
                                        <option>15:00</option>
                                        <option>16:00</option>
                                        <option>17:00</option>
                                        <option>18:00</option>
                                        <option>19:00</option>
                                        <option>20:00</option>
                                        <option>21:00</option>
                                        <option>22:00</option>
                                        <option>23:00</option>
                                        <option>24:00</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="formgroup">
                                    <label>Seats</label>
                                    <input type="text" name="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="formgroup">
                                    <label>Menu</label>
                                    <select>
                                        <option>Trendy Tradition</option>
                                        <option>Spicy & Salty</option>
                                        <option>From Tandoor</option>
                                    </select>
                                </div>
                                <div class="formgroup">
                                    <label>Service Type</label>
                                    <select>
                                        <option value="Home private meal">Home private meal</option>
                                        <option value="Food Delivery">Food Delivery</option>
                                        <option value="Restaurant private meal">Restaurant private meal</option>
                                        <option value="Chef onsite cooking">Chef onsite cooking</option>
                                        <option value="Cooking workshop">Cooking workshop</option>
                                        <option class="Daily meal">Daily meal</option>
                                    </select>
                                </div>
                                <div class="formgroup">
                                    <label>Message</label>
                                    <textarea rows="5" placeholder="Please tell about your plan and let us know who will join the event. Don't forget to mention if you any restrictions"></textarea>
                                </div>
                            </div>
                        </div>
                        <p>
                            You can take your time to discuss your plan with chef. Once you agree the service, you will be asked to confirm the booking with the payment afterwards.
                        </p>
                        <button class="inputBtn redbg txt-upr clr-white">Continue</button>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade deliveryModel" id="deliveryModel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="enquiryForm">
                    <div class="frmttl">
                        Food Delivery
                    </div>
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="formgroup">
                                    <label>First Name</label>
                                    <input type="text" placeholder="Enter your First Name">
                                </div>
                                <div class="formgroup">
                                    <label>Last Name</label>
                                    <input type="text" placeholder="Enter your Last Name">
                                </div>
                                <div class="formgroup">
                                    <label>Email</label>
                                    <input type="text" placeholder="Enter your Email Address">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="formgroup">
                                    <label>Country Code</label>
                                    <input type="text" placeholder="Country Code">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="formgroup">
                                    <label>Contact No.</label>
                                    <input type="number" placeholder="Enter your Mobile No.">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="formgroup">
                                    <label>Delivery Address</label>
                                   <textarea placeholder="Enter Your Delivery Address"></textarea>
                                </div>
                            </div>
                        </div>
                        <button class="inputBtn redbg txt-upr clr-white">Continue</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bookItroModel" id="bookItroModel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="enquiryForm">
                    <div class="frmttl">
                        Book Introduction
                    </div>
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="formgroup">
                                    <label>Message</label>
                                    <textarea placeholder="Introduce your self and tell about your food preferences"></textarea>
                                </div>
                            </div>
                        </div>
                        <button class="inputBtn redbg txt-upr clr-white">Continue</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');?>
<script src="<?php echo base_url();?>front/js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 9,
            nav: true,
            loop: false,
            navigation: false,
            responsive: {
                0: {
                    items: 1
                },
                500: {
                    items: 1
                },
                600: {
                    items: 2
                },
                700: {
                     items:2
                    },
                900: {
                     items:3
                    }
            }
        })
    });
</script>
<script>
        // Page Scroll
        $(document).ready(function(){
          $('.KthcnBlockNacList li a[href^="#"]').on('click',function (e) {
            e.preventDefault();
  
            var target = this.hash;
            var targetID = $(target);
            var targetOffset = targetID.offset().top;
            var targetScroll = targetOffset - 100;
  
            $('html, body').stop().animate({
              'scrollTop': targetScroll
            }, 900, 'swing', function () {
              //window.location.hash = target;
            });
          });
        });
  
      // Fixed Nav
      jQuery(document).ready(function () {
          $(window).scroll(function(){
              var scrollTop = $(".aboutKthcnBlock").offset().top - 80;
              var scrollBtm = $(".aboutinstagram").offset().top - 100;
              //console.log(scrollBtm);
              if($(window).scrollTop() > scrollTop){
                  $('.KthcnBlockNacList').addClass('fixed');
              } else {
                $('.KthcnBlockNacList').removeClass('fixed');
              }
              if($(window).scrollTop() > scrollBtm){
                  //console.log('bottom');
                  $('.KthcnBlockNacList').removeClass('fixed');
              }
  
              var windscroll = $(window).scrollTop();
              $('.scrollSection').each(function(i) {
                  if ($(this).position().top <= windscroll + -300) {
                      $('.KthcnBlockNacList li a.active').removeClass('active');
                      $('.KthcnBlockNacList li').eq(i).find('a').addClass('active');
                  }
              });
           });
      });
 </script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
 <script>
    $(document).ready(function() {
    var dateInput = $('input[name="date"]'); // Our date input has the name "date"
    dateInput.datepicker({
    format: 'yy/mm/dd',
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
    });

    $('#date').datepicker('setStartDate', truncateDate(new Date())); // <-- SO DOES THIS
});

function truncateDate(date) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>

</script>
 <script>
     /*INLINE CALENDER*/
    $(document).ready(function() {
        $('#date').datepicker('setStartDate', truncateDate(new Date())); 
        function truncateDate(date) {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
        };

        jQuery(".makEnquiry").click(function(){
            jQuery("#enquiryModel").modal('show');
        });

        jQuery(".deliveryAddress").click(function(){
            jQuery("#deliveryModel").modal('show');
        });
    });
    
    /*FOOD DELIVERY OPTION*/
    $("#foodCat").change(function(){
        if ( $(this).val() == 'Food Delivery') {
            $("#foodDlvryopt").show();
        } else {
            $("#foodDlvryopt").hide();
          }
        if ($(this).val() == 'Chef onsite cooking') {
            $('#MenuModal').modal('show');
        }    
    });

    

     /*CHEF ONSITE COOKING*/

     function likeDislike(val,val1) {
        var like_rev;
        if(val == 'yes'){
            like_rev = 0;
        }else{
            like_rev = 1;
        }
        var mainuserid = '<?php echo $this->session->userdata('user_id') ?>';
          $.ajax({
                url: '<?php echo base_url(); ?>welcome/likeDislike', // point to server-side PHP script 
                data: {mainuserid:mainuserid,like_rev:like_rev,review_id:val1},                     
                type: 'post',
                success: function(data){
                location.reload();
                 }
                });
       //alert(mainuserid +'==='+ val1);
     }

     function Wishlist(val,val1){
      var url = '<?php echo base_url() ?>';
     // alert(val +'eee'+ val1);
    //  var favroit;
        if(val == '1'){
            favroit = 0;
        }else{
            favroit = 1;
        }
        var mainuserid = '<?php echo $this->session->userdata('user_id') ?>';
          $.ajax({
                url: '<?php echo base_url(); ?>welcome/saveWishlist', // point to server-side PHP script 
                data: {mainuserid:mainuserid,favroit:favroit,chef:val1},                     
                type: 'post',
                success: function(data){
                 // alert(data);
                //location.reload();
                if(data != '0'){
                  location.reload();
                 }else{
                  window.location = url+'becomehost';
                  //alert(data);
                 }
                }
                });

     }

 </script>
</body>
</html>