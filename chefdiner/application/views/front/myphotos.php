<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/my-kitchen.css">

</head>
<body>
<?php $this->load->view('front/header');?>
<div class="wrapper mykitchenPage">
    <div class="container">
    <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
        <div class="row">
            <div class="col-lg-12 col-md-12">
               <h1 class="clr-black"><?php if(isset($getchefdata[0]['kitchen_title'])){ echo $getchefdata[0]['kitchen_title'];}else{
                  echo ucfirst($this->session->userdata('first_name'))."'s Kitchen";
                    }?> <a href="<?php echo base_url() ?>chefdetail">Preview</a></h1>
                <p class="clr-gray"><span class="clr-red">2</span> steps to become a chef!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="KtchnNav">
                    <ul>
                       <li class="kitchen"><a href="mykitchen">My Kitchen</a></li>
                        <li class="profile"><a href="myprofile">Profile</a></li>
                        <li class="address"><a href="myaddress">Address</a></li>
                        <li class="photos active"><a href="myphotos">Photos</a></li>
                        <li class="ktchnmenu"><a href="mymenus">Menu</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="kithchenForm picUploadPage">
                    <form action="<?php echo base_url() ?>welcome/saveMyphoto" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="chefid" value="<?php if(isset($getchefdata[0]['user_id'])){ echo $getchefdata[0]['user_id'];}?>">
                        <div class="form-group fileUpload">
                            <label for="uploadPics">Upload Pictures</label>
                            <input type="file" id="uploadPics" name="fileToupload[]" class="sbmt float-left" multiple> 
                            <p>Pictures attract guests the most! Choose the best ones reflecting your food, your place, preparation process and people enjoying your event. In order for profile to look awesome, at least 3 pictures have to be uploaded.</p>

                            <div class="row">
                            <?php 
                            if(!empty($getchefdata[0]['image1'])){
                                $image1 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image1'];

                            }else{
                                // $image1 = base_url().'front/images/blank.jpg';
                                $image1 = '';
                            }

                            if(!empty($getchefdata[0]['image2'])){
                                $image2 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image2'];

                            }else{
                                //$image2 = base_url().'front/images/blank.jpg';
                                $image2 = '';
                            }
                            if(!empty($getchefdata[0]['image3'])){
                                $image3 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image3'];

                            }else{
                                //$image3 = base_url().'front/images/blank.jpg';
                                $image3 = '';
                            }
                            if(!empty($getchefdata[0]['image4'])){
                                $image4 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image4'];

                            }else{
                                // $image4 = base_url().'front/images/blank.jpg';
                                $image4 = '';
                            }

                            if(!empty($getchefdata[0]['image5'])){
                                $image5 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image5'];

                            }else{
                                // $image5 = base_url().'front/images/blank.jpg';
                                $image5 = '';
                            }
                            if(!empty($getchefdata[0]['image6'])){
                                $image6 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image6'];

                            }else{
                                // $image6 = base_url().'front/images/blank.jpg';
                                $image6 = '';
                            }

                            if(!empty($getchefdata[0]['image7'])){
                                $image7 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image7'];

                            }else{
                               // $image7 = base_url().'front/images/blank.jpg';
                                $image7 = '';
                            }
                            if(!empty($getchefdata[0]['image8'])){
                                $image8 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image8'];

                            }else{
                                // $image8 = base_url().'front/images/blank.jpg';
                                $image8 = '';
                            }

                            if(!empty($getchefdata[0]['image9'])){
                                $image9 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image9'];

                            }else{
                                // $image9 = base_url().'front/images/blank.jpg';
                                 $image9 = '';
                            }
                             if(!empty($getchefdata[0]['image10'])){
                                $image10 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image10'];

                            }else{
                                // $image10 = base_url().'front/images/blank.jpg';
                                $image10 = '';
                            }
                            if(!empty($getchefdata[0]['image11'])){
                                $image11 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image11'];

                            }else{
                                // $image11 = base_url().'front/images/blank.jpg';
                                $image11 ='';
                            }

                            if(!empty($getchefdata[0]['image12'])){
                                $image12 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image12'];

                            }else{
                                // $image12 = base_url().'front/images/blank.jpg';
                                $image12 = '';
                            }
                            if(!empty($getchefdata[0]['image13'])){
                                $image13 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image13'];

                            }else{
                                // $image13 = base_url().'front/images/blank.jpg';
                                $image13 = '';
                            }
                            if(!empty($getchefdata[0]['image14'])){
                                $image14 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image14'];

                            }else{
                                //$image14 = base_url().'front/images/blank.jpg';
                                 $image14 ='';
                            }

                            if(!empty($getchefdata[0]['image15'])){
                                $image15 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image15'];

                            }else{
                                // $image15 = base_url().'front/images/blank.jpg';
                                 $image15 = '';
                            }
                            if(!empty($getchefdata[0]['image16'])){
                                $image16 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image16'];

                            }else{
                                // $image16 = base_url().'front/images/blank.jpg';
                                $image16 = '';
                            }

                            if(!empty($getchefdata[0]['image17'])){
                                $image17 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image17'];

                            }else{
                                // $image17 = base_url().'front/images/blank.jpg';
                                $image17 = '';
                            }
                            if(!empty($getchefdata[0]['image18'])){
                                $image18 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image18'];

                            }else{
                                // $image18 = base_url().'front/images/blank.jpg';
                                $image18 = '';
                            }

                            if(!empty($getchefdata[0]['image19'])){
                                $image19 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image19'];

                            }else{
                                /*$image19 = base_url().'front/images/blank.jpg';*/
                                 $image19 = '';
                            }
                             if(!empty($getchefdata[0]['image20'])){
                                $image20 = base_url().'assets/kitchengallery/'.$getchefdata[0]['image20'];

                            }else{
                                // $image20 = base_url().'front/images/blank.jpg';
                                $image20 = '';
                            }
                             ?>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image1; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image2; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image3; ?>" alt="">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image4; ?>" alt="">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image5; ?>" alt="">
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image6; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image7; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image8; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image9; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image10; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image11; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image12; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image13; ?>" alt="">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image14; ?>" alt="">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image15; ?>" alt="">
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image16; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image17; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image18; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image19; ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="uploadedPics">
                                        <img src="<?php echo $image20; ?>" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group sbmt-row">
                            <input type="submit" value="Next" class="txt-upr sbmt float-left">
                            <a href="#" class="txt-upr prvw float-right">Preview</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');?>
</body>
</html>