<footer class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="ftrAppBox">
                    <p>Download ChefDiner app</p>
                    <a href="">
                        <img src="<?php echo base_url();?>front/images/ftrappstore.png" alt="">
                    </a>
                    <a href="">
                        <img src="<?php echo base_url();?>front/images/ftrplaystore.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ftrLogo text-center">
                    <a href="">
                        <img src="<?php echo base_url();?>front/images/ftrlogo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ftrSocial">
                    <a href=""><img src="<?php echo base_url();?>front/images/fb.png" alt></a>
                    <a href=""><img src="<?php echo base_url();?>front/images/twt.png" alt></a>
                    <a href=""><img src="<?php echo base_url();?>front/images/in.png" alt></a>
                    <a href=""><img src="<?php echo base_url();?>front/images/pint.png" alt></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="col-lg-12 col-md-12">
                    <div class="ftrLink">
                        <ul>
                            <li><a href=""><?php echo $this->lang->line('privacy')?></a></li>
                            <li><a href=""> <?php echo $this->lang->line('termscondition'); ?>   </a></li>
                            <li><a href=""><?php echo $this->lang->line('howitworks'); ?>  </a></li>
                            <li><a href="">Help</a></li>
                            <li><a href="">Contacts </a></li>
                            <li><a href=""><?php echo $this->lang->line('jobs') ?></a></li>
                            <li><a href="">Subscribe</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="wrapper copyright">
    <div class="text-center">
        <p>
           © 2017 ChefDiner. All rights reserved.
        </p>
    </div>
</div>
<script src="<?php echo base_url();?>front/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>front/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>front/js/jquery.flagstrap.min.js"></script>
<script type="text/javascript">
    $(window).scroll(function() {    
       var scroll = $(window).scrollTop();
       if (scroll >= 40) {
           $("body").addClass("smaller");
       } else {
           $("body").removeClass("smaller");
       }
   });
</script>

<script>
    jQuery(document).ready(function () {
        jQuery('#select_country').attr('data-selected-country','HK');
        jQuery('#select_country').flagStrap();
    });
</script>
<script type="text/javascript">
    /*LOGIN - REGISTER TAB*/
    $('.modal-toggle').click(function (e) {
        var tab = $(this).attr('data-tab');
        $(".tabContent .tab-pane").hide();
        $(".tabContent").find(tab).show();
        $(".tabNav li a").removeClass('active');
        $('.tabNav li a[href="'+tab+'"]').addClass('active');
    });

    $(".tabNav li a").click(function(){
        var tab = $(this).attr('href');
        $(".tabContent .tab-pane").hide();
        $(".tabContent").find(tab).show();
        $(".tabNav li a").removeClass('active');
        $(this).addClass('active');
    });

    /*--------SIGNUP FORM------*/
    $(".rgform").hide();
    $(".signUpemail").click(function() {
        $('.rgform').show('show');
        $(".signUpemail").addClass("disable");
    });
    
</script>
