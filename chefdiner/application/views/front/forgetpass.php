<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title><?php echo $this->lang->line('sitetitle'); ?></title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/host-login.css">
</head>
<body>
<?php $this->load->view('front/header');?>
<div class="wrapper hostLogin">
    <div class="container">
     <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
        <div class="row">
            <div class="col-md-12">
                <div class="sd-tabs sd-tab-interaction">
                    <div class="usersign">
                        <div class="tabContent">
                            <form id="login" class="tab-pane usrLgForm" action="<?php echo base_url() ?>forget" method="post">
                                <div class="loginWith">
                                    <h5>Forgot Password</h5>
                                </div>
                                <div class="formGrp">
                                    <input type="email" class="uname" name="email" placeholder="Email*">
                                    <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                    <input type="submit" name="submit" class="txt-upr" value="Send">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/footer');?>
</body>
</html>