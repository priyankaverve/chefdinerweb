<footer class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="ftrAppBox">
                    <p>Download ChefDiner app</p>
                    <a href="">
                        <img src="<?php echo base_url();?>front/images/ftrappstore.png" alt="">
                    </a>
                    <a href="">
                        <img src="<?php echo base_url();?>front/images/ftrplaystore.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ftrLogo text-center">
                    <a href="">
                        <img src="<?php echo base_url();?>front/images/ftrlogo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ftrSocial">
                    <a href=""><img src="<?php echo base_url();?>front/images/fb.png" alt></a>
                    <a href=""><img src="<?php echo base_url();?>front/images/twt.png" alt></a>
                    <a href=""><img src="<?php echo base_url();?>front/images/in.png" alt></a>
                    <a href=""><img src="<?php echo base_url();?>front/images/pint.png" alt></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="col-lg-12 col-md-12">
                    <div class="ftrLink">
                        <ul>
                            <li><a href="<?php echo base_url() ?>privacypolicy">Privacy policy </a></li>
                            <li><a href="<?php echo base_url() ?>termscondition"> Terms and Conditions   </a></li>
                            <li><a href="">How it works?  </a></li>
                            <li><a href="">Help</a></li>
                            <li><a href="">Contacts </a></li>
                            <li><a href="">Jobs</a></li>
                            <li><a href="">Subscribe</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="wrapper copyright">
    <div class="text-center">
        <p>
           © 2017 ChefDiner. All rights reserved.
        </p>
    </div>
</div>

<script src="<?php echo base_url();?>front/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>front/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>front/js/jquery.flagstrap.min.js"></script>
<script src="<?= base_url()?>front/js/intlTelInput.js"></script>
<script src="<?= base_url()?>front/js/utils.js"></script>

<script type="text/javascript">
    $(window).scroll(function() {    
       var scroll = $(window).scrollTop();
       if (scroll >= 40) {
           $("body").addClass("smaller");
       } else {
           $("body").removeClass("smaller");
       }
   });
</script>

<script>

    jQuery(document).ready(function () {
        jQuery('#select_country').attr('data-selected-country','HKD');
        jQuery('#select_country').flagStrap();
    });
</script>
<script type="text/javascript">
    /*LOGIN - REGISTER TAB*/
    $('.modal-toggle').click(function (e) {
        var tab = $(this).attr('data-tab');
        $(".tabContent .tab-pane").hide();
        $(".tabContent").find(tab).show();
        $(".tabNav li a").removeClass('active');
        $('.tabNav li a[href="'+tab+'"]').addClass('active');
    });

    $(".tabNav li a").click(function(){
        var tab = $(this).attr('data-id');
        $(".tabContent .tab-pane").hide();
        $(".tabContent").find(tab).show();
        $(".tabNav li a").removeClass('active');
        $(this).addClass('active');
    });

    /*--------SIGNUP FORM------*/
    $(".rgform").hide();
    $(".signUpemail").click(function() {
        $('.rgform').show('show');
        $(".signUpemail").addClass("disable");
    });

    /*------LANGUAGE SELECT----------*/

   /* $('.lang_selected').click(function() {
        $('.lang_values').toggle();
    });

    $('.lang_item').click(function() {
     var value = $(this).text();

    $('.lang_selected').attr('data-value', value);
        $('.lang_selected').html(value);
        $('.lang_values').toggle();
    });*/

     $('.lang_selected').click(function() {
        $('.lang_values').toggle();
    });

     /*--------SIGNUP FORM------*/
   $(".accDropDown a").click(function() {
        $('.AccNav').toggle();
        
    });

    $('.lang_item').click(function() {
    var value = $(this).text();
      $.ajax(
      {
        url:"<?php echo base_url(); ?>welcome/switchLang",
        type:"POST",
        data:{language : value},
        success:function(data)
        { 
            // alert(data);
          /* if(data != 0){
            var newval = data;
           }*/

        /*$('.lang_selected').attr('data-value', value);

        $('.lang_selected').html(value);
        $('.lang_values').toggle();*/

        if(data =='english'){
         var val = 'English';
        }else if(data =='traditional-chinese'){
            val ='T.Chinese';
        }else{
            val ='S.Chinese';
         
        }
        $('#sessionlanguage').text(val);
       // $('#sessionlanguage').innerHTML = val;
         $('.lang_values').toggle();
       /* $('.lang_selected').attr('data-value', val);

        $('.lang_selected').html(val);
        $('.lang_values').toggle();*/

         location.reload(); 
        }
        
      });
    /*$('.lang_selected').attr('data-value', value);

        $('.lang_selected').html(value);
        $('.lang_values').toggle();*/

       
    });
    /*****User Registration******/
function submitRegister(){
var language = '<?php echo $this->session->userdata('site_lang') ?>';   
// alert(language);
var first_name = $("#first_name").val();
var last_name = $("#last_name").val();
var email = $("#email").val();
var password = $("#password").val();
var cpassword = $("#confrm_password").val();
var phone_number = $("#phone_number").val();
if (first_name == ''|| last_name == '' || email == '' || password == '' || cpassword == '' || phone_number == '') {
/*alert("Please fill all fields...!!!!!!");*/
$('#statusMsg').html('<span style="color:red;">Please fill all fields.</span>');
} /*else if ((password.length) < 8) {
alert("Password should atleast 8 character in length...!!!!!!");
}*/ else if (!(password).match(cpassword)) {
// alert("Your passwords don't match. Try again?");
$('#statusMsg').html('<span style="color:red;">Your passwords did not match.</span>');
} else {
   $.ajax(
      {
        url:"<?php echo base_url(); ?>welcome/userRegister",
        type:"POST",
        dataType :'json',
        data:{language : language,fname:first_name,lname:last_name,email:email,password:password,contact:phone_number},
        success:function(data)
        { 
       //alert(data.txt);
       if(data.txt == 'AE'){
        $('#statusMsg').html('<span style="color:red;">'+data.msg+'</span>');
       }else if(data.txt =='NR'){
        $('#statusMsg').html('<span style="color:red;">'+data.msg+'</span>');
       }else if(data.txt =='IE'){
        $('#statusMsg').html('<span style="color:red;">'+data.msg+'</span>');
       }else if(data.txt =='MN'){
        $('#statusMsg').html('<span style="color:red;">'+data.msg+'</span>');
       }else{
        $('#statusMsg').html('<span style="color:green;">'+data.msg+'</span>');
       }
    
        }
        
      });
    }
  }

  /*****User login******/

function submitLogin(){
var language = '<?php echo $this->session->userdata('site_lang') ?>';   
// alert(language);
var loginemail = $("#loginemail").val();
var loginpassword = $("#loginpassword").val();
if (loginemail == ''|| loginpassword == '') {
/*alert("Please fill all fields...!!!!!!");*/
$('#statusMsgLogin').html('<span style="color:red;">Please fill all fields.</span>');
}

else {
   $.ajax(
      {
        url:"<?php echo base_url(); ?>welcome/userLogin",
        type:"POST",
        dataType :'json',
        data:{loginemail : loginemail,loginpassword:loginpassword},
        success:function(data)
        { 
      // alert(data.txt);
       if(data.txt == 'NA'){
        $('#statusMsgLogin').html('<span style="color:red;">'+data.msg+'</span>');
       }else if(data.txt =='WP'){
        $('#statusMsgLogin').html('<span style="color:red;">'+data.msg+'</span>');
       }else if(data.txt =='IE'){
        $('#statusMsgLogin').html('<span style="color:red;">'+data.msg+'</span>');
       }else{
      
        // similar behavior as clicking on a link
        window.location.href = '<?php echo base_url() ?>';
      //  $('#statusMsgLogin').html('<span style="color:green;">'+data.msg+'</span>');
       }
    
        }
        
      });
}
  }

    /*****Become a Host******/

function submitHostLogin(){
var language = '<?php echo $this->session->userdata('site_lang') ?>';   
// alert(language);
var hostEmail = $("#hostEmail").val();
var hostPass = $("#hostPass").val();



if (hostEmail == ''|| hostPass == '') {
/*alert("Please fill all fields...!!!!!!");*/
$('#statusHostLogin').html('<span style="color:red;">Please fill all fields.</span>');
}

else {
   $.ajax(
      {
        url:"<?php echo base_url(); ?>welcome/userLogin",
        type:"POST",
        dataType :'json',
        data:{loginemail : hostEmail,loginpassword:hostPass},
        success:function(data)
        { 
     //  alert(data);
    if(data.txt == 'NA'){
        $('#statusHostLogin').html('<span style="color:red;">'+data.msg+'</span>');
       }else if(data.txt =='WP'){
        $('#statusHostLogin').html('<span style="color:red;">'+data.msg+'</span>');
       }else if(data.txt =='IE'){
        $('#statusHostLogin').html('<span style="color:red;">'+data.msg+'</span>');
       }else{

        // similar behavior as clicking on a link
        window.location.href = '<?php echo base_url() ?>';
      //  $('#statusMsgLogin').html('<span style="color:green;">'+data.msg+'</span>');
       }
        }
        
      });
}
  }

  /*****Host register****/
  function submithostRegister(){
var language = '<?php echo $this->session->userdata('site_lang') ?>';   
// alert(language);
var first_name = $("#first_name1").val();
var last_name = $("#last_name1").val();
var email = $("#email1").val();
var password = $("#password1").val();
var cpassword = $("#confrm_password1").val();
var phone_number = $("#phone_number1").val();
if (first_name == ''|| last_name == '' || email == '' || password == '' || cpassword == '' || phone_number == '') {
/*alert("Please fill all fields...!!!!!!");*/
$('#statushostMsg').html('<span style="color:red;">Please fill all fields.</span>');
} /*else if ((password.length) < 8) {
alert("Password should atleast 8 character in length...!!!!!!");
}*/ else if (!(password).match(cpassword)) {
// alert("Your passwords don't match. Try again?");
$('#statushostMsg').html('<span style="color:red;">Your passwords did not match.</span>');
} else {
   $.ajax(
      {
        url:"<?php echo base_url(); ?>welcome/userRegister",
        type:"POST",
        dataType :'json',
        data:{language : language,fname:first_name,lname:last_name,email:email,password:password,contact:phone_number},
        success:function(data)
        { 
       //alert(data.txt);
       if(data.txt == 'AE'){
        $('#statushostMsg').html('<span style="color:red;">'+data.msg+'</span>');
       }else if(data.txt =='NR'){
        $('#statushostMsg').html('<span style="color:red;">'+data.msg+'</span>');
       }else if(data.txt =='IE'){
        $('#statushostMsg').html('<span style="color:red;">'+data.msg+'</span>');
       }else if(data.txt =='MN'){
        $('#statushostMsg').html('<span style="color:red;">'+data.msg+'</span>');
       }else{
        $('#statushostMsg').html('<span style="color:green;">'+data.msg+'</span>');
       }
    
        }
        
      });
    }
  }


var password = document.getElementById("passwordRef")
  , confirm_password = document.getElementById("confirm_passwordRef");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

var loggedinuser = '<?php echo $this->session->userdata('user_id') ?>'; 
//alert(loggedinuser);
if(loggedinuser == ''){
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
}


/********Select country,state and city********/
/******in my addrees file*****/
$(document).ready(function(){

  /*****get country list******/

$('#country').on('change',function(){
        var countryID = $('#country').val();
        var countyName = $("#country option:selected").text();
        if(countyName !=''){
          $('#countryname').val(countyName);
        }
        
        // /alert(countryname);
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url(); ?>welcome/selectCount',
                data:'country_id='+countryID,
                success:function(html){
                 // console.log(html);
                    $('#state').html(html);
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    
    $('#state').on('change',function(){
        var stateID = $('#state').val();
        var stateName = $("#state option:selected").text();
        if(stateName !=''){
          $('#statesname').val(stateName);
        }
        if(stateID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url(); ?>welcome/selectState',
                // data:'state_id='+stateID,
                data:{'state_id':stateID,'statesname':statesname},
                success:function(html){
                    $('#city').html(html);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });


 var counID = $('#country').val();
 
 //alert(statesname);
 if(counID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url(); ?>welcome/selectCount',
                data:'country_id='+counID,
                success:function(html){
                 // console.log(html);
                    $('#state').html(html);
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }

var statesname = $('#statesname').val();
var state ='';

            $.ajax({
                type:'POST',
                url:'<?php echo base_url(); ?>welcome/selectState',
                // data:'state_id='+stateID,
                data:{'state_id':state},
                success:function(html){
                    $('#city').html(html);
                }
            }); 
        
});


</script>


<script type="text/javascript">

/*****Contact number for myaddress form*****/
$(function(){

      $("#phone").intlTelInput({
            autoHideDialCode: true,
            autoPlaceholder: true,
            separateDialCode: true,
            nationalMode: true,
            geoIpLookup: function (callback) {
                $.get("http://ipinfo.io", function () {}, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            initialCountry: "hk",
        });

        // get the country data from the plugin
        var countryData = $.fn.intlTelInput.getCountryData(),
          telInput = $("#phone"),
          addressDropdown = $("#listcountry");

        // init plugin
        telInput.intlTelInput({
          utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.5.0/js/utils.js" // just for formatting/placeholders etc
        });

        // populate the country dropdown
        $.each(countryData, function(i, country) {
          addressDropdown.append($("<option></option>").attr("value", country.iso2).text(country.name));
        });

        // listen to the telephone input for changes
        telInput.on("countrychange", function() {
          var countryCode = telInput.intlTelInput("getSelectedCountryData").iso2;
          addressDropdown.val(countryCode);
        });

        // trigger a fake "change" event now, to trigger an initial sync
        telInput.trigger("countrychange");

        // listen to the address dropdown for changes
        addressDropdown.change(function() {
          var countryCode = $(this).val();
          telInput.intlTelInput("setCountry", countryCode);
        });

        // update the hidden input on submit
        $("#form").submit(countryData,function(i, country) {
          $("#countryCode").val(telInput.intlTelInput("getSelectedCountryData").name);
        //  console.log($("#countryCode").val(telInput.intlTelInput("getSelectedCountryData").name));
          $("#phonefull").val('+' + telInput.intlTelInput("getSelectedCountryData").dialCode + $("#phone").val());      
        });
      });

</script>
