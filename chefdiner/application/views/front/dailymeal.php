<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/chef-box.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/box.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/font-awesome.css">
</head>
<body>
<?php
$this->load->view('front/header');
?>
<div class="wrapper boxpage">
    <div class="container forsrvices ">
            <h3 class="secTitle dlymeal">
                    Daily Meal
           </h3>
        <div class="row">
         <?php 
                       // print_r($dailymealchef);
                        if(!empty($dailymealchef)){
                            foreach ($dailymealchef as $key => $value) {

                                $imgs = $value->allimage;
                                $allimg  = explode("|", $imgs);
                                $allimg  = array_values(array_filter($allimg));



                            if(!empty($value->cuisine)){
                                $cuisine = explode(",", $value->cuisine);

                            }else{
                                $cuisine = '';
                            }

                          //  print_r($cuisine);
                            if(!empty($cuisine)){
                            if(count($cuisine) == 1){
                                
                                $cuisine1 = $cuisine[0];
                                $cuisine2 = '';
                            }elseif(count($cuisine) > 2){
                                
                                $cuisine1 = $cuisine[0];
                                $cuisine2 = $cuisine[1];
                            }else{
                                $cuisine1 = '';
                                $cuisine2 = '';
                            }
                            }else{
                                $cuisine1 = '';
                                $cuisine2 = '';
                            }
                            if($value->profile_pic == ''){
                                $profile_pic = base_url().'front/images/blank.jpg';
                            }else{
                                $profile_pic = $value->profile_pic;
                            }

                            

                             ?>
            <div class="col-md-3">
                <div class="chfBoxOne">
                    <div class="img">
                                    <a href="<?php echo base_url(); ?>chefdetail/<?php echo $value->user_id; ?>">
                                        <!-- <img src="<?php //echo base_url();?>front/images/food4.jpg" alt=""> -->
                                        <img src="<?php echo $allimg[0]; ?>" alt="">
                                        <div class="shade"></div>
                                        <div class="price clr-red">
                                        <?=$value->currency?><?=$value->price?>/Guest
                                        </div>
                                        <div class="bkMrkNmr clr-red">
                                            <?=$value->favouritecount;?>
                                        </div>
                                        <div class="ctgry">
                                        <ul>
                                                <li><?=$cuisine1;?></li>
                                                <li class="mrRight"><?=$cuisine2;?></li>
                                            </ul> 
                                        </div>
                                    </a>
                                </div>
                    <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <div class="chfPrflImg">
                                                <img class="img-fluid" src="<?=$profile_pic;?>" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                            <?php
                                           $rountRat = round($value->average_rating);
                                           //echo $rountRat;
                                           if($rountRat > 0 && $rountRat <=0.5){
                                          
                                           $valhalf5 = '0.5';
                                           $chkhalf5 = 'checked';

                                           }else{
                                            $valhalf5 = '0.5';
                                            $chkhalf5 = '';
                                           }

                                           if($rountRat > 0.5 && $rountRat <=1){
                                          
                                           $valfull1 = '1.0';
                                           $chkfull1 = 'checked';

                                           }else{
                                            $valfull1 = '1.0';
                                            $chkfull1 = '';
                                           }

                                           if($rountRat > 1 && $rountRat <=1.5){
                                          
                                           $valhalf1 = '1.5';
                                           $chkhalf1 = 'checked';

                                           }else{
                                            $valhalf1 = '1.5';
                                            $chkhalf1 = '';
                                           }
                                           if($rountRat > 1.5 && $rountRat <=2){
                                          
                                           $valfull2 = '2';
                                           $chkfull2 = 'checked';

                                           }else{
                                            $valfull2 = '2';
                                            $chkfull2 = '';
                                           }

                                           if($rountRat > 2 && $rountRat <=2.5){
                                          
                                           $valhalf2 = '2.5';
                                           $chkhalf2 = 'checked';

                                           }else{
                                            $valhalf2 = '2.5';
                                            $chkhalf2 = '';
                                           }

                                           if($rountRat > 2.5 && $rountRat <=3){
                                          
                                           $valfull3 = '3';
                                           $chkfull3 = 'checked';

                                           }else{
                                            $valfull3 = '3';
                                            $chkfull3 = '';
                                           }

                                          if($rountRat > 3 && $rountRat <=3.5){
                                          
                                           $valhalf3 = '3.5';
                                           $chkhalf3 = 'checked';

                                           }else{
                                            $valhalf3 = '3.5';
                                            $chkhalf3 = '';
                                           }
                                           if($rountRat > 3.5 && $rountRat <=4){
                                          
                                           $valfull4 = '4';
                                           $chkfull4 = 'checked';

                                           }else{
                                            $valfull4= '4';
                                            $chkfull4 = '';
                                           }
                                           
                                           if($rountRat > 4 && $rountRat <=4.5){
                                          
                                           $valhalf4 = '4.5';
                                           $chkhalf4 = 'checked';

                                           }else{
                                            $valhalf4= '4.5';
                                            $chkhalf4 = '';
                                           }

                                           if($rountRat > 4.5 && $rountRat <=5){
                                          
                                           $valfull5 = '5';
                                           $chkfull5 = 'checked';

                                           }else{
                                            $valfull5= '5';
                                            $chkfull5 = '';
                                           }
                                           

                                            ?>
                                                <li class="rateStar">
                                                <div class="cstmr-reviews">
                                                        <fieldset class="rating">
                                                            <input type="radio" id="stara<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull5?>" <?=$chkfull5?> disabled="disable" />
                                                            <label class = "full" for="stara<?=$value->id?>" title="Awesome - 5 stars"></label>

                                                            <input type="radio" id="starb<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf4?>" <?=$chkhalf4?> disabled="disable" />
                                                            <label class="half" for="starb<?=$value->id?>" title="Pretty good - 4.5 stars"></label>

                                                            <input type="radio" id="starc<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull4?>" <?=$chkfull4?> disabled="disable"  />
                                                            <label class = "full" for="starc<?=$value->id?>" title="Pretty good - 4 stars"></label>

                                                            <input type="radio" id="stard<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf3?>" <?=$chkhalf3?> disabled="disable" />
                                                            <label class="half" for="stard<?=$value->id?>" title="Meh - 3.5 stars"></label>

                                                            <input type="radio" id="stare<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull3?>" <?=$chkfull3?> disabled="disable"/>
                                                            <label class = "full" for="stare<?=$value->id?>" title="Meh - 3 stars"></label>

                                                            <input type="radio" id="starf<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf2?>" <?=$chkhalf2?> disabled="disable" />
                                                            <label class="half" for="starf<?=$value->id?>" title="Kinda bad - 2.5 stars"></label>

                                                            <input type="radio" id="starg<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull2?>" <?=$chkfull2?> disabled="disable" />
                                                            <label class = "full" for="starg<?=$value->id?>" title="Kinda bad - 2 stars"></label>

                                                            <input type="radio" id="starh<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf1?>" <?=$chkhalf1?> disabled="disable"  />
                                                            <label class="half" for="starh<?=$value->id?>" title="Meh - 1.5 stars"></label>

                                                            <input type="radio" id="stari<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull1?>" <?=$chkfull1?> disabled="disable" />
                                                            <label class = "full" for="stari<?=$value->id?>" title="Sucks big time - 1 star"></label>

                                                            <input type="radio" id="starj<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf5?>" <?=$chkhalf5?> disabled="disable" />
                                                            <label class="half" for="starj<?=$value->id?>" title="Sucks big time - 0.5 stars"></label>
                                                        </fieldset>
                                                    </div>
                                           
                                                </li>
                                                <li class="rateCount">
                                                    <?php 
                                                    if(is_float($value->average_rating)){
                                                    $avr_rating = $value->average_rating;

                                                    }else{
                                                    $avr_rating  = number_format((float)$value->average_rating, 1, '.', '');
                                                    }
                                                    ?>
                                                    <span>
                                                        <?=$avr_rating;?>
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span><?=$value->totalreview?></span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl"><?=ucfirst($value->kitchen_title);?></span></a>
                                            <p><?=ucfirst($value->chef_city);?>,<?=ucfirst($value->chef_country);?></p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/><?=$value->recommended;?>
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/><?=$value->no_recommended;?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                </div>
            </div>
           <?php       }

                        }else{
                ?>
                        
                        <div class="no-content">No Records found</div>

                        <?php } ?>
                        
            
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>
</body>
</html>