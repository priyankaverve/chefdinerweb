<?php 
if(!$this->session->userdata('site_lang')){
$lan = 'English';
}else{
  if($this->session->userdata('site_lang') == 'english'){
    $lan = 'English';
  }elseif($this->session->userdata('site_lang') == 'traditional-chinese'){
     $lan ='T.Chinese';
  }else{
     $lan = 'S.Chinese';
  }
}

$sitLan = $this->session->userdata('site_lang');
if(!empty($sitLan)){
  $sitLan = $this->session->userdata('site_lang');
}else{
  $sitLan = 'english';
}
?>
<header class="header wrapper">
  <div class="container">
    <div class="row logoRow">
      <div class="col-lg-4 col-md-4 col-sm-5 col-4">
        <div class="tpLeft">
          <div class="lang float-left">
            <div class="lang_selected" id="sessionlanguage"><?php echo $lan;?></div>
            <div class="lang_values">
              <div class="lang_item">English</div>
              <!-- <div class="lang_item" data-value="chinese">Chinese</div> -->
              <div class="lang_item">T.Chinese</div>
              <div class="lang_item">S.Chinese</div>
            </div>
          </div>
          <div class="currency float-left">
            <form class="form-horizontal ">
              <div class="form-group">
                <div class="flagstrap" id="select_country" data-input-name="NewBuyer_country" data-selected-country=""></div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-3 col-5">
        <div class="logo">
          <a href="<?php echo base_url();?>">
            <img class="txt-center" src="<?php echo base_url();?>front/images/logo.png" alt="">
          </a>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-3">
        <div class="tpRight">
        <?php  if($this->session->userdata('user_id') !='' && $this->session->userdata('is_chef') != '1'){
         
       /* echo $this->session->userdata('user_id');die;*/
        ?>
          <div class="bcmHost float-left">
            <a href="<?php echo base_url() ?>becomehost"><span>Become a host</span></a>
          </div>

        <?php  }elseif($this->session->userdata('user_id') == ''){?>
        <div class="bcmHost float-left">
            <a href="<?php echo base_url() ?>becomehost"><span>Become a host</span></a>
          </div>
         <?php } ?>
          <?php if($this->session->userdata('user_id') != ''){
            $cls = 'afterLogin';
            }else{
              $cls='';
              } ?>
          <div class="usersign <?=$cls?>">
            <ul class="">
            <?php if($this->session->userdata('user_id') == '') { ?>
              <li class="ntmob"><a class="signup modal-toggle" href="javascript:;" data-tab="#register" data-toggle="modal" data-target="#login-register-modal"><span>Signup</span></a></li>
              <li><a class="login modal-toggle" href="javascript:;" data-tab="#login" data-toggle="modal" data-target="#login-register-modal"><span><?php echo $this->lang->line('login'); ?></span></a></li>
              <?php }else{ ?>
                 <li class="accDropDown">
                 <a class="signup" href="javascript:;"><span><?php echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name'); ?></span></a>

                 <ul class="AccNav">
                  <li><a href="<?php echo base_url();?>editpofile">Edit Profile</a></li>
                  <li><a href="<?php echo base_url();?>socialnetwork">Account Settings</a></li>
                  <li><a href="<?php echo base_url();?>chefdetail">My kitchen</a></li>
                  <li><a href="<?php echo base_url();?>mykitchen">Edit my kitchen</a></li>
                  <li><a href="<?php echo base_url();?>wishlist">Wish List</a></li>
                  <li><a href="">Dining credits</a></li>
                   <li><a  href="<?php echo base_url();?>logout/<?php echo $sitLan?>"><span>Logout</span></a></li>
                </ul>
                 </li>
               
                <li><a class="message" href="javascript:;"><span>Message</span></a></li>
           <?php     } ?>
            </ul>
            <div class="modal" id="login-register-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-offset-top">
                  <div class="modal-content">
                    <div class="modal-body">
                      <div class="sd-tabs sd-tab-interaction">
                        <div class="">
                          <ul class="tabNav">
                            <li><a href="javascript:;" data-id="#login" class="active login"><span>Login</span></a></li>
                            <li><a href="javascript:;" data-id="#register" class="signup"> <span>Signup</span></a></li>
                          </ul>
                          <div class="tabContent">
                            <form id="login" class="tab-pane usrLgForm" action="" method="post">
                              <div class="loginWith">
                                <h5>Login to ChefDiner</h5>
                                <a href="javascript:;"><img src="<?php echo base_url();?>front/images/fbl.png" alt=""></a>
                                
                                <p>
                                  If you don't prefer Social Media, feel free to
                                  use email and password to login.
                                </p>
                              </div>

                              <div class="formGrp">
                                  <p id="statusMsgLogin">
                                  </p>
                               </div>
                              <div class="formGrp">
                                <input type="text" class="uname" id="loginemail" name="" placeholder="Email*">
                                <span class="unptbg"></span>
                              </div>
                              <div class="formGrp">
                                <input type="password" class="upw" id="loginpassword" placeholder="Password" name="" required>
                                <span class="unptbg"></span>
                              </div>
                              <div class="fgtpw"><a href="<?php echo base_url() ?>forgetPass">Forgot Password</a></div>
                              <div class="formGrp">
                               <button type="button" class="txt-upr"  onclick="submitLogin()">continue</button>
                              </div>
                            </form>
                            <form id="register" class="tab-pane usrLgForm">
                              
                              <div class="loginWith">
                                <h5>Sign Up with ChefDiner</h5>
                                <a href="javascript:;"><img src="<?php echo base_url();?>front/images/fbl.png" alt=""></a>
                                <!-- <a href="javascript:;"><img src="<?php //echo base_url();?>front/images/twtl.png" alt=""></a>
                                <a href="javascript:;"><img src="<?php //echo base_url();?>front/images/gpl.png" alt=""></a> -->
                              </div>

                              <div class="formGrp">
                                  <a href="javascript:;" class="signUpemail">Sign up with email</a>
                              </div>
                              <div class="formGrp">
                                  <p id="statusMsg">
                                  </p>
                               </div>
                              <div class="rgform">
                                <div class="formGrp">
                                  <input type="text" class="uname" name="" id="first_name" placeholder="First Name">
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <input type="text" class="uname" name="" id="last_name" placeholder="Last Name">
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <input type="text" class="uemail" name="" id="email" placeholder="Email">
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <input type="password" class="upw" id="password" placeholder="Password" name="" required>
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <input type="password" class="upw" id="confrm_password"  placeholder="Confirm Password" name="" required>
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <input type="number" class="unmbr" id="phone_number" min="0" placeholder="Phone Number" name="" required>
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <p>
                                    We will be using your phone number to send you information about bookings and other important events.
                                  </p>
                                  
                                </div>
                                <div class="sgntc"><input type="checkbox" checked> Agree  <a href='javascript:;'>Term & Conditions & Privacy Policy</a></div>
                                <div class="formGrp">
                                <button type="button" class="txt-upr"  onclick="submitRegister()">Register</button>
                                    <!-- <input type="submit" class="txt-upr" value="Register"> -->
                                </div>
                              </div>
                            </form>
                            
                          </div>
                        </div>
                      </div>
                      <button type="button" class="btn btn-default mdlclose" data-dismiss="modal">
                        <img src="<?php echo base_url();?>front/images/mdlclose.png" alt="">
                      </button>
                    </div>
                  
                  </div>  
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>  
  </div>
</header>


