
                       
                  
                    <div class="modal fade menuModel" id="EditMenuModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2 class="clr-black">Menu Description</h2>
                                        </div>
                                    </div>
                                    <form action="<?php echo base_url() ?>addmenus" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="chefid" value="<?php if(isset($getchefdata[0]['user_id'])){ echo $getchefdata[0]['user_id'];}?>">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="clr-gray">Menu Title</label>    
                                                    <input type="text" class="input" name="menutitle">
                                                </div>
                                                <div class="form-group menuPrice">
                                                    <label class="clr-gray">Price Per Person</label>    
                                                    <input type="text" class="input float-left" name="price_per_person">
                                                   <div class="flagstrap float-left" id="select_nenu_currency" data-input-name="NewBuyer_country" data-selected-country=""></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group dishImg">
                                                    <label class="clr-red" for="dishImgUpload">Upload Image</label>
                                                    <input type="file" id="dishImgUpload" name="fileToUpload">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <h4 class="clr-gray">Service Type</h4>    
                                                    <ul class="serviceBlck menuService"> 
                                                        <li>
                                                            <input class="radio" type="checkbox" id="FoodDelivery" name="serveType[]" value="Food Delivery">
                                                            <label class="radiobLable" for="FoodDelivery"> Food Delivery</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="PrivateMeal" name="serveType[]" value="Home Private Meal">
                                                            <label class="radiobLable" for="PrivateMeal">Home Private Meal</label>
                                                        </li>
                                                        <li class="mrRight">
                                                            <input class="radio" type="checkbox" id="DailyMeal" name="serveType[]" value="Daily Meal">
                                                            <label class="radiobLable" for="DailyMeal">Daily Meal</label>
                                                        </li>
                                                        <li class="">
                                                            <input class="radio" type="checkbox" id="privateMeal" name="serveType[]" value="Restaurant Private Meal">
                                                            <label class="radiobLable" for="privateMeal">Restaurant Private Meal</label>
                                                        </li>
                                                        <li class="">
                                                            <input class="radio" type="checkbox" id="onsiteCooking" name="serveType[]" value="Chef Onsite Cooking">
                                                            <label class="radiobLable" for="onsiteCooking">Chef Onsite Cooking</label>
                                                        </li>
                                                        <li class="">
                                                            <input class="radio" type="checkbox" id="cookingWorkshop" name="serveType[]" value="Cooking Workshop">
                                                            <label class="radiobLable" for="cookingWorkshop">Cooking Workshop</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="form-group untensilbox">
                                                    <p>
                                                        Please kindly let chef know what tools you can provide at your place.
                                                    </p>
                                                    <ul class="serviceBlck menuService utensilList"> 
                                                        <li>
                                                            <input class="radio" type="checkbox" id="potatoMasher" name="kitchenTool[]" value="Potato masher">
                                                            <label class="radiobLable" for="potatoMasher"> Potato masher</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="measuringCups" name="kitchenTool[]" value="Measuring cups/jug and spoons">
                                                            <label class="radiobLable" for="measuringCups">Measuring cups/jug and spoons</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="chefKnife" name="kitchenTool[]" value="Chef Knife">
                                                            <label class="radiobLable" for="chefKnife">Chef Knife</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="parkingKnife" name="kitchenTool[]" value="Paring Knife">
                                                            <label class="radiobLable" for="parkingKnife">Paring Knife</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="potatoricer" name="kitchenTool[]" value="Potato ricer">
                                                            <label class="radiobLable" for="potatoricer"> Potato ricer</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="ovenMitt" name="kitchenTool[]" value="Oven mitt/glove">
                                                            <label class="radiobLable" for="ovenMitt">Oven mitt/glove</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="serrated" name="kitchenTool[]" value="Serrated/Bread Knife">
                                                            <label class="radiobLable" for="serrated">Serrated/Bread Knife</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="vegetablepeelers" name="kitchenTool[]" value="Vegetable peelers">
                                                            <label class="radiobLable" for="vegetablepeelers">Vegetable peelers</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="sharpening" name="kitchenTool[]" value="Sharpening Steel">
                                                            <label class="radiobLable" for="sharpening">Sharpening Steel</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="garlicpress" name="kitchenTool[]" value="Garlic press">
                                                            <label class="radiobLable" for="garlicpress">Garlic press</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="bbqtools" name="kitchenTool[]" value="BBQ Tools">
                                                            <label class="radiobLable" for="bbqtools">BBQ Tools</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="electricsharpener" name="kitchenTool[]" value="Electric Sharpener" >
                                                            <label class="radiobLable" for="electricsharpener">Electric Sharpener</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="cuttingboard" name="kitchenTool[]" value="Egg separater">
                                                            <label class="radiobLable" for="cuttingboard">Cutting Board</label>
                                                        </li>

                                                        <li>
                                                            <input class="radio" type="checkbox" id="eggseparater" name="kitchenTool[]" value="Egg separater">
                                                            <label class="radiobLable" for="eggseparater">Egg separater</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="choppers" name="kitchenTool[]" value="Choppers">
                                                            <label class="radiobLable" for="choppers">Choppers</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="fishscaler" name="kitchenTool[]" value="Fish scaler">
                                                            <label class="radiobLable" for="choppers">Fish scaler</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="lemonpress" name="kitchenTool[]" value="Lemon press/Lemon squeezer">
                                                            <label class="radiobLable" for="lemonpress">Lemon press/Lemon squeezer</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="icecreamscoop" name="kitchenTool[]" value="Ice cream scoop">
                                                            <label class="radiobLable" for="icecreamscoop">Ice cream scoop</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="peppershakers" name="kitchenTool[]" value="Pepper mill,salt & pepper shakers">
                                                            <label class="radiobLable" for="peppershakers">Pepper mill,salt & pepper shakers</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="tongs" name="kitchenTool[]" value="Tongs">
                                                            <label class="radiobLable" for="tongs"> </label>
                                                        </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="roolingpin" name="kitchenTool[]" value="Rolling pin">
                                                            <label class="radiobLable" for="roolingpin">Rolling pin</label>
                                                        </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="canopener" name="kitchenTool[]" value="Can opener">
                                                            <label class="radiobLable" for="canopener">Can opener</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="meatgrinder" name="kitchenTool[]" value="Meat grinder">
                                                            <label class="radiobLable" for="meatgrinder">Meat grinder</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="saladspinner" name="kitchenTool[]" value="Salad spinner">
                                                            <label class="radiobLable" for="meatgrinder">Salad spinner</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="juicer" name="kitchenTool[]" value="Juicer">
                                                            <label class="radiobLable" for="juicer">Juicer</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="blender" name="kitchenTool[]" value="Blender">
                                                            <label class="radiobLable" for="blender">Blender</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="ricecooker" name="kitchenTool[]" value="Rice Cooker">
                                                            <label class="radiobLable" for="ricecooker">Rice Cooker</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="toaster" name="kitchenTool[]" value="Toaster">
                                                            <label class="radiobLable" for="toaster">Toaster</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="waterpurifier" name="kitchenTool[]" value="Water purifier">
                                                            <label class="radiobLable" for="waterpurifier">Water purifier</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="slowcooker" name="kitchenTool[]" value="Slow cooker">
                                                            <label class="radiobLable" for="slowcooker">Slow cooker</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="sandwichmaker" name="kitchenTool[]" value="Sandwich maker">
                                                            <label class="radiobLable" for="sandwichmaker">Sandwich maker</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="foodprocessor" name="kitchenTool[]" value="Food processor">
                                                            <label class="radiobLable" for="foodprocessor">Food processor</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="microwaveoven" name="kitchenTool[]" value="Microwave oven">
                                                            <label class="radiobLable" for="microwaveoven">Microwave oven</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="breadmaker" name="kitchenTool[]" value="Bread Maker">
                                                            <label class="radiobLable" for="breadmaker">Bread Maker</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="castiron" name="kitchenTool[]" value="Cast Iron Skillet">
                                                            <label class="radiobLable" for="castiron">Cast Iron Skillet</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="dutchoven" name="kitchenTool[]" value="Dutch oven">
                                                            <label class="radiobLable" for="dutchoven">Dutch ovent</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="nonstick" name="kitchenTool[]" value="Non Stick Skillet">
                                                            <label class="radiobLable" for="nonstick">Non Stick Skillet</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="stockpot" name="kitchenTool[]" value="Stock Pot">
                                                            <label class="radiobLable" for="stockpot">Stock Pot</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="grillpan" name="kitchenTool[]" value="Grill Pan">
                                                            <label class="radiobLable" for="grillpan">Grill Pan</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="reemedbaking" name="kitchenTool[]" value="Reemed baking sheets">
                                                            <label class="radiobLable" for="reemedbaking">Reemed baking sheets</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="saucepans" name="kitchenTool[]" value="Saucepans">
                                                            <label class="radiobLable" for="saucepans">Saucepans</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="wok" name="kitchenTool[]" value="Wok">
                                                            <label class="radiobLable" for="wok">Wok</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="familygriddle" name="kitchenTool[]" value="Family Griddle">
                                                            <label class="radiobLable" for="familygriddle">Family Griddle</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="roastinpan" name="kitchenTool[]" value="Roasting Pans">
                                                            <label class="radiobLable" for="roastinpan">Roasting Pans</label>
                                                         </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                   <label>Cuisine Type</label>
                                                   <select id="mainCat" class="input" name="cuisine">
                                                       <option value="Hong Kong Style">Hong Kong Style</option>
                                                       <option value="Chinese">Chinese</option>
                                                       <option value="Cantonese">Cantonese</option>
                                                       <option value="Taiwanese">Taiwanese</option>
                                                       <option value="Japanese">Japanese</option>
                                                       <option value="Korean">Korean</option>
                                                       <option value="Thai">Thai</option>
                                                       <option value="Other Asians">Other Asians</option>
                                                       <option value="Italian">Italian</option>
                                                       <option value="French">French</option>
                                                       <option value="Western">Western</option>
                                                       <option value="Middle Eastern / Mediterranean">Middle Eastern / Mediterranean</option>
                                                       <option value="Latin American">Latin American</option>
                                                       <option value="Multinational">Multinational</option>
                                                   </select>
                                                </div>
                                                <div class="form-group cusnSub">
                                                    <label>Cuisine Subcategory</label>
                                                    <div class="input">
                                                        <ul id="subCat" class="serviceBlck">
                                                            <li class="catHeading">
                                                                Choose Subcategory
                                                            </li> 
                                                            <li>
                                                                <input class="radio" type="checkbox" id="Shunde">
                                                                <label class="radiobLable" for="Shunde">Shunde</label>
                                                            </li>
                                                            <li>
                                                                <input class="radio" type="checkbox" id="Beijing">
                                                                <label class="radiobLable" for="Beijing">Beijing</label>
                                                            </li>
                                                            <li class="mrRight">
                                                                <input class="radio" type="checkbox" id="Jingchuanhu">
                                                                <label class="radiobLable" for="Jingchuanhu">Jingchuanhu</label>
                                                            </li>
                                                            <li class="mrRight">
                                                                <input class="radio" type="checkbox" id="Guangxi">
                                                                <label class="radiobLable" for="Guangxi">Guangxi</label>
                                                            </li>
                                                            <li class="mrRight">
                                                                <input class="radio" type="checkbox" id="Northeastern">
                                                                <label class="radiobLable" for="Northeastern">Northeastern</label>
                                                            </li>
                                                            <li class="mrRight">
                                                                <input class="radio" type="checkbox" id="Shandong">
                                                                <label class="radiobLable" for="Shandong">Shandong</label>
                                                            </li>
                                                            <li class="mrRight">
                                                                <input class="radio" type="checkbox" id="Xinjiang">
                                                                <label class="radiobLable" for="Xinjiang">Xinjiang</label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="selectedCatBlock">
                                                </div>
                                            </div>
                                            <input type="hidden" name="cuisinname" id="cuisinname">
                                            <input type="hidden" name="subcuisinname" id="subcuisinname">
                                            <div class="col-md-12">
                                                <a id="addCat" href="javascript:;" class="float-left addMenuDish">Add</a>
                                                <a href="javascript:;" class="float-right addMenuDish">Save Details</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


           

<script>
   
        /*-----Utensil Box If user select On Site Cooking-----*/
        $(".untensilbox").hide();
            $("#onsiteCooking").click(function() {
            if($(this).is(":checked")) {
                $(".untensilbox").show();
            } else {
                $(".untensilbox").hide();
            }
        });


        jQuery("#MenuModal").modal('show');
        //ADD Menu in Right Block.....
        var catA = {
                "catA":["catASubItemA", "catASubItemB", "catASubItemC", "catASubItemD", "catASubItemE"],
                "catB":["catBSubItemA", "catBSubItemB", "catBSubItemC"],
                "catC":["catCSubItemA", "catCSubItemB", "catCSubItemC"],
                "catd":["catdSubItemA", "catdSubItemB", "catdSubItemC"]
            };
        //console.log(catA);
        var cuisne = "catA,catC";
        var subcuisne = "catASubItemA,catASubItemC,catCSubItemA,catCSubItemC,catCSubItemB";
        cuisne = cuisne.split(',');
        subcuisne = subcuisne.split(',');
        //console.log(cuisne);
        //console.log(subcuisne);
        var catAChecked = {};
        for( var i = 0, l = cuisne.length; i < l; i++ ) {
            console.log('main: ', cuisne[i]);
            var objectkey = cuisne[i];
            var exjectObj = catA[cuisne[i]];
            //console.log(exjectObj);
            catAChecked[objectkey] = [];
            catcheckedfun(exjectObj, objectkey);
        }
        function catcheckedfun(exjectObj, objectkey) {
            for( var i = 0, l = exjectObj.length; i < l; i++ ) {
                //console.log('sub: ', exjectObj[i]);
                var a = subcuisne.indexOf(exjectObj[i]);
                if (a != '-1') {
                    catAChecked[objectkey].push(subcuisne[a]);
                }
            }
        }

        console.log('catAChecked: ',catAChecked);

        $("#mainCat").html('');
        var mianCatHtml = '';
            $.each(catA, function(key, value){
                mianCatHtml += '<option value="'+key+'">'+key+'</option>';
                //console.log('key------', key);
            });
        $("#mainCat").html(mianCatHtml);    
        //console.log(catA);
        var firstCat = 'catA';
        
        subCat(firstCat);

        function subCat(firstCat) {
            $("#subCat").html('');
            var subCathtml = '<li class="catHeading">'+firstCat+'</li>';
            for (var i = 0; i < catA[firstCat].length; i++) {
                subCathtml += '<li> <input class="radio" type="checkbox" value="'+catA[firstCat][i]+'" id="Shunde'+i+'">';
                subCathtml += '<label class="radiobLable" for="Shunde'+i+'">'+catA[firstCat][i]+'</label> </li>';
            }
            //console.log(subCathtml);
            $("#subCat").html(subCathtml);
            if (catAChecked[firstCat]) {
                var obj = catAChecked[firstCat];
                //console.log(firstCat+': has value');
                for( var i = 0, lingth = obj.length; i < lingth; i++ ) {
                    var chval = obj[i];
                    //console.log(chval);
                    $('#subCat input[value="'+chval+'"]').attr('checked','checked');
                }
            }
        }

        $("#mainCat").change(function(){
            firstCat = $(this).val();
            subCat(firstCat);
        });

        
        $("#addCat").click(function(){
            var label = $("#subCat").find('.catHeading').text();
            delete catAChecked[label]; 
            $("#subCat .radio").each(function(){
                if($(this).is(':checked')) {
                    var checkedVal = $(this).val();
                    if (catAChecked[label]) {
                        catAChecked[label].push(checkedVal);
                    } else {
                        catAChecked[label] = [];
                        catAChecked[label].push(checkedVal);
                    }
                } 
            });
            //console.log('click add btn: ', catAChecked);
            catblock();
        });

        /*-----For Added Menus Remove----*/
        $(".selectedCatBlock").on('click', '.close', function(){
            var lenth = $(this).parents('.form-group').find('li').length;
            var key = $(this).parents('.form-group').find('.clr-black').attr("title");
            var subcatval = $(this).parent('li').find('.txt').text();
            //console.log('subcatval', subcatval);
            if (lenth == 1) {
                //$(this).parents('.form-group').remove();
                delete catAChecked[key];
            } else {
                //$(this).parent('li').remove();
                catAChecked[key].splice($.inArray(subcatval, catAChecked[key]),1);
            }
            catblock();
            firstCat = key;
            subCat(firstCat);
        });
        //selected cat block html
        catblock();
        function catblock() {
            $(".selectedCatBlock").html('');
            //console.log(catAChecked);
            var catListhtml = '';
            $.each(catAChecked, function(key, value){
                //console.log('test: ',key);
                if (catAChecked[key].length !== 0) {
                    //console.log('this null ', catAChecked[key]);
                    catListhtml += '<div class="form-group"><div class="cat-box">';
                    catListhtml += '<h4 title="'+key+'" class="clr-black">'+key+'</h4> <ul class="subCatList">';
                    for (var i = 0; i < catAChecked[key].length; i++) {
                        //console.log('subcheck : ',catAChecked[key][i]);
                        catListhtml += '<li><span class="float-left txt">'+catAChecked[key][i]+'</span> <span class="float-left close">X</span></li>';
                    }
                    catListhtml += '</ul></div></div>';
                }
            });
            $(catListhtml).appendTo('.selectedCatBlock');
        }


        $(".addMenuDish.float-right").click(function(){
            console.log(catAChecked);
            //var newstring = '';
            cuisne = '';
            subcuisne = '';
            //console.log(catAChecked);
            $.each(catAChecked, function(key, value){
                var objkey = key;
                //newstring += '|'+objkey+',';
                cuisne += objkey+',';
                newstringsub(objkey);
            });
            function newstringsub(objkey){
                for (var i = 0; i < catAChecked[objkey].length; i++) {
                    subcuisne += catAChecked[objkey][i]+',';
                    /*if (catAChecked[objkey].length == i+1) {
                        newstring += catAChecked[objkey][i];
                    } else {
                        newstring += catAChecked[objkey][i]+',';
                    }*/
                }
            }
            //console.log(newstring);
            console.log("cuisne", cuisne);
            console.log("subcuisne", subcuisne);
        });


  
</script>


</body>
</html>