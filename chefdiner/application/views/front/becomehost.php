<!DOCTYPE html>
<html lang="en">
<head>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title><?php echo $this->lang->line('sitetitle'); ?></title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/become-host.css">
</head>
<body>
<?php
$this->load->view('front/header');
?>

<div class="wrapper becomeHost">

    <div class="container">
    
        <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
        <?php endif;?>
        
        <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
        <?php endif;?>
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <div class="hostFormSec">
                    <h1 class="clr-black">Become a host at ChefDiner</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p>
                    <form class="hostform" action="<?php echo base_url()?>UserbecomeHost" method="post">
                        <div class="ques clr-black">
                            <p>How did you learn to cook?</p>
                            <ul class="answerBlck"> 
                                <li>
                                    <input class="radio" type="radio" id="Family" name="learnCook" value="My Family">
                                    <label class="radiobLable" for="Family"> My Family</label>
                                </li>
                                <li>
                                    <input class="radio" type="radio" id="books" name="learnCook" value="Recipe books">
                                    <label class="radiobLable" for="books">Recipe books</label>
                                </li>
                                <li class="mrRight">
                                    <input class="radio" type="radio" id="Trainings" name="learnCook" value="Professional Trainings">
                                    <label class="radiobLable" for="Trainings">Professional Trainings</label>
                                </li>
                            </ul>
                        </div>
                        <div class="ques clr-black">
                            <p>How many people can you host for a dinner?</p>
                            <ul class="answerBlck"> 
                                <li>
                                    <input class="radio" type="radio" id="up5" name="seatrange" value="up5">
                                    <label class="radiobLable" for="up5"> Up to 5</label>
                                </li>
                                <li>
                                    <input class="radio" type="radio" id="6to10" name="seatrange" value="6to10">
                                    <label class="radiobLable" for="6to10">From 6 to 10</label>
                                </li>
                                <li class="mrRight">
                                    <input class="radio" type="radio" id="mr10" name="seatrange" value="mr10">
                                    <label class="radiobLable" for="mr10">more than 10</label>
                                </li>
                            </ul>
                        </div>
                        <div class="ques clr-black">
                            <p>How often do you want to cook?</p>
                            <ul class="answerBlck"> 
                                <li>
                                    <input class="radio" type="radio" id="Everyday" name="cookfood" value="Everyday">
                                    <label class="radiobLable" for="Everyday"> Everyday</label>
                                </li>
                                <li>
                                    <input class="radio" type="radio" id="coupleTime" name="cookfood" value="A couple times a week">
                                    <label class="radiobLable" for="coupleTime">A couple times a week</label>
                                </li>
                                <li class="mrRight">
                                    <input class="radio" type="radio" id="onceweek" name="cookfood" value="Once a week">
                                    <label class="radiobLable" for="onceweek">Once a week</label>
                                </li>
                            </ul>
                        </div>
                        <div class="ques clr-black">
                            <p>Service Type</p>
                            <ul class="serviceBlck"> 
                                <li>
                                    <input class="radio" type="checkbox" id="FoodDelivery" name="serveType[]" value="Food delivery">
                                    <label class="radiobLable" for="FoodDelivery"> Food Delivery</label>
                                </li>
                                <li>
                                    <input class="radio" type="checkbox" id="PrivateMeal" name="serveType[]" value="Home private meal">
                                    <label class="radiobLable" for="PrivateMeal">Home Private Meal</label>
                                </li>
                                <li class="mrRight">
                                    <input class="radio" type="checkbox" id="DailyMeal" name="serveType[]" value="Daily meal">
                                    <label class="radiobLable" for="DailyMeal">Daily Meal</label>
                                </li>
                            </ul>
                            <ul class="serviceBlck"> 
                                <li>
                                    <input class="radio" type="checkbox" id="RestaurantPrivate" name="serveType[]" value="Restaurant private meal">
                                    <label class="radiobLable" for="RestaurantPrivate">Restaurant Private Meal</label>
                                </li>
                                <li>
                                    <input class="radio" type="checkbox" id="OnsiteCooking" name="serveType[]" value="Chef onsite cooking">
                                    <label class="radiobLable" for="OnsiteCooking">Chef Onsite Cooking</label>
                                </li>
                                <li class="mrRight">
                                    <input class="radio" type="checkbox" id="CookingWorkshop" name="serveType[]" value="Cooking workshop">
                                    <label class="radiobLable" for="CookingWorkshop">Cooking Workshop</label>
                                </li>
                            </ul>
                        </div>
                        <div class="currency ques clr-black">
                            <p>Select Currency</p>
                            <div class="form-group">
                                <div class="flagstrap" id="select_host_currency" data-input-name="NewBuyer_country" data-selected-country=""></div>
                            </div>

                            <input id="currency" type = "hidden" name="currency" value = "" />
                        </div>
                        <div class="ques">
                            <input type="submit" name="submit" value="Continue">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="hostFormSec no-brdr">
                    <h2 class="clr-black">Join ChefDiner & Earn <span><a href="#" class="clr-red">(Term & Condition)</a></span></h2>
                    <div class="reanBnr">
                        <img src="<?php echo base_url();?>front/images/earnbnr.jpg" alt="">
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper howItWorksSec">
    <div class="container">
            <h2 class="text-center clr-black">How it works</h2>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="text-center hwitBox">
                    <div class="img">
                        <img src="<?php echo base_url();?>front/images/cheficon.png" alt="">
                    </div>
                    <h5 class="clr-black">
                        Create your chef's profile
                    </h5>
                    <p>
                       You will set up your chef's profile with different kinds of services and menus, adding wonderful pictures and give your contact details which will only be displayed after you approve the reservations.
                    </p>
                </div>    
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="text-center hwitBox">
                    <div class="img">
                        <img src="<?php echo base_url();?>front/images/booking-recive.png" alt="">
                    </div>
                    <h5 class="clr-black">
                        Receive and approve bookings
                    </h5>
                    <p>
                        Guests will choose one of your menus, select the service they require and select the date and the number of seats for the booking. You can then reply to guest’s messages and approve or reject their booking requests. After approval, they will be able to pay for it.
                    </p>
                </div>    
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="text-center hwitBox">
                    <div class="img">
                        <img src="<?php echo base_url();?>front/images/get-paid.png" alt="">
                    </div>
                    <h5 class="clr-black">
                        Host dinners and get paid
                    </h5>
                    <p>
                        After a successful payment, you will be notified. That’s the time for you to get ready to provide service! Surely you can always contact your guests via ChefDiner. After the event, you will get paid!
                    </p>
                </div>    
            </div>
        </div>
    </div>
</div>

<div class="wrapper joinWith">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="joinEarn">
                    <p>You will earn HK$36,000 monthly. Start hosting!</p>
                    <a href="" class=""><img src="<?php echo base_url();?>front/images/fbjoin.png" alt=""></a>
                    <a href="" class=""><img src="<?php echo base_url();?>front/images/emailsign.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>
<script>
$('input:radio').click(function() {
    $('input:radio[name='+$(this).attr('name')+']').parent().removeClass('active');
    $(this).parent().addClass('active');
});
</script>



</body>
</html>