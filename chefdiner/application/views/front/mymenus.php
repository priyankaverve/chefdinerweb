<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/my-kitchen.css">


<style type="text/css">
.dishImg {
    position: relative;
}

.dishImg label{
     position: absolute;
    width: 157px;
    height: 118px;
    z-index: 3;
    background: rgba(0,0,0,0.1);
}


    .upldedImg {
    position: absolute;
    width: 157px;
    height: 118px;
    overflow: hidden;
    top: 25px;
    border-radius: 4px;
    z-index: 2;
}
.upldedImg img{
    max-width: 100%
   }
</style>
</head>
<body>
<?php
$this->load->view('front/header'); ?>
<div class="wrapper mykitchenPage">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h1 class="clr-black"><?php if(isset($getchefdata[0]['kitchen_title'])){ echo $getchefdata[0]['kitchen_title'];}else{
                  echo ucfirst($this->session->userdata('first_name'))."'s Kitchen";
                    }?> <a href="<?php echo base_url() ?>chefdetail">Preview</a></h1>
                <p class="clr-gray"><span class="clr-red">2</span> steps to become a chef!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="KtchnNav">
                    <ul>
                        <li class="kitchen"><a href="mykitchen">My Kitchen</a></li>
                        <li class="profile"><a href="myprofile">Profile</a></li>
                        <li class="address"><a href="myaddress">Address</a></li>
                        <li class="photos"><a href="myphotos">Photos</a></li>
                        <li class="ktchnmenu active"><a href="mymenus">Menu</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 col-md-9">
                <div class="kithchenForm picUploadPage">
                    <div class="form-group sbmt-row fileUpload">
                        <a href="javascript:;" class="txt-upr addMenu float-left">Add New Menu</a>
                    </div>
                    <div class="row clearFix">
                        <div class="col-lg-7">
                            <div class="menuItemSection">
                            <?php 
                            if(!empty($getmenus)){  
                                foreach ($getmenus as $key => $value) {
                                    if($value['menu_image']){
                                        $menu_image = base_url().'assets/menuImgae/'.$value['menu_image'];

                                    }else{
                                        $menu_image =base_url().'front/images/blank.jpg';
                                    }

                                     ?>
                                    
                                    
                                <div class="menuItemBlock">
                                    <div class="itmeDetail">
                                        <h4 class="clr-red ItmName"><a href="javascript:;" onclick='getsubmenu(<?php echo $value['id'] ?>)' class="clr-red"><?=$value['menu_title']?></a></h4>
                                        <p class="itmPrc clr-gray"><?=$value['currency']?> <?=$value['actualprice']?> per guest</p>
                                        <div class="srvcType">
                                            <h5>Service Type:-</h5>
                                            <p class="clr-gray"><?=$value['service_type']?></p>
                                        </div>
                                        <div class="itmCusinType srvcType">
                                            <h5>Cuisine type :-</h5>
                                            <p class="clr-gray"><?=$value['cusine']?></p>
                                        </div>
                                    </div>  
                                    <div class="itemIng">
                                        <div class="img">
                                            <img src="<?php echo $menu_image;?>" alt="">
                                        </div>
                                    </div>
                                    <?php
                                    if($value['status'] != '1'){
                                        $class='draftmenu';
                                        $text = 'Publish';
                                    }else{
                                        $class='';
                                        $text = 'Draft';
                                    }
                                    ?>
                                    <div class="menuItemOpt">
                                        <ul>
                                            <li class="up"><a href="javascript:;">Up</a></li>
                                            <li class="down"><a href="javascript:;">Down</a></li>
                                            <li><a href="javascript:;" onclick="draftMenu(<?php echo $value['id'] ?>,<?php echo $value['status'] ?>)" class="<?=$class;?>" ><?=$text;?></a></li>
                                            <li><a href="javascript:;" dataid="<?php echo $value['id'] ?>" class="editMenus">Edit</a></li>
                                            <li><a href="javascript:;" onclick="deleteMenu(<?php echo $value['id'] ?>)" >Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <?php  }
                                } ?>

                            </div>
                        </div>
                        <div class="col-lg-5" id="DishDiv" style="display: none;">
                        <div id="submenuDIv"></div>
                           
                               <!--  <div class="itmeDetail">
                                    <h4 class="clr-black ItmName">Gong Bao Chicken</h4>
                                    <p class="itmPrc clr-gray">Main Course</p>
                                </div>  
                                <div class="itemIng">
                                    <div class="img">
                                        <img src="images/chefitem.png" alt="">
                                    </div>
                                </div>
                                <div class="menuItemOpt">
                                    <ul>
                                        <li class="up"><a href="javascript:;">Up</a></li>
                                        <li class="down"><a href="javascript:;">Down</a></li>
                                        <li><a href="javascript:;">Draft</a></li>
                                        <li><a href="javascript:;">Edit</a></li>
                                        <li><a href="javascript:;">Delete</a></li>
                                    </ul>
                                </div> -->
                                <div class="form-group sbmt-row fileUpload">
                                    <!-- <a href="javascript:;" id="addDish" class="txt-upr addDish float-right">Add New Dish</a> -->
                                    <button id="addDish" class="txt-upr addDish float-right">Add New Dish</button>
                                </div>
                                
                        </div>
                    </div>
                    <div class="form-group sbmt-row">
                        <a href="javascript:;" class="txt-upr sbmt float-left">Complete</a>
                        <a href="#" class="txt-upr prvw float-right">Preview</a>
                    </div>
                    <div class="modal fade menuModel" id="MenuModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2 class="clr-black">Menu Description</h2>
                                        </div>
                                        <div class="col-md-12" id="statushostMsg"></div>
                                    </div>
                                    <form>
                                    <input type="hidden" name="chefid" id="chefid" value="<?php if(isset($getchefdata[0]['user_id'])){ echo $getchefdata[0]['user_id'];}?>">
                                  
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="clr-gray">Menu Title</label>    
                                                    <input type="text" class="input" name="menutitle" id="menutitle" required>
                                                </div>
                                                <div class="form-group menuPrice">
                                                    <label class="clr-gray">Price Per Person</label>    
                                                    <input type="number" min="0" class="input float-left" name="price_per_person" id="price_per_person">
                                                   <div class="flagstrap float-left" id="select_nenu_currency" data-input-name="NewBuyer_country" data-selected-country=""></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group dishImg">
                                                    <label class="clr-red" for="dishImgUpload">Upload Image</label>
                                                    <input type="file" id="dishImgUpload" name="fileToUpload">
                                                    <div class="upldedImg" id="upldedImg1">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <h4 class="clr-gray">Service Type</h4>    
                                                    <ul class="serviceBlck menuService"> 
                                                        <li>
                                                            <input class="radio" type="checkbox" id="FoodDelivery" name="serveType[]" value="Food delivery">
                                                            <label class="radiobLable" for="FoodDelivery"> Food Delivery</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="PrivateMeal" name="serveType[]" value="Home private meal">
                                                            <label class="radiobLable" for="PrivateMeal">Home Private Meal</label>
                                                        </li>
                                                        <li class="mrRight">
                                                            <input class="radio" type="checkbox" id="DailyMeal" name="serveType[]" value="Daily meal">
                                                            <label class="radiobLable" for="DailyMeal">Daily Meal</label>
                                                        </li>
                                                        <li class="">
                                                            <input class="radio" type="checkbox" id="privateMeal" name="serveType[]" value="Restaurant private meal">
                                                            <label class="radiobLable" for="privateMeal">Restaurant Private Meal</label>
                                                        </li>
                                                        <li class="">
                                                            <input class="radio" type="checkbox" id="onsiteCooking" name="serveType[]" value="Chef onsite cooking">
                                                            <label class="radiobLable" for="onsiteCooking">Chef Onsite Cooking</label>
                                                        </li>
                                                        <li class="">
                                                            <input class="radio" type="checkbox" id="cookingWorkshop" name="serveType[]" value="Cooking workshop">
                                                            <label class="radiobLable" for="cookingWorkshop">Cooking Workshop</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="form-group untensilbox">
                                                    <p>
                                                        Please kindly let chef know what tools you can provide at your place.
                                                    </p>
                                                    <ul class="serviceBlck menuService utensilList"> 
                                                        <li>
                                                            <input class="radio" type="checkbox" id="potatoMasher" name="kitchenTool[]" value="Potato masher">
                                                            <label class="radiobLable" for="potatoMasher"> Potato masher</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="measuringCups" name="kitchenTool[]" value="Measuring cups/jug and spoons">
                                                            <label class="radiobLable" for="measuringCups">Measuring cups/jug and spoons</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="chefKnife" name="kitchenTool[]" value="Chef Knife">
                                                            <label class="radiobLable" for="chefKnife">Chef Knife</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="parkingKnife" name="kitchenTool[]" value="Paring Knife">
                                                            <label class="radiobLable" for="parkingKnife">Paring Knife</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="potatoricer" name="kitchenTool[]" value="Potato ricer">
                                                            <label class="radiobLable" for="potatoricer"> Potato ricer</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="ovenMitt" name="kitchenTool[]" value="Oven mitt/glove">
                                                            <label class="radiobLable" for="ovenMitt">Oven mitt/glove</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="serrated" name="kitchenTool[]" value="Serrated/Bread Knife">
                                                            <label class="radiobLable" for="serrated">Serrated/Bread Knife</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="vegetablepeelers" name="kitchenTool[]" value="Vegetable peelers">
                                                            <label class="radiobLable" for="vegetablepeelers">Vegetable peelers</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="sharpening" name="kitchenTool[]" value="Sharpening Steel">
                                                            <label class="radiobLable" for="sharpening">Sharpening Steel</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="garlicpress" name="kitchenTool[]" value="Garlic press">
                                                            <label class="radiobLable" for="garlicpress">Garlic press</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="bbqtools" name="kitchenTool[]" value="BBQ Tools">
                                                            <label class="radiobLable" for="bbqtools">BBQ Tools</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="electricsharpener" name="kitchenTool[]" value="Electric Sharpener" >
                                                            <label class="radiobLable" for="electricsharpener">Electric Sharpener</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="cuttingboard" name="kitchenTool[]" value="Egg separater">
                                                            <label class="radiobLable" for="cuttingboard">Cutting Board</label>
                                                        </li>

                                                        <li>
                                                            <input class="radio" type="checkbox" id="eggseparater" name="kitchenTool[]" value="Egg separater">
                                                            <label class="radiobLable" for="eggseparater">Egg separater</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="choppers" name="kitchenTool[]" value="Choppers">
                                                            <label class="radiobLable" for="choppers">Choppers</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="fishscaler" name="kitchenTool[]" value="Fish scaler">
                                                            <label class="radiobLable" for="choppers">Fish scaler</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="lemonpress" name="kitchenTool[]" value="Lemon press/Lemon squeezer">
                                                            <label class="radiobLable" for="lemonpress">Lemon press/Lemon squeezer</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="icecreamscoop" name="kitchenTool[]" value="Ice cream scoop">
                                                            <label class="radiobLable" for="icecreamscoop">Ice cream scoop</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="peppershakers" name="kitchenTool[]" value="Pepper mill,salt & pepper shakers">
                                                            <label class="radiobLable" for="peppershakers">Pepper mill,salt & pepper shakers</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="tongs" name="kitchenTool[]" value="Tongs">
                                                            <label class="radiobLable" for="tongs"> </label>
                                                        </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="roolingpin" name="kitchenTool[]" value="Rolling pin">
                                                            <label class="radiobLable" for="roolingpin">Rolling pin</label>
                                                        </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="canopener" name="kitchenTool[]" value="Can opener">
                                                            <label class="radiobLable" for="canopener">Can opener</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="meatgrinder" name="kitchenTool[]" value="Meat grinder">
                                                            <label class="radiobLable" for="meatgrinder">Meat grinder</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="saladspinner" name="kitchenTool[]" value="Salad spinner">
                                                            <label class="radiobLable" for="meatgrinder">Salad spinner</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="juicer" name="kitchenTool[]" value="Juicer">
                                                            <label class="radiobLable" for="juicer">Juicer</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="blender" name="kitchenTool[]" value="Blender">
                                                            <label class="radiobLable" for="blender">Blender</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="ricecooker" name="kitchenTool[]" value="Rice Cooker">
                                                            <label class="radiobLable" for="ricecooker">Rice Cooker</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="toaster" name="kitchenTool[]" value="Toaster">
                                                            <label class="radiobLable" for="toaster">Toaster</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="waterpurifier" name="kitchenTool[]" value="Water purifier">
                                                            <label class="radiobLable" for="waterpurifier">Water purifier</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="slowcooker" name="kitchenTool[]" value="Slow cooker">
                                                            <label class="radiobLable" for="slowcooker">Slow cooker</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="sandwichmaker" name="kitchenTool[]" value="Sandwich maker">
                                                            <label class="radiobLable" for="sandwichmaker">Sandwich maker</label>
                                                         </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="foodprocessor" name="kitchenTool[]" value="Food processor">
                                                            <label class="radiobLable" for="foodprocessor">Food processor</label>
                                                        </li>
                                                        <li>
                                                            <input class="radio" type="checkbox" id="microwaveoven" name="kitchenTool[]" value="Microwave oven">
                                                            <label class="radiobLable" for="microwaveoven">Microwave oven</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="breadmaker" name="kitchenTool[]" value="Bread Maker">
                                                            <label class="radiobLable" for="breadmaker">Bread Maker</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="castiron" name="kitchenTool[]" value="Cast Iron Skillet">
                                                            <label class="radiobLable" for="castiron">Cast Iron Skillet</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="dutchoven" name="kitchenTool[]" value="Dutch oven">
                                                            <label class="radiobLable" for="dutchoven">Dutch ovent</label>
                                                         </li>
                                                         <li>
                                                            <input class="radio" type="checkbox" id="nonstick" name="kitchenTool[]" value="Non Stick Skillet">
                                                            <label class="radiobLable" for="nonstick">Non Stick Skillet</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="stockpot" name="kitchenTool[]" value="Stock Pot">
                                                            <label class="radiobLable" for="stockpot">Stock Pot</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="grillpan" name="kitchenTool[]" value="Grill Pan">
                                                            <label class="radiobLable" for="grillpan">Grill Pan</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="reemedbaking" name="kitchenTool[]" value="Reemed baking sheets">
                                                            <label class="radiobLable" for="reemedbaking">Reemed baking sheets</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="saucepans" name="kitchenTool[]" value="Saucepans">
                                                            <label class="radiobLable" for="saucepans">Saucepans</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="wok" name="kitchenTool[]" value="Wok">
                                                            <label class="radiobLable" for="wok">Wok</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="familygriddle" name="kitchenTool[]" value="Family Griddle">
                                                            <label class="radiobLable" for="familygriddle">Family Griddle</label>
                                                         </li>
                                                          <li>
                                                            <input class="radio" type="checkbox" id="roastinpan" name="kitchenTool[]" value="Roasting Pans">
                                                            <label class="radiobLable" for="roastinpan">Roasting Pans</label>
                                                         </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                   <label>Cuisine Type</label>
                                                   <select id="mainCat" class="input" name="cuisine">
                                                       <option value="Hong Kong Style">Hong Kong Style</option>
                                                       <option value="Chinese">Chinese</option>
                                                       <option value="Cantonese">Cantonese</option>
                                                       <option value="Taiwanese">Taiwanese</option>
                                                       <option value="Japanese">Japanese</option>
                                                       <option value="Korean">Korean</option>
                                                       <option value="Thai">Thai</option>
                                                       <option value="Other Asians">Other Asians</option>
                                                       <option value="Italian">Italian</option>
                                                       <option value="French">French</option>
                                                       <option value="Western">Western</option>
                                                       <option value="Middle Eastern / Mediterranean">Middle Eastern / Mediterranean</option>
                                                       <option value="Latin American">Latin American</option>
                                                       <option value="Multinational">Multinational</option>
                                                   </select>
                                                </div>
                                                <div class="form-group cusnSub">
                                                    <label>Cuisine Subcategory</label>
                                                    <div class="input">
                                                        <ul id="subCat" class="serviceBlck">
                                                        
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="selectedCatBlock">
                                                </div>
                                            </div>
                                            <input type="hidden" name="menuimg" id="menuimg">
                                            <input type="hidden" name="menuid" id="menuid" value="">
                                            <input type="hidden" name="cuisinname" id="cuisinname" value="">
                                            <input type="hidden" name="subcuisinname" id="subcuisinname" value="">
                                            <div class="col-md-12">
                                                <a id="addCat" href="javascript:;" class="float-left addMenuDish">Add</a>
                                                <a href="javascript:;" class="float-right addMenuDish">Save Details</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                    
                    <div class="modal fade dishModal" id="DishModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h2 class="clr-black">Dish Description</h2>
                                        </div>
                                        <div class="col-md-12" id="statusdishMsg"></div>
                                    </div>
                                    <form>
                                    <input type="hidden" name="submenuid" id="submenuid">
                                    <input type="hidden" name="submenuimg" id="submenuimg">
                                    <input type="hidden" name="menuidfordish" id="menuidfordish">
                                    <input type="hidden" name="chefids" id="chefids" value="<?php if(isset($getchefdata[0]['user_id'])){ echo $getchefdata[0]['user_id'];}?>">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="clr-gray">Dish Title</label>    
                                                    <input type="text" class="input" id="dishtitle">
                                                </div>
                                                <div class="form-group">
                                                    <label>Course Type</label>
                                                    <select id="mainCat1" class="input">
                                                    <option value="Appetizer">Appetizer</option>
                                                    <option value="Starter">Starter</option>
                                                    <option value="Soup">Soup</option>
                                                    <option value="Salad">Salad</option>
                                                    <option value="Main Course">Main Course</option>
                                                    <option value="Dessert">Dessert</option>
                                                    <option value="Drink">Drink</option>
                                                        
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group dishImg">
                                                    <label class="clr-red" for="dishImgUpload1">Upload Image</label>
                                                    <input type="file" id="dishImgUpload1">
                                                    <div class="upldedImg" id="upldedImg">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                               <button class="float-right addMenuDish" onclick="savemenuDish();">Save Details</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php $this->load->view('front/footer'); ?>

<script>
    var mianCatHtml;
    var catA = {
                "Hong Kong Style":["Hong Kong Style"],
                "Chinese":["All Chinese", "Guangdong", "Chiu Chow","Hakka","Sichuan","Shanghai","Yunnan","Hunan","Taiwan","Shunde","Beijing","Jingchuanhu","Guangxi","Northeastern","Shandong","Xinjiang","Village Food","Fujian","Jiang-Zhe","Guizhou","Shanxi(Shan)","Mongolia","Shanxi(Jin)","Hubei","Huaiyang"],
                "Cantonese":["All Cantonese", "Guangdong", "Chiu Chow","Hakka","Shunde"],
                "Taiwanese":["Taiwanese"],
                "Japanese":["Japanese"],
                "Korean":["Korean"],
                "Thai":["Thai"],
                "Other Asians":["All Asian", "Vietnamese", "Indonesian","Singaporean","Malaysian","Philippines","Indian","Nepalese","Sri Lanka"],
                "Italian":["Italian"],
                "French":["French"],
                "Western":["All Western", "Italian", "French","American","British","Spanish","German","Belgian","Australian","Portuguese","Swiss","Irish","Russian","Dutch","Austrian","Western"],
                "Middle Eastern / Mediterranean":["All Middle Eastern / Mediterranean", "Middle Eastern", "Mediterranean","Turkish","Lebanon","Moroccan","Egyptian","Afrian","Jewish","Greek"],
                "Latin American":["All Latin American", "Mexican", "Cuba","Aargentinian","Peruvian","Brazillian"],
                "Multinational":["Multinational"]
                
            };
    var cuisne;
    var subcuisne;
    var catAChecked = {};
    firstCat = '';

    jQuery(document).ready(function () {
      //  jQuery("#addDish").attr('disabled', 'disabled');


        /*-----Utensil Box If user select On Site Cooking-----*/
        $(".untensilbox").hide();
            $("#onsiteCooking").click(function() {
            if($(this).is(":checked")) {
                $(".untensilbox").show();
            } else {
                $(".untensilbox").hide();
            }
        });


       jQuery(".addMenu").click(function(){
            jQuery("#MenuModal").modal('show');
            jQuery('#select_nenu_currency').attr('data-selected-country','HKD');
            jQuery('#select_nenu_currency').flagStrap();

            cuisne = '';
            subcuisne = '';
            cuisne = cuisne.split(',');
            subcuisne = subcuisne.split(',');
           // alert(cuisne+subcuisne);
            
          //  console.log(cuisne);
          //  console.log(subcuisne);
            catAChecked = {};
            if (cuisne != '') {
                for( var i = 0, l = cuisne.length; i < l; i++ ) {
                    console.log('main: ', cuisne[i]);
                    var objectkey = cuisne[i];
                    var exjectObj = catA[cuisne[i]];
                    //console.log(exjectObj);
                    catAChecked[objectkey] = [];
                    catcheckedfun(exjectObj, objectkey);
                }
                function catcheckedfun(exjectObj, objectkey) {
                    for( var i = 0, l = exjectObj.length; i < l; i++ ) {
                        //console.log('sub: ', exjectObj[i]);
                        var a = subcuisne.indexOf(exjectObj[i]);
                        if (a != '-1') {
                            catAChecked[objectkey].push(subcuisne[a]);
                        }
                    }
                }
            }

            console.log('catAChecked: ',catAChecked);

            $("#mainCat").html('');
                mianCatHtml = '';
                $.each(catA, function(key, value){
                    mianCatHtml += '<option value="'+key+'">'+key+'</option>';
                    //console.log('key------', key);
                });
            $("#mainCat").html(mianCatHtml);    
            //console.log(catA);
            firstCat = 'Hong Kong Style';
            
            subCat(firstCat);

           
        });
        
        function subCat(firstCat) {
            $("#subCat").html('');
            var subCathtml = '<li class="catHeading">'+firstCat+'</li>';
            for (var i = 0; i < catA[firstCat].length; i++) {
                subCathtml += '<li> <input class="radio" type="checkbox" value="'+catA[firstCat][i]+'" id="Shunde'+i+'">';
                subCathtml += '<label class="radiobLable" for="Shunde'+i+'">'+catA[firstCat][i]+'</label> </li>';
            }
            //console.log(subCathtml);
            $("#subCat").html(subCathtml);
            if (catAChecked[firstCat]) {
                var obj = catAChecked[firstCat];
                //console.log(firstCat+': has value');
                for( var i = 0, lingth = obj.length; i < lingth; i++ ) {
                    var chval = obj[i];
                    //console.log(chval);
                    $('#subCat input[value="'+chval+'"]').attr('checked','checked');
                }
            }
            }

        $("#mainCat").change(function(){
            firstCat = $(this).val();
            subCat(firstCat);
        });

        
        $("#addCat").click(function(){
            var label = $("#subCat").find('.catHeading').text();
            delete catAChecked[label]; 
            $("#subCat .radio").each(function(){
                if($(this).is(':checked')) {
                    var checkedVal = $(this).val();
                    if (catAChecked[label]) {
                        catAChecked[label].push(checkedVal);
                    } else {
                        catAChecked[label] = [];
                        catAChecked[label].push(checkedVal);
                    }
                } 
            });
            //console.log('click add btn: ', catAChecked);
            catblock();
        });

        /*-----For Added Menus Remove----*/
        $(".selectedCatBlock").on('click', '.close', function(){
            var lenth = $(this).parents('.form-group').find('li').length;
            var key = $(this).parents('.form-group').find('.clr-black').attr("title");
            var subcatval = $(this).parent('li').find('.txt').text();
            //console.log('subcatval', subcatval);
            if (lenth == 1) {
                //$(this).parents('.form-group').remove();
                delete catAChecked[key];
            } else {
                //$(this).parent('li').remove();
                catAChecked[key].splice($.inArray(subcatval, catAChecked[key]),1);
            }
            catblock();
            firstCat = key;
            subCat(firstCat);
        });
        //selected cat block html
        catblock();
        function catblock() {
            $(".selectedCatBlock").html('');
            //console.log(catAChecked);
            var catListhtml = '';
            $.each(catAChecked, function(key, value){
                //console.log('test: ',key);
                if (catAChecked[key].length !== 0) {
                    //console.log('this null ', catAChecked[key]);
                    catListhtml += '<div class="form-group"><div class="cat-box">';
                    catListhtml += '<h4 title="'+key+'" class="clr-black">'+key+'</h4> <ul class="subCatList">';
                    for (var i = 0; i < catAChecked[key].length; i++) {
                        //console.log('subcheck : ',catAChecked[key][i]);
                        catListhtml += '<li><span class="float-left txt">'+catAChecked[key][i]+'</span> <span class="float-left close">X</span></li>';
                    }
                    catListhtml += '</ul></div></div>';
                }
            });
            $(catListhtml).appendTo('.selectedCatBlock');
        }
        var menuimage;


        $(".addMenuDish.float-right").click(function(){
            // /alert(menuimage);
           // console.log(catAChecked);
            //var newstring = '';
            cuisne = [];
            subcuisne = [];
            //console.log(catAChecked);
            $.each(catAChecked, function(key, value){
                var objkey = key;
                //newstring += '|'+objkey+',';
                cuisne.push(objkey);

               // cuisne += objkey+',';
                newstringsub(objkey);
            });
            function newstringsub(objkey){
                for (var i = 0; i < catAChecked[objkey].length; i++) {
                    //subcuisne += catAChecked[objkey][i]+',';
                   subcuisne.push(catAChecked[objkey][i]);
                }
            }
            var menuid = $('#menuid').val();
            var chefid = $('#chefid').val();
            var cuisine = cuisne.join(',');
            var subcuisnes = subcuisne.join(',');
            var menutitle = $('#menutitle').val();
            var personprice = $("input[name=price_per_person]").val();
            var menuimg = $('#menuimg').val();
                 if(menuimage =='' ){
                    menuimage = menuimg;
                 }
            var select_nenu_currency = $('#select_nenu_currency option:selected').text();
            var serType = $("input:checkbox[name='serveType[]']:checked").map(function() {return this.value;}).get().join(',');
            var kitchenTool = $("input:checkbox[name='kitchenTool[]']:checked").map(function() {return this.value;}).get().join(',');
           
            

            if (menutitle == ''|| personprice == '' || select_nenu_currency == '' || serType == '' || 
            subcuisnes == '' || cuisine =='' || menuimage =='') {
/*alert("Please fill all fields...!!!!!!");*/

                $('#statushostMsg').html('<span style="color:red;">Please fill all fields.</span>');
                }else{
                     $.ajax(
                      {
                        url:"<?php echo base_url(); ?>index.php/welcome/addMenu",
                        type:"POST",
                        dataType :'html',
                        data:{ cuisine:cuisine,
                            subcuisnes:subcuisnes,
                            menutitle:menutitle,
                            personprice:personprice,
                            select_nenu_currency:select_nenu_currency,
                            serType:serType,
                            kitchenTool:kitchenTool,
                            chefid:chefid,
                            menuimage:menuimage,
                            menuid:menuid,menuimg:menuimg},
                            success:function(data)
                            { 
                                // /alert(data);
                                if(data == '1'){
                                  
                                   location.reload();

                            }
                            
                      
                        }

        
                    });

                }

     
        });

            $('#dishImgUpload').on('change', function (e)
            {
            var imgurlnew = '<?php echo base_url(); ?>assets/menuImgae';
           var data = new FormData();
           var files = e.target.files;
           
           for (var i = 0; i < files.length; i++) {
            var file = files[i];
               data.append('file', file, file.name);
               $.ajax({
                url: '<?php echo base_url(); ?>welcome/uploadImage', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data,                         
                type: 'post',
                success: function(data){
                    if(data !=''){
                        menuimage = data;
                        $('#upldedImg1').prepend('<img src="'+imgurlnew+'/'+menuimage+'">');
                    }else{
                        $('#statushostMsg').html('<span style="color:red;">Something went wrong please select new image.</span>');
                    }
                 
                }
                });
           }
           

        
});
    });
</script>

<script type="text/javascript">
var menuidfordish;
var dishimage;
      function getsubmenu(val){
        //alert(val);
    $.ajax(
      {
        url:"<?php echo base_url(); ?>index.php/welcome/getsubmenus",
        type:"POST",
        data:{menuid : val},
        
        success:function(data)
        { 
           // console.log(data);
           
            $("#DishDiv").css("display", "block");
            if(data !=''){
            $('#submenuDIv').html(data);
            menuidfordish = val
            $("#DishDiv").css("display", "block");
           }
           
        }
         
      
      });
    }
     /*-------------Dish Edit Or Add Popup--------*/
        jQuery("#addDish").click(function(){ 
            jQuery("#DishModal").modal('show');
            jQuery('#menuidfordish').val(menuidfordish);
            
        });
function savemenuDish(){
     var menuids = $('#menuidfordish').val();
     var dishtitle = $('#dishtitle').val();
     var mainCat1 = $('#mainCat1').val();
     var chefids = $('#chefids').val();
     var submenuid = $('#submenuid').val();
     var submenuimg = $('#submenuimg').val();
     if(dishimage =='' ){
        dishimage = submenuimg;
     }
      if (menuids == ''|| mainCat1 == '' || dishimage == '' ) {
/*alert("Please fill all fields...!!!!!!");*/

                $('#statusdishMsg').html('<span style="color:red;">Please fill all fields.</span>');
                }else{
                     $.ajax(
                      {
                        url:"<?php echo base_url(); ?>index.php/welcome/addsubMenu",
                        type:"POST",
                        dataType :'html',
                        data:{ menuids:menuids,
                            mainCat1:mainCat1,
                            chefids:chefids,
                            dishimage:dishimage,
                            dishtitle:dishtitle,
                            submenuid:submenuid,
                            submenuimg:submenuimg},
                        success:function(data)
                        { 
                            //alert(data);
                            if(data == '1'){
                             location.reload();
                             }
                            
                    }

      });

  }
    
}
$('#dishImgUpload1').on('change', function (e)
            {
        var imgurl = '<?php echo base_url(); ?>assets/submenuImage';
           var data = new FormData();
           var files = e.target.files;
           
           for (var i = 0; i < files.length; i++) {
            var file = files[i];
               data.append('file', file, file.name);
               $.ajax({
                url: '<?php echo base_url(); ?>index.php/welcome/uploadishImage', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data,                         
                type: 'post',
                success: function(data){
                    if(data !=''){
                        dishimage = data;
                        $('#upldedImg').prepend('<img src="'+imgurl+'/'+dishimage+'">');
                    }else{
                        $('#statushostMsg').html('<span style="color:red;">Something went wrong please select new image.</span>');
                    }
             
                }
                });
           }
           

      
});

function draftMenu(val,val1){
   var draftstatus;
   if(val1 == 1){
        draftstatus = 0; 
   }else{
        draftstatus = 1;
   }
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/welcome/draftMenu', // point to server-side PHP script 
                data: {menuid:val,draftstatus:draftstatus},                         
                type: 'post',
                success: function(data){
                     if(data == '1'){
                               location.reload();
                            }
                 }
                });
}
function deleteMenu(val){
   
    $.ajax({
                url: '<?php echo base_url(); ?>index.php/welcome/deleteMenu', // point to server-side PHP script 
                data: {menuid:val},                         
                type: 'post',
                success: function(data){
                     if(data == '1'){
                        alert('Menu deleted sucessfully');
                               location.reload();
                            }
                 }
                });
}
function draftSubmenu(val,val1){
    var draftsubstatus;
   if(val1 == 1){
        draftsubstatus = 0; 
   }else{
        draftsubstatus = 1;
   }
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/welcome/draftSubmenu', // point to server-side PHP script 
                data: {submenuid:val,draftsubstatus:draftsubstatus},                         
                type: 'post',
                success: function(data){
                     if(data == '1'){

                        location.reload();
                       //  $("#DishDiv").css("display", "block");

                               
                            }
                 }
                });
}

function deleteSubmenu(val){
     $.ajax({
                url: '<?php echo base_url(); ?>index.php/welcome/deleteSubmenu', // point to server-side PHP script 
                data: {submenuid:val},                         
                type: 'post',
                success: function(data){
                     if(data == '1'){
                        alert('Dish deleted sucessfully');
                               location.reload();
                            }
                 }
                });

}

function editSubmenu(val){
   // alert(val);
    /*****get submenu array*******/
    var imgurl = '<?php echo base_url(); ?>assets/submenuImage';
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/welcome/getsubmenuarray', // point to server-side PHP script 
                data: {submenuid:val}, 
                dataType:'json',                        
                type: 'post',
                success: function(data){
                    console.log(data);
                  $('#dishtitle').val(data.dish_name);
                  $('#dishtitle').val(data.dish_name);
                  $('#submenuid').val(data.id);
                  $('#submenuimg').val(data.dish_image);
                  $('#menuidfordish').val(data.menu_id);
                  $( "#mainCat1 option:selected" ).text(data.dish_category);
                  $('#upldedImg').prepend('<img src="'+imgurl+'/'+data.dish_image+'">');

                  jQuery("#DishModal").modal('show');
                    // console.log(data);
                 }
                });

}
$(document).ready(function() {
            $(".menuItemOpt .editMenus").click(function(){
                var val = $(this).attr('dataid');
                editMenus(val);
            });

function editMenus(val){
     var imgurl = '<?php echo base_url(); ?>assets/menuImgae';
            //alert(val);
                $.ajax({
                url: '<?php echo base_url(); ?>index.php/welcome/editChefmenus', // point to server-side PHP script
                data: {menuid:val},
                dataType:'json',                      
                type: 'post',
                success: function(data){
                     console.log(data);
                    $('#menutitle').val(data.menu_title);
                    $('#menuid').val(data.id);
                    $('#menuimg').val(data.menu_image);
                    $('#price_per_person').val(data.actualprice);
                    $('#select_nenu_currency option:selected').text(data.currency);
                    $('#upldedImg1').prepend('<img src="'+imgurl+'/'+data.menu_image+'">');

                    jQuery('#select_nenu_currency').attr('data-selected-country',data.currency);
                    jQuery('#select_nenu_currency').flagStrap();
                    var str = data.service_type;
                    var strarray = str.split(',');
                    for (var i = 0; i < strarray.length; i++) {
                            $("input[type=checkbox][name='serveType[]']").filter(function () {
                                return this.value == strarray[i];
                            }).prop("checked", true);
                            if(strarray[i] == 'Chef Onsite Cooking'){
                                $('.untensilbox').show();
                            }
                     }

                     var str1 = data.kitchen_tool;
                     var strarray1 = str1.split(',');
                     for (var i = 0; i < strarray1.length; i++) {
                            $("input[type=checkbox][name='kitchenTool[]']").filter(function () {
                                return this.value == strarray1[i];
                            }).prop("checked", true);
                     }
                     // $('#cuisinname').val(data.cusine);
                     // $('#subcuisinname').val(data.subcusine);
                    $('input[name="cuisinname"]').attr('value',data.cusine);
                    $('input[name="subcuisinname"]').attr('value',data.subcusine);
                    cuisne = data.cusine;
                    subcuisne = data.subcusine;
                     //cuisineSubcuisine(cui,scui);
                    // console.log($('input[name="cuisinname"]').attr('value',data.cusine));
                    //  console.log($('input[name="subcuisinname"]').attr('value',data.subcusine));

                    console.log("cuisne "+cuisne+' dataid '+data.id);
                    console.log("subcuisne "+subcuisne+' data-menu_title '+data.menu_title);
                    cuisne = cuisne.split(',');
                    subcuisne = subcuisne.split(',');
                   
                    catAChecked = {};
                    if (cuisne != '') {
                        for( var i = 0, l = cuisne.length; i < l; i++ ) {
                            console.log('main: ', cuisne[i]);
                            var objectkey = cuisne[i];
                            var exjectObj = catA[cuisne[i]];
                            //console.log(exjectObj);
                            catAChecked[objectkey] = [];
                            catcheckedfun(exjectObj, objectkey);
                        }
                        function catcheckedfun(exjectObj, objectkey) {
                            for( var i = 0, l = exjectObj.length; i < l; i++ ) {
                                //console.log('sub: ', exjectObj[i]);
                                var a = subcuisne.indexOf(exjectObj[i]);
                                if (a != '-1') {
                                    catAChecked[objectkey].push(subcuisne[a]);
                                }
                            }
                        }
                    }

                    console.log('catAChecked: ',catAChecked);

                    $("#mainCat").html('');
                        mianCatHtml = '';
                        $.each(catA, function(key, value){
                            mianCatHtml += '<option value="'+key+'">'+key+'</option>';
                            //console.log('key------', key);
                        });
                    $("#mainCat").html(mianCatHtml);    
                    //console.log(catA);
                    var firstCat = 'Hong Kong Style';
                    
                    subCat(firstCat);

                    function subCat(firstCat) {
                        $("#subCat").html('');
                        var subCathtml = '<li class="catHeading">'+firstCat+'</li>';
                        for (var i = 0; i < catA[firstCat].length; i++) {
                            subCathtml += '<li> <input class="radio" type="checkbox" value="'+catA[firstCat][i]+'" id="Shunde'+i+'">';
                            subCathtml += '<label class="radiobLable" for="Shunde'+i+'">'+catA[firstCat][i]+'</label> </li>';
                        }
                        //console.log(subCathtml);
                        $("#subCat").html(subCathtml);
                        if (catAChecked[firstCat]) {
                            var obj = catAChecked[firstCat];
                            //console.log(firstCat+': has value');
                            for( var i = 0, lingth = obj.length; i < lingth; i++ ) {
                                var chval = obj[i];
                                //console.log(chval);
                                $('#subCat input[value="'+chval+'"]').attr('checked','checked');
                            }
                        }
                    }

                    catblock();
                    function catblock() {
                        $(".selectedCatBlock").html('');
                        //console.log(catAChecked);
                        var catListhtml = '';
                        $.each(catAChecked, function(key, value){
                            //console.log('test: ',key);
                            if (catAChecked[key].length !== 0) {
                                //console.log('this null ', catAChecked[key]);
                                catListhtml += '<div class="form-group"><div class="cat-box">';
                                catListhtml += '<h4 title="'+key+'" class="clr-black">'+key+'</h4> <ul class="subCatList">';
                                for (var i = 0; i < catAChecked[key].length; i++) {
                                    //console.log('subcheck : ',catAChecked[key][i]);
                                    catListhtml += '<li><span class="float-left txt">'+catAChecked[key][i]+'</span> <span class="float-left close">X</span></li>';
                                }
                                catListhtml += '</ul></div></div>';
                            }
                        });
                        $(catListhtml).appendTo('.selectedCatBlock');
                    }



                    jQuery("#MenuModal").modal('show');
                 }
                });
            }

});
</script>

</body>
</html> 