<?php 
if(!$this->session->userdata('site_lang')){
$lan = 'English';
}else{
  if($this->session->userdata('site_lang') == 'english'){
    $lan = 'English';
  }elseif($this->session->userdata('site_lang') == 'traditional-chinese'){
     $lan ='T.Chinese';
  }else{
     $lan = 'S.Chinese';
  }
}
?>

<header class="header wrapper">
  <div class="container">
    <div class="row logoRow">
      <div class="col-lg-4 col-md-4 col-sm-5 col-4">
        <div class="tpLeft">
          <div class="lang float-left">
            <div class="lang_selected" id="sessionlanguage"><?php echo $lan;?></div>
            <div class="lang_values">
              <div class="lang_item">English</div>
              <!-- <div class="lang_item" data-value="chinese">Chinese</div> -->
              <div class="lang_item">T.Chinese</div>
              <div class="lang_item">S.Chinese</div>
            </div>
          </div>
          <div class="currency float-left">
            <form class="form-horizontal ">
              <div class="form-group">
                <div class="flagstrap" id="select_country" data-input-name="NewBuyer_country" data-selected-country=""></div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-3 col-5">
        <div class="logo">
          <a href="">
            <img class="txt-center" src="<?php echo base_url();?>front/images/logo.png" alt="">
          </a>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-3">
        <div class="tpRight">
          <div class="bcmHost float-left">
            <a href=""><span>Become a host</span></a>
          </div>
          <div class="usersign">
            <ul class="">
              <li class="ntmob"><a class="signup modal-toggle" href="javascript:;" data-tab="#register" data-toggle="modal" data-target="#login-register-modal"><span>Signup</span></a></li>
              <li><a class="login modal-toggle" href="javascript:;" data-tab="#login" data-toggle="modal" data-target="#login-register-modal"><span>Login</span></a></li>
            </ul>
            <div class="modal" id="login-register-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-offset-top">
                  <div class="modal-content">
                    <div class="modal-body">
                      <div class="sd-tabs sd-tab-interaction">
                        <div class="">
                          <ul class="tabNav">
                            <li><a href="javascript:;" data-id="#login" class="active login"><span>Login</span></a></li>
                            <li><a href="javascript:;" data-id="#register" class="signup"> <span>Signup</span></a></li>
                          </ul>
                          <div class="tabContent">
                            <form id="login" class="tab-pane usrLgForm" action="" method="post">
                              <div class="loginWith">
                                <h5>Login to ChefDiner</h5>
                                <a href="javascript:;"><img src="<?php echo base_url();?>front/images/fbl.png" alt=""></a>
                                <a href="javascript:;"><img src="<?php echo base_url();?>front/images/twtl.png" alt=""></a>
                                <a href="javascript:;"><img src="<?php echo base_url();?>front/images/gpl.png" alt=""></a>
                                <p>
                                  If you don't prefer Social Media, feel free to
                                  use email and password to login.
                                </p>
                              </div>
                              <div class="formGrp">
                                <input type="email" class="uname" name="" placeholder="UserName or Email*">
                                <span class="unptbg"></span>
                              </div>
                              <div class="formGrp">
                                <input type="password" class="upw" placeholder="Password" name="" required>
                                <span class="unptbg"></span>
                              </div>
                              <div class="fgtpw"><a href="javascript:;">Forgot Password</a></div>
                              <div class="formGrp">
                                <input type="submit" class="txt-upr" value="continue">
                              </div>
                            </form>
                            <form id="register" class="tab-pane usrLgForm">
                              <div class="loginWith">
                                <h5>Sign Up with ChefDiner</h5>
                                <a href="javascript:;"><img src="<?php echo base_url();?>front/images/fbl.png" alt=""></a>
                                <a href="javascript:;"><img src="<?php echo base_url();?>front/images/twtl.png" alt=""></a>
                                <a href="javascript:;"><img src="<?php echo base_url();?>front/images/gpl.png" alt=""></a>
                              </div>
                              <div class="formGrp">
                                  <a href="javascript:;" class="signUpemail">Sign up with email</a>
                              </div>
                              <div class="rgform">
                                <div class="formGrp">
                                  <input type="text" class="uname" name="" placeholder="First Name">
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <input type="text" class="uname" name="" placeholder="Last Name">
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <input type="text" class="uemail" name="" placeholder="Email">
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <input type="password" class="upw" placeholder="Password" name="" required>
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <input type="password" class="upw" placeholder="Confirm Password" name="" required>
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <input type="number" class="unmbr" placeholder="Phone Number" name="" required>
                                  <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                  <p>
                                    We will be using your phone number to send you information about bookings and other important events.
                                  </p>
                                  
                                </div>
                                <div class="sgntc"><input type="checkbox" checked> Agree  <a href='javascript:;'>Term & Conditions & Privacy Policy</a></div>
                                <div class="formGrp">
                                    <input type="submit" class="txt-upr" value="Register">
                                </div>
                              </div>
                            </form>
                            
                          </div>
                        </div>
                      </div>
                      <button type="button" class="btn btn-default mdlclose" data-dismiss="modal">
                        <img src="<?php echo base_url();?>front/images/mdlclose.png" alt="">
                      </button>
                    </div>
                  
                  </div>  
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>  
  </div>
</header>





