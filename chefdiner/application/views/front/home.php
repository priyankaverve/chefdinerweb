<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title><?php echo $this->lang->line('sitetitle'); ?></title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/home.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/chef-box.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/owl.css">

 
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
</head>
<body>
<?php
$this->load->view('front/header');
?>

<div class="wrapper srchBnr">
  
    <div class="container" style="height:550px;">
     <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="bnrInner">
                    <h2 class="text-center">
                        Discover secret social dining places
                    </h2>
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled 
                    </p>
                    <div class="chfSrch">
                        <div class="srchBox">
                            <div class="district_selected float-left" data-value="value 0">District Select</div>
                            <div class="district_values">
                              <div class="district_item" data-value="District1">District 1</div>
                              <div class="district_item" data-value="District2">District 2</div>
                              <div class="district_item" data-value="District3">District 3</div>
                              <div class="district_item" data-value="District4">District 4</div>
                              <div class="district_item" data-value="District4">District 4</div>
                              <div class="district_item" data-value="District4">District 4</div>
                              <div class="district_item" data-value="District4">District 4</div>
                              <div class="district_item" data-value="District4">District 4</div>
                              <div class="district_item" data-value="District4">District 4</div>
                            </div>
                            <input type="text" name="date"  class="float-left" placeholder="Date">
                            <input type="submit" value="Search">
                            <a href="" class="advncSrch" data-toggle="modal" data-target="#advncSrch">Advanced Search</a>
                        </div>
                        <div class="modal" id="advncSrch" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-offset-top">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="advsrchBox">
                                            <h5 class="text-center">Advanced Search</h5>
                                            <div class="schfld text-center">
                                                <h6 class="">No. of Seats</h6>
                                                <div class="qutBox-1">
                                                    <span class="minus"></span>
                                                    <span class="qutInput">
                                                        <input readonly="" type="text" value="1" name="">
                                                    </span>
                                                    <span class="plus"></span>
                                                </div>
                                            </div>
                                            <div class="schfld text-center">
                                                <input type="text" placeholder="Select Date">
                                            </div>
                                            <div class="schfld text-center">
                                               <select>
                                                   <option>District</option>
                                                   <option>District</option>
                                                   <option>District</option>
                                                   <option>District</option>
                                                   <option>District</option>
                                                   <option>District</option>
                                               </select>
                                            </div>
                                            <div class="schfld text-center">
                                                <select>
                                                    <option>Cuisines</option>
                                                    <option>Indian</option>
                                                    <option>chinese</option>
                                                    <option>Italian</option>
                                                    <option>Thai</option>
                                                    <option>Continental</option>
                                                </select>
                                            </div>
                                            <div class="schfld text-center">
                                                <h6 class="">Price Per guest</h6>
                                                <div class="priceBox">
                                                    <div id="slider-range"></div>
                                                    <input class="value" type="text" id="amount" readonly>
                                                </div>
                                            </div>
                                            <div class="srchAply">
                                                <a href="" class="float-right txt-upr">Apply</a>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-default mdlclose txt-upr" data-dismiss="modal">
                                           Cancel
                                        </button>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="steps">
                        <ul>
                            <li>
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/chseprice.png" alt="">
                                </div>
                                <div class="stepDtl">
                                    <h6 class="txt-upr"><?php echo $this->lang->line('step1'); ?></h6>
                                    <p><?php echo $this->lang->line('step1text'); ?></p>
                                </div>
                            </li>
                            <li>
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/chsemenu.png" alt="">
                                </div>
                                <div class="stepDtl">
                                    <h6 class="txt-upr"><?php echo $this->lang->line('step2'); ?></h6>
                                    <p><?php echo $this->lang->line('step2text'); ?></p>
                                </div>
                            </li>
                            <li>
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/chsechf.png" alt="">
                                </div>
                                <div class="stepDtl">
                                    <h6 class="txt-upr"><?php echo $this->lang->line('step3'); ?></h6>
                                    <p><?php echo $this->lang->line('step3text'); ?></p>
                                </div>
                            </li>
                            <li>
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/chsepmnt.png" alt="">
                                </div>
                                <div class="stepDtl">
                                    <h6 class="txt-upr"><?php echo $this->lang->line('step4'); ?></h6>
                                    <p><?php echo $this->lang->line('step4text'); ?></p>
                                </div>
                            </li>
                            <li>
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/chsefood.png" alt="">
                                </div>
                                <div class="stepDtl">
                                    <h6 class="txt-upr"><?php echo $this->lang->line('step5'); ?></h6>
                                    <p><?php echo $this->lang->line('step5text'); ?></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper whatHot">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="secTitle wtht">
                    <span class="float-left">What’s Hot</span>
                    <a href="<?php echo base_url() ?>exploreworld" class="float-right clr-red">Show All</a>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
            <?php
                if(!empty($whatshot)){

                        $i=0;
                        $len = count($whatshot);
                        foreach ($whatshot as $key => $value) {
                            if ($i == $len - 1) {
                                   $class='mrRight';
                                }else{
                                    $class='';
                                }

                                if($value['menu_image'] !=''){
                                    $menu_image = base_url().'assets/menuImgae/'.$value['menu_image'];
                                }else{
                                    $menu_image = base_url().'front/images/blank.jpg';
                                }

                        ?>
                            
                     
                <div class="htdish <?=$class?>">
                    <div class="img">
                        <a href="javascript:void(0)"><img src="<?php echo $menu_image; ?>"" alt=""></a>
                    </div>
                    <div class="dshDtl">
                        <a href="javascript:void(0)"><span class="ttl"><?=ucfirst($value['menu_title']);?></span></a>
                        <p><?=ucfirst($value['chef_city']);?>,<?=ucfirst($value['chef_country']);?></p>
                    </div>
                </div>
              
               
                

                <?php   
                $i++;
                 }
                }
             ?>
            </div>
        </div>
    </div>
</div>

<div class="wrapper middlePart">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12 leftScrlBlock">
                <h3 class="secTitle chfrnking">
                    Chef Ranking
                </h3>
                <div class="blckSec">
                    <h5 class="smlTitle wklyBest">
                        <span class="float-left">Weekly Best Rated</span>
                        <a href="" class="float-right clr-red">Show All</a>
                    </h5>
                    <div class="chfBoxOne">
                        <div class="img">
                            <a href="">
                                <img src="<?php echo base_url();?>front/images/food1.jpg" alt="">
                                <div class="rank">
                                    1
                                </div>
                            </a>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <a href="">
                                <img src="<?php echo base_url();?>front/images/food2.jpg" alt="">
                                <div class="rank">
                                    2
                                </div>
                            </a>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <a href="">
                                <img src="<?php echo base_url();?>front/images/food3.jpg" alt="">
                                <div class="rank">
                                    3
                                </div>
                            </a>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="smlTitle mstviewed">
                        <span class="float-left">Most Viewed</span>
                        <a href="" class="float-right clr-red">Show All</a>
                    </h5>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food1.jpg" alt="">
                            <div class="rank">
                                1
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food2.jpg" alt="">
                            <div class="rank">
                                2
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food3.jpg" alt="">
                            <div class="rank">
                                3
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h5 class="smlTitle mstbkmeked">
                        <span class="float-left">Most Bookmarked</span>
                        <a href="" class="float-right clr-red">Show All</a>
                    </h5>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food1.jpg" alt="">
                            <div class="rank">
                                1
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food2.jpg" alt="">
                            <div class="rank">
                                2
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food3.jpg" alt="">
                            <div class="rank">
                                3
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="forsrvices">
                
                    <h3 class="secTitle dlymeal">
                        Daily Meal
                        <a href="<?php echo base_url(); ?>dailymeals" class="float-right clr-red">Show All</a>
                    </h3>
                    <div class="owl-carousel owl-theme">
                    <?php 
                       // print_r($dailymealchef);
                        if(!empty($dailymealchef)){
                            foreach ($dailymealchef as $key => $value) {

                                $imgs = $value->allimage;
                                $allimg  = explode("|", $imgs);
                                $allimg  = array_values(array_filter($allimg));



                            if(!empty($value->cuisine)){
                                $cuisine = explode(",", $value->cuisine);

                            }else{
                                $cuisine = '';
                            }

                          //  print_r($cuisine);
                            if(!empty($cuisine)){
                            if(count($cuisine) == 1){
                                
                                $cuisine1 = $cuisine[0];
                                $cuisine2 = '';
                            }elseif(count($cuisine) > 2){
                                
                                $cuisine1 = $cuisine[0];
                                $cuisine2 = $cuisine[1];
                            }else{
                                $cuisine1 = '';
                                $cuisine2 = '';
                            }
                            }else{
                                $cuisine1 = '';
                                $cuisine2 = '';
                            }
                            if($value->profile_pic == ''){
                                $profile_pic = base_url().'front/images/blank.jpg';
                            }else{
                                $profile_pic = $value->profile_pic;
                            }

                            

                             ?>
                              
                    
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <a href="<?php echo base_url(); ?>chefdetail/<?php echo $value->user_id; ?>">
                                        <!-- <img src="<?php //echo base_url();?>front/images/food4.jpg" alt=""> -->
                                        <img src="<?php echo $allimg[0]; ?>" alt="">
                                        <div class="shade"></div>
                                        <div class="price clr-red">
                                        <?=$value->currency?><?=$value->price?>/Guest
                                        </div>
                                        <div class="bkMrkNmr clr-red">
                                            <?=$value->favouritecount;?>
                                        </div>
                                        <div class="ctgry">
                                        <ul>
                                                <li><?=$cuisine1;?></li>
                                                <li class="mrRight"><?=$cuisine2;?></li>
                                            </ul> 
                                        </div>
                                    </a>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <div class="chfPrflImg">
                                                <img class="img-fluid" src="<?=$profile_pic;?>" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                            <?php
                                           $rountRat = round($value->average_rating);
                                           //echo $rountRat;
                                           if($rountRat > 0 && $rountRat <=0.5){
                                          
                                           $valhalf5 = '0.5';
                                           $chkhalf5 = 'checked';

                                           }else{
                                            $valhalf5 = '0.5';
                                            $chkhalf5 = '';
                                           }

                                           if($rountRat > 0.5 && $rountRat <=1){
                                          
                                           $valfull1 = '1.0';
                                           $chkfull1 = 'checked';

                                           }else{
                                            $valfull1 = '1.0';
                                            $chkfull1 = '';
                                           }

                                           if($rountRat > 1 && $rountRat <=1.5){
                                          
                                           $valhalf1 = '1.5';
                                           $chkhalf1 = 'checked';

                                           }else{
                                            $valhalf1 = '1.5';
                                            $chkhalf1 = '';
                                           }
                                           if($rountRat > 1.5 && $rountRat <=2){
                                          
                                           $valfull2 = '2';
                                           $chkfull2 = 'checked';

                                           }else{
                                            $valfull2 = '2';
                                            $chkfull2 = '';
                                           }

                                           if($rountRat > 2 && $rountRat <=2.5){
                                          
                                           $valhalf2 = '2.5';
                                           $chkhalf2 = 'checked';

                                           }else{
                                            $valhalf2 = '2.5';
                                            $chkhalf2 = '';
                                           }

                                           if($rountRat > 2.5 && $rountRat <=3){
                                          
                                           $valfull3 = '3';
                                           $chkfull3 = 'checked';

                                           }else{
                                            $valfull3 = '3';
                                            $chkfull3 = '';
                                           }

                                          if($rountRat > 3 && $rountRat <=3.5){
                                          
                                           $valhalf3 = '3.5';
                                           $chkhalf3 = 'checked';

                                           }else{
                                            $valhalf3 = '3.5';
                                            $chkhalf3 = '';
                                           }
                                           if($rountRat > 3.5 && $rountRat <=4){
                                          
                                           $valfull4 = '4';
                                           $chkfull4 = 'checked';

                                           }else{
                                            $valfull4= '4';
                                            $chkfull4 = '';
                                           }
                                           
                                           if($rountRat > 4 && $rountRat <=4.5){
                                          
                                           $valhalf4 = '4.5';
                                           $chkhalf4 = 'checked';

                                           }else{
                                            $valhalf4= '4.5';
                                            $chkhalf4 = '';
                                           }

                                           if($rountRat > 4.5 && $rountRat <=5){
                                          
                                           $valfull5 = '5';
                                           $chkfull5 = 'checked';

                                           }else{
                                            $valfull5= '5';
                                            $chkfull5 = '';
                                           }
                                           

                                            ?>
                                                <li class="rateStar">
                                                <div class="cstmr-reviews">
                                                        <fieldset class="rating">
                                                            <input type="radio" id="stara<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull5?>" <?=$chkfull5?> disabled="disable" />
                                                            <label class = "full" for="stara<?=$value->id?>" title="Awesome - 5 stars"></label>

                                                            <input type="radio" id="starb<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf4?>" <?=$chkhalf4?> disabled="disable" />
                                                            <label class="half" for="starb<?=$value->id?>" title="Pretty good - 4.5 stars"></label>

                                                            <input type="radio" id="starc<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull4?>" <?=$chkfull4?> disabled="disable"  />
                                                            <label class = "full" for="starc<?=$value->id?>" title="Pretty good - 4 stars"></label>

                                                            <input type="radio" id="stard<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf3?>" <?=$chkhalf3?> disabled="disable" />
                                                            <label class="half" for="stard<?=$value->id?>" title="Meh - 3.5 stars"></label>

                                                            <input type="radio" id="stare<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull3?>" <?=$chkfull3?> disabled="disable"/>
                                                            <label class = "full" for="stare<?=$value->id?>" title="Meh - 3 stars"></label>

                                                            <input type="radio" id="starf<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf2?>" <?=$chkhalf2?> disabled="disable" />
                                                            <label class="half" for="starf<?=$value->id?>" title="Kinda bad - 2.5 stars"></label>

                                                            <input type="radio" id="starg<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull2?>" <?=$chkfull2?> disabled="disable" />
                                                            <label class = "full" for="starg<?=$value->id?>" title="Kinda bad - 2 stars"></label>

                                                            <input type="radio" id="starh<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf1?>" <?=$chkhalf1?> disabled="disable"  />
                                                            <label class="half" for="starh<?=$value->id?>" title="Meh - 1.5 stars"></label>

                                                            <input type="radio" id="stari<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valfull1?>" <?=$chkfull1?> disabled="disable" />
                                                            <label class = "full" for="stari<?=$value->id?>" title="Sucks big time - 1 star"></label>

                                                            <input type="radio" id="starj<?=$value->id?>" name="rating<?=$value->id?>" value="<?=$valhalf5?>" <?=$chkhalf5?> disabled="disable" />
                                                            <label class="half" for="starj<?=$value->id?>" title="Sucks big time - 0.5 stars"></label>
                                                        </fieldset>
                                                    </div>
                                           
                                                </li>
                                                <li class="rateCount">
                                                    <?php 
                                                    if(is_float($value->average_rating)){
                                                    $avr_rating = $value->average_rating;

                                                    }else{
                                                    $avr_rating  = number_format((float)$value->average_rating, 1, '.', '');
                                                    }
                                                    ?>
                                                    <span>
                                                        <?=$avr_rating;?>
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span><?=$value->totalreview?></span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl"><?=ucfirst($value->kitchen_title);?></span></a>
                                            <p><?=ucfirst($value->chef_city);?>,<?=ucfirst($value->chef_country);?></p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/><?=$value->recommended;?>
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/><?=$value->no_recommended;?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <?php       }

                        }
                ?>
                        
                        
                    </div>
                </div>
                <div class="forsrvices">
                    <h3 class="secTitle wkendDish">
                        Weekend Meal for this Week
                        <a href="" class="float-right clr-red">Show All</a>
                    </h3>
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <a href="">
                                        <img src="<?php echo base_url();?>front/images/food4.jpg" alt="">
                                        <div class="shade"></div>
                                        <div class="price clr-red">
                                        HKD125/Guest
                                        </div>
                                        <div class="bkMrkNmr clr-red">
                                            54
                                        </div>
                                        <div class="ctgry">
                                        <ul>
                                                <li>Indian</li>
                                                <li class="mrRight">Continental</li>
                                            </ul> 
                                        </div>
                                    </a>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <a href="">
                                        <img src="<?php echo base_url();?>front/images/food5.jpg" alt="">
                                        <div class="shade"></div>
                                        <div class="price clr-red">
                                        HKD125/Guest
                                        </div>
                                        <div class="bkMrkNmr clr-red">
                                            54
                                        </div>
                                        <div class="ctgry">
                                        <ul>
                                                <li>Indian</li>
                                                <li class="mrRight">Continental</li>
                                            </ul> 
                                        </div>
                                    </a>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <a href="">
                                        <img src="<?php echo base_url();?>front/images/food4.jpg" alt="">
                                        <div class="shade"></div>
                                        <div class="price clr-red">
                                        HKD125/Guest
                                        </div>
                                        <div class="bkMrkNmr clr-red">
                                            54
                                        </div>
                                        <div class="ctgry">
                                        <ul>
                                                <li>Indian</li>
                                                <li class="mrRight">Continental</li>
                                            </ul> 
                                        </div>
                                    </a>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="forsrvices">
                    <h3 class="secTitle nwchf">
                        New Chef from Last 7 Days
                        <a href="<?php echo base_url() ?>newchef" class="float-right clr-red">Show All</a>
                    </h3>
                    <div class="owl-carousel owl-theme">
                    <?php
                    if(!empty($newchef)){
                        if(count($newchef) > 2){
                            $cls1 = 'owl-carousel';
                        }else{
                            $cls1 ='';
                        }

                    }else{
                        $cls1 ='';
                        } ?>
                   
                   <?php 
                       // print_r($dailymealchef);
                        if(!empty($newchef)){
                            foreach ($newchef as $key => $value) {

                                $imgs = $value->allimage;
                                $allimg  = explode("|", $imgs);
                                $allimg  = array_values(array_filter($allimg));



                            if(!empty($value->cuisine)){
                                $cuisine = explode(",", $value->cuisine);

                            }else{
                                $cuisine = '';
                            }

                          //  print_r($cuisine);
                            if(!empty($cuisine)){
                            if(count($cuisine) == 1){
                                
                                $cuisine1 = $cuisine[0];
                                $cuisine2 = '';
                            }elseif(count($cuisine) > 2){
                                
                                $cuisine1 = $cuisine[0];
                                $cuisine2 = $cuisine[1];
                            }else{
                                $cuisine1 = '';
                                $cuisine2 = '';
                            }
                            }else{
                                $cuisine1 = '';
                                $cuisine2 = '';
                            }
                            if($value->profile_pic == ''){
                                $profile_pic = base_url().'front/images/blank.jpg';
                            }else{
                                $profile_pic = $value->profile_pic;
                            }

                            

                             ?>
                         <!-- <div class="<?=$cls1?> owl-theme">  -->    
                     
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <a href="<?php echo base_url(); ?>chefdetail/<?php echo $value->user_id; ?>">
                                        <img src="<?php echo $allimg[0]; ?>" alt="">
                                        <div class="shade"></div>
                                        <div class="price clr-red">
                                        <?=$value->currency?><?=$value->price?>/Guest
                                        </div>
                                        <div class="bkMrkNmr clr-red">
                                            <?=$value->favouritecount;?>
                                        </div>
                                        <div class="ctgry">
                                        <ul>
                                                <li><?=$cuisine1;?></li>
                                                <li class="mrRight"><?=$cuisine2;?></li>
                                            </ul> 
                                        </div>
                                    </a>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <div class="chfPrflImg">
                                                <img class="img-fluid" src="<?=$profile_pic;?>" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                            <?php
                                           $rountRatNewchef = round($value->average_rating);
                                           //echo $rountRat;
                                           if($rountRatNewchef > 0 && $rountRatNewchef <=0.5){
                                          
                                           $valhalf5Newchef = '0.5';
                                           $chkhalf5Newchef = 'checked';

                                           }else{
                                            $valhalf5Newchef = '0.5';
                                            $chkhalf5Newchef = '';
                                           }

                                           if($rountRatNewchef > 0.5 && $rountRatNewchef <=1){
                                          
                                           $valfull1Newchef = '1.0';
                                           $chkfull1Newchef = 'checked';

                                           }else{
                                            $valfull1Newchef = '1.0';
                                            $chkfull1Newchef = '';
                                           }

                                           if($rountRatNewchef > 1 && $rountRatNewchef <=1.5){
                                          
                                           $valhalf1Newchef = '1.5';
                                           $chkhalf1Newchef = 'checked';

                                           }else{
                                            $valhalf1Newchef = '1.5';
                                            $chkhalf1Newchef = '';
                                           }
                                           if($rountRatNewchef > 1.5 && $rountRatNewchef <=2){
                                          
                                           $valfull2Newchef = '2';
                                           $chkfull2Newchef = 'checked';

                                           }else{
                                            $valfull2Newchef = '2';
                                            $chkfull2Newchef = '';
                                           }

                                           if($rountRatNewchef > 2 && $rountRatNewchef <=2.5){
                                          
                                           $valhalf2Newchef = '2.5';
                                           $chkhalf2Newchef = 'checked';

                                           }else{
                                            $valhalf2Newchef = '2.5';
                                            $chkhalf2Newchef = '';
                                           }

                                           if($rountRatNewchef > 2.5 && $rountRatNewchef <=3){
                                          
                                           $valfull3Newchef = '3';
                                           $chkfull3Newchef = 'checked';

                                           }else{
                                            $valfull3Newchef = '3';
                                            $chkfull3Newchef = '';
                                           }

                                          if($rountRatNewchef > 3 && $rountRatNewchef <=3.5){
                                          
                                           $valhalf3Newchef = '3.5';
                                           $chkhalf3Newchef = 'checked';

                                           }else{
                                            $valhalf3Newchef = '3.5';
                                            $chkhalf3Newchef = '';
                                           }
                                           if($rountRatNewchef > 3.5 && $rountRatNewchef <=4){
                                          
                                           $valfull4Newchef = '4';
                                           $chkfull4Newchef = 'checked';

                                           }else{
                                            $valfull4Newchef= '4';
                                            $chkfull4Newchef = '';
                                           }
                                           
                                           if($rountRatNewchef > 4 && $rountRatNewchef <=4.5){
                                          
                                           $valhalf4Newchef = '4.5';
                                           $chkhalf4Newchef = 'checked';

                                           }else{
                                            $valhalf4Newchef = '4.5';
                                            $chkhalf4Newchef = '';
                                           }

                                           if($rountRatNewchef > 4.5 && $rountRatNewchef <=5){
                                          
                                           $valfull5Newchef = '5';
                                           $chkfull5Newchef = 'checked';

                                           }else{
                                            $valfull5Newchef = '5';
                                            $chkfull5Newchef = '';
                                           }
                                           

                                            ?>
                                                <li class="rateStar">
                                                <div class="cstmr-reviews">
                                                        <fieldset class="rating">
                                                            <input type="radio" id="staraNewchef<?=$value->id?>" name="ratingaNewchef<?=$value->id?>" value="<?=$valfull5Newchef?>" <?=$chkfull5Newchef?> disabled="disable" />
                                                            <label class = "full" for="staraNewchef<?=$value->id?>" title="Awesome - 5 stars"></label>

                                                            <input type="radio" id="starbNewchef<?=$value->id?>" name="ratingbNewchef<?=$value->id?>" value="<?=$valhalf4Newchef?>" <?=$chkhalf4Newchef?> disabled="disable" />
                                                            <label class="half" for="starbNewchef<?=$value->id?>" title="Pretty good - 4.5 stars"></label>

                                                            <input type="radio" id="starcNewchef<?=$value->id?>" name="ratingcNewchef<?=$value->id?>" value="<?=$valfull4Newchef?>" <?=$chkfull4Newchef?> disabled="disable"  />
                                                            <label class = "full" for="starcNewchef<?=$value->id?>" title="Pretty good - 4 stars"></label>

                                                            <input type="radio" id="stardNewchef<?=$value->id?>" name="ratingdNewchef<?=$value->id?>" value="<?=$valhalf3Newchef?>" <?=$chkhalf3Newchef?> disabled="disable" />
                                                            <label class="half" for="stardNewchef<?=$value->id?>" title="Meh - 3.5 stars"></label>

                                                            <input type="radio" id="stareNewchef<?=$value->id?>" name="ratingeNewchef<?=$value->id?>" value="<?=$valfull3Newchef?>" <?=$chkfull3Newchef?> disabled="disable"/>
                                                            <label class = "full" for="stareNewchef<?=$value->id?>" title="Meh - 3 stars"></label>

                                                            <input type="radio" id="starfNewchef<?=$value->id?>" name="ratingfNewchef<?=$value->id?>" value="<?=$valhalf2Newchef?>" <?=$chkhalf2Newchef?> disabled="disable" />
                                                            <label class="half" for="starfNewchef<?=$value->id?>" title="Kinda bad - 2.5 stars"></label>

                                                            <input type="radio" id="stargNewchef<?=$value->id?>" name="ratinggNewchef<?=$value->id?>" value="<?=$valfull2Newchef?>" <?=$chkfull2Newchef?> disabled="disable" />
                                                            <label class = "full" for="stargNewchef<?=$value->id?>" title="Kinda bad - 2 stars"></label>

                                                            <input type="radio" id="starhNewchef<?=$value->id?>" name="ratinghNewchef<?=$value->id?>" value="<?=$valhalf1Newchef?>" <?=$chkhalf1Newchef?> disabled="disable"  />
                                                            <label class="half" for="starhNewchef<?=$value->id?>" title="Meh - 1.5 stars"></label>

                                                            <input type="radio" id="stariNewchef<?=$value->id?>" name="ratingiNewchef<?=$value->id?>" value="<?=$valfull1Newchef?>" <?=$chkfull1Newchef?> disabled="disable" />
                                                            <label class = "full" for="stariNewchef<?=$value->id?>" title="Sucks big time - 1 star"></label>

                                                            <input type="radio" id="starjNewchef<?=$value->id?>" name="ratingjNewchef<?=$value->id?>" value="<?=$valhalf5Newchef?>" <?=$chkhalf5Newchef?> disabled="disable" />
                                                            <label class="half" for="starjNewchef<?=$value->id?>" title="Sucks big time - 0.5 stars"></label>
                                                        </fieldset>
                                                    </div>
                                           
                                                </li>
                                                <li class="rateCount">
                                                    <?php 
                                                    if(is_float($value->average_rating)){
                                                    $avr_rating = $value->average_rating;

                                                    }else{
                                                    $avr_rating  = number_format((float)$value->average_rating, 1, '.', '');
                                                    }
                                                    ?>
                                                    <span>
                                                        <?=$avr_rating;?>
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span><?=$value->totalreview?></span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl"><?=ucfirst($value->kitchen_title);?></span></a>
                                            <p><?=ucfirst($value->chef_city);?>,<?=ucfirst($value->chef_country);?></p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/><?=$value->recommended;?>
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/><?=$value->no_recommended;?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <?php       }

                        }else{
                ?>
                        
                        <div class="no-content">No Records found</div>

                        <?php } ?>
                    </div>
                    
                </div>
                <div class="forsrvices">
                    <h3 class="secTitle picks">
                        Our Picks
                        <a href="<?php echo base_url() ?>ourpicks" class="float-right clr-red">Show All</a>
                    </h3>
                    <div class="owl-carousel owl-theme">   

                    <?php
                    if(!empty($ourpickchef)){
                        if(count($ourpickchef) > 2){
                            $cls = 'owl-carousel';
                        }else{
                            $cls ='';
                        }

                    }else{
                        $cls ='';
                        } ?>

                    
                    
                    <?php 
                       // print_r($dailymealchef);
                        if(!empty($ourpickchef)){
                            foreach ($ourpickchef as $key => $value) {

                                $imgs = $value->allimage;
                                $allimg  = explode("|", $imgs);
                                $allimg  = array_values(array_filter($allimg));



                            if(!empty($value->cuisine)){
                                $cuisine = explode(",", $value->cuisine);

                            }else{
                                $cuisine = '';
                            }

                          //  print_r($cuisine);
                            if(!empty($cuisine)){
                            if(count($cuisine) == 1){
                                
                                $cuisine1 = $cuisine[0];
                                $cuisine2 = '';
                            }elseif(count($cuisine) > 2){
                                
                                $cuisine1 = $cuisine[0];
                                $cuisine2 = $cuisine[1];
                            }else{
                                $cuisine1 = '';
                                $cuisine2 = '';
                            }
                            }else{
                                $cuisine1 = '';
                                $cuisine2 = '';
                            }
                            if($value->profile_pic == ''){
                                $profile_pic = base_url().'front/images/blank.jpg';
                            }else{
                                $profile_pic = $value->profile_pic;
                            }

                            

                             ?>
                        
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <a href="<?php echo base_url(); ?>chefdetail/<?php echo $value->user_id; ?>">
                                        <img src="<?php echo $allimg[0]; ?>" alt="">
                                        <div class="shade"></div>
                                        <div class="price clr-red">
                                        <?=$value->currency?><?=$value->price?>/Guest
                                        </div>
                                        <div class="bkMrkNmr clr-red">
                                            <?=$value->favouritecount;?>
                                        </div>
                                        <div class="ctgry">
                                        <ul>
                                                <li><?=$cuisine1;?></li>
                                                <li class="mrRight"><?=$cuisine2;?></li>
                                            </ul> 
                                        </div>
                                    </a>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <div class="chfPrflImg">
                                                <img class="img-fluid" src="<?=$profile_pic;?>" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                            <?php
                                           $rountRatourpick = round($value->average_rating);
                                           //echo $rountRat;
                                           if($rountRatourpick > 0 && $rountRatourpick <=0.5){
                                          
                                           $valhalf5ourpick = '0.5';
                                           $chkhalf5ourpick = 'checked';

                                           }else{
                                            $valhalf5ourpick = '0.5';
                                            $chkhalf5ourpick = '';
                                           }

                                           if($rountRatourpick > 0.5 && $rountRatourpick <=1){
                                          
                                           $valfull1ourpick = '1.0';
                                           $chkfull1ourpick = 'checked';

                                           }else{
                                            $valfull1ourpick = '1.0';
                                            $chkfull1ourpick = '';
                                           }

                                           if($rountRatourpick > 1 && $rountRatourpick <=1.5){
                                          
                                           $valhalf1ourpick = '1.5';
                                           $chkhalf1ourpick = 'checked';

                                           }else{
                                            $valhalf1ourpick = '1.5';
                                            $chkhalf1ourpick = '';
                                           }
                                           if($rountRatourpick > 1.5 && $rountRatourpick <=2){
                                          
                                           $valfull2ourpick = '2';
                                           $chkfull2ourpick = 'checked';

                                           }else{
                                            $valfull2ourpick = '2';
                                            $chkfull2ourpick = '';
                                           }

                                           if($rountRatourpick > 2 && $rountRatourpick <=2.5){
                                          
                                           $valhalf2ourpick = '2.5';
                                           $chkhalf2ourpick = 'checked';

                                           }else{
                                            $valhalf2ourpick = '2.5';
                                            $chkhalf2ourpick = '';
                                           }

                                           if($rountRatourpick > 2.5 && $rountRatourpick <=3){
                                          
                                           $valfull3ourpick = '3';
                                           $chkfull3ourpick = 'checked';

                                           }else{
                                            $valfull3ourpick = '3';
                                            $chkfull3ourpick = '';
                                           }

                                          if($rountRatourpick > 3 && $rountRatourpick <=3.5){
                                          
                                           $valhalf3ourpick = '3.5';
                                           $chkhalf3ourpick = 'checked';

                                           }else{
                                            $valhalf3ourpick = '3.5';
                                            $chkhalf3ourpick = '';
                                           }
                                           if($rountRatourpick > 3.5 && $rountRatourpick <=4){
                                          
                                           $valfull4ourpick = '4';
                                           $chkfull4ourpick = 'checked';

                                           }else{
                                            $valfull4ourpick= '4';
                                            $chkfull4ourpick = '';
                                           }
                                           
                                           if($rountRatourpick > 4 && $rountRatourpick <=4.5){
                                          
                                           $valhalf4ourpick = '4.5';
                                           $chkhalf4ourpick = 'checked';

                                           }else{
                                            $valhalf4ourpick = '4.5';
                                            $chkhalf4ourpick = '';
                                           }

                                           if($rountRatourpick > 4.5 && $rountRatourpick <=5){
                                          
                                           $valfull5ourpick = '5';
                                           $chkfull5ourpick = 'checked';

                                           }else{
                                            $valfull5ourpick = '5';
                                            $chkfull5ourpick = '';
                                           }
                                           

                                            ?>
                                                <li class="rateStar">
                                                <div class="cstmr-reviews">
                                                        <fieldset class="rating">
                                                            <input type="radio" id="staraourpick<?=$value->id?>" name="ratingaourpick<?=$value->id?>" value="<?=$valfull5ourpick?>" <?=$chkfull5ourpick?> disabled="disable" />
                                                            <label class = "full" for="staraourpick<?=$value->id?>" title="Awesome - 5 stars"></label>

                                                            <input type="radio" id="starbourpick<?=$value->id?>" name="ratingbourpick<?=$value->id?>" value="<?=$valhalf4ourpick?>" <?=$chkhalf4ourpick?> disabled="disable" />
                                                            <label class="half" for="starbourpick<?=$value->id?>" title="Pretty good - 4.5 stars"></label>

                                                            <input type="radio" id="starcourpick<?=$value->id?>" name="ratingcourpick<?=$value->id?>" value="<?=$valfull4ourpick?>" <?=$chkfull4ourpick?> disabled="disable"  />
                                                            <label class = "full" for="starcourpick<?=$value->id?>" title="Pretty good - 4 stars"></label>

                                                            <input type="radio" id="stardourpick<?=$value->id?>" name="ratingdourpick<?=$value->id?>" value="<?=$valhalf3ourpick?>" <?=$chkhalf3ourpick?> disabled="disable" />
                                                            <label class="half" for="stardourpick<?=$value->id?>" title="Meh - 3.5 stars"></label>

                                                            <input type="radio" id="stareourpick<?=$value->id?>" name="ratingeourpick<?=$value->id?>" value="<?=$valfull3ourpick?>" <?=$chkfull3ourpick?> disabled="disable"/>
                                                            <label class = "full" for="stareourpick<?=$value->id?>" title="Meh - 3 stars"></label>

                                                            <input type="radio" id="starfourpick<?=$value->id?>" name="ratingfourpick<?=$value->id?>" value="<?=$valhalf2ourpick?>" <?=$chkhalf2ourpick?> disabled="disable" />
                                                            <label class="half" for="starfourpick<?=$value->id?>" title="Kinda bad - 2.5 stars"></label>

                                                            <input type="radio" id="stargourpick<?=$value->id?>" name="ratinggourpick<?=$value->id?>" value="<?=$valfull2ourpick?>" <?=$chkfull2ourpick?> disabled="disable" />
                                                            <label class = "full" for="stargourpick<?=$value->id?>" title="Kinda bad - 2 stars"></label>

                                                            <input type="radio" id="starhourpick<?=$value->id?>" name="ratinghourpick<?=$value->id?>" value="<?=$valhalf1ourpick?>" <?=$chkhalf1ourpick?> disabled="disable"  />
                                                            <label class="half" for="starhourpick<?=$value->id?>" title="Meh - 1.5 stars"></label>

                                                            <input type="radio" id="stariourpick<?=$value->id?>" name="ratingiourpick<?=$value->id?>" value="<?=$valfull1ourpick?>" <?=$chkfull1ourpick?> disabled="disable" />
                                                            <label class = "full" for="stariourpick<?=$value->id?>" title="Sucks big time - 1 star"></label>

                                                            <input type="radio" id="starjourpick<?=$value->id?>" name="ratingjourpick<?=$value->id?>" value="<?=$valhalf5ourpick?>" <?=$chkhalf5ourpick?> disabled="disable" />
                                                            <label class="half" for="starjourpick<?=$value->id?>" title="Sucks big time - 0.5 stars"></label>
                                                        </fieldset>
                                                    </div>
                                           
                                                </li>
                                                <li class="rateCount">
                                                    <?php 
                                                    if(is_float($value->average_rating)){
                                                    $avr_rating = $value->average_rating;

                                                    }else{
                                                    $avr_rating  = number_format((float)$value->average_rating, 1, '.', '');
                                                    }
                                                    ?>
                                                    <span>
                                                        <?=$avr_rating;?>
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span><?=$value->totalreview?></span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl"><?=ucfirst($value->kitchen_title);?></span></a>
                                            <p><?=ucfirst($value->chef_city);?>,<?=ucfirst($value->chef_country);?></p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/><?=$value->recommended;?>
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/><?=$value->no_recommended;?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <?php       }

                        }else{
                ?>
                        
                        <div class="no-content">No Records found</div>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row fturedTesti">
            <div class="col-lg-12 col-md-12">
                <h3 class="secTitle fturedreview">
                    Featured Reviews
                </h3>
                <div class="owl-carousel owl-theme reviewSlide">
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review1.jpg" alt="">
                                <div class="shade"></div>
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                   <h6>Amarit Phayangay</h6>
                                   <div class="rate">
                                        Rated <span>4.0 </span>
                                   </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review2.jpg" alt="">
                                <div class="shade"></div>
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                    <h6>Amarit Phayangay</h6>
                                    <div class="rate">
                                        Rated <span>4.0 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review3.jpg" alt="">
                                <div class="shade"></div>
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                    <h6>Amarit Phayangay</h6>
                                    <div class="rate">
                                        Rated <span>4.0 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review1.jpg" alt="">
                                <div class="shade"></div>
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                    <h6>Amarit Phayangay</h6>
                                    <div class="rate">
                                        Rated <span>4.0 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review2.jpg" alt="">
                                <div class="shade"></div>
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                    <h6>Amarit Phayangay</h6>
                                    <div class="rate">
                                        Rated <span>4.0 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review3.jpg" alt="">
                                <div class="shade"></div>
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                    <h6>Amarit Phayangay</h6>
                                    <div class="rate">
                                        Rated <span>4.0 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="wrapper appDwnld">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="appLink">
                    <div class="appLinkBox">
                        <h5>Download ChefDiner for free on App Store or Google Play</h5>
                        <p>We'll send you a link, open it on your phone to download the app</p>
                        <div class="nmbrSec">
                            <input type="number" placeholder="+123456790">
                            <input type="submit" value="Send SMS">
                        </div>
                        <div class="appStore">
                            <a href=""><img src="<?php echo base_url();?>front/images/appstore.png" alt=""></a>
                            <a href=""><img src="<?php echo base_url();?>front/images/playstore.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="mobox">
                    <img src="<?php echo base_url();?>front/images/mobiles.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper explore">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="secTitle globe">
                    Explore the world
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <div class="zigZag tall">
                   <a href="">
                       <img src="<?php echo base_url();?>front/images/food7.jpg" alt="">
                        <div class="shade"></div>
                        <div class="cntryName">
                            Hongkong
                        </div>
                    </a>
                </div>
                <div class="zigZag small">
                    <a href="">
                        <img src="<?php echo base_url();?>front/images/food8.jpg" alt="">
                        <div class="shade"></div>
                        <div class="cntryName">
                            Singapore
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="zigZag small">
                    <img src="<?php echo base_url();?>front/images/food9.jpg" alt="">
                    <div class="shade"></div>
                    <div class="cntrySrch text-center">
                        Where to?
                        <div class="cntrysrchBox">
                            <input type="text" placeholder="Hongkong">
                            <input type="submit" value="">
                        </div>  
                    </div>
                </div>
                <div class="zigZag tall">
                    <a href="">
                        <img src="<?php echo base_url();?>front/images/food10.jpg" alt="">
                        <div class="shade"></div>
                        <div class="cntryName">
                                Philippines
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="zigZag tall">
                    <a href="">
                        <img src="<?php echo base_url();?>front/images/food11.jpg" alt="">
                        <div class="shade"></div>
                        <div class="cntryName">
                                Thailand
                        </div>
                    </a>
                </div>
                <div class="zigZag small">
                    <a href="">
                        <img src="<?php echo base_url();?>front/images/food12.jpg" alt="">
                        <div class="shade"></div>
                        <div class="cntryName">
                                Malaysia
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper newsletter">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="nwsBlock text-center">
                    <div class="newheading">
                        Stay updated with delicious news and upcoming events
                    </div>
                    <div class="cntrysrchBox">
                        <input type="email" placeholder="Email">
                        <input type="submit" value="send" class="txt-upr">
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper rfer">
    <div class="container">
        <div class="row">
            <div class="push-lg-2 col-lg-8 push-md-2 col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="referBlock text-center">
                            <img src="<?php echo base_url();?>front/images/chinaloc.png" alt="">
                            <p>
                                Biggest private dining network in China
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="referBlock text-center">
                            <img src="<?php echo base_url();?>front/images/reffer.png" alt="">
                            <p>
                                Refer a friend and get up to 100 USD
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="referBlock text-center">
                            <img src="<?php echo base_url();?>front/images/topratings.png" alt="">
                            <p>
                                9 out of 10 guests give the highest ratings
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>

<script src="<?php echo base_url();?>front/js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        var owl = $('.reviewSlide');
        owl.owlCarousel({
            margin: 9,
            nav: true,
            loop: false,
            navigation: false,
            responsive: {
                0: {
                    items: 1
                },
                500: {
                    items: 1
                },
                600: {
                    items: 2
                },
                700: {
                        items:3
                    },
                900: {
                        items:4
                    },
                1100: {
                    items: 5
                }
            }
        })
    });
</script>
<script>
   $(document).ready(function() {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 9,
            nav: true,
            loop: false,
            navigation: false,
            responsive: {
                0: {
                    items: 1
                },
                500: {
                    items: 1
                },
                600: {
                    items: 2
                },
                700: {
                         items:3
                     },
                900: {
                         items:3
                     },
                1100: {
                    items: 3
                }
            }
        })
    });
</script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 300,
            values: [ 0, 300 ],
            slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            }
        });
        $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) + " - $" + $( "#slider-range" ).slider( "values", 1 ) );
    });
</script>
<script type="text/javascript">
    jQuery(".qutBox-1 .minus").click(function(){
       var curVal =(parseInt(jQuery(".qutBox-1 .qutInput input").val())-1);
       var val = (curVal-1) < 0 ? 0 :curVal -1;
       jQuery(".qutBox-1 .qutInput input").val(val);
   });
   jQuery(".qutBox-1 .plus").click(function(){
       var curVal = jQuery(".qutBox-1 .qutInput input").val();
       jQuery(".qutBox-1 .qutInput input").val(parseInt(curVal) + parseInt(1));
   });


   /*------LANGUAGE SELECT----------*/

   $('.district_selected').click(function() {
        $('.district_values').toggle();
    });

    $('.district_item').click(function() {
     var value = $(this).text();

    $('.district_selected').attr('data-value', value);
        $('.district_selected').html(value);
        $('.district_values').toggle();
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function() {
  var dateInput = $('input[name="date"]'); // Our date input has the name "date"
  dateInput.datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#date').datepicker('setStartDate', truncateDate(new Date())); // <-- SO DOES THIS
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>




</body>
</html>