<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title>Chef Dinner</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/my-kitchen.css">

</head>
<body>
<?php $this->load->view('front/header');?>
<div class="wrapper myProfileEditPage">
    <div class="container">
    <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="KtchnNav">
                    <ul>
                        <li class="profile"><a href="editpofile">Profile</a></li>
                        <li class="pwreset active"><a href="changepassword">Password Change</a></li>
                        <li class="preferences"><a href="socialnetwork">Preferences</a></li>
                    </ul>
                </div>
            </div>
            
            <div class="col-lg-9 col-md-9">
                <div class="kithchenForm editprofile">
                    <form action="<?php echo base_url() ?>changePass" method="post">
                        <div class="leftSec prflChngePw">
                            <h2 class="clr-black">Change Password</h2>
                            <div class="form-group clearfix">
                                <h4 class="clr-black">Old Password</h4>
                                <input type="password" class="input" placeholder="Enter Your Old Password" name="oldpassword" id="oldpassword" onblur="checkPass()">
                                <span id="message1"></span>
                             </div>
                             <div class="form-group clearfix"><a href="<?php echo base_url() ?>forgetPass" class="clr-red">In case you forgot your old password</a></div>
                            <div class="form-group clearfix">
                                <h4 class="clr-black">New Password</h4>
                                <input type="password" id="newpassword" class="input" placeholder="Enter Your New Password" name="newpassword" onblur="validate()">
                            </div>
                            <div class="form-group clearfix">
                                <h4 class="clr-black">Repeat Your New Password</h4>
                                <input type="password" id="repeatpass" class="input" placeholder="Repeat Your New Password" name="repeatpass" onblur="validate()">
                            </div>
                           <div class="form-group clearfix"> <span id="message"></span></div>
                            <div class="form-group sbmt-row">
                                <!-- <input type="submit" id="submit" value="Change Password" class="txt-upr sbmt float-left" /> -->
                                <button type="submit" id="submit" class="txt-upr sbmt float-left">Change Password</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/footer');?>
<script type="text/javascript">

// $('input[type=submit]').on('click', validate);
function checkPass(){
    var chefid = <?php echo $this->session->userdata('user_id') ?>;
    var oldpassword = $('#oldpassword').val();
    if(oldpassword != ''){
        $('#submit').attr('disabled',false);
         $.ajax({
                type:'POST',
                url:'<?php echo base_url(); ?>index.php/welcome/checkPass',
                // data:'state_id='+stateID,
                data:{chefid:chefid,oldpassword:oldpassword},
                success:function(data){
                    console.log(data);
                    if(data != '0'){
                         $('#submit').attr('disabled',false);
                    }else{
                        alert('Your old password is not matched');
                        $('#submit').attr('disabled',true);
                    }
                    //$('#city').html(html);
                }
            }); 

    }else{
        //alert('Please fill your old password');
        $('#message1').html('Please fill your old password').css('color', 'red');
        $('#submit').attr('disabled',true);
    }

}

function validate() {
  var password1 = $("#newpassword").val();
  var password2 = $("#repeatpass").val();

    if(password1 == password2) {
        $('#message').html('Password is same').css('color', 'green');
         $('#submit').attr('disabled',false);
         
    }
    else {
        $('#message').html("Password not same").css('color', 'red');
         $('#submit').attr('disabled',true);
       //  alert('hello');

    } 
}
//   $('#newpassword, #repeatpass').on('blut', function () {
//   if ($('#newpassword').val() == $('#repeatpass').val()) {
//     $('#message').html('Matching').css('color', 'green');
//     $('#submit').attr({disabled:false});
//   //  $('#submit').attr('disabled',false);

//   } else 
//     $('#message').html('Not Matching').css('color', 'red');
//     $('#submit').attr({disabled:true});
//    // $('#submit').attr('disabled',true);
// });
</script>
</body>
</html>