<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title><?php echo $this->lang->line('sitetitle'); ?></title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/home.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/chef-box.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/owl.css">


</head>
<body>
<?php
$this->load->view('front/header');
?>

<div class="wrapper srchBnr">
    <!-- <img src="images/top-banner.png" alt=""> -->
    <div class="container" style="height:550px;">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="bnrInner">
                    <h2 class="text-center">
                        Discover secret social dining places
                    </h2>
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled 
                    </p>
                    <div class="chfSrch">
                        <div class="srchBox">
                            <select class="float-left">
                                <option>District</option>
                                <option>District</option>
                                <option>District</option>
                                <option>District</option>
                                <option>District</option>
                                <option>District</option>
                            </select>
                            <input type="text" name=""  class="float-left" placeholder="Date">
                            <input type="submit" value="Search">
                            <a href="" class="advncSrch" data-toggle="modal" data-target="#advncSrch">Advanced Search</a>
                        </div>
                        <div class="modal" id="advncSrch" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-offset-top">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="advsrchBox">
                                            <h5 class="text-center">Advanced Search</h5>
                                            <div class="schfld text-center">
                                                <h6 class="">No. of Seats</h6>
                                                <div class="qutBox-1">
                                                    <span class="minus"></span>
                                                    <span class="qutInput">
                                                        <input readonly="" type="text" value="1" name="">
                                                    </span>
                                                    <span class="plus"></span>
                                                </div>
                                            </div>
                                            <div class="schfld text-center">
                                                <input type="text" placeholder="Select Date">
                                            </div>
                                            <div class="schfld text-center">
                                               <select>
                                                   <option>District</option>
                                                   <option>District</option>
                                                   <option>District</option>
                                                   <option>District</option>
                                                   <option>District</option>
                                                   <option>District</option>
                                               </select>
                                            </div>
                                            <div class="schfld text-center">
                                                <select>
                                                    <option>Cuisines</option>
                                                    <option>Indian</option>
                                                    <option>chinese</option>
                                                    <option>Italian</option>
                                                    <option>Thai</option>
                                                    <option>Continental</option>
                                                </select>
                                            </div>
                                            <div class="schfld text-center">
                                                <h6 class="">Price Per guest</h6>
                                                <div class="priceBox">
                                                    <div id="slider-range"></div>
                                                    <input class="value" type="text" id="amount" readonly>
                                                </div>
                                            </div>
                                            <div class="srchAply">
                                                <a href="" class="float-right txt-upr">Apply</a>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-default mdlclose txt-upr" data-dismiss="modal">
                                           Cancel
                                        </button>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="steps">
                        <ul>
                            <li>
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/chseprice.png" alt="">
                                </div>
                                <div class="stepDtl">
                                    <h6 class="txt-upr"><?php echo $this->lang->line('step1'); ?></h6>
                                    <p><?php echo $this->lang->line('step1text'); ?></p>
                                </div>
                            </li>
                            <li>
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/chsemenu.png" alt="">
                                </div>
                                <div class="stepDtl">
                                    <h6 class="txt-upr"><?php echo $this->lang->line('step2'); ?></h6>
                                    <p><?php echo $this->lang->line('step2text'); ?></p>
                                </div>
                            </li>
                            <li>
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/chsechf.png" alt="">
                                </div>
                                <div class="stepDtl">
                                    <h6 class="txt-upr"><?php echo $this->lang->line('step3'); ?></h6>
                                    <p><?php echo $this->lang->line('step3text'); ?></p>
                                </div>
                            </li>
                            <li>
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/chsepmnt.png" alt="">
                                </div>
                                <div class="stepDtl">
                                    <h6 class="txt-upr"><?php echo $this->lang->line('step4'); ?></h6>
                                    <p><?php echo $this->lang->line('step4text'); ?></p>
                                </div>
                            </li>
                            <li>
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/chsefood.png" alt="">
                                </div>
                                <div class="stepDtl">
                                    <h6 class="txt-upr"><?php echo $this->lang->line('step5'); ?></h6>
                                    <p><?php echo $this->lang->line('step5text'); ?></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper whatHot">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="secTitle wtht">
                    <span class="float-left">What’s Hot</span>
                    <a href="" class="float-right clr-red">Show All</a>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="htdish">
                    <div class="img">
                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>front/images/htdsh1.jpg" alt=""></a>
                    </div>
                    <div class="dshDtl">
                        <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                        <p>District/City,Country</p>
                    </div>
                </div>
                <div class="htdish">
                    <div class="img">
                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>front/images/htdsh2.jpg" alt=""></a>
                    </div>
                    <div class="dshDtl">
                        <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                        <p>District/City,Country</p>
                    </div>
                </div>
                <div class="htdish">
                    <div class="img">
                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>front/images/htdsh3.jpg" alt=""></a>
                    </div>
                    <div class="dshDtl">
                        <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                        <p>District/City,Country</p>
                    </div>
                </div>
                <div class="htdish">
                    <div class="img">
                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>front/images/htdsh4.jpg" alt=""></a>
                    </div>
                    <div class="dshDtl">
                        <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                        <p>District/City,Country</p>
                    </div>
                </div>
                <div class="htdish">
                    <div class="img">
                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>front/images/htdsh1.jpg" alt=""></a>
                    </div>
                    <div class="dshDtl">
                        <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                        <p>District/City,Country</p>
                    </div>
                </div>
                <div class="htdish">
                    <div class="img">
                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>front/images/htdsh2.jpg" alt=""></a>
                    </div>
                    <div class="dshDtl">
                        <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                        <p>District/City,Country</p>
                    </div>
                </div>
                <div class="htdish">
                    <div class="img">
                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>front/images/htdsh3.jpg" alt=""></a>
                    </div>
                    <div class="dshDtl">
                        <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                        <p>District/City,Country</p>
                    </div>
                </div>
                <div class="htdish mrRight">
                    <div class="img">
                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>front/images/htdsh4.jpg" alt=""></a>
                    </div>
                    <div class="dshDtl">
                        <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                        <p>District/City,Country</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper middlePart">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12 leftScrlBlock">
                <h3 class="secTitle chfrnking">
                    Chef Ranking
                </h3>
                <div class="blckSec">
                    <h5 class="smlTitle wklyBest">
                        <span class="float-left">Weekly Best Rated</span>
                        <a href="" class="float-right clr-red">Show All</a>
                    </h5>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food1.jpg" alt="">
                            <div class="rank">
                                1
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food2.jpg" alt="">
                            <div class="rank">
                                2
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food3.jpg" alt="">
                            <div class="rank">
                                3
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 class="smlTitle mstviewed">
                        <span class="float-left">Most Viewed</span>
                        <a href="" class="float-right clr-red">Show All</a>
                    </h5>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food1.jpg" alt="">
                            <div class="rank">
                                1
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food2.jpg" alt="">
                            <div class="rank">
                                2
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food3.jpg" alt="">
                            <div class="rank">
                                3
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h5 class="smlTitle mstbkmeked">
                        <span class="float-left">Most Bookmarked</span>
                        <a href="" class="float-right clr-red">Show All</a>
                    </h5>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food1.jpg" alt="">
                            <div class="rank">
                                1
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food2.jpg" alt="">
                            <div class="rank">
                                2
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chfBoxOne">
                        <div class="img">
                            <img src="<?php echo base_url();?>front/images/food3.jpg" alt="">
                            <div class="rank">
                                3
                            </div>
                        </div>
                        <div class="chfDtl">
                            <div class="float-left">
                                <div class="chfImg">
                                    <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                </div>
                                <div class="rate">
                                    <span>4.0 </span>
                                </div>
                            </div>
                            <div class="float-left">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="forsrvices">
                    <h3 class="secTitle dlymeal">
                        Daily Meal
                    </h3>
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food4.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food5.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food4.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="forsrvices">
                    <h3 class="secTitle wkendDish">
                        Weekend Meal for this Week
                    </h3>
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food4.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food5.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food4.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="forsrvices">
                    <h3 class="secTitle nwchf">
                        New Chef from Last 7 Days
                    </h3>
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food4.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food5.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food4.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="forsrvices">
                    <h3 class="secTitle picks">
                        Our Picks
                    </h3>
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food4.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food5.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chfBoxOne">
                                <div class="img">
                                    <img src="<?php echo base_url();?>front/images/food4.jpg" alt="">
                                    <div class="price clr-red">
                                       HKD125/Guest
                                    </div>
                                    <div class="bkMrkNmr clr-red">
                                        54
                                    </div>
                                    <div class="ctgry">
                                       <ul>
                                            <li>Indian</li>
                                            <li class="mrRight">Continental</li>
                                        </ul> 
                                    </div>
                                </div>
                                <div class="chfDtl">
                                    <div class="float-left">
                                        <div class="chfImg">
                                            <img class="img-fluid" src="<?php echo base_url();?>front/images/chefone.png" alt="">
                                        </div>
                                    </div>
                                    <div class="float-left">
                                        <div class="ratingBox">
                                            <ul>
                                                <li class="rateStar">
                                                    <img src="<?php echo base_url();?>front/images/fourstar.png" alt="">
                                                </li>
                                                <li class="rateCount">
                                                    <span>
                                                        4.0
                                                    </span>
                                                </li>
                                                <li class="rateNmbr text-center mrRight">
                                                    <span>125</span><br>Reviews
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="dshDtl clearfix">
                                        <div class="float-left">
                                            <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                            <p>District/City,Country</p>
                                        </div>
                                        <div class="float-right">
                                            <div class="reaction text-center">
                                                <span>
                                                    <img src="<?php echo base_url();?>front/images/yummy.png" alt=""><br/>125
                                                </span>
                                                <span class="mrRight">
                                                    <img src="<?php echo base_url();?>front/images/bad.png" alt=""><br/>12
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row fturedTesti">
            <div class="col-lg-12 col-md-12">
                <h3 class="secTitle fturedreview">
                    Featured Reviews
                </h3>
                <div class="owl-carousel owl-theme reviewSlide">
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review1.jpg" alt="">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                   <h6>Amarit Phayangay</h6>
                                   <div class="rate">
                                        Rated <span>4.0 </span>
                                   </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review2.jpg" alt="">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                    <h6>Amarit Phayangay</h6>
                                    <div class="rate">
                                        Rated <span>4.0 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review3.jpg" alt="">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                    <h6>Amarit Phayangay</h6>
                                    <div class="rate">
                                        Rated <span>4.0 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review1.jpg" alt="">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                    <h6>Amarit Phayangay</h6>
                                    <div class="rate">
                                        Rated <span>4.0 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review2.jpg" alt="">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                    <h6>Amarit Phayangay</h6>
                                    <div class="rate">
                                        Rated <span>4.0 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="reviewBox">
                            <div class="review-img">
                                <img class="img-fluide" src="<?php echo base_url();?>front/images/review3.jpg" alt="">
                                <div class="dshDtl">
                                    <a href="javascript:void(0)"><span class="ttl">Trendy Tradition</span></a>
                                    <p>District/City,Country</p>
                                </div>
                            </div>
                            <div class="cstmr">
                                <div class="img float-left">
                                    <img class="" src="<?php echo base_url();?>front/images/cstmr.png" alt="">
                                </div>
                                <div class="nmeRate float-left">
                                    <h6>Amarit Phayangay</h6>
                                    <div class="rate">
                                        Rated <span>4.0 </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cmnts mrBottom">
                                <div class="cmntTitle">
                                    <a href="javascript:Void(0);" class="clr-red">Good food and various kinds...</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit, sed do eiusmod...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="wrapper appDwnld">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="appLink">
                    <div class="appLinkBox">
                        <h5>Download ChefDiner for free on App Store or Google Play</h5>
                        <p>We'll send you a link, open it on your phone to download the app</p>
                        <div class="nmbrSec">
                            <input type="number" placeholder="+123456790">
                            <input type="submit" value="Send SMS">
                        </div>
                        <div class="appStore">
                            <a href=""><img src="<?php echo base_url();?>front/images/appstore.png" alt=""></a>
                            <a href=""><img src="<?php echo base_url();?>front/images/playstore.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="mobox">
                    <img src="<?php echo base_url();?>front/images/mobiles.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper explore">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="secTitle globe">
                    Explore the world
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <div class="zigZag tall">
                    <img src="<?php echo base_url();?>front/images/food7.jpg" alt="">
                    <div class="cntryName">
                        Hongkong
                    </div>
                </div>
                <div class="zigZag small">
                    <img src="<?php echo base_url();?>front/images/food8.jpg" alt="">
                    <div class="cntryName">
                        Singapore
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="zigZag small">
                    <img src="<?php echo base_url();?>front/images/food9.jpg" alt="">
                    <div class="cntrySrch text-center">
                        Where to?
                        <div class="cntrysrchBox">
                            <input type="text" placeholder="Hongkong">
                            <input type="submit" value="">
                        </div>  
                    </div>
                </div>
                <div class="zigZag tall">
                    <img src="<?php echo base_url();?>front/images/food10.jpg" alt="">
                    <div class="cntryName">
                            Philippines
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="zigZag tall">
                    <img src="<?php echo base_url();?>front/images/food11.jpg" alt="">
                    <div class="cntryName">
                            Thailand
                    </div>
                </div>
                <div class="zigZag small">
                    <img src="<?php echo base_url();?>front/images/food12.jpg" alt="">
                    <div class="cntryName">
                            Malaysia
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper newsletter">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="nwsBlock text-center">
                    <div class="newheading">
                        Stay updated with delicious news and upcoming events
                    </div>
                    <div class="cntrysrchBox">
                        <input type="email" placeholder="Email">
                        <input type="submit" value="send" class="txt-upr">
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper rfer">
    <div class="container">
        <div class="row">
            <div class="push-lg-2 col-lg-8 push-md-2 col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="referBlock text-center">
                            <img src="<?php echo base_url();?>front/images/chinaloc.png" alt="">
                            <p>
                                Biggest private dining network in China
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="referBlock text-center">
                            <img src="<?php echo base_url();?>front/images/reffer.png" alt="">
                            <p>
                                Refer a friend and get up to 100 USD
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="referBlock text-center">
                            <img src="<?php echo base_url();?>front/images/topratings.png" alt="">
                            <p>
                                9 out of 10 guests give the highest ratings
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>

<script src="<?php echo base_url();?>front/js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        var owl = $('.reviewSlide');
        owl.owlCarousel({
            margin: 9,
            nav: true,
            loop: true,
            navigation: false,
            responsive: {
                0: {
                    items: 1
                },
                500: {
                    items: 1
                },
                600: {
                    items: 2
                },
                700: {
                        items:3
                    },
                900: {
                        items:4
                    },
                1100: {
                    items: 5
                }
            }
        })
    });
</script>
<script>
   $(document).ready(function() {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 9,
            nav: true,
            loop: true,
            navigation: false,
            responsive: {
                0: {
                    items: 1
                },
                500: {
                    items: 1
                },
                600: {
                    items: 2
                },
                700: {
                         items:3
                     },
                900: {
                         items:3
                     },
                1100: {
                    items: 3
                }
            }
        })
    });
</script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 300,
            values: [ 0, 300 ],
            slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            }
        });
        $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) + " - $" + $( "#slider-range" ).slider( "values", 1 ) );
    });
</script>
<script type="text/javascript">
    jQuery(".qutBox-1 .minus").click(function(){
       var curVal =(parseInt(jQuery(".qutBox-1 .qutInput input").val())-1);
       var val = (curVal-1) < 0 ? 0 :curVal -1;
       jQuery(".qutBox-1 .qutInput input").val(val);
   });
   jQuery(".qutBox-1 .plus").click(function(){
       var curVal = jQuery(".qutBox-1 .qutInput input").val();
       jQuery(".qutBox-1 .qutInput input").val(parseInt(curVal) + parseInt(1));
   });
</script>






</body>
</html>