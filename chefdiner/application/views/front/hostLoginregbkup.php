<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title><?php echo $this->lang->line('sitetitle'); ?></title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/host-login.css">
</head>
<body>
<?php

$this->load->view('front/header');

?>
<div class="wrapper hostLogin">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sd-tabs sd-tab-interaction">
                    <div class="usersign">
                        <ul class="tabNav">
                            <li><a href="javascript:;" data-id="#loginHost" class="active login"><span>Login</span></a></li>
                            <li><a href="javascript:;" data-id="#registerHost" class="signup"> <span>Signup</span></a></li>
                        </ul>
                        <div class="tabContent">
                            <form id="loginHost" class="tab-pane usrLgForm" action="" method="post">
                                <div class="loginWith">
                                    <h5>Login to ChefDiner</h5>
                                    <a href="javascript:;"><img src="<?php echo base_url();?>front/images/fbl.png" alt=""></a>
                                    
                                    <p>
                                        If you don't prefer Social Media, feel free to
                                        use email and password to login.
                                    </p>
                                </div>

                                <div class="formGrp">
                                  <p id="statusHostLogin">
                                  </p>
                               </div>
                                <div class="formGrp">
                                    <input type="email" class="uname" id="hostEmail" name="" placeholder="UserName or Email*">
                                    <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                    <input type="password" class="upw" id="hostPass" placeholder="Password" name="" required>
                                    <span class="unptbg"></span>
                                </div>
                                <div class="fgtpw"><a href="forgot-password.html">Forgot Password</a></div>
                                <div class="formGrp">
                                    <!-- <input type="submit" class="txt-upr" value="continue"> -->
                                    <button type="button" class="txt-upr"  onclick="submitHostLogin()">continue</button>
                                </div>
                            </form>
                            <form id="registerHost" class="tab-pane usrLgForm" style="display:none;">
                                <div class="loginWith">
                                    <h5>Sign Up with ChefDiner</h5>
                                    <a href="javascript:;"><img src="<?php echo base_url();?>front/images/fbl.png" alt=""></a>
                                   
                                </div>
                                <div class="formGrp">
                                        <a href="javascript:;" class="signUpemail">Sign up with email</a>
                                </div>
                                <div class="rgform">
                                    <div class="formGrp">
                                        <input type="text" class="uname" name="" placeholder="First Name">
                                        <span class="unptbg"></span>
                                    </div>
                                    <div class="formGrp">
                                        <input type="text" class="uname" name="" placeholder="Last Name">
                                        <span class="unptbg"></span>
                                    </div>
                                    <div class="formGrp">
                                        <input type="text" class="uemail" name="" placeholder="Email">
                                        <span class="unptbg"></span>
                                    </div>
                                    <div class="formGrp">
                                        <input type="password" class="upw" placeholder="Password" name="" required>
                                        <span class="unptbg"></span>
                                    </div>
                                    <div class="formGrp">
                                        <input type="password" class="upw" placeholder="Confirm Password" name="" required>
                                        <span class="unptbg"></span>
                                    </div>
                                    <div class="formGrp">
                                        <input type="number" class="unmbr" placeholder="Phone Number" name="" required>
                                        <span class="unptbg"></span>
                                    </div>
                                    <div class="formGrp">
                                        <p>
                                            We will be using your phone number to send you information about bookings and other important events.
                                        </p>
                                        
                                    </div>
                                    <div class="sgntc"><input type="checkbox" checked> Agree  <a href='javascript:;'>Term & Conditions & Privacy Policy</a></div>
                                    <div class="formGrp">
                                            <input type="submit" class="txt-upr" value="Register">
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>

</body>
</html>