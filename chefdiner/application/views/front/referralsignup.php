<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title><?php echo $this->lang->line('sitetitle'); ?></title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/host-login.css">
</head>
<body>
<?php

$this->load->view('front/header');

?>
<div class="wrapper hostLogin">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sd-tabs sd-tab-interaction">
                    <div class="usersign">
                        <ul class="tabNav">
                        <li><a href="javascript:;" data-id="#registerHost" class="active signup"> <span>Signup</span></a></li>
                        </ul>
                        <div class="tabContent">
                         <div class="loginWith">
                                  <h5>Sign Up with ChefDiner</h5>
                                
                                </div>
                            
                                
                                <?php if($this->session->flashdata('error')):?>
                                    <div class="alert alert-danger">
                                      <?php echo $this->session->flashdata('error');?>
                                    </div>
                                  <?php endif;?>
                                    
                                  <?php if($this->session->flashdata('success')):?>
                                    <div class="alert alert-success">
                                      <?php echo $this->session->flashdata('success');?>
                                    </div>
                                  <?php endif;?>
                                    <?php 
                                    $langu =  $this->session->userdata('site_lang');
                                      if($langu !=''){
                                        $langu =  $this->session->userdata('site_lang');
                                      }else{
                                        $langu = 'english';
                                      }
                                ?>
                                <!-- <div class="formGrp">
                                        <a href="javascript:;" class="signUpemail">Sign up with email</a>
                                </div> -->
                               <form class="tab-pane usrLgForm"  method="post" action="<?php echo base_url();?>RegisterRef">
                              <!--   <div class="rgform"> -->
                                    <div class="formGrp">
                                        <input type="text" class="uname" name="first_name" placeholder="First Name" required>
                                        <span class="unptbg"></span>
                                    </div>
                                    <div class="formGrp">
                                        <input type="text" class="uname" name="last_name" placeholder="Last Name">
                                        <span class="unptbg" required></span>
                                    </div>
                                    <div class="formGrp">
                                        <input type="text" class="uemail" name="email" placeholder="Email">
                                        <span class="unptbg" required></span>
                                    </div>
                                    <div class="formGrp">
                                        <input type="password" class="upw"  name="password" id="passwordRef" placeholder="Password" name="" required>
                                        <span class="unptbg"></span>
                                    </div>
                                    <div class="formGrp">
                                        <input type="password" class="upw" name="confrm_password" id="confirm_passwordRef"  placeholder="Confirm Password" name="" required>
                                        <span class="unptbg"></span>
                                    </div>
                                   
                                    <div class="formGrp">
                                        <input type="number" class="unmbr" name="phone_number" placeholder="Phone Number" name="" required>
                                        <span class="unptbg"></span>
                                    </div>
                                    <input type="hidden" name="rcode" value="<?php if(isset($rcode) && !empty($rcode)) { echo $rcode; }?>">
                                    <div class="formGrp">
                                        <p>
                                            We will be using your phone number to send you information about bookings and other important events.
                                        </p>
                                        
                                    </div>
                                    <div class="sgntc"><input type="checkbox" checked> Agree  <a href='javascript:;'>Term & Conditions & Privacy Policy</a></div>
                                    <div class="formGrp">
                                     <button type="submit" class="txt-upr"  >Register</button>
                                          <!--   <input type="submit" class="txt-upr" value="Register"> -->
                                    </div>
                               <!--  </div> -->
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('front/footer');
?>

</body>
</html>