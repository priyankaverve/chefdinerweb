<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<title><?php echo $this->lang->line('sitetitle'); ?></title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/header-footer.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>front/css/host-login.css">
</head>
<body>
<?php $this->load->view('front/header');?>
<div class="wrapper hostLogin">
    <div class="container">
     <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error');?>
        </div>
      <?php endif;?>
        
      <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success');?>
        </div>
      <?php endif;?>
        <div class="row">
            <div class="col-md-12">
                <div class="sd-tabs sd-tab-interaction">
                    <div class="usersign">
                        <div class="tabContent">
                            <form id="login" class="tab-pane usrLgForm" action="<?php echo base_url() ?>updateforget" method="post">
                            <input  id="email" name="email" value="<?php if(isset($email)){ echo $email;} ?>" type="hidden">
                            <input  id="userid" name="userid" value="<?php if(isset($userid)){ echo $userid;} ?>" type="hidden">
                                <div class="loginWith">
                                    <h5>Create New Password</h5>
                                </div>
                                <div class="formGrp">
                                    <input type="password" class="uname" name="newpass" id="newpassword" placeholder="New Password*" onblur="validate()">
                                    <span class="unptbg"></span>
                                </div>
                                <div class="formGrp">
                                    <input type="password" class="uname" name="confirmpass" id="repeatpass" placeholder="Confirm Password*" onblur="validate()">
                                    <span class="unptbg"></span>
                                </div>
                                 <div class="form-group"> <span id="message"></span></div>
                                <div class="formGrp">
                                    <!-- <input type="submit" name="submit" class="txt-upr" value="Save"> -->
                                     <button type="submit" name="submit" id="submit" class="txt-upr">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/footer');?>
<script type="text/javascript">
    function validate() {
  var password1 = $("#newpassword").val();
  var password2 = $("#repeatpass").val();

    if(password1 == password2) {
        $('#message').html('Password is same').css('color', 'green');
         $('#submit').attr('disabled',false);
         
    }
    else {
        $('#message').html("Password not same").css('color', 'red');
         $('#submit').attr('disabled',true);
       //  alert('hello');

    } 
}
</script>
</body>
</html>