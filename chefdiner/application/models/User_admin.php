<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_admin extends CI_Model{
    
    function __construct() {
		parent::__construct();
    }

    function isAdminLoggedIn($value,$value1){
        $query = "SELECT * FROM  `admin_login` WHERE  `username` =  '$value' AND  `password` =  '$value1'";
        //print_r($query);die;
		$query = $this->db->query($query);

        if($query->num_rows()){
            $res = $query->row();
            //set admin session
            $userdata = array('admin_id'=>$res->admin_id,
				'name'=>$res->name,
				'username'=>$res->username,
				'email'=>$res->email,
                'logged_in_back' => true
			);
            $this->session->set_userdata($userdata);
			return TRUE;
        } else {
            return FALSE;   
        }
	}

    /*****Privacy Policy by language*******/
    function getpolicy($sitelang,$userid){
        if($sitelang == ''){
            $sitelang ='english';
        }
       $query = "SELECT * FROM  `privacy_policy` WHERE status='1' and language ='$sitelang' and user_id='$userid' LIMIT 1";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }

    /*******Save Policy*******/
	function savePolicy($updateData,$id,$language){
        $where = @$this->db->where(array('id'=> $id,'language'=>$language));
        $updateData = @$this->db->update('privacy_policy',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }
    /******Terms & condition policy*****/
    function gettermsconditions($sitelang,$userid){
        if($sitelang == ''){
            $sitelang ='english';
        }
       $query = "SELECT * FROM  `terms_condition` WHERE status='1' and language ='$sitelang' and user_id='$userid' LIMIT 1";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }
    /*****Save terms & condition******/

    function saveTerms($updateData,$id,$language){
        $where = @$this->db->where(array('id'=> $id,'language'=>$language));
        $updateData = @$this->db->update('terms_condition',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    /*******Get all jobs******/
	
    function getalljobs($sitelang,$userid){
        if($sitelang == ''){
            $sitelang ='english';
        }
       $query = "SELECT * FROM  `jobs` WHERE status='1' and language ='$sitelang' and user_id='$userid' ";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }

    /******Add new jobs*****/
    function saveJobs($insertData){
        if(!empty($insertData)){
            $this->db->insert('jobs',$insertData);
            return TRUE;
        }else{
            return FALSE;
        }
    }

    /****get a job**/
    function getjobs($id){
        $query = "SELECT * FROM  `jobs` WHERE status='1' and id ='$id' ";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /******Save updated jobs*****/
    function saveupdatejobs($updateData,$id,$language){
        $where = @$this->db->where(array('id'=> $id,'language'=>$language));
        $updateData = @$this->db->update('jobs',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }
    /*****Delete jobs*****/
    function deletejobs($id){
        if (!empty($id)):
     $where = @$this->db->where('id',$id);
            $this->db->where($where);
            $this->db->delete('jobs');
        else: return "Invalid Input Provided";
        endif;
    }

    /*******Contact details*******/

    function getcontact($language,$userid){
         if($language == ''){
            $language ='english';
        }
       $query = "SELECT * FROM  `contact` WHERE status='1' and language ='$language' and user_id='$userid' ";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /*****save Contact*****/

    function saveContact($updateData,$id,$language){
        $where = @$this->db->where(array('id'=> $id,'language'=>$language));
        $updateData = @$this->db->update('contact',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

     /*******Get all Users list******/
    
    function getallusers(){
       
       $query = "SELECT * FROM  `register` order by created_at desc ";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }
    /********Get chef users*******/

    function getchefuser($id){
        $query = "SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
         become_chef.user_id = $id and register.is_chef='1'";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }

    /******Verify chef by admin*******/
    function Verfiychef($updateData,$userid){
        $where = @$this->db->where(array('id'=> $userid));
        $updateData = @$this->db->update('register',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    /********Get users*******/

    function getusers($id){
        $query = "SELECT * FROM register where id = '$id' ";
      // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }
    /******Verfiy Usersby admin*******/
    function VerfiyUsers($updateData,$userid){
        $where = @$this->db->where(array('id'=> $userid));
        $updateData = @$this->db->update('register',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }
    /******get menus*****/

    function getallList(){
        // $query = "SELECT * FROM menus LEFT JOIN register on menus.user_id = register.id where menus.user_id = register.id";
        $query = "SELECT m.id as menu_id, m.user_id,m.menu_title,m.menu_image,m.service_type,m.price_per_person,m.currency,m.created_at, m.updated_at,m.status,r.id,r.first_name,r.last_name FROM menus m,register r WHERE r.id = m.user_id
        order by m.created_at desc";
        //print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /******** get sub menu ********/

    function getallsubmenu($id){
        $query = "SELECT * FROM `sub_menus` WHERE `menu_id` = '$id' order by created_at desc";
        //print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function getmenudata($id){
        $query = "SELECT * FROM `menus` WHERE `id` = '$id'";
        //print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    

    function getsubmenudata($id){
        $query = "SELECT * FROM `sub_menus` WHERE `id` = '$id'";
        //print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    function Savemenu($updateData,$menuid){
        $where = @$this->db->where(array('id'=> $menuid));
        $updateData = @$this->db->update('menus',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    function SaveSubmenu($updateData,$menuid){
        $where = @$this->db->where(array('id'=> $menuid));
        $updateData = @$this->db->update('sub_menus',$updateData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    /********Delete menus********/

    function deleteMenus($menuid){
        if(!empty($menuid)){
            $tablename = 'menus';
            $where = array('id'=>$menuid);
            $this->db->where($where);
          $deleteData =  $this->db->delete($tablename);

          if($deleteData){
            $tablename1 = 'sub_menus';
            $where1 = array('menu_id'=>$menuid);
            $this->db->where($where1);
          $this->db->delete($tablename1);
          }

            return TRUE;
        }else{
            return FALSE;
        }

    }

    /*******Delete submenus********/
    function deletesubMenus($submenuid){
        if(!empty($submenuid)){
            $tablename = 'sub_menus';
            $where = array('id'=>$submenuid);
            $this->db->where($where);
            $this->db->delete($tablename);
            return TRUE;
        }else{
            return FALSE;
        }
    }

    /******Delete User*****/


    function deleteusers($userid){
        if(!empty($userid)){
            $tablename = 'register';
            $where = array('id'=>$userid);
            $this->db->where($where);
          $deleteData =  $this->db->delete($tablename);

          if($deleteData){
            $tablename1 = 'become_chef';
            $where1 = array('user_id'=>$userid);
            $this->db->where($where1);
          $this->db->delete($tablename1);
          }

            return TRUE;
        }else{
            return FALSE;
        }

    }

    /*******get booking data for payment******/
    function getbookingCode($bookingid,$userid){
        $query = "SELECT * FROM `booknow` WHERE `booknow_id` = '$bookingid'  and `main_user_id` = '$userid'";
       // print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }

    function updateBooking($bookingid,$data){
        $where = @$this->db->where(array('booknow_id'=> $bookingid));
        $updateData = @$this->db->update('booknow',$data,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    function getbookingCode1($id){
        $query = "SELECT * FROM `payment` WHERE `booking_id` = '$id' ";
        //print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();

    }

    public function insertstripe($data){
        // $this->db->insert('payments', $data);
        // return ($this->db->affected_rows() != 1) ? false : true;
        $insert = $this->db->insert('payment', $data);
        
        //return the status
        if($insert){
            return $this->db->insert_id();;
        }else{
            return false;
        }
    }

    function getPayDetails($id){
        $query = "SELECT * FROM `payment` WHERE `payement_id` = '$id' ";
        //print_r($query);die;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /*******Get booking******/
    function getAcceptedbooking() {
        $query = "SELECT b.booknow_id,b.booking_id,b.chef_id,b.main_user_id,b.date, b.time,b.total_seats, b.menu_id,b.menu_price, b.menu_currency, b.service_type,b.food_delivery, b.kitchen_tool, b.first_name, b.last_name, b.email,b.contact_number, b.delivery_address,b.user_msg, b.created_at,b.updated_at,b.status,b.booking_status,b.shared_id,b.payment_type,b.payment_status,b.payment_device,r.id as chefids,r.first_name as cheffname,r.last_name as cheflname FROM booknow b , register r WHERE b.chef_id = r.id and b.booking_status ='Accepted' order by b.created_at desc";
          $query = $this->db->query($query);
        return $query->result_array();
    }

      function getwaitingBooking() {
        $query = "SELECT b.booknow_id,b.booking_id,b.chef_id,b.main_user_id,b.date, b.time,b.total_seats, b.menu_id,b.menu_price, b.menu_currency, b.service_type,b.food_delivery, b.kitchen_tool, b.first_name, b.last_name, b.email,b.contact_number, b.delivery_address,b.user_msg, b.created_at,b.updated_at,b.status,b.booking_status,b.shared_id,b.payment_type,b.payment_status,b.payment_device,r.id as chefids,r.first_name as cheffname,r.last_name as cheflname FROM booknow b , register r WHERE b.chef_id = r.id and b.booking_status ='Waiting for approval' order by b.created_at desc";
      //  print_r($query);die;
          $query = $this->db->query($query);
        return $query->result_array();
    }

    function getRejectedbooking(){
         $query = "SELECT b.booknow_id,b.booking_id,b.chef_id,b.main_user_id,b.date, b.time,b.total_seats, b.menu_id,b.menu_price, b.menu_currency, b.service_type,b.food_delivery, b.kitchen_tool, b.first_name, b.last_name, b.email,b.contact_number, b.delivery_address,b.user_msg, b.created_at,b.updated_at,b.status,b.booking_status,b.shared_id,b.payment_type,b.payment_status,b.payment_device,r.id as chefids,r.first_name as cheffname,r.last_name as cheflname FROM booknow b , register r WHERE b.chef_id = r.id and b.booking_status ='Rejected' order by b.created_at desc";
          $query = $this->db->query($query);
        return $query->result_array();

    }

    function referralamount(){
        $query = "SELECT * FROM referral_user";
          $query = $this->db->query($query);
        return $query->result_array();
    }

    function saveReferralAmount($rid,$data){

        $where = @$this->db->where(array('r_id'=> $rid));
        $updateData = @$this->db->update('referral_user',$data,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }

    }

    function getCurr($bookingid){
        $query = "SELECT menu_currency FROM booknow where booknow_id='$bookingid'";
          $query = $this->db->query($query);
        return $query->result_array();
    }

    function checkRef($id){
        $query = "SELECT user_referral_code FROM register where id='$id'";
          $query = $this->db->query($query);
        return $query->result_array();
    }

    function GetuserRef($refCode){
         $query = "SELECT * FROM register where referal_code='$refCode'";
          $query = $this->db->query($query);
        return $query->result_array();
    }
    function getRefAmt(){
         $query = "SELECT * FROM referral_user";
          $query = $this->db->query($query);
        return $query->result_array();
    }

    function updateUsers($refuse,$uData){
        $where = @$this->db->where(array('id'=> $refuse));
        $updateData = @$this->db->update('register',$uData,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }

    /*****count users******/

    function verifyusers(){
        $query = "SELECT * FROM register where is_verify='yes'";
          $query = $this->db->query($query);
        return $query->result_array();

    }

    function notverifyusers(){
        $query = "SELECT * FROM register where is_verify='no'";
          $query = $this->db->query($query);
        return $query->result_array();

    }

    function totalChef(){
        $query = "SELECT * FROM become_chef";
          $query = $this->db->query($query);
        return $query->result_array();

    }

    function getpayment(){
        $query = "SELECT * FROM `payment` order by created_at desc limit 4";
          $query = $this->db->query($query);
        return $query->result_array();
    }

    function getmenus($userid){
         $query = "SELECT * FROM `menus` WHERE user_id ='$userid' and status='1' ";
        // print_r($query);
          $query = $this->db->query($query);
        return $query->result_array();
    }

     function MenusSubmenus($userid, $menuid)
    {
        
        $returnData = array();
         $req = "SELECT sm.id,sm.menu_id,sm.user_id,sm.dish_name,sm.dish_image,sm.dish_category,sm.created_at, sm.updated_at,sm.status,m.id,m.price_per_person as menu_price,m.currency as menucurrency,m.menu_title,m.whatshot,m.menu_counter,m.menu_image FROM sub_menus sm , menus m WHERE sm.menu_id = m.id and sm.menu_id ='$menuid' and sm.user_id ='$userid'";
       // print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                
                if ($val->dish_image != '') {
                    $val->dish_image = base_url() . 'assets/submenuImage/' . $val->dish_image;
                } else {
                    $val->dish_image = '';
                }
                if ($val->menu_image != '') {
                    $val->menu_image = base_url() . 'assets/menuImgae/' . $val->menu_image;
                } else {
                    $val->menu_image = '';
                }

            }
        }
        return $returnData;
        
    }

    /*****Admin charges******/

     function adminCharges(){
        $query = "SELECT * FROM admin_charges";
          $query = $this->db->query($query);
        return $query->result_array();
    }

    /*****Save admin charges******/
    function saveAdmincharges($chrg_id,$data){
         $where = @$this->db->where(array('chrg_id'=> $chrg_id));
        $updateData = @$this->db->update('admin_charges',$data,$where);

        if($updateData){
            return TRUE;
        }else {
            return FALSE;
        }
    }


}
