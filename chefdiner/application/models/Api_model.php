<?php
class Api_model extends CI_Model
{
    
    public function _generate_token()
    {
        $this->load->helper('security');
        $salt    = do_hash(time() . mt_rand());
        $new_key = substr($salt, 0, config_item('rest_key_length'));
        return $new_key;
    }
    
    function userLogin($data)
    {
        $req = $this->db->select('*')->where(array(
            'email' => $data['email'],
            'is_verify' => 'yes'
        ))->get('register');
        
        if ($req->num_rows() > 0) {
            $res = $req->row();
            
            if (md5($data['password']) == $res->password) {
                if ($res->status == '1') { //if user is active
                    $userid      = $res->id;
                    $first_name  = $res->first_name;
                    //udating user deviceid and auth token
                    $updateToken = $this->updateDeviceIdToken($userid, $data['deviceId'], $data['authtoken'], $data['deviceType']);
                    if (!$updateToken) {
                        return FALSE;
                    } else {
                        $returnData['userId']         = $userid;
                        $returnData['first_name']     = $first_name;
                        $returnData['last_name']      = $res->last_name;
                        //$returnData['authtoken'] = $data['authtoken'];
                        $returnData['is_chef']        = $res->is_chef;
                        $returnData['is_verify_chef'] = $res->is_verify_chef;
                        $returnData['phone_number']   = $res->phone_number;
                        $returnData['email']          = $res->email;
                        $returnData['referal_code']   = $res->referal_code;
                        if ($res->facebook_login == 'yes' && $res->facebook_login != '') {
                            if (empty($res->profile_pic)) {
                                $res->profile_pic = '';
                            } else {
                                $res->profile_pic = $res->profile_pic;
                            }
                        } else {
                            if (empty($res->profile_pic)) {
                                $res->profile_pic = '';
                            } else {
                                $res->profile_pic = base_url() . 'assets/chefprofile/' . $res->profile_pic;
                            }
                        }
                        
                        $returnData['profile_pic'] = $res->profile_pic;
                        return $returnData;
                        //return TRUE;
                    }
                    
                } else {
                    return 'NA'; //not active
                }
            } else {
                return 'WP'; //wrong password
            }
        } else {
            return 'NA'; //not active
        }
    }
    
    /**
     * Update user deviceid and auth token while login
     */
    function updateDeviceIdToken($userid, $deviceId, $authToken, $deviceType)
    {
        $req = $this->db->select('id')->where('id', $userid)->get('register');
        if ($req->num_rows()) {
            $this->db->update('register', array(
                'deviceId' => $deviceId,
                'authtoken' => $authToken,
                'deviceType' => $deviceType
            ), array(
                'id' => $userid
            ));
            return TRUE;
        }
        
        return FALSE;
    }
    function userRegister($data)
    {
        if (!empty($data)) {
            /******Check email already exits******/
            
            $email = $data['email'];
            $query = "SELECT *  from register where email ='$email'";
            $req   = $this->db->query($query);
            if ($req->num_rows() > 0) {
                return 'AE'; // email already exist
            } else {
                $this->db->insert('register', $data);
                return array(
                    'id' => $this->db->insert_id()
                );
            }
            
        } else {
            return 'NR'; //not register
        }
        
    }
    
    /**
     * Function for check provided token is valid or not
     */
    function isValidToken($authToken)
    {
        $this->db->select('*');
        $this->db->where('authtoken', $authToken);
        if ($query = $this->db->get('register')) {
            if ($query->num_rows() > 0) {
                //echo "string";die;
                return $query->row();
            }
        }
        
        return FALSE;
    }
    
    /********Become a chef******/
    function becomeChef($data)
    {
        $userid = $data['user_id'];
        
        //check user exists in beacomechef table
        $query = "SELECT *  from become_chef where user_id ='$userid'";
        $req   = $this->db->query($query);
        if ($req->num_rows() > 0) {
            /******Update profile of chef******/
            $chefdetails = $req->result();
            //print_r($chefdetails);
            $chefid      = $chefdetails[0]->id;
            $where       = array(
                'user_id' => $userid,
                'id' => $chefid
            );
            $isUpdate    = $this->db->update('become_chef', $data, $where); // updata on become a chef
            if ($isUpdate) {
                /***update register table****/
                $wh                       = array(
                    'id' => $userid
                );
                $updatedata['is_chef']    = '1';
                $updatedata['first_name'] = $data['chef_fname'];
                $updatedata['last_name']  = $data['chef_lname'];
                if (array_key_exists('profile_pic', $data)) {
                    $updatedata['profile_pic'] = $data['profile_pic'];
                }
                
                $updatedata['phone_number'] = $data['phone_number'];
                $updatedata['currency']     = $data['currency'];
                $updatedata['updated_at']   = date('Y-m-d H:i:s');
                 $updateregister = $this->db->update('register', $updatedata, $wh);
                if ($updateregister){
                    /*****updatemenucurrency*******/
                // $whmenu = array(
                //     'user_id' => $userid
                // );
                //     $updateMenu['currency'] = $data['currency'];
                //     $updatemenu = $this->db->update('menus', $updateMenu, $whmenu);

                $getAllmenus = "SELECT * from menus where user_id ='$userid'";
                $querymenus = $this->db->query($getAllmenus);
                if ($querymenus->num_rows() > 0) {
                    $resmnus = $querymenus->result();
                    $i=0;
                    foreach ($resmnus as $key => $value) {
                       // print_r($value);
                        $menuid= $value->id;
                        $amount = $value->price_per_person;
                        $actualprice = $value->actualprice;
                        $to_cur =$data['currency'];
                        $from_cur =  $value->currency;
                        if($amount > 0 && $to_cur != $from_cur && $actualprice > 0 ){
                           // echo 'if';
                            $convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);
                            $convertedActualPrice = $this->convertCurrency($actualprice, $from_cur, $to_cur);

                        }else{
                           // echo 'else';
                            $convertedPrice = $amount;
                            $convertedActualPrice = $actualprice;
                        }
                         $whmenu = array(
                            'user_id' => $userid,'id'=>$menuid
                        );
                            $updateMenu['price_per_person'] = round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                            $updateMenu['actualprice'] = round($convertedActualPrice, 2, PHP_ROUND_HALF_UP);
                            $updateMenu['currency'] = $to_cur;
                            $updatemenu = $this->db->update('menus', $updateMenu, $whmenu);
                        // $updatemenu = $this->db->query("UPDATE menus SET price_per_person='$convertedPrice' , currency = '$to_cur' WHERE user_id='$userid' and id='$menuid'");
                      //  echo $this->db->last_query();
                        $i++;
                    }
               
                    // $countmenus = count($resmnus);
                    // echo $countmenus;die;

                 }
                  

                }
            }
            return array(
                'user_id' => $userid,
                'id' => $chefid
            ); //record update
        } else {
            /*****register user as chef*******/
            $isSaved      = $this->db->insert('become_chef', $data);
            $lastinserted = $this->db->insert_id();
            if ($isSaved) {
                /***update register table****/
                $wh                       = array(
                    'id' => $userid
                );
                $updatedata['is_chef']    = '1';
                $updatedata['first_name'] = $data['chef_fname'];
                $updatedata['last_name']  = $data['chef_lname'];
                if (array_key_exists('profile_pic', $data)) {
                    $updatedata['profile_pic'] = $data['profile_pic'];
                }
                // $updatedata['profile_pic'] =$data['profile_pic'];
                $updatedata['phone_number'] = $data['phone_number'];
                $updatedata['currency']     = $data['currency'];
                $updatedata['updated_at']   = date('Y-m-d H:i:s');
                $this->db->update('register', $updatedata, $wh);
                
                
            }
            
            return array(
                'user_id' => $userid,
                'id' => $lastinserted
            ); //record update
        }
        
    }
    /****get country*****/
    function country()
    {
        $returnData = array();
        $req        = "SELECT * FROM  `countries`";
        
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        
        return $returnData;
    }
    
    /*******get city******/
    
    function getcity($countryid)
    {
        $returnData = array();
        $statesarr  = array();
        if (!empty($countryid)) {
            /*****get state id by countryid *****/
            
            $req1   = "SELECT * FROM  `states` where country_id = '$countryid'";
            $query1 = $this->db->query($req1);
            if ($query1->num_rows() > 0) {
                $states = $query1->result_array();
                foreach ($states as $key => $value) {
                    //     echo "<pre>";
                    // print_r($value);
                    $statesarr[] = $value['id'];
                }
                // echo "<pre>";
                // print_r($statesarr);
                
                $allstate = implode(",", $statesarr);
                //echo $allstate;
                // die;
                /****Select city***/
                
                
                $req   = "SELECT * FROM  `cities` where state_id in($allstate)";
                $query = $this->db->query($req);
                if ($query->num_rows() > 0) {
                    $returnData = $query->result();
                }
                
                return $returnData;
            }
            
        }
        
    }
    
    /********get Chef*******/
    function getChef($latitude, $longitude,$filter,$seats,$date,$minprice,$maxprice,$country,$loginuserid)
    {
        $idarr      = array();
        $returnData = array();
        $req1       = "SELECT * FROM  `register` where is_verify_chef='yes' 
        and is_chef ='1' and is_verify='yes' and is_user = '1'";
        //print_r($req);die;
        $query1     = $this->db->query($req1);
        if ($query1->num_rows() > 0) {
            $users = $query1->result();
            foreach ($users as $key => $value) {
                //print_r($value);die;
                $idarr[] = $value->id;
                
            }
            if($loginuserid !='' && $loginuserid !='0' &&in_array($loginuserid , $idarr)){
                $key =  array_search($loginuserid,$idarr);
                unset($idarr[$key]);
              // /  print_r($idarr);die;
            }
            $before7Days = date('Y-m-d', strtotime('-7 days'));
           // echo $before7Days;die;
            //print_r($idarr);
            $ids = implode(",", $idarr); 
            // $req = "SELECT * FROM  `become_chef` where user_id in ($ids)";
            // $req="SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
            //          become_chef.user_id in ($ids)";
            // $req="SELECT *,become_chef.id as cid, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) HAVING distance < 30 ORDER BY distance";
           if($filter == 'newchef'){
                $req   = "SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) HAVING distance < 30 and date(become_chef.created_at) >='$before7Days' ORDER BY distance";
           }elseif($filter == 'whatshot'){
            /******get menus*******/
            $req   = "SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) HAVING distance < 30 ORDER BY register.whatshot_counter desc limit 30";
         
           }elseif($filter == 'selectoption'){
            $query = "SELECT * FROM `chef_calendar` WHERE `chef_id` in ($ids) and date = '$date' and ava_status = '1'";
                //print_r($req);die;
                $result = $this->db->query($query);
                if ($result->num_rows() > 0) {
                    $chefs = $result->result();
                    foreach ($chefs as $key => $value) {
                        //print_r($value);die;
                        $chefidarr[] = $value->chef_id;
                        
                    }
                }else{
                    $chefidarr = '';
                }

                if(!empty($chefidarr)){
                    $chefids = implode(",", $chefidarr); 
                }else{
                    $chefids ='';
                }


            /******Get chef price on menu table*****/
            if(!empty($chefids)){
            $que = "SELECT * FROM `menus` WHERE price_per_person >= '$minprice'  and price_per_person <= '$maxprice' and user_id in ($chefids)";
             $res = $this->db->query($que);
                if ($res->num_rows() > 0) {
                    $chefsuser = $res->result();
                    foreach ($chefsuser as $key => $value) {
                        //print_r($value);die;
                        $chefarr[] = $value->user_id;
                        
                    }
                }else{
                    $chefarr='';
                }

            }else{
                $chefarr='';
            }
            
           

                if(!empty($chefarr)){
                    $chefUid = implode(",", array_unique($chefarr)); 
                }else{
                    $chefUid ='';
                }
            


//echo $chefUid;die;
                if(!empty($chefUid)){
                    // $req   = "SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.chef_country = '$country' and become_chef.from_guest = '$seats' and become_chef.upto_guest >= '$seats' and become_chef.user_id in ($chefUid) HAVING distance < 30 ORDER BY register.whatshot_counter desc limit 30";
                $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.chef_country = '$country' and become_chef.from_guest = '$seats' and become_chef.upto_guest >= '$seats' and become_chef.user_id in ($chefUid) ORDER BY register.whatshot_counter desc limit 30";
                }elseif(!empty($chefids)){
                // $req   = "SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.chef_country = '$country' and become_chef.from_guest = '$seats' and become_chef.upto_guest >= '$seats' and become_chef.user_id in ($chefids) HAVING distance < 30 ORDER BY register.whatshot_counter desc limit 30";
                    $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.chef_country = '$country' and become_chef.from_guest = '$seats' and become_chef.upto_guest >= '$seats' and become_chef.user_id in ($chefids) ORDER BY register.whatshot_counter desc limit 30";
                }else{
                    // $req   = "SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.chef_country = '$country' and become_chef.from_guest = '$seats' and become_chef.upto_guest >= '$seats' and become_chef.user_id in ($ids) HAVING distance < 30 ORDER BY register.whatshot_counter desc limit 30";
                    $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.chef_country = '$country' and become_chef.from_guest = '$seats' and become_chef.upto_guest >= '$seats' and become_chef.user_id in ($ids) ORDER BY register.whatshot_counter desc limit 30";
                }
    

    //print_r($req);die;

           }else{
                 $req   = "SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) HAVING distance < 30 ORDER BY distance";
           }
            
            // $req="SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef  WHERE become_chef.user_id in ($ids) HAVING distance < 30 ORDER BY distance";
           // print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
                return $returnData;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    /********Add a menu******/
    function addMenu($data)
    {
        $menuid = $data['id'];
        if (!empty($menuid) && $menuid != '0') {
            //check user exists in beacomechef table
            $query = "SELECT * FROM `menus` where id ='$menuid'";
            $req   = $this->db->query($query);
            if ($req->num_rows() > 0) {
                $data['updated_at'] = date('Y-m-d H:i:s');
                /******Update profile of chef******/
                $menudetails        = $req->result();
                //print_r($chefdetails);
                $userid             = $menudetails[0]->user_id;
                $where              = array(
                    'user_id' => $userid,
                    'id' => $menuid
                );
                $isUpdate           = $this->db->update('menus', $data, $where); // updata on become a chef
                /*****Save cuisine in become chef table****/

                
                if ($isUpdate) {
                    $cuisine = $data['cusine'];
                    $subcusine = $data['subcusine'];
                    
                    // $where1 = array('user_id' => $userid);
                    // $this->db->set('cuisine', 'CONCAT(cuisine,\',\',\''.$cuisine.'\')');
                    
                    // $this->db->update('become_chef',$cuisine,$where1); // updata on become_chef  table
                    
                    $update = "UPDATE become_chef SET cuisine = CONCAT(cuisine, ',$cuisine') ,subcusine = CONCAT(subcusine , ',$subcusine') WHERE user_id = '$userid'";
                    //echo "UPDATE become_chef SET cuisine = CONCAT(cuisine, ',$cuisine') ,subcusine = CONCAT(subcusine , ',$subcusine') WHERE user_id = '$userid'";die;
                    $req1   = $this->db->query($update);
                    // UPDATE become_chef SET cuisine = CONCAT(cuisine, ',Hong Kong Style,Chinese') WHERE user_id = '1'
                    if($req1){
                          $where1              = array(
                                        'id' => $userid
                                       
                                    );
                                    $rdata['is_menu'] = '1';
                                    $isUpdate1           = $this->db->update('register', $rdata, $where1); // updata on become a chef
                    }
                    

                }
                
                return array(
                    'user_id' => $userid,
                    'id' => $menuid
                ); //record update
            }
        } else {
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            /*****register user as chef*******/
            $isSaved            = $this->db->insert('menus', $data);
            $lastinserted       = $this->db->insert_id();
            if ($isSaved) {
                $cuisine = $data['cusine'];
                $userid  = $data['user_id'];
                $subcusine = $data['subcusine'];
                
                // $where = array('user_id' => $userid);
                // $this->db->set('cuisine', 'CONCAT(cuisine,\',\',\''.$cuisine.'\')', FALSE);
                
                // $this->db->update('become_chef',$cuisine,$where); // updata on become_chef table
                $update = "UPDATE become_chef SET cuisine = CONCAT(cuisine, ',$cuisine'),subcusine = CONCAT(subcusine , ',$subcusine')  WHERE user_id = '$userid'";
                $req1   = $this->db->query($update);
                  if($req1){
                          $where1              = array(
                                        'id' => $userid
                                       
                                    );
                                    $rdata['is_menu'] = '1';
                                    $isUpdate1           = $this->db->update('register', $rdata, $where1); // updata on become a chef
                    }
                    
                
            }
            return array(
                'id' => $lastinserted
            ); //record insert
        }
        
    }
    
    /*******Add/Edit sub menu*******/
    function addSubMenu($data)
    {
        $dishid = $data['id'];
        if (!empty($dishid) && $dishid != '0') {
            //check user exists in beacomechef table
            $query = "SELECT * FROM `sub_menus` where id ='$dishid'";
            $req   = $this->db->query($query);
            if ($req->num_rows() > 0) {
                /******Update profile of chef******/
                $menudetails = $req->result();
                //print_r($chefdetails);
                $userid      = $menudetails[0]->user_id;
                $menuid      = $menudetails[0]->menu_id;
                $where       = array(
                    'user_id' => $userid,
                    'id' => $dishid,
                    'menu_id' => $menuid
                );
                $isUpdate    = $this->db->update('sub_menus', $data, $where); // updata on become a chef
                
                return array(
                    'user_id' => $userid,
                    'id' => $dishid
                ); //record update
            }
        } else {
            /*****register user as chef*******/
            $isSaved      = $this->db->insert('sub_menus', $data);
            $lastinserted = $this->db->insert_id();
            
            return array(
                'id' => $lastinserted
            ); //record insert
        }
    }
    
    /*****get menu list*****/
    function getmenutitleprice($menu_id)
    {
        $returnData = array();
        $req        = "SELECT * FROM  `menus` where id ='$menu_id'";
        
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            
        }
        
        return $returnData;
    }
    /****get country*****/
    function getMenus($userid)
    {
        $returnData = array();
        // $req        = "SELECT * FROM  `menus` where status ='1' and user_id='$userid'";
         $req        = "SELECT * FROM  `menus` where user_id='$userid'";
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                
                if (empty($val->menu_image)) {
                    $val->menu_image = '';
                } else {
                    $val->menu_image = base_url() . 'assets/menuImgae/' . $val->menu_image;
                }
            }
        }
        
        return $returnData;
    }
    
    /******Get counter of menus********/
    function getcounterdish()
    {
        $returnData = array();
        $req        = "SELECT * FROM  `sub_menus`";
        
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        
        return $returnData;
    }
    /*****get sub menu*****/
    
    function getSubMenus($menuid)
    {
        $returnData = array();
        $req        = "SELECT * FROM  `sub_menus` where menu_id ='$menuid'";
        
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                
                if (empty($val->dish_image)) {
                    $val->dish_image = '';
                } else {
                    $val->dish_image = base_url() . 'assets/submenuImage/' . $val->dish_image;
                }
            }
        }
        
        return $returnData;
    }
    
    /********Delete menus********/
    
    function deleteMenus($menuid)
    {
        if (!empty($menuid)) {
            $tablename = 'menus';
            $where     = array(
                'id' => $menuid
            );
            $this->db->where($where);
            $deleteData = $this->db->delete($tablename);
            
            if ($deleteData) {
                $tablename1 = 'sub_menus';
                $where1     = array(
                    'menu_id' => $menuid
                );
                $this->db->where($where1);
                $this->db->delete($tablename1);
            }
            
            return TRUE;
        } else {
            return FALSE;
        }
        
    }
    
    /******Delete sub menu****/
    
    function deleteSubmenus($submenu)
    {
        if (!empty($submenu)) {
            $tablename = 'sub_menus';
            $where     = array(
                'id' => $submenu
            );
            $this->db->where($where);
            $this->db->delete($tablename);
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /******Update user profile******/
    function UpdateuserProfile($data, $userid)
    {
        
        $where    = array(
            'id' => $userid
        );
        $isUpdate = $this->db->update('register', $data, $where); // updata on user profile
        if ($isUpdate) {
            //get chef details if user is chef
            $req   = "SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
         become_chef.user_id = '$userid' and register.is_chef='1' and register.is_verify_chef='yes' ";
          //  print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                //update chef table
                $res                = $query->row();
                // echo "<pre>";
                // print_r($res);
                $where1             = array(
                    'user_id' => $userid
                );
                $chef['chef_fname'] = $res->first_name;
                $chef['chef_lname'] = $res->last_name;
                $chef['currency'] = $data['currency'];
                if (array_key_exists('profile_pic', $data)) {
                    $chef['profile_pic'] = $data['profile_pic'];
                }
                
                $chef['updated_at'] = date('Y-m-d H:i:s');
                //print_r($chef);die;
                $isUpdatechef       = $this->db->update('become_chef', $chef, $where1);
                if ($isUpdatechef){
                    

                /*******Get all menus ****/
                $getAllmenus = "SELECT * from menus where user_id ='$userid'";
                $querymenus = $this->db->query($getAllmenus);
                if ($querymenus->num_rows() > 0) {
                    $resmnus = $querymenus->result();
                    $i=0;
                    foreach ($resmnus as $key => $value) {
                       // print_r($value);
                        $menuid= $value->id;
                        $amount = $value->price_per_person;
                        $actualamt = $value->actualprice;
                        $to_cur =$data['currency'];
                        $from_cur =  $value->currency;
                        if($amount > 0 && $to_cur != $from_cur && $actualamt > 0){
                           // echo 'if';
                            $convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);
                            $convertedActualPrice = $this->convertCurrency($actualamt, $from_cur, $to_cur);

                        }else{
                           // echo 'else';
                            $convertedPrice = $amount;
                            $convertedActualPrice = $actualamt;
                        }
                         $whmenu = array(
                            'user_id' => $userid,'id'=>$menuid
                        );
                            $updateMenu['price_per_person'] = round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                            $updateMenu['actualprice'] = round($convertedActualPrice, 2, PHP_ROUND_HALF_UP);
                            $updateMenu['currency'] = $to_cur;
                            $updatemenu = $this->db->update('menus', $updateMenu, $whmenu);
                        // $updatemenu = $this->db->query("UPDATE menus SET price_per_person='$convertedPrice' , currency = '$to_cur' WHERE user_id='$userid' and id='$menuid'");
                        //echo $this->db->last_query();
                        $i++;
                    }
               
                    // $countmenus = count($resmnus);
                   // echo $countmenus;die;

                 }

                }
                return array(
                    'id' => $userid,
                    'user' => 'UC'
                ); //user and chef updated successfully
            }
            return array(
                'id' => $userid,
                'user' => 'U'
            ); //user profile updated successfully
        } else {
            return array(
                'id' => $userid,
                'user' => 'U'
            ); //user profile updated successfully
        }
        
    }
    /***********get user info*************/
    function getuserinfo($userid)
    {
        $returnData = array();
        $req        = "SELECT * FROM register  WHERE id = '$userid' and is_verify = 'yes'";
        //print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                    if (empty($val->profile_pic)) {
                        $val->profile_pic = '';
                    } else {
                        $val->profile_pic = $val->profile_pic;
                    }
                } else {
                    if (empty($val->profile_pic)) {
                        $val->profile_pic = '';
                    } else {
                        $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                    }
                }

                $from_cur = 'USD';
                $amount = $val->credited_amount;
                /*$val->convertedCur = $to_cur;
                $val->convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);*/
                $to_cur = $val->currency;
                if($to_cur == ''){
                    $to_cur ='HKD';
                }
                if($amount > 0 && $to_cur != $from_cur){
                    $convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);

                }else{
                    $convertedPrice = $amount;
                }
                
                
                
                //$val->credited_amount = round($convertedPrice);
                $val->credited_amount = round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                
            }
            return $returnData;
        } else {
            return FALSE;
        }
    }
    
    /******Get chef info*******/
    function getchefinfo($userid)
    {
        $returnData = array();
        /*$req="SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
        become_chef.user_id = '$userid' and register.is_chef='1' and register.is_verify_chef='yes'";*/
        $req        = "SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
         become_chef.user_id = '$userid' and register.is_chef='1'";
        
        //print_r($req);die;
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                    if (empty($val->profile_pic)) {
                        $val->profile_pic = '';
                    } else {
                        $val->profile_pic = $val->profile_pic;
                    }
                } else {
                    if (empty($val->profile_pic)) {
                        $val->profile_pic = '';
                    } else {
                        $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                    }
                }

                $from_cur = 'USD';
                $amount = $val->credited_amount;
                //echo $amount;die;
                /*$val->convertedCur = $to_cur;
                $val->convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);*/
                $to_cur = $val->currency;
                if($to_cur == ''){
                    $to_cur='HKD';
                }
                //echo $to_cur .'dfdsf'. $from_cur;
                if($amount > 0 && $to_cur != $from_cur){
                   // echo 'if';die;
                    $convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);
                     //echo $convertedPrice;die;
                }else{
                    $convertedPrice = $amount;
                }
                
               
                
               // $val->credited_amount = round($convertedPrice);
                $val->credited_amount = round($convertedPrice, 2, PHP_ROUND_HALF_UP);

                 if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                
            }
            return $returnData;
        } else {
            return FALSE;
        }
    }
    
    /********Add a Permission******/
    function addPermission($data)
    {
        $id = $data['id'];
        if (!empty($id) && $id != '0') {
            //check user exists in beacomechef table
            $query = "SELECT * FROM `permission` where id ='$id'";
            $req   = $this->db->query($query);
            if ($req->num_rows() > 0) {
                
                $where    = array(
                    'id' => $id
                );
                $isUpdate = $this->db->update('permission', $data, $where); // updata on become a chef
                
                return array(
                    'id' => $id
                ); //record update
            }
        } else {
            /*****register user as chef*******/
            $isSaved      = $this->db->insert('permission', $data);
            $lastinserted = $this->db->insert_id();
            
            return array(
                'id' => $lastinserted
            ); //record insert
        }
        
    }
    
    /*******Get Permission******/
    function getPermission($userid)
    {
        $returnData = array();
        $req        = "SELECT * FROM  `permission` where user_id='$userid'";
        
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        
        return $returnData;
    }
    /******Kitchen gallery******/
    // function KitchenGallery($data){
    //     $gid = $data['id'];
    //     $userid = $data['chef_id'];
    //     if(!empty($gid) && $gid !='0'){
    //     //check user exists in beacomechef table
    //     $query ="SELECT * FROM `kitchen_gallery` where id ='$gid'";
    //     $req= $this->db->query($query);
    //         if($req->num_rows() > 0){
    
    //             $where = array('id' => $gid,'chef_id'=>$userid);
    //             $data['updated_at'] = date('Y-m-d H:i:s');
    //             $isUpdate = $this->db->update('kitchen_gallery',$data,$where); // updata on become a chef
    
    //             return array('id'=>$gid);//record update
    //         }
    // }else{
    //             /*****register user as chef*******/
    //             $data['created_at'] = date('Y-m-d H:i:s');
    //             $data['updated_at'] = date('Y-m-d H:i:s');
    //             $isSaved = $this->db->insert('kitchen_gallery',$data);
    //             $lastinserted = $this->db->insert_id();
    
    //             return array('id'=>$lastinserted);//record insert
    //         }
    
    // }
    
    function KitchenGallery($data)
    {
        $gid    = $data['id'];
        $userid = $data['user_id'];
        if (!empty($gid) && $gid != '0') {
            //check user exists in beacomechef table
            $query = "SELECT * FROM `become_chef` where id ='$gid'";
            $req   = $this->db->query($query);
            if ($req->num_rows() > 0) {
                
                $where              = array(
                    'id' => $gid,
                    'user_id' => $userid
                );
                $data['updated_at'] = date('Y-m-d H:i:s');
               
                $isUpdate           = $this->db->update('become_chef', $data, $where); // updata on become a chef
                if($isUpdate){
                    $where1              = array(
                    'id' => $userid
                   
                );
                $rdata['is_pic'] = '1';
                $isUpdate1           = $this->db->update('register', $rdata, $where1); // updata on become a chef
                }
                
                return array(
                    'id' => $gid
                ); //record update
            }
        } else {
            /*****register user as chef*******/
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
           
            $isSaved            = $this->db->insert('become_chef', $data);
            $lastinserted       = $this->db->insert_id();
            if($lastinserted){
                    $where1              = array(
                    'id' => $userid
                   
                );
                $rdata['is_pic'] = '1';
                $isUpdate1           = $this->db->update('register', $rdata, $where1); // updata on become a chef
                }
            return array(
                'id' => $lastinserted
            ); //record insert
        }
        
    }
    
    /******Get kitchen gallery********/
    // function getKitchengallery($chefid){
    //         $returnData = array();
    //         $req="SELECT * FROM kitchen_gallery WHERE chef_id = '$chefid'";
    //          //print_r($req);die;
    //         $query = $this->db->query($req);
    //         if($query->num_rows() >0){
    //             $returnData = $query->result();
    //             foreach($returnData as $val){
    
    //             if(empty($val->image1)){
    //             $val->image1 = '';
    //             } else {
    //             $val->image1 = base_url().'assets/kitchengallery/'.$val->image1;
    //             }
    
    //             if(empty($val->image2)){
    //             $val->image2 = '';
    //             } else {
    //             $val->image2 = base_url().'assets/kitchengallery/'.$val->image2;
    //             }
    //             if(empty($val->image3)){
    //             $val->image3 = '';
    //             } else {
    //             $val->image3 = base_url().'assets/kitchengallery/'.$val->image3;
    //             }
    //             if(empty($val->image4)){
    //             $val->image4 = '';
    //             } else {
    //             $val->image4 = base_url().'assets/kitchengallery/'.$val->image4;
    //             }
    //             if(empty($val->image5)){
    //             $val->image5 = '';
    //             } else {
    //             $val->image5 = base_url().'assets/kitchengallery/'.$val->image5;
    //             }
    //             if(empty($val->image6)){
    //             $val->image6 = '';
    //             } else {
    //             $val->image6 = base_url().'assets/kitchengallery/'.$val->image6;
    //             }
    //             if(empty($val->image7)){
    //             $val->image7 = '';
    //             } else {
    //             $val->image7 = base_url().'assets/kitchengallery/'.$val->image7;
    //             }
    //             if(empty($val->image8)){
    //             $val->image8 = '';
    //             } else {
    //             $val->image8 = base_url().'assets/kitchengallery/'.$val->image8;
    //             }
    //             if(empty($val->image9)){
    //             $val->image9 = '';
    //             } else {
    //             $val->image9 = base_url().'assets/kitchengallery/'.$val->image9;
    //             }
    //             if(empty($val->image10)){
    //             $val->image10 = '';
    //             } else {
    //             $val->image10 = base_url().'assets/kitchengallery/'.$val->image10;
    //             }
    //             if(empty($val->image11)){
    //             $val->image11 = '';
    //             } else {
    //             $val->image11 = base_url().'assets/kitchengallery/'.$val->image11;
    //             }
    //             if(empty($val->image12)){
    //             $val->image12 = '';
    //             } else {
    //             $val->image12 = base_url().'assets/kitchengallery/'.$val->image12;
    //             }
    //             if(empty($val->image13)){
    //             $val->image13 = '';
    //             } else {
    //             $val->image13 = base_url().'assets/kitchengallery/'.$val->image13;
    //             }
    //             if(empty($val->image14)){
    //             $val->image14 = '';
    //             } else {
    //             $val->image14 = base_url().'assets/kitchengallery/'.$val->image14;
    //             }
    //             if(empty($val->image15)){
    //             $val->image15 = '';
    //             } else {
    //             $val->image15 = base_url().'assets/kitchengallery/'.$val->image15;
    //             }
    //             if(empty($val->image16)){
    //             $val->image16 = '';
    //             } else {
    //             $val->image16 = base_url().'assets/kitchengallery/'.$val->image16;
    //             }
    //             if(empty($val->image17)){
    //             $val->image17 = '';
    //             } else {
    //             $val->image17 = base_url().'assets/kitchengallery/'.$val->image17;
    //             }
    //             if(empty($val->image18)){
    //             $val->image18 = '';
    //             } else {
    //             $val->image18 = base_url().'assets/kitchengallery/'.$val->image18;
    //             }
    //             if(empty($val->image19)){
    //             $val->image19 = '';
    //             } else {
    //             $val->image19 = base_url().'assets/kitchengallery/'.$val->image19;
    //             }if(empty($val->image20)){
    //             $val->image20 = '';
    //             } else {
    //             $val->image20 = base_url().'assets/kitchengallery/'.$val->image20;
    //             }
    
    //             }
    //             return $returnData;
    //         }else{
    //             return FALSE;
    //         }
    //     }
    
    function getKitchengallery($chefid)
    {
        $returnData = array();
        $req        = "SELECT * FROM become_chef WHERE  user_id = '$chefid'";
        //print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                
                if (empty($val->image1)) {
                    $val->image1 = '';
                } else {
                    $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                }
                
                if (empty($val->image2)) {
                    $val->image2 = '';
                } else {
                    $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                }
                if (empty($val->image3)) {
                    $val->image3 = '';
                } else {
                    $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                }
                if (empty($val->image4)) {
                    $val->image4 = '';
                } else {
                    $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                }
                if (empty($val->image5)) {
                    $val->image5 = '';
                } else {
                    $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                }
                if (empty($val->image6)) {
                    $val->image6 = '';
                } else {
                    $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                }
                if (empty($val->image7)) {
                    $val->image7 = '';
                } else {
                    $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                }
                if (empty($val->image8)) {
                    $val->image8 = '';
                } else {
                    $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                }
                if (empty($val->image9)) {
                    $val->image9 = '';
                } else {
                    $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                }
                if (empty($val->image10)) {
                    $val->image10 = '';
                } else {
                    $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                }
                if (empty($val->image11)) {
                    $val->image11 = '';
                } else {
                    $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                }
                if (empty($val->image12)) {
                    $val->image12 = '';
                } else {
                    $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                }
                if (empty($val->image13)) {
                    $val->image13 = '';
                } else {
                    $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                }
                if (empty($val->image14)) {
                    $val->image14 = '';
                } else {
                    $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                }
                if (empty($val->image15)) {
                    $val->image15 = '';
                } else {
                    $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                }
                if (empty($val->image16)) {
                    $val->image16 = '';
                } else {
                    $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                }
                if (empty($val->image17)) {
                    $val->image17 = '';
                } else {
                    $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                }
                if (empty($val->image18)) {
                    $val->image18 = '';
                } else {
                    $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                }
                if (empty($val->image19)) {
                    $val->image19 = '';
                } else {
                    $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                }
                if (empty($val->image20)) {
                    $val->image20 = '';
                } else {
                    $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                }
                
            }
            return $returnData;
        } else {
            return FALSE;
        }
    }
    
    /*********Delete Kitchen Gallery*********/
    // function DeleteKitchenGallery($data){
    //     $kid = $data['id'];
    //     $chef = $data['chef_id'];
    
    // $where = array('id' => $kid,'chef_id'=>$chef);
    // $isUpdate = $this->db->update('kitchen_gallery',$data,$where); // updata on user profile
    // if($isUpdate){
    
    // return array('id'=>$kid);//user profile updated successfully
    // }else{
    //      return FALSE;
    // }
    
    // }
    
    function DeleteKitchenGallery($data)
    {
        $kid  = $data['id'];
        $chef = $data['user_id'];
        
        $where    = array(
            'id' => $kid,
            'user_id' => $chef
        );
        $isUpdate = $this->db->update('become_chef', $data, $where); // updata on user profile
        if ($isUpdate) {
            
            return array(
                'id' => $kid
            ); //user profile updated successfully
        } else {
            return FALSE;
        }
        
    }
    
    // function getprice(){
    
    //      $returnData = array();
    //     $req = "SELECT * FROM  `menus`";
    
    //     $query = $this->db->query($req);
    //     if($query->num_rows() >0){
    //     $returnData = $query->result();
    //          }
    
    // return $returnData;
    
    // }
    
    /*******Store user lat long**********/
    function userLatLong($data, $user_id)
    {
        
        $where    = array(
            'id' => $user_id
        );
        $isUpdate = $this->db->update('register', $data, $where); // updata on user profile
        if ($isUpdate) {
            
            return array(
                'id' => $user_id
            ); //user profile updated successfully
        } else {
            return FALSE;
        }
        
        
    }
    
    function getfave()
    {
        $returnData = array();
        $req        = "SELECT * FROM  `favourite_chef`";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    /********Insert favouriot******/
    function favouriteChef($data)
    {
        if (!empty($data)) {
            $isSaved      = $this->db->insert('favourite_chef', $data);
            $lastinserted = $this->db->insert_id();
            
            return array(
                'id' => $lastinserted
            );
        } else {
            return FALSE;
        }
    }
    
    /***********Delete chef***********/
    function deletefavouriteChef($data)
    {
        
        if (!empty($data)) {
            $userid    = $data['user_id'];
            $chefid    = $data['chef_id'];
            $tablename = 'favourite_chef';
            $where     = array(
                'user_id' => $userid,
                'chef_id' => $chefid
            );
            $this->db->where($where);
            $deleteData = $this->db->delete($tablename);
            return TRUE;
        } else {
            return FALSE;
        }
        
    }
    
    /**********Get Chef like or dislike**********/
    function wishlistChef($userid, $chefid)
    {
        
        $returnData = array();
        $req        = "SELECT * FROM  `favourite_chef` where user_id = '$userid' and chef_id='$chefid' ";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
        
    }
    /*function wishlistChef($userid){
    
    $returnData = array();
    $req = "SELECT * FROM  `favourite_chef` where user_id = '$userid' ";
    $query = $this->db->query($req);
    if($query->num_rows() >0){
    $returnData = $query->result();
    }
    return $returnData;
    
    }
    */
    function getMenusSubmenus($userid)
    {
        $returnData = array();
        // $req        = "SELECT * FROM `menus` where user_id = '$userid' ";
        $req        = "SELECT * FROM `menus` where status ='1' and  user_id = '$userid' ";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    
    /*function MenusSubmenus($userid,$menuid){
    
    $returnData = array();
    // $req="SELECT * FROM menus LEFT JOIN sub_menus ON menus.id = sub_menus.menu_id WHERE menus.id = '$menuid' and menus.user_id = sub_menus.user_id and menus.user_id = '$userid'";
    $req="SELECT * FROM menus LEFT JOIN sub_menus ON menus.id = sub_menus.menu_id WHERE menus.id in ($menuid) and menus.user_id = sub_menus.user_id and menus.user_id = '$userid'";
    $query = $this->db->query($req);
    if($query->num_rows() >0){
    $returnData = $query->result();
    foreach($returnData as $val){
    if($val->menu_image !=''){
    $val->menu_image = base_url().'assets/menuImgae/'.$val->menu_image;
    }else{
    $val->menu_image = '';
    }
    if($val->dish_image !=''){
    $val->dish_image = base_url().'assets/submenuImage/'.$val->dish_image;
    }else{
    $val->dish_image = '';
    }
    }
    }
    return $returnData;
    
    }*/
    function MenusSubmenus($userid, $menuid,$to_cur)
    {
        
        $returnData = array();
        // $req="SELECT * FROM menus LEFT JOIN sub_menus ON menus.id = sub_menus.menu_id WHERE menus.id = '$menuid' and menus.user_id = sub_menus.user_id and menus.user_id = '$userid'";
        /*$req        = "SELECT * FROM sub_menus where user_id ='$userid' and menu_id  ='$menuid' ";*/
        // $req = "SELECT sm.id,sm.menu_id,sm.menu_title,sm.menu_price,sm.currency,sm.user_id,sm.dish_name,sm.dish_image,sm.dish_category,sm.created_at, sm.updated_at,sm.status,m.id,m.price_per_person,m.currency as menucurrency FROM sub_menus sm , menus m WHERE sm.menu_id = m.id and sm.menu_id ='$menuid' and sm.user_id ='$userid'";

         $req = "SELECT sm.id,sm.menu_id,sm.user_id,sm.dish_name,sm.dish_image,sm.dish_category,sm.created_at, sm.updated_at,sm.status,m.id,m.price_per_person as menu_price,m.currency as menucurrency,m.menu_title,m.whatshot,m.menu_counter,m.menu_image FROM sub_menus sm , menus m WHERE sm.menu_id = m.id and sm.menu_id ='$menuid' and sm.user_id ='$userid'";
       // print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                
                if ($val->dish_image != '') {
                    $val->dish_image = base_url() . 'assets/submenuImage/' . $val->dish_image;
                } else {
                    $val->dish_image = '';
                }
                if ($val->menu_image != '') {
                    $val->menu_image = base_url() . 'assets/menuImgae/' . $val->menu_image;
                } else {
                    $val->menu_image = '';
                }

                $from_cur = $val->menucurrency;
                 if($from_cur ==''){
                    $from_cur = 'HKD';
                 }
                $amount = $val->menu_price;
                
                $val->currency = $to_cur;
                if($to_cur != $from_cur && $amount >0){
                  $convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);
                }else{
                    // $convertedPrice = $val->menu_price;
                    $convertedPrice = $amount;
                }
                
                //$val->menu_price = round($convertedPrice);
                $val->menu_price = round($convertedPrice, 2, PHP_ROUND_HALF_UP);
            }
        }
        return $returnData;
        
    }
    
    function ChefmenuList($chefid,$to_cur)
    {
       // echo $to_cur;die;
        if($to_cur == ''){
            $to_cur = 'HKD';
        }
        $returnData = array();
        $req        = "SELECT * FROM menus where user_id ='$chefid' ";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                $from_cur = $val->currency;
                if($from_cur == ''){
                    $from_cur = 'HKD';
                }
                $amount = $val->price_per_person;
                /*$val->convertedCur = $to_cur;
                $val->convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);*/
                if ($val->menu_image != '') {
                    $val->menu_image = base_url() . 'assets/menuImgae/' . $val->menu_image;
                } else {
                    $val->menu_image = '';
                }
                $val->currency = $to_cur;
                if($to_cur != $from_cur && $amount > 0){
                    $convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);
                }else{
                    $convertedPrice = $amount;
                }
                
               // $val->price_per_person = round($convertedPrice);
                $val->price_per_person = round($convertedPrice, 2, PHP_ROUND_HALF_UP);


            }
        }
        return $returnData;
        
    }
    

    /***************/
    function convertCurrency($amount, $from, $to) {
     $url = 'http://finance.google.com/finance/converter?a=' . $amount . '&from=' . $from . '&to=' . $to;
     $data = file_get_contents($url);
         preg_match_all("/<span class=bld>(.*)<\/span>/", $data, $converted);
         $final = preg_replace("/[^0-9.]/", "", $converted[1][0]);
        return round($final, 3);
    }
    /********Get favroite chef*******/
    function getfavroite($user_id)
    {
        $returnData = array();
        $req        = "SELECT * FROM `favourite_chef` where user_id = '$user_id' ";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    
    function getchefprofile($chefids)
    {
        $returnData = array();
        if (!empty($chefids)) {
            // $req="SELECT * FROM become_chef where id in ($chefids) ";
            $req   = "SELECT *FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($chefids) ";
            //print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if ($val->profile_pic != '') {
                            $val->profile_pic = $val->profile_pic;
                        } else {
                            $val->profile_pic = '';
                        }
                        
                    } else {
                        if ($val->profile_pic != '') {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        } else {
                            $val->profile_pic = '';
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
            }
        }
        
        return $returnData;
        
    }
    
    
    function getfavechef($chefids)
    {
        $returnData = array();
        $req        = "SELECT * FROM  `favourite_chef` where chef_id in ($chefids) ";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    
    function getprice()
    {
        $returnData = array();
        $req        = "SELECT t.* FROM menus t JOIN ( SELECT *, MIN(price_per_person) minVal FROM menus GROUP BY user_id ) t2 ON t.price_per_person = t2.minVal AND t.user_id = t2.user_id";
        //print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
/*****max price******/
     function getmaxprice()
    {
        $returnData = array();
        $req        = "SELECT t.* FROM menus t JOIN ( SELECT *, MAX(price_per_person) maxVal FROM menus GROUP BY user_id ) t2 ON t.price_per_person = t2.maxVal AND t.user_id = t2.user_id";
        //print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    
    /*********Book Now*********/
    function bookNow($data, $sharedid)
    {
        $booknow_id = $data['booknow_id'];
        $booking_id = $data['booking_id'];
        
        //check user exists in beacomechef table
        $query = "SELECT *  from booknow where booknow_id ='$booknow_id' and booking_id = '$booking_id'";
        $req   = $this->db->query($query);
        if ($req->num_rows() > 0) {
            /******Update profile of chef******/
            $where    = array(
                'booknow_id' => $booknow_id,
                'booking_id' => $booking_id
            );
            $isUpdate = $this->db->update('booknow', $data, $where); // updata on become a chef
            

            return array(
                'booknow_id' => $booknow_id,
                'booking_id' => $booking_id
            ); //record update
        } else {
            /*****register user as chef*******/
            $data['shared_id'] = $sharedid;
            $isSaved           = $this->db->insert('booknow', $data);
            $lastinserted      = $this->db->insert_id();
            /******Get booking id ******/
            $query1            = "SELECT *  from booknow where booknow_id ='$lastinserted'";
            $req1              = $this->db->query($query1);
            if ($req1->num_rows() > 0) {
                $bookingdetails = $req1->result();
                //print_r($chefdetails);
                $bookingkey     = $bookingdetails[0]->booking_id;
            }
            return array(
                'booknow_id' => $lastinserted,
                'booking_id' => $bookingkey
            ); //record update
        }
        
    }
    
    /*********Get Cuisine********/
    function getCuisine()
    {
        $returnData = array();
        $req        = "SELECT * FROM  `menus` ";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    
    
    function checktoken($authid)
    {
        $returnData = array();
        $req        = "SELECT * FROM  `register` where fb_auth_id='$authid' and is_verify ='yes'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    
    function getdatafb($sid)
    {
        $returnData = array();
        
        $req   = "SELECT * FROM  `register` where id='$sid' and is_verify ='yes'";
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    
    
    
    
    function updateInstagramtoken($userid, $data)
    {
        $where    = array(
            'id' => $userid
        );
        $isUpdate = $this->db->update('register', $data, $where); // updata on user profile
        if ($isUpdate) {
            
            return array(
                'id' => $userid
            ); //user profile updated successfully
        } else {
            return FALSE;
        }
    }
    
    function getupcomingMeals($userid)
    {
        $returnData = array();
        
        $date   = date('Y-m-d');
        //echo CURRENT_TIMESTAMP;die;
        // $date=date_create("2013-12-19");
        // echo date_format($date,"D, M.dS");die;
        $dt     = date('Y-m-d');
        $time   = date("G:i:s");
        $dttime = $dt . ' ' . $time;
        // echo $dttime;die;
        /*$req = "SELECT * FROM booknow LEFT JOIN kitchen_gallery ON booknow.chef_id = kitchen_gallery.chef_id  LEFT JOIN menus ON booknow.menu_id = menus.id  WHERE booknow.user_id = '4' and booknow.chef_id = menus.user_id and booknow.date >= '$date' and STR_TO_DATE(booknow.time, '%l:%i %p') >= '$time'";*/
        /*$req = "SELECT * FROM booknow LEFT JOIN kitchen_gallery ON booknow.chef_id = kitchen_gallery.chef_id  LEFT JOIN menus ON booknow.menu_id = menus.id  WHERE booknow.main_user_id = '$userid' and booknow.chef_id = menus.user_id and booknow.date >= '$date'";*/
        /*$req="SELECT * FROM booknow LEFT JOIN become_chef ON booknow.chef_id = become_chef.user_id LEFT JOIN menus ON booknow.menu_id = menus.id WHERE booknow.main_user_id = '$userid' and booknow.chef_id = menus.user_id and booknow.date >= '$date' order by booknow.date ASC ";*/
        /*$req = "SELECT b.booknow_id,b.booking_id,b.chef_id,b.main_user_id, b.date, b.time,b.total_seats,b.menu_id,b.menu_price,b.menu_currency, b.booking_status,bac.kitchen_title,bac.kitchen_descrition,bac.user_id ,m.id, m.user_id as menuuserid,m.menu_title, bac.image1,bac.image2,bac.image3,bac.image4,bac.image5,bac.image6,bac.image7,bac.image8,bac.image9,bac.image10,bac.image11,bac.image12,bac.image13, bac.image14,bac.image15,bac.image16,bac.image17, bac.image18,bac.image19,bac.image20 FROM booknow b ,become_chef bac,menus m WHERE b.chef_id = bac.user_id and b.menu_id = m.id and b.chef_id = m.user_id and b.date >= '$date' and b.main_user_id = '$userid' order by b.date ASC";*/
        $req    = "SELECT b.booknow_id,b.booking_id,b.chef_id,b.main_user_id, b.date, b.time,b.total_seats,b.menu_id,b.menu_price,b.menu_currency, b.booking_status,bac.kitchen_title,bac.kitchen_descrition,bac.user_id ,m.id, m.user_id as menuuserid,m.menu_title, bac.image1,bac.image2,bac.image3,bac.image4,bac.image5,bac.image6,bac.image7,bac.image8,bac.image9,bac.image10,bac.image11,bac.image12,bac.image13, bac.image14,bac.image15,bac.image16,bac.image17, bac.image18,bac.image19,bac.image20,b.shared_id FROM booknow b ,become_chef bac,menus m WHERE b.chef_id = bac.user_id and b.menu_id = m.id and b.chef_id = m.user_id and b.date >= '$date' and find_in_set('$userid',b.shared_id)  and TIMESTAMP(b.date, b.time) > '$dttime' order by b.date ASC";
        //print_r($req);die;
        $query  = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                $val->olddate = $val->date;
                
                if (!empty($val->time)) {
                    /*$date_one = date('Y-m-d H:i:s');
                    $date_two = $val->date.' '.$val->time;
                    echo $date_two;
                    
                    $seconds = abs(strtotime($date_two) - strtotime($date_one));
                    
                    $dtF = new \DateTime('@0');
                    $dtT = new \DateTime("@$seconds");
                    echo $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');die;*/
                    //echo secondsToTime($dif);die;
                    
                    $dt1        = $val->date . ' ' . $val->time;
                    $end        = date_create($dt1);
                    $start      = date_create(); // Current time and date
                    //print_r($start);
                    $diff       = date_diff($start, $end);
                    //print_r($diff);die;
                    $val->hours = $diff->h . ' hours';
                    $val->days  = $diff->d . ' days';
                } else {
                    $val->hours = '';
                    $val->days  = '';
                }
                if (empty($val->image1)) {
                    $val->image1 = '';
                } else {
                    $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                }
                
                if (!empty($val->date)) {
                    /*$datenew = date_create('"'.$val->date.'"');*/
                    $datenew   = $val->date;
                    $val->date = date('D, M.dS', strtotime($datenew));
                    //$val->date = date_format($datenew,"D, M.dS");
                }
                
                
                if (empty($val->image2)) {
                    $val->image2 = '';
                } else {
                    $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                }
                if (empty($val->image3)) {
                    $val->image3 = '';
                } else {
                    $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                }
                if (empty($val->image4)) {
                    $val->image4 = '';
                } else {
                    $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                }
                if (empty($val->image5)) {
                    $val->image5 = '';
                } else {
                    $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                }
                if (empty($val->image6)) {
                    $val->image6 = '';
                } else {
                    $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                }
                if (empty($val->image7)) {
                    $val->image7 = '';
                } else {
                    $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                }
                if (empty($val->image8)) {
                    $val->image8 = '';
                } else {
                    $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                }
                if (empty($val->image9)) {
                    $val->image9 = '';
                } else {
                    $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                }
                if (empty($val->image10)) {
                    $val->image10 = '';
                } else {
                    $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                }
                if (empty($val->image11)) {
                    $val->image11 = '';
                } else {
                    $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                }
                if (empty($val->image12)) {
                    $val->image12 = '';
                } else {
                    $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                }
                if (empty($val->image13)) {
                    $val->image13 = '';
                } else {
                    $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                }
                if (empty($val->image14)) {
                    $val->image14 = '';
                } else {
                    $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                }
                if (empty($val->image15)) {
                    $val->image15 = '';
                } else {
                    $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                }
                if (empty($val->image16)) {
                    $val->image16 = '';
                } else {
                    $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                }
                if (empty($val->image17)) {
                    $val->image17 = '';
                } else {
                    $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                }
                if (empty($val->image18)) {
                    $val->image18 = '';
                } else {
                    $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                }
                if (empty($val->image19)) {
                    $val->image19 = '';
                } else {
                    $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                }
                if (empty($val->image20)) {
                    $val->image20 = '';
                } else {
                    $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                }
                
                $allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                
                $allimagearr = explode("|", $allimage);
                $allimagearr = array_values(array_filter($allimagearr));
                if (!empty($allimagearr)) {
                    $val->kitchenimage = $allimagearr[0];
                } else {
                    $val->kitchenimage = '';
                }
                
                unset($val->image1);
                unset($val->image2);
                unset($val->image3);
                unset($val->image4);
                unset($val->image5);
                unset($val->image6);
                unset($val->image7);
                unset($val->image8);
                unset($val->image9);
                unset($val->image10);
                unset($val->image11);
                unset($val->image12);
                unset($val->image13);
                unset($val->image14);
                unset($val->image15);
                unset($val->image16);
                unset($val->image17);
                unset($val->image18);
                unset($val->image19);
                unset($val->image20);
                //print_r($allimagearr);die;
            }
            return $returnData;
        } else {
            return FALSE;
        }
        
    }
    
    
    /*function secondsToTime($seconds) {
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
    }*/
    
    function gethistoryMeals($userid)
    {
        $returnData = array();
        
        
        
        $date  = date('Y-m-d');
        // $date=date_create("2013-12-19");
        // echo date_format($date,"D, M.dS");die;
        // $time = date("G:i:s");
        // echo $time;die;
        /*$req = "SELECT * FROM booknow LEFT JOIN kitchen_gallery ON booknow.chef_id = kitchen_gallery.chef_id  LEFT JOIN menus ON booknow.menu_id = menus.id  WHERE booknow.user_id = '4' and booknow.chef_id = menus.user_id and booknow.date >= '$date' and STR_TO_DATE(booknow.time, '%l:%i %p') >= '$time'";*/
        /*$req = "SELECT * FROM booknow LEFT JOIN kitchen_gallery ON booknow.chef_id = kitchen_gallery.chef_id  LEFT JOIN menus ON booknow.menu_id = menus.id  WHERE booknow.main_user_id = '$userid' and booknow.chef_id = menus.user_id and booknow.date >= '$date'";*/
        /*$req="SELECT * FROM booknow LEFT JOIN become_chef ON booknow.chef_id = become_chef.user_id LEFT JOIN menus ON booknow.menu_id = menus.id WHERE booknow.main_user_id = '$userid' and booknow.chef_id = menus.user_id and booknow.date >= '$date' order by booknow.date ASC ";*/
        /*$req = "SELECT b.booknow_id,b.booking_id,b.chef_id,b.main_user_id, b.date, b.time,b.total_seats,b.menu_id,b.menu_price,b.menu_currency, b.booking_status,bac.kitchen_title,bac.kitchen_descrition,bac.user_id ,m.id, m.user_id as menuuserid,m.menu_title, bac.image1,bac.image2,bac.image3,bac.image4,bac.image5,bac.image6,bac.image7,bac.image8,bac.image9,bac.image10,bac.image11,bac.image12,bac.image13, bac.image14,bac.image15,bac.image16,bac.image17, bac.image18,bac.image19,bac.image20 FROM booknow b ,become_chef bac,menus m WHERE b.chef_id = bac.user_id and b.menu_id = m.id and b.chef_id = m.user_id and b.date >= '$date' and b.main_user_id = '$userid' order by b.date ASC";*/
        $req   = "SELECT b.booknow_id,b.booking_id,b.chef_id,b.main_user_id, b.date, b.time,b.total_seats,b.menu_id,b.menu_price,b.menu_currency, b.booking_status,bac.kitchen_title,bac.kitchen_descrition,bac.user_id ,m.id, m.user_id as menuuserid,m.menu_title, bac.image1,bac.image2,bac.image3,bac.image4,bac.image5,bac.image6,bac.image7,bac.image8,bac.image9,bac.image10,bac.image11,bac.image12,bac.image13, bac.image14,bac.image15,bac.image16,bac.image17, bac.image18,bac.image19,bac.image20,b.shared_id FROM booknow b ,become_chef bac,menus m WHERE b.chef_id = bac.user_id and b.menu_id = m.id and b.chef_id = m.user_id and b.date <= '$date' and find_in_set('$userid',b.shared_id) order by b.date ASC";
        //print_r($req);die;
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                $val->olddate = $val->date;
                
                
                if (!empty($val->time)) {
                    $dt1        = $val->date . ' ' . $val->time;
                    $end        = date_create($dt1);
                    $start      = date_create(); // Current time and date
                    $diff       = date_diff($start, $end);
                    $val->hours = $diff->h . ' hours';
                    $val->days  = $diff->d . ' days';
                } else {
                    $val->hours = '';
                    $val->days  = '';
                }
                if (empty($val->image1)) {
                    $val->image1 = '';
                } else {
                    $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                }
                
                if (empty($val->image2)) {
                    $val->image2 = '';
                } else {
                    $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                }
                if (empty($val->image3)) {
                    $val->image3 = '';
                } else {
                    $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                }
                if (empty($val->image4)) {
                    $val->image4 = '';
                } else {
                    $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                }
                if (empty($val->image5)) {
                    $val->image5 = '';
                } else {
                    $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                }
                if (empty($val->image6)) {
                    $val->image6 = '';
                } else {
                    $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                }
                if (empty($val->image7)) {
                    $val->image7 = '';
                } else {
                    $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                }
                if (empty($val->image8)) {
                    $val->image8 = '';
                } else {
                    $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                }
                if (empty($val->image9)) {
                    $val->image9 = '';
                } else {
                    $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                }
                if (empty($val->image10)) {
                    $val->image10 = '';
                } else {
                    $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                }
                if (empty($val->image11)) {
                    $val->image11 = '';
                } else {
                    $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                }
                if (empty($val->image12)) {
                    $val->image12 = '';
                } else {
                    $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                }
                if (empty($val->image13)) {
                    $val->image13 = '';
                } else {
                    $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                }
                if (empty($val->image14)) {
                    $val->image14 = '';
                } else {
                    $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                }
                if (empty($val->image15)) {
                    $val->image15 = '';
                } else {
                    $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                }
                if (empty($val->image16)) {
                    $val->image16 = '';
                } else {
                    $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                }
                if (empty($val->image17)) {
                    $val->image17 = '';
                } else {
                    $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                }
                if (empty($val->image18)) {
                    $val->image18 = '';
                } else {
                    $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                }
                if (empty($val->image19)) {
                    $val->image19 = '';
                } else {
                    $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                }
                if (empty($val->image20)) {
                    $val->image20 = '';
                } else {
                    $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                }
                
                $allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                
                $allimagearr = explode("|", $allimage);
                $allimagearr = array_values(array_filter($allimagearr));
                if (!empty($allimagearr)) {
                    $val->kitchenimage = $allimagearr[0];
                } else {
                    $val->kitchenimage = '';
                }
                
                if (!empty($val->date)) {
                    /*$datenew = date_create('"'.$val->date.'"');*/
                    $datenew   = $val->date;
                    $val->date = date('D, M.dS', strtotime($datenew));
                    //$val->date = date_format($datenew,"D, M.dS");
                }
                
                unset($val->image1);
                unset($val->image2);
                unset($val->image3);
                unset($val->image4);
                unset($val->image5);
                unset($val->image6);
                unset($val->image7);
                unset($val->image8);
                unset($val->image9);
                unset($val->image10);
                unset($val->image11);
                unset($val->image12);
                unset($val->image13);
                unset($val->image14);
                unset($val->image15);
                unset($val->image16);
                unset($val->image17);
                unset($val->image18);
                unset($val->image19);
                unset($val->image20);
                //print_r($allimagearr);die;
            }
            return $returnData;
        } else {
            return FALSE;
        }
    }
    
    function sharedBooking($userid, $bookingcode)
    {
        
        /*******Get previous sharedid*****/
        $query = "SELECT * from booknow WHERE booking_id = '$bookingcode'";
        $req1  = $this->db->query($query);
        if ($req1->num_rows() > 0) {
            
            $details     = $req1->result();
            //print_r($chefdetails);
            $seats       = $details[0]->total_seats;
            $sharedid    = $details[0]->shared_id;
            $sharedidarr = explode(",", $sharedid);
        }
        if (!empty($sharedidarr) && !in_array($userid, $sharedidarr)) {
            $sId = $sharedid . ',' . $userid;
        } else {
            $sId = $sharedid;
        }
        $data['shared_id'] = $sId;
        
        /******Update records*****/
        if (count($sharedidarr) != $seats) {
            $where    = array(
                'booking_id' => $bookingcode
            );
            $isUpdate = $this->db->update('booknow', $data, $where); // updata on user profile
            //echo $this->db->last_query();die;
            if ($isUpdate) {
                return 'UP'; //not active
                //return TRUE;
            } else {
                return 'NUP';
            }
        } else {
            return 'BF';
        }
        
    }
    
    
    /******Cancel Booking****/
    
    function cancelBooking($bookingcode, $userId)
    {
        if (!empty($bookingcode)) {
            /*$tablename = 'booknow';
            $where = array('booking_id'=>$bookingcode);
            $this->db->where($where);
            $this->db->FIND_IN_SET('$userId', shared_id);
            $this->db->delete($tablename);
            echo $this->db->last_query();die;*/
            $query = $this->db->query("DELETE FROM `booknow` WHERE booknow_id ='$bookingcode' and find_in_set('$userId' , shared_id) ");
            //echo $this->db->last_query();die;
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /*********Change details of booking********/
    function changeDetails($bookingid, $userId, $data)
    {
        if (!empty($bookingid) && !empty($userId)) {
            $where    = array(
                'booknow_id' => $bookingid,
                'main_user_id' => $userId
            );
            $isUpdate = $this->db->update('booknow', $data, $where); // updata on user profile
            return array(
                'booknow_id' => $bookingid,
                'main_user_id' => $userId
            );
        } else {
            return FALSE;
        }
        
    }
    
    function getDetails($booknow_id, $userId)
    {
        $returnData = array();
        
        $req   = "SELECT * FROM booknow LEFT JOIN payment ON booknow.booknow_id = payment.booking_id WHERE
booknow.booknow_id = '$booknow_id' and payment.main_user_id = '$userId' and booknow.main_user_id = payment.main_user_id";
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    
    function chefCalendar($data)
    {
        $date       = $data['date'];
        $chefid     = $data['chef_id'];
        $returnData = array();
        $query      = "SELECT *  from `chef_calendar` where `date` = '$date' and `chef_id` = '$chefid'";
        //$query ="SELECT *  from `chef_calendar` where `calendar_id` ='$calendar_id'";
        $req        = $this->db->query($query);
        if ($req->num_rows() > 0) {
            $returnData = $req->result();
            $where      = array(
                'date' => $date,
                'chef_id' => $chefid
            );
            $isUpdate   = $this->db->update('chef_calendar', $data, $where);
            $query2     = "SELECT *  from chef_calendar where `date` = '$date' and `chef_id` = '$chefid'";
            $req2       = $this->db->query($query2);
            if ($req2->num_rows() > 0) {
                $returnData = $req2->result();
            }
            return $returnData; //record update
        } else {
            $isSaved      = $this->db->insert('chef_calendar', $data);
            $lastinserted = $this->db->insert_id();
            $query1       = "SELECT *  from chef_calendar where calendar_id ='$lastinserted'";
            $req1         = $this->db->query($query1);
            if ($req1->num_rows() > 0) {
                $returnData = $req1->result();
            }
            return $returnData; //record update
        }
        
    }
    
    
    
    function getchefCalendar($chef_id)
    {
        $returnData = array();
        $req        = "SELECT `calendar_id`, `chef_id`, `date`, `ava_status` FROM `chef_calendar` WHERE chef_id= '$chef_id'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
        
    }
    
    function getTimeforDate($chef_id, $date)
    {
        $returnData = array();
        $req        = "SELECT * FROM `chef_calendar` WHERE `chef_id`= '$chef_id' and `date` = '$date'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    
    function saveTimeforDate($chef_id, $date, $data)
    {
        if (!empty($chef_id) && !empty($date) && !empty($data)) {
            $where    = array(
                'chef_id' => $chef_id,
                'date' => $date
            );
            $isUpdate = $this->db->update('chef_calendar', $data, $where); // updata on user profile
            if ($isUpdate) {
                $returnData = array();
                $req        = "SELECT * FROM `chef_calendar` WHERE `chef_id`= '$chef_id' and `date` = '$date'";
                $query      = $this->db->query($req);
                if ($query->num_rows() > 0) {
                    $returnData = $query->result();
                }
                return $returnData;
            } else {
                return FALSE;
            }
            //return array('booknow_id'=>$bookingid,'main_user_id'=>$userId);
        } else {
            return FALSE;
        }
    }
    
    function getBookingList($chef_id, $date)
    {
        $returnData = array();
        $req        = "SELECT b.booknow_id,b.booking_id,b.chef_id,b.main_user_id,b.date,b.time,b.total_seats,b.menu_id, b.menu_price,b.menu_currency, b.service_type,b.food_delivery,b.kitchen_tool,b.first_name,b.last_name,b.email, b.contact_number,b.delivery_address,b.user_msg, b.created_at,b.updated_at,b.status,b.booking_status,b.shared_id,b.payment_type, b.payment_status, b.payment_device,m.id,m.menu_title FROM booknow b,menus m WHERE b.menu_id = m.id and b.chef_id = '$chef_id' and b.date = '$date'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    
    function acceptRejectbooking($chef_id, $date, $bookingid, $data)
    {
        if (!empty($chef_id) && !empty($date) && !empty($data) && !empty($bookingid)) {
            $where    = array(
                'chef_id' => $chef_id,
                'date' => $date,
                'booknow_id' => $bookingid
            );
            $isUpdate = $this->db->update('booknow', $data, $where); // updata on user profile
            if ($isUpdate) {
                $returnData = array();
               /* $req = "SELECT b.booknow_id,b.booking_id,b.chef_id,b.main_user_id,b.date,b.time,b.total_seats,b.menu_id, b.menu_price,b.menu_currency, b.service_type,b.food_delivery,b.kitchen_tool,b.first_name,b.last_name,b.email, b.contact_number,b.delivery_address,b.user_msg, b.created_at,b.updated_at,b.status,b.booking_status,b.shared_id,b.payment_type, b.payment_status, b.payment_device,m.id,m.menu_title FROM booknow b,menus m WHERE b.menu_id = m.id and b.chef_id = '$chef_id' and b.date = '$date'";*/

                $req = "SELECT b.booknow_id,b.booking_id,b.chef_id,b.main_user_id,b.date,b.time,b.total_seats,b.menu_id, b.menu_price,b.menu_currency, b.service_type,b.food_delivery,b.kitchen_tool,b.first_name,b.last_name,b.email, b.contact_number,b.delivery_address,b.user_msg, b.created_at,b.updated_at,b.status,b.booking_status,b.shared_id,b.payment_type, b.payment_status, b.payment_device,m.id,m.menu_title FROM booknow b,menus m WHERE b.menu_id = m.id and b.chef_id = '$chef_id' and b.date = '$date' and b.booknow_id='$bookingid' and b.booking_status ='Accepted'";
                //print_r($req );die;
                $query      = $this->db->query($req);
                if ($query->num_rows() > 0) {
                    $returnData = $query->result();
                }
                return $returnData;
            } else {
                return FALSE;
            }
            //return array('booknow_id'=>$bookingid,'main_user_id'=>$userId);
        } else {
            return FALSE;
        }
    }
    /******Save reviws and rating*********/
    function saveReviewRating($data)
    {
        $bookingid    = $data['booking_id'];
        $main_user_id = $data['main_user_id'];
        if (!empty($data)) {
            $Saved        = $this->db->insert('review_rating', $data);
            $lastinserted = $this->db->insert_id();
            //echo $lastinserted;
            if ($Saved && !empty($lastinserted)) {
                //    echo 'dfsdfsdf';die;
                $update['submit_review'] = 'yes';
                $where                   = array(
                    'booknow_id' => $bookingid,
                    'main_user_id' => $main_user_id
                );
                $isUpdate                = $this->db->update('booknow', $update, $where);
                
                // echo $this->db->last_query();die;
                
                
                
            }
            return array(
                'id' => $lastinserted
            );
            
        } else {
            return FALSE;
        }
    }
    
    /********get review and rating*********/
    function getReviewRating($chef_id)
    {
        
        $returnData = array();
        $req        = "SELECT * FROM `review_rating` WHERE `chef_id`= '$chef_id'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result_array();
        }
        return $returnData;
        
    }
    
    /*****List of review and rating*****/
    function getRRlist($chef_id)
    {
        $returnData = array();
        $req        = "SELECT r.rr_id,r.chef_id,r.main_user_id, r.avr_rating,r.review,r.images,r.created_at,r.updated_at,u.id,u.first_name,u.last_name,u.profile_pic FROM review_rating r , register u WHERE u.id = r.main_user_id and r.chef_id='$chef_id'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                //print_r($val);
                //echo $val->profile_pic;
                if (empty($val->profile_pic)) {
                    $val->profile_pic = '';
                } else {
                    $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                }
                
                $images = explode("|", $val->images);
                $i      = 0;
                foreach ($images as $value) {
                    $val->newimages[$i] = base_url() . 'assets/reviewrating/' . $value;
                    //print_r($value);
                    $i++;
                }
                //echo date("d F Y | H:i", strtotime($val->created_at));die;
                $val->newdate = date("d F Y | H:i", strtotime($val->created_at));
            }
            //die;
        }
        return $returnData;
    }
    
    function getchefrating()
    {
        $returnData = array();
        $req        = "SELECT r.rr_id, r.chef_id, r.main_user_id,r.review,r.emoji_comment,r.avr_rating,b.id,b.user_id FROM review_rating r ,become_chef b WHERE r.chef_id = b.user_id";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result_array();
        }
        return $returnData;
        
    }
    
    function updateRating($chef_id, $updateData)
    {
        if (!empty($chef_id) && !empty($updateData)) {
            $where    = array(
                'user_id' => $chef_id
            );
            $isUpdate = $this->db->update('become_chef', $updateData, $where);
            return TRUE;
        } else {
            return FALSE;
        }
        
    }
    
    function getChefdeviceid($chefid)
    {
        
        $returnData = array();
        $req        = "SELECT * FROM `register` WHERE id = '$chefid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
        
    }
    
    function updateBookingStatus($bookingid, $chefid, $data)
    {
        if (!empty($bookingid) && !empty($chefid) && !empty($data)) {
            $where    = array(
                'booknow_id' => $bookingid,
                'chef_id' => $chefid
            );
            $isUpdate = $this->db->update('booknow', $data, $where);
            return array(
                'booknow_id' => $bookingid,
                'chef_id' => $chefid
            );
        } else {
            return FALSE;
        }
    }
    
    function getuserid($chefid, $bookingid)
    {
        
        $returnData = array();
        $req        = "SELECT * FROM `booknow` WHERE booknow_id = '$bookingid' and chef_id = '$chefid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
        
    }

    function getLoggedinusercurrency($main_user_id){
        $returnData = array();
        $req        = "SELECT currency FROM `register` WHERE id = '$main_user_id'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

    function getReferralAmount($userid){
        $returnData = array();
        $req        = "SELECT id,credited_amount,currency FROM `register` WHERE id = '$userid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();

            foreach ($returnData as $val) {
                $from_cur = 'USD';
                $amount = $val->credited_amount;
                /*$val->convertedCur = $to_cur;
                $val->convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);*/
                $to_cur = $val->currency;
                if($amount > 0 && $to_cur != $from_cur){
                    $convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);
                }else{
                    $convertedPrice = $amount;
                }
                
                
                
                //$val->credited_amount = round($convertedPrice);
                $val->credited_amount = round($convertedPrice, 2, PHP_ROUND_HALF_UP);


            }
        }
        return $returnData;
    }

    function getAmount($userid){
        $returnData = array();
        $req        = "SELECT id,credited_amount,currency FROM `register` WHERE id = '$userid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
     function updateUserCre($main_user_id,$updateData){
         if (!empty($main_user_id) && !empty($updateData)) {
            $where    = array(
                'id' => $main_user_id
            );
            $isUpdate = $this->db->update('register', $updateData, $where);
            return array(
                'id' => $main_user_id
            );
        } else {
            return FALSE;
        }
     }

     function menucounter($menuid){
        $returnData = array();
        $req        = "SELECT * FROM `menus` WHERE id = '$menuid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
     }

     function updatemenucounter($menuid,$updateCounter){
        if (!empty($menuid) && !empty($updateCounter)) {
            $where    = array(
                'id' => $menuid
            );
            $isUpdate = $this->db->update('menus', $updateCounter, $where);
            return array(
                'id' => $menuid
            );
        } else {
            return FALSE;
        }
     }

     function getMaxCounter($chefmenuid){
        $returnData = array();
        $req        = "SELECT MAX(menu_counter) as maxcounter,id,user_id FROM menus where user_id='$chefmenuid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
     }

     function updateWhatshot($chefmenuid,$chefCounter){
         if (!empty($chefmenuid) && !empty($chefCounter)) {
            $where    = array(
                'id' => $chefmenuid
            );
            $isUpdate = $this->db->update('register', $chefCounter, $where);
            return array(
                'id' => $chefmenuid
            );
        } else {
            return FALSE;
        }
     }

      /********Insert likeReview******/
    function likeReview($data)
    {
        if (!empty($data)) {
            $userid    = $data['main_user_id'];
            $review_id    = $data['review_id'];
            $query = "SELECT *  from like_review where main_user_id ='$userid' and review_id = '$review_id'";
            $req   = $this->db->query($query);
            if ($req->num_rows() > 0) {
                $returnData = $req->result();
                foreach ($returnData as $key => $value) {
                  $lid = $value->like_id;
                }

            $where    = array(
                'like_id' => $lid
            );
            $isUpdate = $this->db->update('like_review', $data, $where);
            return array(
                'like_id' => $lid
            );

            }else{
                $isSaved      = $this->db->insert('like_review', $data);
            $lastinserted = $this->db->insert_id();
            
            return array(
                'like_id' => $lastinserted
            );
            }
            
        } else {
            return FALSE;
        }

    }
    
    /***********Delete likeReview***********/
    function deletelikeReview($data)
    {
        
        if (!empty($data)) {
            $userid    = $data['main_user_id'];
            $review_id    = $data['review_id'];
            $tablename = 'like_review';
            $where     = array(
                'main_user_id' => $userid,
                'review_id' => $review_id
            );
            $this->db->where($where);
            $deleteData = $this->db->delete($tablename);
            return TRUE;
        } else {
            return FALSE;
        }
        
    }

    /***********/

    function getLikeReview(){
        $returnData = array();
        $req        = "SELECT * FROM like_review";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

    function getLikeYES($main_user_id){
        $returnData = array();
      //  $req        = "SELECT * FROM like_review where review_id = '$rr_id' and main_user_id ='$main_user_id'";
       /* $req = "SELECT * FROM review_rating LEFT JOIN like_review ON review_rating.rr_id = like_review.review_id WHERE
           review_rating.main_user_id = like_review.main_user_id and review_rating.main_user_id = '$main_user_id' and review_rating.rr_id ='$rr_id'";*/
           $req = "SELECT * FROM like_review where main_user_id ='$main_user_id'";
          // print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

    function cuisineFilter($cuisine){

        $returnData = array();
    
           $req = "SELECT * FROM `become_chef` WHERE find_in_set('$cuisine',`cuisine`)";
          // print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            return $returnData;
        }else{
            return FALSE;
        }
        

    }

    /*****Subcuisine filter*******/
    function subcuisineFilter($subcuisine){

        $returnData = array();
    
           $req = "SELECT * FROM `become_chef` WHERE find_in_set('$subcuisine',`subcusine`)";
          // print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;

    }


  
     function getChefCuisines($latitude,$longitude,$subcuisinename){

$returnData = array();
$req   = "SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE find_in_set('$subcuisinename',become_chef.subcusine) HAVING distance < 30  ORDER BY distance";
           
     //      print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
             foreach ($returnData as $val) {

                  if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }

             }


        }
        return $returnData;

    }
    

    function getBookingDate($chef_id){

        $returnData = array();
    
           $req = "SELECT `chef_id`,`date` as bookingdate FROM `booknow` WHERE chef_id = '$chef_id'";
          // print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;

    }

     function updateDraft($menuid,$data){
      if(!empty($menuid) && !empty($data)){
         $where = array('id' => $menuid);
         $isUpdate = $this->db->update('menus', $data, $where); // updata on 
         return array(
                'id' => $menuid
            );
      }else{
         return FALSE;
      }

    }

    function getadmincharge(){
        $query = "SELECT * FROM admin_charges";
          $query = $this->db->query($query);
        return $query->result();
    }

}
?>