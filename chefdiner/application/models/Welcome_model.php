<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome_model extends CI_Model{

  public function _generate_token()
  {
    $this->load->helper('security');
    $salt = do_hash(time().mt_rand());
    $new_key = substr($salt, 0, config_item('rest_key_length'));
    return $new_key;
  }

  /******User Register*****/
    
   function userRegister($data){
    if(!empty($data)){
      /******Check email already exits******/

      $email = $data['email'];
      $query ="SELECT *  from register where email ='$email'";
        $req= $this->db->query($query);
      if($req->num_rows() > 0){
        return 'AE';// email already exist
      }else{
      $this->db->insert('register',$data);
      return array('id'=> $this->db->insert_id());
      }
      
    }else{
      return 'NR';//not register
    }

  }

  /*****User Login*******/

  function userLogin($data){
  /*  print_r($data);die;*/
    $req = $this->db->select('*')->where(array('email'=>$data['email'],'is_verify'=>'yes'))->get('register');
 
    if($req->num_rows() > 0){
      $res = $req->row();
      
      if(md5($data['password']) == $res->password){
        if($res->status =='1'){
           $userdata = array(
                'user_id'=>$res->id,
                'first_name'=>$res->first_name,
                'last_name'=>$res->last_name,
                'is_chef'=>$res->is_chef,
                'is_verify'=>$res->is_verify,
                'is_verify_chef' => $res->is_verify_chef,
                'email'=>$res->email,
                'referal_code'=>$res->referal_code
              );
            $this->session->set_userdata($userdata);
            return 'SL';
          
          
        } else {
          return 'NA'; //not active
        }
      } else {
        return 'WP'; //wrong password
      }
    }
    else {
      return 'NA'; //not active
      }
  }


  /********Confirm mail*********/

   function ConfirmMail($data){
    $res = array();
      $email = $data;
      // echo $email;die;
       $query = "SELECT * FROM  `register` WHERE  `email` =  '$email' and is_verify ='no'";
        // print_r($query);die;
         $query = $this->db->query($query);
    
        // return $query->result_array();

        if($query->num_rows() > 0){
            $res = $query->row();
            //update status

            $activatelink = $this->db->query("UPDATE register SET is_verify='yes' WHERE email='" .$res->email . "' ");
            if($activatelink){
            $userdata = array(
                'user_id'=>$res->id,
                'first_name'=>$res->first_name,
                'last_name'=>$res->last_name,
                'is_chef'=>$res->is_chef,
                'is_verify'=>$res->is_verify,
                'is_verify_chef' => $res->is_verify_chef,
                'email'=>$res->email,
                'referal_code'=>$res->referal_code
              );
            $this->session->set_userdata($userdata);
             return $res;

            }
        }
        else {
            return FALSE;   

        }
    }

    function getuserCode($data){
      $returnData = array();
      $query ="SELECT * FROM  `register` WHERE  `email` =  '$data' and is_verify ='yes' ";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
    }
    function checkVal($hostEmail){
      // if(!empty($hostEmail)){
      /******Check email already exits******/

      $query ="SELECT *  from register where email ='$hostEmail' ";
        $req= $this->db->query($query);
      if($req->num_rows() > 0){
        $res = $req->row();
      $Verstatus = $res->is_verify;
      if($Verstatus == 'yes'){
        return $res;
      }else{
        return 'NV'; //not verified
      }
      }else{
        return 'NE'; //not exists
      }
    
    }

    function checkischef($userid){
      $returnData = array();
      $query ="SELECT *  from become_chef where user_id ='$userid'";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }

    }

    function getuserchef($userid){
      $returnData = array();
      $query ="SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
         become_chef.user_id = $userid and register.is_chef='1'";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }

    }

    function UserbecomeHost($data){
    $userid = $data['user_id'];

    //check user exists in beacomechef table
    $query ="SELECT *  from become_chef where user_id ='$userid'";
        $req= $this->db->query($query);
      if($req->num_rows() > 0){
        /******Update profile of chef******/
        $chefdetails = $req->result();
        //print_r($chefdetails);
        $chefid = $chefdetails[0]->id;
        $where = array('user_id' => $userid,'id'=>$chefid);
        $isUpdate = $this->db->update('become_chef',$data,$where); // updata on become a chef
        if($isUpdate){
          /***update register table****/
          $wh = array('id' => $userid);
          $updatedata['is_chef']= '1';
          $updatedata['first_name'] =$data['chef_fname'];
          $updatedata['last_name'] =$data['chef_lname'];
          /*if(array_key_exists('profile_pic', $data)){
            $updatedata['profile_pic'] =$data['profile_pic'];
          }*/
          
         // $updatedata['phone_number'] =$data['phone_number']; 
          $updatedata['currency'] =$data['currency']; 
          $updatedata['updated_at'] = date('Y-m-d H:i:s');
          $this->db->update('register',$updatedata,$wh);

        $this->session->set_userdata('is_chef', '1');
        }
            return array('user_id' => $userid,'id'=>$chefid);//record update
      }else{
        /*****register user as chef*******/
        $isSaved = $this->db->insert('become_chef',$data);
        $lastinserted = $this->db->insert_id();
        if($isSaved){
          /***update register table****/
          $wh = array('id' => $userid);
          $updatedata['is_chef']= '1';
          $updatedata['first_name'] =$data['chef_fname'];
          $updatedata['last_name'] =$data['chef_lname'];
         /* if(array_key_exists('profile_pic', $data)){
            $updatedata['profile_pic'] =$data['profile_pic'];
          }*/
          // $updatedata['profile_pic'] =$data['profile_pic'];
        // /  $updatedata['phone_number'] =$data['phone_number']; 
          $updatedata['currency'] =$data['currency']; 
          $updatedata['updated_at'] = date('Y-m-d H:i:s');
          $this->db->update('register',$updatedata,$wh);
         $this->session->set_userdata('is_chef', '1');
        }

        return array('user_id' => $userid,'id'=>$lastinserted);//record update
      }

  }


  function getrefAmount(){
    $returnData = array();
      $query ="SELECT *  from referral_user LIMIT 1";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
  }

  function getCodeUser($user_referral_code){
     $returnData = array();
      $query ="SELECT *  from register where referal_code ='$user_referral_code'";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
  }

  function saveCredit($lastId,$updateData){
      $where    = array(
            'id' => $lastId
        );
        $isUpdate = $this->db->update('register', $updateData, $where); // updata on user profile
        // print_r($this->db->last_query());die;
        if ($isUpdate) {
            
            return array(
                'id' => $lastId
            ); //user profile updated successfully
        } else {
            return FALSE;
        }
  }

  function getchefprofiledata($chefid){

     $returnData = array();
    //  $query ="SELECT *  from become_chef where user_id ='$chefid'";
     $query = "SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
         become_chef.user_id = '$chefid'";

         //print_r($query);die;
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result();
        foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }

  }

 function getchefdata($chefid){

     $returnData = array();
    //  $query ="SELECT *  from become_chef where user_id ='$chefid'";
     $query = "SELECT * FROM register LEFT JOIN become_chef ON register.id = become_chef.user_id WHERE
         become_chef.user_id = '$chefid'";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result_array();
      
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }

  }
function updateKitchen($chefid,$data){
     $where    = array(
            'user_id' => $chefid
        );
        $isUpdate = $this->db->update('become_chef', $data, $where); // updata on chef profile
        if ($isUpdate) {
            $where1    = array(
            'id' => $chefid
        );
          $updatedata['currency']  = $data['currency'];

          $isUpdate1 = $this->db->update('register', $updatedata, $where1); // updata on user profile
          if($isUpdate1){
             // $whmenu = array(
             //        'user_id' => $chefid
             //    );
             //        $updateMenu['currency'] = $data['currency'];
             //        $updatemenu = $this->db->update('menus', $updateMenu, $whmenu);
             //        if ($updatemenu){
             //        /*****updatemenucurrency*******/
             //            $whsubmenu = array(
             //                'user_id' => $chefid
             //            );
             //                $updateSubMenu['currency'] = $data['currency'];
             //                $updatemenu= $this->db->update('sub_menus', $updateSubMenu, $whsubmenu);

             //            }

               /*******Get all menus ****/
                $getAllmenus = "SELECT * from menus where user_id ='$chefid'";
                $querymenus = $this->db->query($getAllmenus);
                if ($querymenus->num_rows() > 0) {
                    $resmnus = $querymenus->result();
                    $i=0;
                    foreach ($resmnus as $key => $value) {
                       // print_r($value);
                        $menuid= $value->id;
                        $amount = $value->price_per_person;
                        $to_cur =$data['currency'];
                        $from_cur =  $value->currency;
                        if($amount > 0 && $to_cur != $from_cur){
                           // echo 'if';
                            $convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);

                        }else{
                           // echo 'else';
                            $convertedPrice = $amount;
                        }
                         $whmenu = array(
                            'user_id' => $chefid,'id'=>$menuid
                        );
                            $updateMenu['price_per_person'] = round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                            $updateMenu['currency'] = $to_cur;
                            $updatemenu = $this->db->update('menus', $updateMenu, $whmenu);
                        // $updatemenu = $this->db->query("UPDATE menus SET price_per_person='$convertedPrice' , currency = '$to_cur' WHERE user_id='$userid' and id='$menuid'");
                        //echo $this->db->last_query();
                        $i++;
                    }
               
                    // $countmenus = count($resmnus);
                   // echo $countmenus;die;

                 }
          }
          return array('id' => $chefid);

        } else {
            return FALSE;
        }
}
  function updateProfile($chefid,$data){

    $where    = array(
            'user_id' => $chefid
        );
        $isUpdate = $this->db->update('become_chef', $data, $where); // updata on chef profile
        if ($isUpdate) {
            $where1    = array(
            'id' => $chefid
        );
         $updatedata['profile_pic']  = $data['profile_pic'];
            $updatedata['language']  = $data['language'];
          $isUpdate1 = $this->db->update('register', $updatedata, $where1); // updata on user profile
          return array('id' => $chefid);

        } else {
            return FALSE;
        }

  }

  function getCountry(){
     $returnData = array();
      $query ="SELECT * FROM countries ORDER BY name ASC";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result_array();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
  }

  function getState($country_id){
     $returnData = array();
      $query ="SELECT * FROM states where country_id = '$country_id' ORDER BY name ASC";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result_array();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }

  }
  function getCity($state_id){
    $returnData = array();
      $query ="SELECT * FROM cities where state_id = '$state_id' ORDER BY name ASC";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result_array();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
  }

   function updateMyaddress($chefid,$data){

    $where    = array(
            'user_id' => $chefid
        );
        $isUpdate = $this->db->update('become_chef', $data, $where); // updata on chef profile
        if ($isUpdate) {
            $where1    = array(
            'id' => $chefid
        );
            $updatedata['phone_number']  = $data['phone_number'];
            //$updatedata['language']  = $data['language'];
          $isUpdate1 = $this->db->update('register', $updatedata, $where1); // updata on user profile
          return array('id' => $chefid);

        } else {
            return FALSE;
        }

  }

  function getCitywithout(){
    $returnData = array();
      $query ="SELECT * FROM cities ORDER BY name ASC";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result_array();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
  }

  function updateMyphoto($chefid,$data){
    $where    = array(
            'user_id' => $chefid
        );
        $isUpdate = $this->db->update('become_chef', $data, $where); // updata on chef profile
        if ($isUpdate) {
        
          return array('user_id' => $chefid);

        } else {
            return FALSE;
        }

  }

  function getmenus($chefid){
    $returnData = array();
      $query ="SELECT * FROM menus  where user_id = '$chefid' and status = '1' ORDER BY id desc";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result_array();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
  }

  function getmenusforchef($chefid){
    $returnData = array();
      $query ="SELECT * FROM menus  where user_id = '$chefid' ORDER BY id desc";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result_array();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
  }

  function  whatshot(){
    $returnData = array();
      $query ="SELECT * FROM become_chef LEFT JOIN menus ON become_chef.user_id = menus.user_id WHERE menus.menu_counter > 3 order by menus.menu_counter DESC";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result_array();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
  }


  function dailymealchef($chefid ='')
    {
     // echo $chefid;die;
        $idarr      = array();
        $returnData = array();
        $req1       = "SELECT * FROM  `register` where is_verify_chef='yes' 
        and is_chef ='1' and is_verify='yes' and is_user = '1'";
        //print_r($req);die;
        $query1     = $this->db->query($req1);
        if ($query1->num_rows() > 0) {
            $users = $query1->result();
            foreach ($users as $key => $value) {
                //print_r($value);die;
                $idarr[] = $value->id;
                
            }

            if($chefid !='' && $chefid !='0' &&  in_array($chefid , $idarr)){
              $idarr = array_unique($idarr,SORT_REGULAR);
                $key =  array_search($chefid,$idarr);
                unset($idarr[$key]);
              // /  print_r($idarr);die;
            }
           
            $ids = implode(",", $idarr); 
            
          

          $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) and find_in_set('Daily Meal',become_chef.service_type)";

//print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
                return $returnData;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /*******our pick chef start*********/
    function ourpickchef($chefid ='')
    {
     // echo $chefid;die;
        $idarr      = array();
        $returnData = array();
        $req1       = "SELECT * FROM  `register` where is_verify_chef='yes' 
        and is_chef ='1' and is_verify='yes' and is_user = '1'";
        //print_r($req);die;
        $query1     = $this->db->query($req1);
        if ($query1->num_rows() > 0) {
            $users = $query1->result();
            foreach ($users as $key => $value) {
                //print_r($value);die;
                $idarr[] = $value->id;
                
            }

            if($chefid !='' && $chefid !='0' &&  in_array($chefid , $idarr)){
              $idarr = array_unique($idarr,SORT_REGULAR);
                $key =  array_search($chefid,$idarr);
                unset($idarr[$key]);
              // /  print_r($idarr);die;
            }
           
            $ids = implode(",", $idarr); 
            
          

          $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) and register.whatshot_counter > 0 ORDER BY register.whatshot_counter desc limit 30";

//print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
                return $returnData;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /********our pick chef end********/

    function getfave()
    {
        $returnData = array();
        $req        = "SELECT * FROM  `favourite_chef`";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

    function getprice()
    {
        $returnData = array();
        $req        = "SELECT t.* FROM menus t JOIN ( SELECT *, MIN(price_per_person) minVal FROM menus GROUP BY user_id ) t2 ON t.price_per_person = t2.minVal AND t.user_id = t2.user_id";
        //print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
/*****max price******/
     function getmaxprice()
    {
        $returnData = array();
        $req        = "SELECT t.* FROM menus t JOIN ( SELECT *, MAX(price_per_person) maxVal FROM menus GROUP BY user_id ) t2 ON t.price_per_person = t2.maxVal AND t.user_id = t2.user_id";
        //print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    function getchefrating()
    {
        $returnData = array();
        $req        = "SELECT r.rr_id, r.chef_id, r.main_user_id,r.review,r.emoji_comment,r.avr_rating,b.id,b.user_id FROM review_rating r ,become_chef b WHERE r.chef_id = b.user_id";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result_array();
        }
        return $returnData;
        
    }

    function newchef($chefid=''){
      $before7Days = date('Y-m-d', strtotime('-7 days'));
       $idarr      = array();
        $returnData = array();
        $req1       = "SELECT * FROM  `register` where is_verify_chef='yes' 
        and is_chef ='1' and is_verify='yes' and is_user = '1'";
        //print_r($req);die;
        $query1     = $this->db->query($req1);
        if ($query1->num_rows() > 0) {
            $users = $query1->result();
            foreach ($users as $key => $value) {
                //print_r($value);die;
                $idarr[] = $value->id;
                
            }

            if($chefid !='' && $chefid !='0' &&in_array($chefid , $idarr)){
              $idarr = array_unique($idarr,SORT_REGULAR);
                $key =  array_search($chefid,$idarr);
                unset($idarr[$key]);
              // /  print_r($idarr);die;
            }
           
            $ids = implode(",", $idarr); 
            
              // $req   = "SELECT *, (3959 * acos (cos(radians($latitude)) * cos(radians(chef_latitude) ) * cos( radians(chef_longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians(chef_latitude ) ) ) ) AS distance FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) HAVING distance < 30 ORDER BY distance";

          $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) and date(become_chef.created_at) >='$before7Days'";

//print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
                return $returnData;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

     function getsubmenu($menuid)
    {
        $returnData = array();
        $req        = "SELECT * FROM  `sub_menus` where menu_id ='$menuid' order by created_at desc";
     //   print_r($req);
        $query = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                
                if (empty($val->dish_image)) {
                    $val->dish_image = '';
                } else {
                    $val->dish_image = base_url() . 'assets/submenuImage/' . $val->dish_image;
                }
            }
        }
        
        return $returnData;
    }

     function getLoggedinusercurrency($main_user_id){
        $returnData = array();
        $req        = "SELECT currency FROM `register` WHERE id = '$main_user_id'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    /****chef details page query*********/

   function getfavechef($chefid)
    {
        $returnData = array();
        $req        = "SELECT * FROM  `favourite_chef` where chef_id = '$chefid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }
    function getminprice($chefid)
    {
        $returnData = array();
        $req        = "SELECT t.* FROM menus t JOIN ( SELECT *, MIN(price_per_person) minVal FROM menus GROUP BY user_id ) t2 ON t.price_per_person = t2.minVal AND t.user_id = t2.user_id and t.user_id = '$chefid'";
        //print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

     function getmaxpricechef($chefid)
    {
        $returnData = array();
        $req        = "SELECT t.* FROM menus t JOIN ( SELECT *, MAX(price_per_person) maxVal FROM menus GROUP BY user_id ) t2 ON t.price_per_person = t2.maxVal AND t.user_id = t2.user_id and t.user_id = '$chefid'";
        //print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

     function getRating($chefid)
    {
        $returnData = array();
        $req        = "SELECT r.rr_id, r.chef_id, r.main_user_id,r.review,r.emoji_comment,r.avr_rating,b.id,b.user_id FROM review_rating r ,become_chef b WHERE r.chef_id = b.user_id and r.chef_id = '$chefid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result_array();
        }
        return $returnData;
        
    }
    function MenusSubmenus($userid, $menuid,$to_cur)
    {
        
        $returnData = array();
        // $req="SELECT * FROM menus LEFT JOIN sub_menus ON menus.id = sub_menus.menu_id WHERE menus.id = '$menuid' and menus.user_id = sub_menus.user_id and menus.user_id = '$userid'";
        /*$req        = "SELECT * FROM sub_menus where user_id ='$userid' and menu_id  ='$menuid' ";*/
        $req = "SELECT sm.id,sm.menu_id,sm.user_id,sm.dish_name,sm.dish_image,sm.dish_category,sm.created_at, sm.updated_at,sm.status,m.id,m.price_per_person as menu_price,m.currency as menucurrency,m.menu_title,m.whatshot,m.menu_counter FROM sub_menus sm , menus m WHERE sm.menu_id = m.id and sm.menu_id ='$menuid' and sm.user_id ='$userid'";
       // print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                
                if ($val->dish_image != '') {
                    $val->dish_image = base_url() . 'assets/submenuImage/' . $val->dish_image;
                } else {
                    $val->dish_image = '';
                }
                // if(!empty($val->menucurrency)){
                //   $from_cur = $val->menucurrency;
                // }else{
                //   $from_cur = 'HKD';
                // }
                
                // $amount = $val->price_per_person;
                
                // $val->currency = $to_cur;
                // if($to_cur != $from_cur && $amount !=0){
                //   $convertedPrice = $this->convertCurrency($amount, $from_cur, $to_cur);
                // }else{
                //     $convertedPrice = $val->menu_price;
                // }
                
                // $val->menu_price = round($convertedPrice);
            }
        }
        return $returnData;
        
    }
      /***************/
    function convertCurrency($amount, $from, $to) {
    //  echo $amount . $from . $to;
     $url = 'http://finance.google.com/finance/converter?a=' . $amount . '&from=' . $from . '&to=' . $to;
     $data = file_get_contents($url);
         preg_match_all("/<span class=bld>(.*)<\/span>/", $data, $converted);
         $final = preg_replace("/[^0-9.]/", "", $converted[1][0]);
        return round($final, 3);//die;
    }

    /********get review and rating*********/
    function getReviewRating($chef_id)
    {
        
        $returnData = array();
        $req        = "SELECT * FROM `review_rating` WHERE `chef_id`= '$chef_id'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result_array();
        }
        return $returnData;
        
    }

    function getRRlist($chef_id)
    {
        $returnData = array();
        $req        = "SELECT r.rr_id,r.chef_id,r.main_user_id, r.avr_rating,r.review,r.images,r.created_at,r.updated_at,u.id,u.first_name,u.last_name,u.profile_pic FROM review_rating r , register u WHERE u.id = r.main_user_id and r.chef_id='$chef_id'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
            foreach ($returnData as $val) {
                //print_r($val);
                //echo $val->profile_pic;
                if (empty($val->profile_pic)) {
                    $val->profile_pic = '';
                } else {
                    $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                }
                
                $images = explode("|", $val->images);
                $i      = 0;
                foreach ($images as $value) {
                    $val->newimages[$i] = base_url() . 'assets/reviewrating/' . $value;
                    //print_r($value);
                    $i++;
                }
                //echo date("d F Y | H:i", strtotime($val->created_at));die;
                $val->newdate = date("d F Y | H:i", strtotime($val->created_at));
            }
            //die;
        }
        return $returnData;
    }

    function getLikeReview(){
        $returnData = array();
        $req        = "SELECT * FROM like_review";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

     function getLikeYES($main_user_id){
        $returnData = array();
      //  $req        = "SELECT * FROM like_review where review_id = '$rr_id' and main_user_id ='$main_user_id'";
       /* $req = "SELECT * FROM review_rating LEFT JOIN like_review ON review_rating.rr_id = like_review.review_id WHERE
           review_rating.main_user_id = like_review.main_user_id and review_rating.main_user_id = '$main_user_id' and review_rating.rr_id ='$rr_id'";*/
           $req = "SELECT * FROM like_review where main_user_id ='$main_user_id'";
          // print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

    function addMenu($data,$menuid){
      if(!empty($data)){
        if(!empty($menuid)){
          $where = array('id'=>$menuid);
          $isUpdate = $this->db->update('menus', $data, $where); // updata on 

          return array(
                'id' => $menuid
            );

        }else{
           $this->db->insert('menus',$data);
           $lastinserted = $this->db->insert_id();
        }
     
     
      return array('id'=> $lastinserted);
      }else{
      return FALSE;//not register
    }
    }

    function getmenudata($menuid){
       $returnData = array();
        $req        = "SELECT * FROM menus where id = '$menuid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

    function addsubMenu($data){
      $usersid = $data['user_id'];
      $submenuid = $data['id'];
      if(!empty($data)){
        if(!empty($submenuid)){
          $where = array('id'=>$submenuid);
          $isUpdate = $this->db->update('sub_menus', $data, $where); // updata on 

          return array(
                'id' => $submenuid
            );

        }else{

        
            $isSaved      = $this->db->insert('sub_menus', $data);
            $lastinserted = $this->db->insert_id();
            if($lastinserted){
                                        $where1              = array(
                                        'id' => $usersid
                                       
                                    );
                                    $rdata['is_menu'] = '1';
                                    $isUpdate1           = $this->db->update('register', $rdata, $where1); // updata on become a chef
                    }
                  }
            
            return array(
                'id' => $lastinserted
            ); //record insert
      }else{
      return FALSE;//not register
    }
    }

    function updateDraft($menuid,$data){
      if(!empty($menuid) && !empty($data)){
         $where = array('id' => $menuid);
         $isUpdate = $this->db->update('menus', $data, $where); // updata on 
         return array(
                'id' => $menuid
            );
      }else{
         return FALSE;
      }

    }

    function updateSubDraft($submenuid,$data){
      if(!empty($submenuid) && !empty($data)){
         $where = array('id' => $submenuid);
         $isUpdate = $this->db->update('sub_menus', $data, $where); // updata on 
         return array(
                'id' => $submenuid
            );
      }else{
         return FALSE;
      }
    }
/********Delete menus********/
    function deleteMenus($menuid)
    {
        if (!empty($menuid)) {
            $tablename = 'menus';
            $where     = array(
                'id' => $menuid
            );
            $this->db->where($where);
            $deleteData = $this->db->delete($tablename);
            
            if ($deleteData) {
                $tablename1 = 'sub_menus';
                $where1     = array(
                    'menu_id' => $menuid
                );
                $this->db->where($where1);
                $this->db->delete($tablename1);
            }
            
            return 1;
        } else {
            return 0;
        }
        
    }

     /******Delete sub menu****/
    
    function deleteSubmenu($submenu)
    {
        if (!empty($submenu)) {
            $tablename = 'sub_menus';
            $where     = array(
                'id' => $submenu
            );
            $this->db->where($where);
            $this->db->delete($tablename);
            return 1;
        } else {
            return 0;
        }
    }

    function getsubmenuarray($submenuid){
      $returnData = array();
        $req        = "SELECT * FROM sub_menus where id = '$submenuid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;

    }

    function getmenusforedit($menuid){
        $returnData = array();
        $req        = "SELECT * FROM menus where id = '$menuid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

    function getpermission($chefid){
        $returnData = array();
        $req        = "SELECT * FROM permission where user_id = '$chefid'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;

    }
    function checkchefpass($chefid,$oldpassword)
    {
     $returnData = array();
        $req        = "SELECT * FROM register where id = '$chefid' and password = '$oldpassword'";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

    function changePass($chefid,$newpass)
    {
      if(!empty($chefid) && !empty($newpass)){
        $data['password'] = $newpass;
         $where = array('id' => $chefid);
         $isUpdate = $this->db->update('register', $data, $where); // updata on 
         return array(
                'id' => $chefid
            );
      }else{
         return FALSE;
      }

    }

    function checkemail($email){
     $query = "SELECT * FROM  `register` where email = '$email'";
     
        $query = $this->db->query($query);
    
        return $query->result();
}
  function UpdatePass($password,$id,$email){

       $ChangePass = $this->db->query("UPDATE register SET password='" . $password . "' WHERE id='" . $id . "' and email = '".$email."'");
    //   print_r("UPDATE register SET password='" . $password . "' WHERE id='" . $id . "' and email = '".$email."'");die;
       //print_r("UPDATE users SET password='" . $data . "' WHERE user_id='" . $id . "' and email = '".$email."'");die;
            if($ChangePass){
             return array(
                'id' => $id
            );
            }else {
            return FALSE;
            }
    }
    function saveSetting($chefid,$permissionid,$data)
    {
     if(!empty($permissionid)){
      /*******update data****/
      $where = array('id' => $permissionid,'user_id' => $chefid);
      $isUpdate = $this->db->update('permission', $data, $where); // updata on 
      if($isUpdate && $data['instagram_login'] =='0'){
        $w = array('id'=>$chefid);
        $up['access_token'] = '';
        $this->db->update('register', $up, $w); // updata on 
      }
         return array('id' => $permissionid,'user_id' => $chefid);

     }else{
      $this->db->insert('permission', $data);
      $lastinserted = $this->db->insert_id();
      return array(
          'id' => $lastinserted
      );
     }
    }
        /********Insert likeReview******/
    function likeReview($data)
    {
        if (!empty($data)) {
            $userid    = $data['main_user_id'];
            $review_id    = $data['review_id'];
            $query = "SELECT *  from like_review where main_user_id ='$userid' and review_id = '$review_id'";
            $req   = $this->db->query($query);
            if ($req->num_rows() > 0) {
                $returnData = $req->result();
                foreach ($returnData as $key => $value) {
                  $lid = $value->like_id;
                }

            $where    = array(
                'like_id' => $lid
            );
            $isUpdate = $this->db->update('like_review', $data, $where);
            return array(
                'like_id' => $lid
            );

            }else{
                $isSaved      = $this->db->insert('like_review', $data);
            $lastinserted = $this->db->insert_id();
            
            return array(
                'like_id' => $lastinserted
            );
            }
            
        } else {
            return FALSE;
        }

    }
    
    /***********Delete likeReview***********/
    function deletelikeReview($data)
    {
        
        if (!empty($data)) {
            $userid    = $data['main_user_id'];
            $review_id    = $data['review_id'];
            $tablename = 'like_review';
            $where     = array(
                'main_user_id' => $userid,
                'review_id' => $review_id
            );
            $this->db->where($where);
            $deleteData = $this->db->delete($tablename);
            return TRUE;
        } else {
            return FALSE;
        }
        
    }

    /*******Privacy policy********/
    function privacypolicy($lan){
     $returnData = array();
      $query ="SELECT * FROM `privacy_policy` WHERE language = '$lan'";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result_array();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
    }
    /********Terms & conditions********/
     /*******Privacy policy********/
    function termscondition($lan){
     $returnData = array();
      $query ="SELECT * FROM `terms_condition` WHERE language = '$lan'";
      $req= $this->db->query($query);
       if($req->num_rows() > 0){
       $returnData =  $req->result_array();
       return $returnData;
      // print_r($returnData);
       }else{
        return FALSE;
       }
  }
  /*******wishlist Chef*********/
  function wishlistChef($user,$id)
  {     
        $returnData = array();
        $req        = "SELECT * FROM  `favourite_chef` where user_id = '$user' and chef_id='$id' "; 
     //   print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
  }
  /********Insert favouriot******/
    function favouriteChef($data)
    {
        if (!empty($data)) {
            $isSaved      = $this->db->insert('favourite_chef', $data);
            $lastinserted = $this->db->insert_id();
            
            return array(
                'id' => $lastinserted
            );
        } else {
            return FALSE;
        }
    }
    
    /***********Delete chef***********/
    function deletefavouriteChef($data)
    {
        
        if (!empty($data)) {
            $userid    = $data['user_id'];
            $chefid    = $data['chef_id'];
            $tablename = 'favourite_chef';
            $where     = array(
                'user_id' => $userid,
                'chef_id' => $chefid
            );
            $this->db->where($where);
            $deleteData = $this->db->delete($tablename);
            return TRUE;
        } else {
            return FALSE;
        }
        
    }

    function whatshostchef($chefid ='')
    {
     // echo $chefid;die;
        $idarr      = array();
        $returnData = array();
        $req1       = "SELECT * FROM  `register` where is_verify_chef = 'yes' 
        and is_chef ='1' and is_verify='yes' and is_user = '1'";
        //print_r($req);die;
        $query1     = $this->db->query($req1);
        if ($query1->num_rows() > 0) {
            $users = $query1->result();
            foreach ($users as $key => $value) {
                //print_r($value);die;
                $idarr[] = $value->id;
                
            }

            if($chefid !='' && $chefid !='0' &&  in_array($chefid , $idarr)){
              $idarr = array_unique($idarr,SORT_REGULAR);
                $key =  array_search($chefid,$idarr);
                unset($idarr[$key]);
              // /  print_r($idarr);die;
            }
           
            $ids = implode(",", $idarr); 
            
          

          $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) ORDER BY register.whatshot_counter desc";

//print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
                return $returnData;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /*******Service counter for explore world page start*********/
    function DailyMeal($chefid='',$language){
      if($language == 'english'){
        $string = 'Daily Meal';

      }elseif($language == 'traditional-chinese'){
        $string = '加雙筷';
      }else{
        $string = '加双筷';
      }

       $returnData = array();
       // $req        = "SELECT * FROM  `become_chef` where find_in_set('$string',service_type) and user_id !='$chefid'"; 
       $req = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id !='$chefid' and find_in_set('$string',become_chef.service_type) and register.is_verify_chef ='yes'";
       //print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;

    }

    function CookingWorkshop($chefid='',$language){
      if($language == 'english'){
        $string = 'Cooking Workshop';

      }elseif($language == 'traditional-chinese'){
        $string = '烹飪教室';
      }else{
        $string = '烹饪教室';
      }

       $returnData = array();
        // $req        = "SELECT * FROM  `become_chef` where find_in_set('$string',service_type) 
        // and user_id !='$chefid'"; 
        $req = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id !='$chefid' and find_in_set('$string',become_chef.service_type) and register.is_verify_chef ='yes'";
     //   print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;

    }

    function FoodDelivery($chefid='',$language){

      if($language == 'english'){
        $string = 'Food Delivery';

      }elseif($language == 'traditional-chinese'){
        $string = '食物外送';
      }else{
        $string = '食物外送';
      }

       $returnData = array();
       // $req        = "SELECT * FROM  `become_chef` where find_in_set('$string',service_type) and user_id !='$chefid' "; 
        $req = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id !='$chefid' and find_in_set('$string',become_chef.service_type) and register.is_verify_chef ='yes'";
     //   print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;

    }


    function OnsiteCooking($chefid='',$language){

      if($language == 'english'){
        $string = 'Chef Onsite Cooking';

      }elseif($language == 'traditional-chinese'){
        $string = '廚師親臨烹調';
      }else{
        $string = '厨师亲临烹调';
      }
       $returnData = array();
       // $req        = "SELECT * FROM  `become_chef` where find_in_set('$string',service_type) and user_id !='$chefid' ";
        $req = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id !='$chefid' and find_in_set('$string',become_chef.service_type) and register.is_verify_chef ='yes'"; 
     //   print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;

    }

    function HomePrivateMeal($chefid='',$language){

      if($language == 'english'){
        $string = 'Home Private Meal';

      }elseif($language == 'traditional-chinese'){
        $string = '廚家私房菜';
      }else{
        $string = '厨家私房菜';
      }
       $returnData = array();
       // $req        = "SELECT * FROM  `become_chef` where find_in_set('$string',service_type) and user_id !='$chefid' "; 
        $req = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id !='$chefid' and find_in_set('$string',become_chef.service_type) and register.is_verify_chef ='yes'";
     //   print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;

    }

    function RestaurantPrivate($chefid='',$language){

      if($language == 'english'){
        $string = 'Restaurant Private Meal';

      }elseif($language == 'traditional-chinese'){
        $string = '餐廳私房菜';
      }else{
        $string = '餐厅私房菜';
      }
       $returnData = array();
      //  $req        = "SELECT * FROM  `become_chef` where find_in_set('$string',service_type) and user_id !='$chefid'"; 
        $req = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id !='$chefid' and find_in_set('$string',become_chef.service_type) and register.is_verify_chef ='yes' ";
     //   print_r($req);die;
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;

    }
    /*******Service counter for explore world page end*********/


    function gethighestRec($chefid ='')
    {
     // echo $chefid;die;
        $idarr      = array();
        $returnData = array();
        $req1       = "SELECT * FROM  `register` where is_verify_chef='yes' 
        and is_chef ='1' and is_verify='yes' and is_user = '1'";
        //print_r($req);die;
        $query1     = $this->db->query($req1);
        if ($query1->num_rows() > 0) {
            $users = $query1->result();
            foreach ($users as $key => $value) {
                //print_r($value);die;
                $idarr[] = $value->id;
                
            }

            if($chefid !='' && $chefid !='0' &&  in_array($chefid , $idarr)){
              $idarr = array_unique($idarr,SORT_REGULAR);
                $key =  array_search($chefid,$idarr);
                unset($idarr[$key]);
              // /  print_r($idarr);die;
            }
           
            $ids = implode(",", $idarr); 
            
          

          $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) ORDER BY become_chef.recommended desc";

//print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
                return $returnData;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    function gethighestBook($chefid ='')
    {
     // echo $chefid;die;
        $idarr      = array();
        $returnData = array();
        // $req1       = "SELECT * FROM  `register` where is_verify_chef='yes' 
        // and is_chef ='1' and is_verify='yes' and is_user = '1'";
        $req1 = "SELECT * , COUNT( chef_id ) AS favcount FROM favourite_chef GROUP BY chef_id ORDER BY favcount DESC ";
        //print_r($req);die;
        $query1     = $this->db->query($req1);
        if ($query1->num_rows() > 0) {
            $users = $query1->result();
            foreach ($users as $key => $value) {
                //print_r($value);die;
                $idarr[] = $value->chef_id;
                
            }

            if($chefid !='' && $chefid !='0' &&  in_array($chefid , $idarr)){
              $idarr = array_unique($idarr,SORT_REGULAR);
                $key =  array_search($chefid,$idarr);
                unset($idarr[$key]);
              // /  print_r($idarr);die;
            }
           
            $ids = implode(",", $idarr); 
            
          

          $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) ORDER BY FIELD(become_chef.user_id,$ids) ";

//print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
                return $returnData;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

     function gethightolow($chefid ='')
    {
     // echo $chefid;die;
        $idarr      = array();
        $returnData = array();
        // $req1       = "SELECT * FROM  `register` where is_verify_chef='yes' 
        // and is_chef ='1' and is_verify='yes' and is_user = '1'";
        $req1 = "SELECT t . * FROM menus t JOIN ( SELECT * , MAX( price_per_person ) maxVal FROM menus GROUP BY user_id
)t2 ON t.price_per_person = t2.maxVal
AND t.user_id = t2.user_id";
        //print_r($req);die;
        $query1     = $this->db->query($req1);
        if ($query1->num_rows() > 0) {
            $users = $query1->result();
            foreach ($users as $key => $value) {
                //print_r($value);die;
                $idarr[] = $value->user_id;
                
            }

            if($chefid !='' && $chefid !='0' &&  in_array($chefid , $idarr)){
              $idarr = array_unique($idarr,SORT_REGULAR);
                $key =  array_search($chefid,$idarr);
                unset($idarr[$key]);
              // /  print_r($idarr);die;
            }
           
            $ids = implode(",", $idarr); 
            
          

          $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) ORDER BY FIELD(become_chef.user_id,$ids) ";

//print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
                return $returnData;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

     function getlowtohigh($chefid ='')
    {
     // / echo $chefid;die;
        $idarr      = array();
        $returnData = array();
        // $req1       = "SELECT * FROM  `register` where is_verify_chef='yes' 
        // and is_chef ='1' and is_verify='yes' and is_user = '1'";
        $req1 = "SELECT t . * FROM menus t JOIN ( SELECT * , MIN( price_per_person ) minVal FROM menus GROUP BY user_id
)t2 ON t.price_per_person = t2.minVal
AND t.user_id = t2.user_id";
        //print_r($req);die;
        $query1     = $this->db->query($req1);
        if ($query1->num_rows() > 0) {
            $users = $query1->result();
            foreach ($users as $key => $value) {
                //print_r($value);die;
                $idarr[] = $value->user_id;
                
            }
           // print_r($idarr);

            if($chefid !='' && $chefid !='0' &&  in_array($chefid , $idarr)){
                $idarr = array_unique($idarr,SORT_REGULAR);
                $key =  array_search($chefid,$idarr);
                unset($idarr[$key]);
                // echo '<pre>';
                // print_r($idarr);die;
            }
           
            $ids = implode(",", $idarr); 
            
          

          $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) ORDER BY FIELD(become_chef.user_id,$ids) ";

//print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
                return $returnData;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
     /********Get favroite chef*******/
    function getfavroite($user_id)
    {
        $returnData = array();
        $req        = "SELECT * FROM `favourite_chef` where user_id = '$user_id' ";
        $query      = $this->db->query($req);
        if ($query->num_rows() > 0) {
            $returnData = $query->result();
        }
        return $returnData;
    }

     function getchefprofile($chefids)
    {
        $returnData = array();
        if (!empty($chefids)) {
            // $req="SELECT * FROM become_chef where id in ($chefids) ";
            $req   = "SELECT *FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($chefids) ";
            //print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if ($val->profile_pic != '') {
                            $val->profile_pic = $val->profile_pic;
                        } else {
                            $val->profile_pic = '';
                        }
                        
                    } else {
                        if ($val->profile_pic != '') {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        } else {
                            $val->profile_pic = '';
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
            }
        }
        
        return $returnData;
        
    }

    /*******Service type filters*******/
    function getServiceType($chefid ='',$arr)
    {
      // echo $chefid;die;
        $countarr = count($arr);
       // echo $countarr;die;
        $str = '';

        if( $countarr > 0){
          for($i=0;$i<$countarr;$i++) {
            $str .= "become_chef.service_type LIKE '%".$arr[$i]."%'";
           
           if ($i != $countarr) $str .=' OR ';
          }

        }
       // rtrim($str,' OR ');
        $str = rtrim($str,' OR ');
        $idarr      = array();
        $returnData = array();
        // $req1       = "SELECT * FROM  `register` where is_verify_chef='yes' 
        // and is_chef ='1' and is_verify='yes' and is_user = '1' and id !='$chefid'";
        $req1 = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id !='$chefid' and register.is_verify_chef='yes'";
        //print_r($req);die;
        $query1     = $this->db->query($req1);
        if ($query1->num_rows() > 0) {
            $users = $query1->result();
            foreach ($users as $key => $value) {
                //print_r($value);die;
                $idarr[] = $value->id;
                
            }

            if($chefid !='' && $chefid !='0' &&  in_array($chefid , $idarr)){
              $idarr = array_unique($idarr,SORT_REGULAR);
                $key =  array_search($chefid,$idarr);
                unset($idarr[$key]);
              // /  print_r($idarr);die;
            }
           
            $ids = implode(",", $idarr); 
            
    if(!empty($str)){
      $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) AND ($str) ORDER BY become_chef.recommended desc";
    }else{
       $req   = "SELECT * FROM become_chef LEFT JOIN register ON become_chef.user_id = register.id WHERE become_chef.user_id in ($ids) ORDER BY become_chef.recommended desc";
    }

         

//print_r($req);die;
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
                $returnData = $query->result();
                foreach ($returnData as $val) {
                    if ($val->facebook_login == 'yes' && $val->facebook_login != '') {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = $val->profile_pic;
                        }
                    } else {
                        if (empty($val->profile_pic)) {
                            $val->profile_pic = '';
                        } else {
                            $val->profile_pic = base_url() . 'assets/chefprofile/' . $val->profile_pic;
                        }
                    }
                    
                    
                    if (empty($val->image1)) {
                        $val->image1 = '';
                    } else {
                        $val->image1 = base_url() . 'assets/kitchengallery/' . $val->image1;
                    }
                    
                    if (empty($val->image2)) {
                        $val->image2 = '';
                    } else {
                        $val->image2 = base_url() . 'assets/kitchengallery/' . $val->image2;
                    }
                    if (empty($val->image3)) {
                        $val->image3 = '';
                    } else {
                        $val->image3 = base_url() . 'assets/kitchengallery/' . $val->image3;
                    }
                    if (empty($val->image4)) {
                        $val->image4 = '';
                    } else {
                        $val->image4 = base_url() . 'assets/kitchengallery/' . $val->image4;
                    }
                    if (empty($val->image5)) {
                        $val->image5 = '';
                    } else {
                        $val->image5 = base_url() . 'assets/kitchengallery/' . $val->image5;
                    }
                    if (empty($val->image6)) {
                        $val->image6 = '';
                    } else {
                        $val->image6 = base_url() . 'assets/kitchengallery/' . $val->image6;
                    }
                    if (empty($val->image7)) {
                        $val->image7 = '';
                    } else {
                        $val->image7 = base_url() . 'assets/kitchengallery/' . $val->image7;
                    }
                    if (empty($val->image8)) {
                        $val->image8 = '';
                    } else {
                        $val->image8 = base_url() . 'assets/kitchengallery/' . $val->image8;
                    }
                    if (empty($val->image9)) {
                        $val->image9 = '';
                    } else {
                        $val->image9 = base_url() . 'assets/kitchengallery/' . $val->image9;
                    }
                    if (empty($val->image10)) {
                        $val->image10 = '';
                    } else {
                        $val->image10 = base_url() . 'assets/kitchengallery/' . $val->image10;
                    }
                    if (empty($val->image11)) {
                        $val->image11 = '';
                    } else {
                        $val->image11 = base_url() . 'assets/kitchengallery/' . $val->image11;
                    }
                    if (empty($val->image12)) {
                        $val->image12 = '';
                    } else {
                        $val->image12 = base_url() . 'assets/kitchengallery/' . $val->image12;
                    }
                    if (empty($val->image13)) {
                        $val->image13 = '';
                    } else {
                        $val->image13 = base_url() . 'assets/kitchengallery/' . $val->image13;
                    }
                    if (empty($val->image14)) {
                        $val->image14 = '';
                    } else {
                        $val->image14 = base_url() . 'assets/kitchengallery/' . $val->image14;
                    }
                    if (empty($val->image15)) {
                        $val->image15 = '';
                    } else {
                        $val->image15 = base_url() . 'assets/kitchengallery/' . $val->image15;
                    }
                    if (empty($val->image16)) {
                        $val->image16 = '';
                    } else {
                        $val->image16 = base_url() . 'assets/kitchengallery/' . $val->image16;
                    }
                    if (empty($val->image17)) {
                        $val->image17 = '';
                    } else {
                        $val->image17 = base_url() . 'assets/kitchengallery/' . $val->image17;
                    }
                    if (empty($val->image18)) {
                        $val->image18 = '';
                    } else {
                        $val->image18 = base_url() . 'assets/kitchengallery/' . $val->image18;
                    }
                    if (empty($val->image19)) {
                        $val->image19 = '';
                    } else {
                        $val->image19 = base_url() . 'assets/kitchengallery/' . $val->image19;
                    }
                    if (empty($val->image20)) {
                        $val->image20 = '';
                    } else {
                        $val->image20 = base_url() . 'assets/kitchengallery/' . $val->image20;
                    }
                    
                    $val->allimage = $val->image1 . '|' . $val->image2 . '|' . $val->image3 . '|' . $val->image4 . '|' . $val->image5 . '|' . $val->image6 . '|' . $val->image7 . '|' . $val->image8 . '|' . $val->image9 . '|' . $val->image10 . '|' . $val->image11 . '|' . $val->image12 . '|' . $val->image13 . '|' . $val->image14 . '|' . $val->image15 . '|' . $val->image16 . '|' . $val->image17 . '|' . $val->image18 . '|' . $val->image19 . '|' . $val->image20;
                    
                    $cusn = explode(",", $val->cuisine);
                    $cusn = array_values(array_unique(array_filter($cusn), SORT_REGULAR));
                    if (!empty($cusn)) {
                        $cusns        = implode(",", $cusn);
                        $val->cuisine = $cusns;
                    } else {
                        $val->cuisine = '';
                    }
                }
                return $returnData;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    function updateinstaToken($access_token,$userid){
      if(!empty($access_token) && !empty($userid)){
        $where = array('id' => $userid);
          $updatedata['access_token']= $access_token;
          $isSaved = $this->db->update('register',$updatedata,$where);
          if($isSaved){
            /*****update permissin******/
            $req = "SELECT * FROM  `permission` where user_id = '$userid' ";
            $query = $this->db->query($req);
            if ($query->num_rows() > 0) {
              $w = array('id' => $userid);
              $data['instagram_login'] = '1';
              $this->db->update('permission',$data,$w);

            }else{
              $data['instagram_login'] = '1';
              $this->db->insert('permission',$data);
            }
          }
          return array('id' => $userid);
      }

    }

    function getadmincharge(){
        $query = "SELECT * FROM admin_charges";
          $query = $this->db->query($query);
        return $query->result();
    }

}
