<?php
$lang['welcome_message'] = '歡迎來到CodexWorld';
$lang['language']='繁體中文';
/*****Admin login form********/
$lang['username'] = '用戶名稱';
$lang['password'] = '密碼';
$lang['lost_password'] = '忘記密碼？';
$lang['login']='登錄';
$lang['lost_password_msg']='在下面輸入您的電子郵件地址，我們將向您發送如何恢復密碼的說明。';
$lang['backtologin']='回到登錄';
$lang['rcover']='恢復';
// $lang['emailadd']='電子郵件地址';
$lang['emailadd']='電子郵件';
$lang['image']='圖片';
/*****Admin main header********/
$lang['welcome'] = '歡迎';
$lang['setting'] = '設置';
$lang['logout'] = '登出';
$lang['myprofile'] = '我的簡歷';
$lang['mytask']='我的任務';
$lang['sitetitle']='廚師與食客';//chef diner


/******Side bar menu******/
$lang['dashboard'] ='仪表板';
$lang['user']='用戶';
$lang['userlist']='用戶列表';
/*****breadcrumb ******/
$lang['home']='家';
$lang['pages']='網頁';
$lang['privacy']='隱私政策';
$lang['jobs']='工作';
$lang['addjobs']='添加作業';
$lang['editjobs']='編輯作業';
$lang['contact']='聯繫';

/****all pages label and placeholder****/
$lang['title']='標題';
$lang['content']='內容';
$lang['save']='保存';
$lang['termscondition']='條款和條件';

/*****Success and error msg*****/
$lang['policysuccss'] = '隱私政策成功保存';
$lang['error']='出現問題，請重試';
$lang['termssuccss'] = '條款和條件成功保存';
$lang['Jobsuccss'] = '工作成功保存';
$lang['Jobdelete'] = '工作成功刪除';
$lang['Contactsuccss'] = '聯繫人已成功保存';
$lang['chefverifysuccess'] = '廚師成功更新';
$lang['userverifysuccess'] = '用戶成功更新';
$lang['menusuccess']='成功保存菜單';
$lang['submenusuccess']='保存子菜單成功';
$lang['deletemenus']='菜單已成功刪除';
$lang['deletesubmenus']='子菜單已成功刪除';

/*******Table content for list*******/
$lang['serialno'] = 'S號';
$lang['usersName'] = '名稱';
$lang['usersEmail'] = 'Email';
$lang['isuser'] = '用戶';
$lang['ischef'] = '廚師嗎';
$lang['isuserverify'] = '用戶驗證';
$lang['ischefverify'] = '廚師驗證';
$lang['jobtitle'] = '工作職銜';
$lang['description'] = '描述';
$lang['action'] = '行動';
$lang['yes'] = '是';
$lang['no'] = '不是';
$lang['verifychef']='驗證廚師';
$lang['preferlanguage']='你想用的語言是？';
$lang['nameque']='你的全名是？';
$lang['speaklanguage']='你能已什麼語言與客人溝通？';
$lang['phonenum']='你的電話號碼是？';
$lang['oftencook']='你多久要做飯？';
$lang['palcelocated']='你的地址在哪裡？';
$lang['numberPeople']='有多少人可以主持晚宴？';
$lang['kitchenTitle']='你廚房的名字是？';
$lang['kitchenDes']='描述你的廚房';
$lang['servicetype']='選擇您的服務類型';
$lang['uploadpic']='上傳個人資料圖片';


$lang['contactno']='電話號碼';
$lang['verifyuser']='驗證用戶';
/*******Menu section********/
$lang['menu'] = '菜單';
$lang['menulist'] = '菜單列表';
$lang['chefname'] = '廚師名稱';
$lang['menutitle'] = '菜單標題';
$lang['menuimage']='菜單圖像';
$lang['priceperperson'] = '每人價格';
$lang['currency'] = '貨幣';
$lang['submenu'] = '子菜單';
$lang['addedat'] = '添加在';
$lang['status'] = '狀態';
$lang['view']='视图';

/*****Sub menu section*****/
$lang['submenulist'] = '子菜單列表';
$lang['dishname'] = '菜名';
$lang['dishimage'] = '菜圖像';
$lang['dishcategory'] = '菜類別';

$lang['active']='活性';
$lang['deactive']='去活';

/****Edit menu***/
$lang['editmenu'] = '編輯菜單';
$lang['service']='服務類型';

/******edit sub menu******/
$lang['editsubmenu'] = '編輯子菜單';
/*******Cuisines*********/
$lang['cuisine1'] = '港式';
$lang['cuisine2'] = '中文';
/*****All Steps********/
$lang['step1'] = '步驟1';
$lang['step2'] = '第2步';
$lang['step3'] = '步驟3';
$lang['step4'] = '步驟4';
$lang['step5'] = '步驟5';
/******All step text******/
$lang['step1text']='選擇服務的日期和時間';
$lang['step2text']='選擇菜單和服務類型';
$lang['step3text']='廚師24小時內確認您的要求';
$lang['step4text']='執行安全付款';
$lang['step5text']='廚師提供服務，您可以和其他美食家一起享用餐點';
$lang['howitworks']='怎麼運行的?';
/********Service type*******/

$lang['daily_meal'] = '加雙筷';
$lang['cooking_workshop'] = '烹飪教室';
$lang['food_delivery'] = '食物外送';
$lang['chef_onsite_cooking'] = '廚師親臨烹調';
$lang['home_private_meal'] = '廚家私房菜';
$lang['restaurant_private_meal'] = '餐廳私房菜';

/*****Filters******/
$lang['highest_recommended'] = '最高評分推薦';
$lang['highest_bookmarked'] = '最多收藏';
$lang['price_lth'] = '價格 - 低到高';
$lang['price_htl'] = '價格 - 從高到低';

?>