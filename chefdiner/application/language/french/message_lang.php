<?php
$lang['welcome_message'] = 'Welcome to CodexWorld';
$lang['language']='English';

/*****Admin login form********/
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['lost_password'] = 'Lost Password?';
$lang['login']='Login';
$lang['lost_password_msg']='Enter your e-mail address below and we will send you instructions how to recover a password.';
$lang['backtologin']='Back to login';
$lang['rcover']='Recover';
$lang['emailadd']='E-mail address';

/*****Admin main header********/
$lang['welcome'] = 'Welcome';
$lang['setting'] = 'Settings';
$lang['logout'] = 'Logout';
$lang['myprofile'] = 'My Profile';
$lang['mytask']='My Task';
$lang['sitetitle']='Chef Diner';
$lang['image']='Image';

/******Side bar menu******/
$lang['dashboard'] ='Dashboard';

/*****breadcrumb ******/
$lang['home']='Home';
$lang['pages']='Pages';
$lang['privacy']='Privacy Policy';
$lang['jobs']='Jobs';
$lang['contact']='Contact';
$lang['addjobs']='Add Jobs';
$lang['editjobs']='Edit Jobs';
/****all pages label and placeholder****/
$lang['title']='Title';
$lang['content']='Content';
$lang['save']='Save';
$lang['termscondition']='Terms & Conditions';

/*****Success and error msg*****/
$lang['policysuccss'] = 'Privacy Policy Saved Successfully';
$lang['error']='Something went wrong,please try again';
$lang['termssuccss'] = 'Terms & Conditions Saved Successfully';
$lang['Jobsuccss'] = 'Jobs Saved Successfully';
$lang['Jobdelete'] = 'Jobs Deleted Successfully';
$lang['Contactsuccss'] = 'Contacts Saved Successfully';
?>