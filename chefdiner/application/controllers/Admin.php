<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'libraries/Stripe/lib/Stripe.php');
class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();

		header("Expires: Tue, 01 Jan 2013 00:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$this->load->library('form_validation');
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
		$this->load->model('user_admin');
        $this->load->library('encrypt');
        $this->load->helper('language');
       //echo $this->session->userdata('site_lang');
        if($this->session->userdata('site_lang') !=''){
        $lan =$this->session->userdata('site_lang');
        $this->lang->load('message',$lan);
        }else{
        $this->lang->load('message','english');
        }
	}
	
	public function index()
	{
       // echo $lan;

		if ($this->session->userdata('admin_id')) {

            redirect('admin/main');
			//$this->load->view('admin/main',$langdata);
        } else {
        	redirect('admin/login');
        	//$this->load->view('admin/login',$langdata);
        }
	}

	public function main()
	{
       // echo $this->session->userdata('site_lang');
        // echo($this->session->userdata('select_lang'));echo($this->session->userdata('default'));die;
		if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           //  echo $this->session->userdata('site_lang');die;
			$data['page']='admindash';
            $data['verifyusers'] = $this->user_admin->verifyusers();
            $data['notverifyusers'] = $this->user_admin->notverifyusers();
            $data['totalChef'] = $this->user_admin->totalChef();
            $data['acceptbooking'] = $this->user_admin->getAcceptedbooking();
            $data['waitingbooking'] = $this->user_admin->getwaitingBooking();
            $data['rejectbooking'] = $this->user_admin->getRejectedbooking();
            $data['payment'] = $this->user_admin->getpayment();
			$this->load->view('admin/main',$data);
        }
	}

	public function login() {
    
        if ($this->session->userdata('admin_id')) {
			redirect('admin/main');
        } else {

            $user = $this->input->post('username');
            $password = $this->input->post('password');

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/login');
            } else {
				$data['username'] = $this->input->post('username');
				$data['password'] = md5($this->input->post('password'));
				$isAdminLoggedIn = $this->user_admin->isAdminLoggedIn($data['username'],$data['password']);
				if($isAdminLoggedIn && $this->session->userdata('admin_id') !=''){
					redirect('admin/main');
				}else {
                    $data['error'] = '<strong>Access Denied</strong> Invalid Username/Password';
                    $this->load->view('admin/login', $data);
                }
            }
        }
	}
	
    function switchLang($language = "") {
        
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        
        redirect($_SERVER['HTTP_REFERER']);
        
    }

    /*********Privacy Policy*******/
    function privacyPolicy(){
            if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
          //  echo $this->session->userdata('site_lang');die;
            $sitelang = $this->session->userdata('site_lang');
            $userid = $this->session->userdata('admin_id');
            //echo $sitelang;die;
            $data['getprivacypolicy'] = $this->user_admin->getpolicy($sitelang,$userid);
            $data['page']='privacypolicy';
            $this->load->view('admin/privacypolicy',$data);
        }
    }

    /*******Terms & Conditions********/
	function termsConditions(){
            if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $sitelang = $this->session->userdata('site_lang');
            $userid = $this->session->userdata('admin_id');
            //echo $sitelang;die;
            $data['gettermsconditions'] = $this->user_admin->gettermsconditions($sitelang,$userid);//   echo $this->session->userdata('site_lang');
            $data['page']='termsconditions';
            $this->load->view('admin/termsconditions',$data);
        }
    }

    /*****Contact******/
    function contact(){
    if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $language = $this->session->userdata('site_lang');
            $userid = $this->session->userdata('admin_id');
            $data['getcontact']= $this->user_admin->getcontact($language,$userid);
            $data['page']='contact';
            $this->load->view('admin/contact',$data);
        }
    }

    /*********save Privacy Policy********/

    function savePolicy(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           if(isset($_POST['submit'])){
            // echo '<pre>';
            // print_r($_POST);die;
            $id = $this->input->post('id');
            $language = $this->input->post('language');
            
            $updateData = array('title'=>$this->input->post('policytitle'),
                'user_id'=>$this->session->userdata('admin_id'),
                'content'=>$this->input->post('policycontent'),
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->savePolicy($updateData,$id,$language);
            if($isSaved){
                 $this->session->set_flashdata('success', $this->lang->line('policysuccss'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

           }
            redirect('admin/privacyPolicy');
           // $this->load->view('admin/contact',$data);
        }
    }

    /*********save Terms & Condition********/

    function saveTerms(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           if(isset($_POST['submit'])){
            // echo '<pre>';
            // print_r($_POST);die;
            $id = $this->input->post('id');
            $language = $this->input->post('language');
            
            $updateData = array('title'=>$this->input->post('termstitle'),
                'content'=>$this->input->post('termscontent'),
                'user_id'=>$this->session->userdata('admin_id'),
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->saveTerms($updateData,$id,$language);
            if($isSaved){
                 $this->session->set_flashdata('success', $this->lang->line('termssuccss'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

           }
            redirect('admin/termsConditions');
           // $this->load->view('admin/contact',$data);
        }
    }

    /******Jobs ******/
    function jobs(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           $sitelang =  $this->session->userdata('site_lang');
           $user_id=$this->session->userdata('admin_id');
            $data['alljobs']=$this->user_admin->getalljobs($sitelang,$user_id);
            $data['page']='jobs';
            $this->load->view('admin/jobs',$data);
        }
    }

    /*****Add new jobs*****/
    function addjobs(){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           $sitelang =  $this->session->userdata('site_lang');
            $user_id=$this->session->userdata('admin_id');
            $data['alljobs']=$this->user_admin->getalljobs($sitelang,$user_id);
            $data['page']='addjobs';
            $this->load->view('admin/addjobs',$data);
        }
    }
    /*** Save Jobs****/
     function savejobs(){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
         }else{
           if(isset($_POST['submit'])){
            // echo '<pre>';
            // print_r($_FILES);
           
            
            $language = $this->input->post('language');

            //Check whether user upload picture
                        if(!empty($_FILES['userimage']['name'])){
                            $config['upload_path'] = 'assets/jobsimage/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['userimage']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('userimage')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }else{
                                $picture = '';
                            }
                        }
            
            $insertData = array('title'=>$this->input->post('jobtitle'),
                'description'=>$this->input->post('jobcontent'),
                'image'=>$picture,
                'language'=>$language,
                'user_id'=>$this->session->userdata('admin_id'),
                'created_at'=>date('y-m-d H:i:s'),
                'updated_at'=>date('y-m-d H:i:s')
                );
             //print_r($insertData);die;
            $isSaved = $this->user_admin->saveJobs($insertData);
            if($isSaved){
                 $this->session->set_flashdata('success', $this->lang->line('Jobsuccss'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

           }
            redirect('admin/jobs');
           // $this->load->view('admin/contact',$data);
        }
     }

     /*******Edit jobs********/
     function editjobs($id){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
       
            $data['getjobs']=$this->user_admin->getjobs($id);
            $data['page']='addjobs';
            $this->load->view('admin/editjobs',$data);
        }
     }

     /********save update jobs*******/

     function updatejobs(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           if(isset($_POST['submit'])){
            // echo '<pre>';
            // print_r($_FILES);
            // print_r($_POST);die;
            $id = $this->input->post('id');
            $language = $this->input->post('language');
             //Check whether user upload picture
                        if(!empty($_FILES['userimage']['name'])){
                            $config['upload_path'] = 'assets/jobsimage/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['userimage']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('userimage')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }else{
                                $picture = $this->input->post('oldimge');
                            }
                        }else{
                            $picture = $this->input->post('oldimge');
                        }
            $updateData = array('title'=>$this->input->post('jobtitle'),
                'description'=>$this->input->post('jobcontent'),
                'image'=>$picture,
                'user_id'=>$this->session->userdata('admin_id'),
                'language'=>$language,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->saveupdatejobs($updateData,$id,$language);
            if($isSaved){
                 $this->session->set_flashdata('success', $this->lang->line('Jobsuccss'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

           }
            redirect('admin/jobs');
           // $this->load->view('admin/contact',$data);
        }
    }

    /******delete jobs*******/
    function deletejobs($id){
        if(!empty($id)){
            $delete = $this->user_admin->deletejobs($id);
            $this->session->set_flashdata('success', $this->lang->line('Jobsuccss'));
        }else{
            $this->session->set_flashdata('error', $this->lang->line('error'));
        }

        redirect('admin/jobs');
    }
    /*********Logout********/
    public function logout(){
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('email');
		$this->session->unset_userdata('name');
        $this->session->unset_userdata('logged_in_back');
        $this->session->unset_userdata('username');
      //  $this->session->unset_userdata('site_lang');
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect('admin','refresh');
    }


     /*****save Contact*****/

    function saveContact(){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           if(isset($_POST['submit'])){
            // echo '<pre>';
            // print_r($_FILES);
            // print_r($_POST);die;
            $id = $this->input->post('id');
            $language = $this->input->post('language');
             //Check whether user upload picture
                        if(!empty($_FILES['userimage']['name'])){
                            $config['upload_path'] = 'assets/contact/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['userimage']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('userimage')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }else{
                                $picture = $this->input->post('oldimge');
                            }
                        }else{
                            $picture = $this->input->post('oldimge');
                        }
            $updateData = array('title'=>$this->input->post('contacttitle'),
                'content'=>$this->input->post('contactcontent'),
                'image'=>$picture,
                'user_id'=>$this->session->userdata('admin_id'),
                'language'=>$language,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->saveContact($updateData,$id,$language);
            if($isSaved){
                 $this->session->set_flashdata('success', $this->lang->line('Contactsuccss'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

           }
            redirect('admin/contact');
           // $this->load->view('admin/contact',$data);
        }
        
    }

    /********Get users list*******/

    public function users()
    {
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
           //  echo $this->session->userdata('site_lang');die;
            $data['userlist']= $this->user_admin->getallusers();
            $data['page']='userlist';
            $this->load->view('admin/users',$data);
        }
    }


    /************** Verify chef ****************/
    function chefverify($id){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $getRefAmt = $this->user_admin->getRefAmt();
            $creditedAmt = $getRefAmt[0]['r_amount_chef'];
           //  echo $this->session->userdata('site_lang');die;
            $checkRef = $this->user_admin->checkRef($id);
            
            if(!empty($checkRef)){
                $refCode = $checkRef[0]['user_referral_code'];
                $GetuserRef = $this->user_admin->GetuserRef($refCode);
            }else{
                 $refCode = '';
                $GetuserRef = '';
            }
            
          /*  if(!empty($GetuserRef)){
                $data['creditedamt'] = $creditedAmt;
            }else{
                $data['creditedamt'] = 0;
            }*/
            $data['referralcode'] = $refCode;
            $data['chefusers']= $this->user_admin->getchefuser($id);
            $data['page']='userlist';
            $this->load->view('admin/verifychef',$data);
        }
    }
    /********verified chef by admin**********/
    function verifiedchef(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            if(isset($_POST['submit'])){
          //  $credited = $this->input->post('creditedamt');
            $getRefAmt = $this->user_admin->getRefAmt();
            $creditedAmt = $getRefAmt[0]['r_amount_chef'];
            $refCode = $this->input->post('referralcode');
            $GetuserRef = $this->user_admin->GetuserRef($refCode);
            if(!empty($GetuserRef)){
                $refuse = $GetuserRef[0]['id'];
                $credited = $GetuserRef[0]['credited_amount'];
            }else{
                $refuse ='';
                $credited = 0;
            }
            $userid = $this->input->post('user_id');
            $email = $this->input->post('email');
            $verifychef = $this->input->post('verifychef');

            if($verifychef != 'no'){
                $amt = $credited+ $creditedAmt ;
            }else{
                $amt = $credited;
            }
             $updateData = array('is_verify_chef'=> $verifychef,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->Verfiychef($updateData,$userid);
            if($isSaved){
                $uData['credited_amount'] = $amt;
                $updateUsers = $this->user_admin->updateUsers($refuse,$uData);

                /******Send mail to chef******/
                                // $message = 'Hello User ,';
                                // $config['mailtype'] = 'html';
                                // $config['charset']  = 'iso-8859-1';
                                // $config['wordwrap'] = TRUE;
                                // $config['newline']  = "\r\n"; 
                                // $this->load->library('email', $config);
                                // $this->load->helper('path');

                                // $this->email->from('test@vervelogic.com', 'Salestrackerr');
                                // $this->email->to($this->input->post('customer_email')); 
                                // // $this->email->cc('another@another-example.com'); 
                                // // $this->email->bcc('them@their-example.com'); 

                                // $this->email->subject('You are verify by Admin');
                                // $this->email->message($message); 
                                // $mail = $this->email->send();
                 $this->session->set_flashdata('success', $this->lang->line('chefverifysuccess'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }
            
        }
         redirect('admin/users');
        }
    }

    /*******verfiy users*******/
    function userverify($id){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{

           //  echo $this->session->userdata('site_lang');die;
            $data['getusers']= $this->user_admin->getusers($id);
            $data['page']='userlist';
            $this->load->view('admin/verifyusers',$data);
        }
        
    }

    /**********verifiedusers********/
    function verifiedusers(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            if(isset($_POST['submit'])){
            $userid = $this->input->post('user_id');
            $email = $this->input->post('email');
            $verifyuser = $this->input->post('verifyuser');
             $updateData = array('is_verify'=> $verifyuser,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->VerfiyUsers($updateData,$userid);
            if($isSaved){

                /******Send mail to chef******/
                                // $message = 'Hello User ,';
                                // $config['mailtype'] = 'html';
                                // $config['charset']  = 'iso-8859-1';
                                // $config['wordwrap'] = TRUE;
                                // $config['newline']  = "\r\n"; 
                                // $this->load->library('email', $config);
                                // $this->load->helper('path');

                                // $this->email->from('test@vervelogic.com', 'Salestrackerr');
                                // $this->email->to($this->input->post('customer_email')); 
                                // // $this->email->cc('another@another-example.com'); 
                                // // $this->email->bcc('them@their-example.com'); 

                                // $this->email->subject('You are verify by Admin');
                                // $this->email->message($message); 
                                // $mail = $this->email->send();

                 $this->session->set_flashdata('success', $this->lang->line('userverifysuccess'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }
            
        }
         redirect('admin/users');
        }
    }

    /*****Get menu list****/

    public function menus()
    {
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $data['menulist']= $this->user_admin->getallList();
            $data['page']='menulist';
            $this->load->view('admin/menus',$data);
        }
    }

    /******Get submenu list*******/

    public function submenu($id){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $data['submenulist']= $this->user_admin->getallsubmenu($id);
            $data['page']='menulist';
            $this->load->view('admin/submenus',$data);
        }
    }

    /*******Edit menus********/

    public function Editmenu($id){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $data['menudata']= $this->user_admin->getmenudata($id);
            $data['page']='menulist';
            $this->load->view('admin/editmenus',$data);
        }
    }

    /******Save edit menu****/

    function savemenus(){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            if(isset($_POST['submit'])){
            $menuid = $this->input->post('menu_id');
            $status = $this->input->post('status');
             $updateData = array('status'=> $status,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->Savemenu($updateData,$menuid);
            if($isSaved){

                
                 $this->session->set_flashdata('success', $this->lang->line('menusuccess'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }
            
        }
         redirect('admin/menus');
        }
    }


    /******Edit sub menu*****/

    function editsubmenu($id){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $data['submenudata']= $this->user_admin->getsubmenudata($id);
            $data['page']='menulist';
            $this->load->view('admin/editsubmenu',$data);
        }
    }

    /*****Save sub menu******/

    function savesubmenus(){
         if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            if(isset($_POST['submit'])){
            $submenu_id = $this->input->post('submenu_id');
            $menu_id = $this->input->post('menu_id');
            $status = $this->input->post('status');
             $updateData = array('status'=> $status,
                'updated_at'=>date('y-m-d H:i:s')
                );
            $isSaved = $this->user_admin->SaveSubmenu($updateData,$submenu_id);
            if($isSaved){

                
                 $this->session->set_flashdata('success', $this->lang->line('submenusuccess'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }
            
        }
         redirect('admin/submenu/'.$menu_id);
        }
    }
/*******Delete menus********/
    function deleteMenus($menuid){
          if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            
            $isSaved = $this->user_admin->deleteMenus($menuid);
            if($isSaved){

                
                 $this->session->set_flashdata('success', $this->lang->line('deletemenus'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

            redirect('admin/menus');
        }
    }
/********Delete submenus*************/
    function deletesubMenus($submenuid,$menuid){
          if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
        // /  echo $menuid;die;
            $isSaved = $this->user_admin->deletesubMenus($submenuid);
            if($isSaved){

                
                 $this->session->set_flashdata('success', $this->lang->line('deletesubmenus'));
            }else{
                $this->session->set_flashdata('error', $this->lang->line('error'));
            }

            redirect('admin/submenu/'.$menuid);
        }
    }

/*****delteusers*******/
function deleteusers($userid){
      if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            
            $isSaved = $this->user_admin->deleteusers($userid);
            if($isSaved){

                
                 $this->session->set_flashdata('success', 'User deleted');
            }else{
                $this->session->set_flashdata('error', 'User not deleted');
            }

            redirect('admin/users');
        }
}

/*******Payment from app*******/
 function paypal(){
  //  $url = $this->uri->segment_array();
  //  print_r($url);
    $url_param = $this->uri->segment(3);
    $url_param1 = $this->uri->segment(4);
    $url_param2 = $this->uri->segment(5);
    $base1 = $url_param . str_repeat('=', strlen($url_param) % 4);
    $base2 = $url_param1 . str_repeat('=', strlen($url_param1) % 4);
    $base3 = $url_param2 . str_repeat('=', strlen($url_param2) % 4);
    $userid = base64_decode($base1);
    $bookingid = base64_decode($base2);
    $device = base64_decode($base3);
    $data['userid'] = $userid;
    $data['bookingid'] = $bookingid;
    $data['device']=$device;
   // / echo $this->uri->segment(3);
    //echo $userid.'=='.$booking.'=='.$device;die;
         $getbookingCode = $this->user_admin->getbookingCode($bookingid,$userid);
        //print_r($getbookingCode);die;
        if(!empty($getbookingCode)){
          //  print_r($getbookingCode);die;
            $amount = $getbookingCode[0]['menu_price'];
            $currency_code = $getbookingCode[0]['menu_currency'];
        }
/*echo  $currency_code;die;*/
        $amount = $this->convertCurrency($amount , $currency_code , 'USD');

        $data['amount'] = $amount;
        $data['currency_code'] = $currency_code;
        $this->load->view('payment',$data);
    }
/******Payment  process*****/
function paymentPaypal(){
    //if(isset($_POST['submit'])){
        $paymentMethod = $this->input->post('payopt');
        $userid = $this->input->post('userid');
        $bookingid = $this->input->post('bookingid');
        $device = $this->input->post('device');
      //  echo $bookingid;
        $getbookingCode = $this->user_admin->getbookingCode($bookingid,$userid);
        //print_r($getbookingCode);die;
        if(!empty($getbookingCode)){
          //  print_r($getbookingCode);die;
            $amount = $getbookingCode[0]['menu_price'];
            $currency_code = $getbookingCode[0]['menu_currency'];
        }
/*echo  $currency_code;die;*/
        $amount = $this->convertCurrency($amount , $currency_code , 'USD');

        /*echo $paymentMethod;die;*/
      //  if(!empty($paymentMethod) && $paymentMethod=='Paypal' && $currency_code !='INR'){

            $paypal_email='chefdiner@ppi-tech.com';
            $return_url =base_url().'index.php/admin/success';
            $notify_url =base_url().'index.php/admin/ipn';
            $cancel_url =base_url().'index.php/admin/cancel/'.$bookingid;
            $vars = array(
                    'cmd' => '_xclick',
                    'business' => $paypal_email,
                    'item_name' => $device,
                    'custom' => $userid,
                    'item_number' => $bookingid,
                    'amount' => $amount,
                    'notify_url' => $notify_url,
                    'return' => $return_url,
                    'cancel_return'=>$cancel_url,
                    'currency_code' => 'USD',
                    'no_note' => 0,
                    'rm'=>2
                );
           //print_r($vars);die;
            header('Location: https://www.sandbox.paypal.com/cgi-bin/webscr?' . http_build_query($vars));

        // }else{

        // $data['userid'] = $this->input->post('userid');
        // $data['bookingid'] = $this->input->post('bookingid');
        // $data['device'] = $this->input->post('device');
        // $data['amount'] = $amount;
        // $data['currency_code'] = $currency_code;

        // $this->load->view('stripe',$data);
        // }

  //  }

                        
}


function convertCurrency($amount, $from, $to) {
     $url = 'http://finance.google.com/finance/converter?a=' . $amount . '&from=' . $from . '&to=' . $to;
     $data = file_get_contents($url);
         preg_match_all("/<span class=bld>(.*)<\/span>/", $data, $converted);
         $final = preg_replace("/[^0-9.]/", "", $converted[1][0]);
        return round($final, 3);
    }
/******Success*******/ 
function success(){
    $userid = $_REQUEST['custom'];
    $bookingid = $_REQUEST['item_number'];
    $txn_id = $_REQUEST['txn_id'];
    $amount = $_REQUEST['mc_gross'];
    $currency_code =$_REQUEST['mc_currency'];
    $payment_status = $_REQUEST['payment_status'];
    $device = $_REQUEST['item_name'];

    
    $insert ="INSERT INTO `payment`(main_user_id,booking_id,txn_id,payment_gross,currency_code,payment_status,device,   payment_type,created_at,updated_at) VALUES('".$userid."','".$bookingid."','".$txn_id."','".$amount."','".$currency_code."','".$payment_status."','".$device."','Paypal','".date('y-m-d H:i:s')."','".date('y-m-d H:i:s')."')";
    //print_r($insert);die;
            $insertRecodrs =  $this->db->query($insert);
            $last_insert_id = $this->db->insert_id();
            if(!empty($last_insert_id)){
                $data['payment_device'] = $device;
                $data['payment_status'] = $payment_status;
                $data['payment_type'] = 'Paypal';
                $data['status']           = 1;
                $updateBooking = $this->user_admin->updateBooking($bookingid,$data);
                 if($device !='ios'){
                redirect('admin/successPaypal');
            }else{
                redirect('chefdiner://');
            }

            }
}

        function cancel($id){
            $data['payment_type'] = 'Paypal';
            $data['payment_status']  = 'Cancel Payment';
            $upBooking=$this->user_admin->updateBooking($id,$data);
            if($upBooking){
                $getbookingCode = $this->user_admin->getbookingCode1($id);
                if(!empty($getbookingCode)){
                  //  print_r($getbookingCode);die;
                    $payment_device = $getbookingCode[0]['device'];
                 //   $currency_code = $getbookingCode[0]['menu_currency'];
                }else{
                    $payment_device = '';
                    }
                }

        if($payment_device !='ios'){
                redirect('admin/successPaypal');
            }else{
                redirect('chefdiner://');
            }

           

        }

    public function stripe(){
  // $url = $this->uri->segment_array();
  //  print_r($url);die;
           
            Stripe::setApiKey('sk_test_jtBrTdwN1oQRg24lXnsctcD7'); //secret api key
            $amount =$this->input->post('amount');
            $amountusd = $amount * 100;
            $charge = Stripe_Charge::create(array(
                        "amount" => $amountusd,
                        "currency" => "USD",
                        "card" => $this->input->post('access_token'),
                       // "description" => "Stripe Payment"
            ));
            //print_r($charge);die;
         //   echo $charge->balance_transaction;die;
            // this line will be reached if no error was thrown above
           $data = array(
                'payement_id' => $charge->id,
                'main_user_id'=>  $this->input->post('userid'),
                'booking_id'=> $this->input->post('bookingid'),
                'txn_id'=>$charge->balance_transaction,
                'payment_status' => 'Success',
                'payment_gross' =>$amount ,
                'currency_code'=>'USD',
                'payment_type'=>'Stripe',
                'device'=>$this->input->post('device'),
                'created_at'=>date('y-m-d H:i:s'),
                'updated_at'=>date('y-m-d H:i:s')
               
            );
       // print_r($data);die();
            $response = $this->user_admin->insertstripe($data);
            if ($response) {
                echo json_encode(array('status' => 200, 'success' => 'Payment successfully completed.','data'=>$response));
                exit();
            } else {
                echo json_encode(array('status' => 500, 'error' => 'Something went wrong. Try after some time.'));
                exit();
            }
        
    }

    function successStripe($id){

       // echo $id;die;
        $getPayDetails = $this->user_admin->getPayDetails($id);
     /*   echo '<pre>';
        print_r($getPayDetails);die;*/
        if(!empty($getPayDetails)){

            $data['payment_device'] = $getPayDetails[0]['device'];
            $data['payment_status'] = $getPayDetails[0]['payment_status'];
            $data['payment_type'] = 'Stripe';
            $data['status'] = 1;
            $device = $getPayDetails[0]['device'];
            $bookingid = $getPayDetails[0]['booking_id'];
            $updateBooking = $this->user_admin->updateBooking($bookingid,$data);
            if($device !='ios'){
                redirect('admin/successPaypal');
            }else{
                redirect('chefdiner://');
            }
        }

    }

    function cancelStripe($id,$device){
       // echo $id.$device;die;
            $data['payment_type'] = 'Stripe';
            $data['payment_status']  = 'Cancel Payment';
            $data['payment_device'] =$device;
            $upBooking=$this->user_admin->updateBooking($id,$data);
            if($upBooking){
       
            if($device !='ios'){
                redirect('admin/successPaypal');
            }else{
                redirect('chefdiner://');
            }

           }

        }

    function waiting(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $data['page'] = 'waiting';
            $data['waitingBooking'] =$this->user_admin->getwaitingBooking();
            $this->load->view('admin/waitingbooking',$data);
        }
    }
    function accepted(){
               if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{

            $data['page'] = 'accepted';
            $data['acceptedBooking'] =$this->user_admin->getAcceptedbooking();
            $this->load->view('admin/acceptbooking',$data);
        }
    }

    function rejected(){
               if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
            $data['page'] = 'rejected';
            $data['rejectedBooking'] =$this->user_admin->getRejectedbooking();
            $this->load->view('admin/rejectbooking',$data);
        }
    }


    /*******Referral amount admin*******/
    function referral(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{

           //  echo $this->session->userdata('site_lang');die;
            $data['page']='referralpage';
            $data['referral']= $this->user_admin->referralamount();
          
            $this->load->view('admin/referralamount',$data);
        }
        
    }

    function saveReferralAmount(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
/*print_r($_POST);*/

            $rid =$this->input->post('r_id');
            $data['r_amount_user']  =$this->input->post('r_amount_user');
            $data['r_amount_chef'] =$this->input->post('r_amount_chef');   
            $date['updated_at'] = date('y-m-d H:i:s');
            $isSaved = $this->user_admin->saveReferralAmount($rid,$data);
            if($isSaved){

                
                 $this->session->set_flashdata('success', 'Referral Amount Updated');
            }else{
                $this->session->set_flashdata('error', 'Referral Amount not updated');
            }

            redirect('admin/referral');
        }
        }

    function menuModal(){
        $userid=$this->input->post('userid');
        $getmenus= $this->user_admin->getmenus($userid);
        //print_r($getmenus);

        if(!empty($getmenus)){
             foreach ($getmenus as $key => $value) {
           $menuid          = $value['id'];
           $MenusSubmenus[] = $this->user_admin->MenusSubmenus($userid, $menuid);
        } if (!empty($MenusSubmenus)) {
            foreach ($MenusSubmenus as $key => $value) {
                
                if (!empty($value)) {
                    $MenusSubmenus1[] = $value;
                }
                // print_r($MenusSubmenus1);
            }
        } else {
            $MenusSubmenus1 = '';
        }
        }else {
            $MenusSubmenus1 = '';
        }
//        echo '<pre>';
// print_r($MenusSubmenus1);
        $html = '<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Menus</h4>
        </div>';
if(!empty($MenusSubmenus1)){
foreach ($MenusSubmenus1 as $key => $value) {
    $html .='<div class="modal-body">
          <p><label><b>Menu Name</b> - '.ucfirst($value[0]->menu_title).'</label><label><b>Menu Price</b> - '.$value[0]->menucurrency.' '.ucfirst($value[0]->menu_price).'</label></p>';
          foreach ($value as $key => $value1) {
            if(!empty($value1->menu_image)){
                $menu_image = $value1->menu_image;

            }else{
                $menu_image =base_url().'front/images/blank.jpg';
            }

            if(!empty($value1->dish_image)){
                $dish_image = $value1->dish_image;

            }else{
                $dish_image =base_url().'front/images/blank.jpg';
            }
           $html .='<p><label><b>Dish Name</b> - '.ucfirst($value1->dish_name).'</label>
           <label><b>Dish Image</b> - <img src='.$dish_image.' height="100px" width="100px"></label></p>';
          }


         $html .='</div>';
}
}
        
        $html .='<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>';

  echo $html;
    }

    /*******Admin Charges*******/
    function admincharge(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{

           //  echo $this->session->userdata('site_lang');die;
            $data['page']='admincharge';
          $data['admincharges']= $this->user_admin->adminCharges();
          
            $this->load->view('admin/admincharge',$data);
        }
        
    }

    /********Save admin charges******/

      function saveAdmincharges(){
        if (!$this->session->userdata('admin_id')) {
            redirect('admin/login');
        }else{
/*print_r($_POST);*/

            $chrg_id =$this->input->post('chrg_id');
            $data['per_amt']  =$this->input->post('per_amt');
            $data['chef_per']  =$this->input->post('chef_per');
            $date['updated_at'] = date('y-m-d H:i:s');
            $isSaved = $this->user_admin->saveAdmincharges($chrg_id,$data);
            if($isSaved){

                
                 $this->session->set_flashdata('success', 'Admin charges Updated');
            }else{
                $this->session->set_flashdata('error', 'Admin charges not updated');
            }

            redirect('admin/admincharge');
        }
        }

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */