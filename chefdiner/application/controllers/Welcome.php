<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'libraries/Stripe/lib/Stripe.php');
class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
        
		parent::__construct();

		header("Expires: Tue, 01 Jan 2013 00:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$this->load->library('form_validation');
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
        $this->load->library('encrypt');
        $this->load->helper('language');
       $this->load->model('welcome_model');
        $this->load->model('user_admin');
        if($this->session->userdata('site_lang') !=''){
        $lan =$this->session->userdata('site_lang');
        $this->lang->load('message',$lan);
        }else{
        $this->lang->load('message','english');
        }
		
    }
 
	public function index()
	{
	//	echo $this->session->userdata('site_lang');die;
		if (!$this->session->userdata('user_id')) {
			$to_cur ='HKD';
			$data['whatshot'] = $this->welcome_model->whatshot();
			$dailymealchef = $this->welcome_model->dailymealchef();
            $newchef = $this->welcome_model->newchef();
          //  print_r($newchef);die;
			$getfave = $this->welcome_model->getfave();
			if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
		            }
		        } else {
		            $chefarrq = '';
		        }
			$getprice = $this->welcome_model->getprice();
	        $getmaxprice = $this->welcome_model->getmaxprice();
	        if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }


            if (!empty($dailymealchef)) {
            foreach ($dailymealchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $dailymealchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $dailymealchef[$key]->favouritecount = 0;
                    }
                } else {
                    $dailymealchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $dailymealchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $dailymealchef[$key]->price = 0;
                    }
                } else {
                    $dailymealchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $dailymealchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $dailymealchef[$key]->maxprice = 0;
                    }
                } else {
                    $dailymealchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $dailymealchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $dailymealchef[$key]->totalreview = 0;
                    }
                } else {
                    $dailymealchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $dailymealchef='';
        }

        /*******new chef*******/
         if (!empty($newchef)) {
            foreach ($newchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $newchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $newchef[$key]->favouritecount = 0;
                    }
                } else {
                    $newchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $newchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $newchef[$key]->price = 0;
                    }
                } else {
                    $newchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $newchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $newchef[$key]->maxprice = 0;
                    }
                } else {
                    $newchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $newchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $newchef[$key]->totalreview = 0;
                    }
                } else {
                    $newchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $newchef='';
        }
/*******Convert daily meal currency and price*********/
        if(!empty($dailymealchef)){
            foreach ($dailymealchef as $key => $value) {

               $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                
             
                
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
                }
                $value->currency = $to_cur;
                // $value->price =  round($convertedPrice);
                // $value->maxprice =  round($convertedmaxPrice);
                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice =  round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);
            }

        
}

/*******Convert new chef currency and price*********/
if(!empty($newchef)){

      //      echo 'if';
            foreach ($newchef as $key => $value) {

               $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                
             
                
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
                }
                $value->currency = $to_cur;
                // $value->price =  round($convertedPrice);
                // $value->maxprice =  round($convertedmaxPrice);
                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice =  round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);
            }

        
}
			// echo '<pre>';
			// print_r($dailymealchef);die;

        /***********Our picks chef start************/
        $ourpickchef = $this->welcome_model->ourpickchef();

        if (!empty($ourpickchef)) {
            foreach ($ourpickchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $ourpickchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $ourpickchef[$key]->favouritecount = 0;
                    }
                } else {
                    $ourpickchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $ourpickchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $ourpickchef[$key]->price = 0;
                    }
                } else {
                    $ourpickchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $ourpickchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $ourpickchef[$key]->maxprice = 0;
                    }
                } else {
                    $ourpickchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $ourpickchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $ourpickchef[$key]->totalreview = 0;
                    }
                } else {
                    $ourpickchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $ourpickchef='';
        }
/*******Convert our pick chef currency and price*********/
if(!empty($ourpickchef)){

      //      echo 'if';
            foreach ($ourpickchef as $key => $value) {

               $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                
             
                
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
                }
                $value->currency = $to_cur;
                // $value->price =  round($convertedPrice);
                // $value->maxprice =  round($convertedmaxPrice);
                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice =  round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);
            }

        
}
        // echo '<pre>';
        // print_r($ourpickchef);die;

        /**********Our picks chef end************/

            $data['dailymealchef'] =$dailymealchef;
            $data['newchef'] =$newchef;
            $data['ourpickchef'] =$ourpickchef;

    		$this->load->view('front/home',$data);


        }else{
        	/*$data['page']='clinicdash';
			$this->load->view('front/dashboard',$data);*/
			$chefid = $this->session->userdata('user_id');
			$data['whatshot'] = $this->welcome_model->whatshot();
            $getLoggedinusercurrency  = $this->welcome_model->getLoggedinusercurrency($chefid);
            if(!empty($getLoggedinusercurrency)){
            $to_cur = $getLoggedinusercurrency[0]->currency;
            }else{
                $to_cur ='HKD';
            }
            $dailymealchef = $this->welcome_model->dailymealchef($chefid);
            $newchef = $this->welcome_model->newchef($chefid);
          //  print_r($newchef);die;
            $getfave = $this->welcome_model->getfave();
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }


            if (!empty($dailymealchef)) {
            foreach ($dailymealchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $dailymealchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $dailymealchef[$key]->favouritecount = 0;
                    }
                } else {
                    $dailymealchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $dailymealchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $dailymealchef[$key]->price = 0;
                    }
                } else {
                    $dailymealchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $dailymealchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $dailymealchef[$key]->maxprice = 0;
                    }
                } else {
                    $dailymealchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $dailymealchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $dailymealchef[$key]->totalreview = 0;
                    }
                } else {
                    $dailymealchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $dailymealchef='';
        }

        /*******new chef*******/
         if (!empty($newchef)) {
            foreach ($newchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $newchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $newchef[$key]->favouritecount = 0;
                    }
                } else {
                    $newchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $newchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $newchef[$key]->price = 0;
                    }
                } else {
                    $newchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $newchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $newchef[$key]->maxprice = 0;
                    }
                } else {
                    $newchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $newchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $newchef[$key]->totalreview = 0;
                    }
                } else {
                    $newchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $newchef='';
        }

        /*********Daily meal currency and price conversion*************/
        if(!empty($dailymealchef)){

            foreach ($dailymealchef as $key => $value) {

                $fromChefcurr = $value->currency;
               if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                // echo 'dffgvfgdfg'.$price;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
             //  echo $to_cur .'==df='. $fromChefcurr .'===='.$price;
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                  //  echo 'if';
               // echo $fromChefcurr .'if'.$to_cur;
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
               // echo 'else';
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
               // echo $convertedPrice;
                }
                $value->currency = $to_cur;
                // $value->price =  round($convertedPrice);
                // $value->maxprice =  round($convertedmaxPrice);

                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice = round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);
            }

        
}

 /*********new chef currency and price conversion*************/
    if(!empty($newchef)){

            foreach ($newchef as $key => $value) {

                $fromChefcurr = $value->currency;
               if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                // echo 'dffgvfgdfg'.$price;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
             //  echo $to_cur .'==df='. $fromChefcurr .'===='.$price;
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                  //  echo 'if';
               // echo $fromChefcurr .'if'.$to_cur;
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
               // echo 'else';
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
               // echo $convertedPrice;
                }
                $value->currency = $to_cur;
                // $value->price =  round($convertedPrice);
                // $value->maxprice =  round($convertedmaxPrice);

                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice = round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);
            }

        
}
            /***********Our picks chef start************/
        $ourpickchef = $this->welcome_model->ourpickchef($chefid);

        if (!empty($ourpickchef)) {
            foreach ($ourpickchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $ourpickchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $ourpickchef[$key]->favouritecount = 0;
                    }
                } else {
                    $ourpickchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $ourpickchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $ourpickchef[$key]->price = 0;
                    }
                } else {
                    $ourpickchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $ourpickchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $ourpickchef[$key]->maxprice = 0;
                    }
                } else {
                    $ourpickchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $ourpickchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $ourpickchef[$key]->totalreview = 0;
                    }
                } else {
                    $ourpickchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $ourpickchef='';
        }
/*********our pick chef currency and price conversion*************/
    if(!empty($ourpickchef)){

            foreach ($ourpickchef as $key => $value) {

                $fromChefcurr = $value->currency;
               if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                // echo 'dffgvfgdfg'.$price;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
             //  echo $to_cur .'==df='. $fromChefcurr .'===='.$price;
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                  //  echo 'if';
               // echo $fromChefcurr .'if'.$to_cur;
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($convertedmaxPrice, $fromChefcurr, $to_cur);
                }else{
               // echo 'else';
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
               // echo $convertedPrice;
                }
                $value->currency = $to_cur;
                // $value->price =  round($convertedPrice);
                // $value->maxprice =  round($convertedmaxPrice);

                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice = round($convertedPrice, 2, PHP_ROUND_HALF_UP);
            }

        
}
        // echo '<pre>';
        // print_r($ourpickchef);die;

        /**********Our picks chef end************/

            $data['dailymealchef'] =$dailymealchef;
            $data['newchef'] =$newchef;
            $data['ourpickchef'] =$ourpickchef;
			$this->load->view('front/home',$data);
        }
		
	}
	
	 // function switchLang($language = "") {
        
  //       $language = ($language != "") ? $language : "english";
  //       $this->session->set_userdata('site_lang', $language);
  //      // echo $this->session->userdata('site_lang');
  //    //   echo $_SERVER['HTTP_REFERER'];die;
  //       //redirect('http://192.168.1.63/chefdiner/index.php');
  //      redirect(base_url());
        
  //   }

	 function switchLang() {
        $lang = $this->input->post('language');
     //   echo $lang;die;
        if($lang =='English'){
        	$language = 'english';
        }elseif($lang =='T.Chinese'){
        	$language = 'traditional-chinese';
        }else{
        	$language = 'simplified-chinese';
        }
        //$language = ($language != "") ? $language : "english";
      $this->session->set_userdata('site_lang', $language);
      // /  echo $this->session->userdata('site_lang');die;
     //   echo $_SERVER['HTTP_REFERER'];die;
        //redirect('http://192.168.1.63/chefdiner/index.php');
       if(!$this->session->userdata('site_lang')){
       	echo 0;
       }else{
       	echo $this->session->userdata('site_lang');
       }
        
        	
        
    }
	

	// function sendsms($number, $message_body, $return = '0') {
	/*function sendsms($return = '0') {


			$sender = 'SEDEMO'; // Need to change

			// $smsGatewayUrl = 'http://springedge.com';

			$smsGatewayUrl = 'https://instantalerts.co';
			
			$apikey = '69iq54a4m4s4ib0agg135o3y0yfbkbmbu'; // Need to change

			$textmessage='this is the test sms.';

			$textmessage = urlencode($textmessage);

			$api_element = '/api/web/send/';

			$mobileno = '7790910393';

			$api_params = $api_element.'?apikey='.$apikey.'&sender='.$sender.'&to='.$mobileno.'&message='.$textmessage;
			$smsgatewaydata = $smsGatewayUrl.$api_params;

			$url = $smsgatewaydata;

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_POST, false);

			curl_setopt($ch, CURLOPT_URL, $url);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); $output = curl_exec($ch);

			curl_close($ch);

			if(!$output){ $output = file_get_contents($smsgatewaydata); }

			if($return == '1'){ return $output; }else{ echo "Sent"; }

}*/

/*********User registration********/

 public function userRegister()
	{
		
		/*******Generate refferal code*******/
		$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$res = "";
		for ($i = 0; $i < 10; $i++) {
		    $res .= $chars[mt_rand(0, strlen($chars)-1)];
		}
			$email = $this->input->post('email');
			$language = $this->input->post('language');
			if($language  == ''){
				$language = 'english';
			}
			$data['first_name'] = $this->input->post('fname');
			$data['last_name'] = $this->input->post('lname');
			$data['email'] = $this->input->post('email');
			$data['status'] = '1';
			$data['is_user'] = '1';
		    $data['referal_code'] = $res;
			$data['password'] = md5($this->input->post('password'));
			$data['phone_number'] = $this->input->post('contact');
			$data['language'] = $language;
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$email = filter_var($email, FILTER_SANITIZE_EMAIL); 
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
				$arr = array('txt'=>'IE','msg'=>'Invalid Email,Please user valid Email Id');
				echo json_encode($arr);
			}else{

				/******Sent mail befoe insert data******/
				//send email  to user
				$to = $this->input->post('email');
				$subject = "Welcome to Chefdiner";
				$from = 'chefdiner@gmail.com';

				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

				// Create email headers
				$headers .= 'From:'.$from."\r\n".
				'Reply-To: '.$from."\r\n" .
				'X-Mailer: PHP/' . phpversion();


				$username =$this->input->post('fname');
				$activationlink = base_url() . 'index.php/welcome/Confirm_Activation/'.base64_encode($this->input->post('email'));
				$message = 'Hello '.ucfirst($username).',Thank you for signing up. We will be using your email to send you information about bookings and other important events.';
				$message .='<button><a href="'.$activationlink.'">Click Here</a></button>';

				$res = $this->welcome_model->userRegister($data);
				
				if(is_string($res) && $res == 'NR'){
					$arr = array('txt'=>'NR','msg'=>'Something went wrong please try again.');
					echo json_encode($arr);
				}elseif(is_string($res) && $res == 'AE'){
					$arr = array('txt'=>'AE','msg'=>'Email id already exist,please try with another email.');
					echo json_encode($arr);
				}else{
					$mail = mail($to, $subject, $message, $headers);
				if($mail){
				$arr = array('txt'=>'RS','msg'=>'Activation  link sent to your email id.Please Verify activation link.');
				echo json_encode($arr);
				} else{
				   $arr = array('txt'=>'MN','msg'=>'Please use valid email id for registration , mail not sent.');
									echo json_encode($arr);
				}
					
				}



				
			}
		
	}


	/********User Login********/

	 public function userLogin()
	{
		
			$authToken = $this->welcome_model->_generate_token();
			$email = $this->input->post('loginemail');
			$data['email'] = $this->input->post('loginemail');
			$data['password'] = $this->input->post('loginpassword');
			//$data['authtoken'] = $authToken;
			
		//	print_r($data);die;

			$email = filter_var($email, FILTER_SANITIZE_EMAIL); 
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
				$arr = array('txt'=>'IE','msg'=>'Invalid Email,Please use valid Email Id');
				echo json_encode($arr);
			}else{
			$res = $this->welcome_model->userLogin($data);
			if(is_string($res) && $res == 'NA'){
					$arr = array('txt'=>'NA','msg'=>'Not Activated.');
					echo json_encode($arr);
			} elseif(is_string($res) && $res == 'WP'){
				$arr = array('txt'=>'WP','msg'=>'Password you entered is wrong.');
					echo json_encode($arr);
			} else{
				$arr = array('txt'=>'SL','msg'=>'Successfully login.');
					echo json_encode($arr);
			}
			
		}
	}

	/******Logout*******/
	 public function logout($lang){
	 	//echo $lang;die;
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('first_name');
		$this->session->unset_userdata('last_name');
        $this->session->unset_userdata('is_chef');
        $this->session->unset_userdata('is_verify_chef');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('referal_code');
        $this->session->set_userdata('site_lang', $lang);
       // $this->session->sess_destroy();
 //$this->session->set_userdata('site_lang', $lang);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
         
        redirect(base_url(),'refresh');
    }

    /*******Confirmation Link********/

    function Confirm_Activation(){
    	$url = $this->uri->segment_array();
    	$url_param = end($url);
    	
		// and later:
		$base_64 = $url_param . str_repeat('=', strlen($url_param) % 4);
		$data = base64_decode($base_64);
		$getemail = $this->welcome_model->ConfirmMail($data);
		//print_r($getemail);die;
		if(!empty($getemail)){
			$refAmount = $this->welcome_model->getrefAmount();
			$refamtUsr = $refAmount[0]->r_amount_user;
            $getcode = $this->welcome_model->getuserCode($data);
            print_r($getcode);die;
			        $code = $getcode[0]->user_referral_code;
                    if(!empty($code)){
                    $getCodeUser = $this->welcome_model->getCodeUser($code);
                    $lastId =$getCodeUser[0]->id;
                    $existAmt = $getCodeUser[0]->credited_amount;
                    $totalRefAmt = $existAmt + $refamtUsr;
                    $updateData['credited_amount'] = $totalRefAmt;
                    $saveCredit = $this->welcome_model->saveCredit($lastId,$updateData);
                    }else{
                         $saveCredit ='';
                    }
			        
					
					if(!empty($saveCredit)){
						redirect(base_url(),'refresh');
					}
		}else{
			$this->load->view('link');
		}
		
    }

    /*****Bacome a host******/
    function becomehost(){
    if (!$this->session->userdata('user_id')) {
    	$this->load->view('front/hostLoginreg');
    }else{
    	$userid = $this->session->userdata('user_id');
    	/****get user if it is chef*****/
    	//$getuserchef = $this->welcome_model->getuserchef($userid);
    	//print_r($getuserchef);die;
    	//$data['getuserchef'] = $getuserchef;
    	// $this->load->view('front/becomehost',$data);
    	$this->load->view('front/becomehost');
    	
    }
    }

    /******Host regoister********/

    function hostregister(){
    	if (!$this->session->userdata('user_id')) {
	    		$this->load->view('front/hostLoginreg');
        }else{
        	/*$data['page']='clinicdash';
			$this->load->view('front/dashboard',$data);*/
			/*$this->load->view('front/home');*/
        }

    	/*print_r($_POST);die;*/
    }

    /*function hostLogin(){
    	$learnCook = $this->input->post('learnCook');
    	$seatrange = $this->input->post('seatrange');
    	$cookfood = $this->input->post('cookfood');
    	$serveType = $this->input->post('serveType');
    	$NewBuyer_country = $this->input->post('NewBuyer_country');
    	$hostEmail = $this->input->post('hostEmail');
    	$hostPass = $this->input->post('hostPass');
    	
    	$checkVal = $this->welcome_model->checkVal($hostEmail);
    	//print_r($checkVal);
    	if(!empty($checkVal) && !is_string($checkVal)){
    		
    		$userid = $checkVal->id;
    		$data['learn_cook'] = $learnCook;
    		$data['service_type'] = $serveType;
    		$data['currency'] = $NewBuyer_country;
    		
    	
    		$ischef = $this->welcome_model->checkischef($userid);
    		// print_r($ischef);die;
    		if(!empty($ischef)){

    		}else{

    		}
    	}elseif (is_string($checkVal) && $checkVal == 'NV') {
    		# code...
    	}else{

    	}
    }*/
/*function stripetest(){
	$this->load->view('test');
}
function stripesuccess(){
	 Stripe::setApiKey('sk_test_jtBrTdwN1oQRg24lXnsctcD7');*/ //secret api key
  /*          $charge = Stripe_Charge::create(array(
                        "amount" => $this->input->post('amount'),
                        "currency" => "USD",
                        "card" => $this->input->post('access_token'),
                       // "description" => "Stripe Payment"
            ));

            $access_token = $_REQUEST['stripeToken'];

            $data = array(
                'payement_id' => $charge->id,
                'main_user_id'=>  $this->input->post('userid'),
                'booking_id'=> $this->input->post('bookingid'),
                'txn_id'=>$access_token,
                'payment_status' => 'Success',
                'payment_gross' => $this->input->post('amount'),
                'currency_code'=>'USD',
                'payment_type'=>'Stripe',
                'device'=>$this->input->post('device'),
                'created_at'=>date('y-m-d H:i:s'),
                'updated_at'=>date('y-m-d H:i:s')
               
            );
       // print_r($data);die();
            $response = $this->user_admin->insertstripe($data);*/
/*	print_r($_REQUEST);
	print_r($_POST);die;
}*/
   
		function UserbecomeHost(){
			// print_r($_POST);
			if (!$this->session->userdata('user_id')) {
		    	$this->load->view('front/hostLoginreg');
		    }else{
		    	if(isset($_POST['submit'])){
		    		$userid = $this->session->userdata('user_id');
		    		$is_chef = $this->session->userdata('is_chef');
		    		$learnCook = $this->input->post('learnCook');
			    	$seatrange = $this->input->post('seatrange');
			    	$cookfood = $this->input->post('cookfood');
			    	$language = $this->session->userdata('site_lang');
			    	$serveType1 = $this->input->post('serveType');
			    	$serveType = implode(",", $serveType1);
			    	if($seatrange == 'up5'){
			    		$from_guest = 1;
			    		$upto_guest = 5;

			    	}elseif($seatrange == '6to10'){
			    		$from_guest = 6;
			    		$upto_guest = 10;
			    	}else{
			    		$from_guest = 10;
			    		$upto_guest = 50;
			    	}
			    // 	echo $serveType;
			    	$NewBuyer_country = $this->input->post('NewBuyer_country');

			    	$data['learn_cook'] = $learnCook;
		    		$data['service_type'] = $serveType;
		    		$data['currency'] = $NewBuyer_country;
		    		$data['from_guest'] = $from_guest;
		    		$data['upto_guest'] = $upto_guest;
		    		$data['user_id'] = $userid;
		    		$data['chef_fname'] = $this->session->userdata('first_name');
		    		$data['chef_lname'] = $this->session->userdata('last_name');
		    		$data['language'] = $language;
		    		$data['created_at'] = date('y-m-d H:i:s');
		    		$data['created_at'] = date('y-m-d H:i:s');
		    		$isSaved = $this->welcome_model->UserbecomeHost($data);
		    		if(!empty($isSaved)){
		    			$this->session->set_flashdata('success', 'You have to request for Become a Host,Please wait for approval.');
		    			redirect('welcome/mykitchen','refresh');
		    		}else{
		    			$this->session->set_flashdata('error', 'Something went wrong please try again.');
		    			redirect('welcome/becomehost','refresh');
		    		}

		    	}else{

		    	}
		    }
		}

		function mykitchen(){

		if (!$this->session->userdata('user_id')) {
    		$this->load->view('front/home');
        }else{
        	$is_chef = $this->session->userdata('is_chef');
        	$chefid = $this->session->userdata('user_id');
        	$data['getchefdata'] = $this->welcome_model->getchefdata($chefid);
        //	echo $is_chef;
        	/*$data['page']='clinicdash';
			$this->load->view('front/dashboard',$data);*/
			$this->load->view('front/mykitchen',$data);
        }

		}  


		function SignUpR($rcode){
			if(!empty($rcode)){
				$data['rcode'] = $rcode;
				$this->load->view('front/referralsignup',$data);
			}

			//echo $rcode;
			

		} 

		function RegisterRef(){

			//print_r($_POST);die;


/*print_r($refAmount);die;*/
			/*******Generate refferal code*******/
		$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$res = "";
		for ($i = 0; $i < 10; $i++) {
		    $res .= $chars[mt_rand(0, strlen($chars)-1)];
		}
			$email = $this->input->post('email');
			$language = $this->input->post('language');
			if($language  == ''){
				$language = 'english';
			}
			$code = $this->input->post('rcode');
			$data['first_name'] = $this->input->post('first_name');
			$data['last_name'] = $this->input->post('last_name');
			$data['email'] = $this->input->post('email');
			$data['status'] = '1';
			$data['is_user'] = '1';
		    $data['referal_code'] = $res;
		    $data['user_referral_code']= $code;
			$data['password'] = md5($this->input->post('password'));
			$data['phone_number'] = $this->input->post('phone_number');
			/****Default curr*****/
			$data['currency'] = 'HKD';
			$data['language'] = $language;
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['updated_at'] = date('Y-m-d H:i:s');
			$email = filter_var($email, FILTER_SANITIZE_EMAIL); 
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
				/*$arr = array('txt'=>'IE','msg'=>'Invalid Email,Please user valid Email Id');
				echo json_encode($arr);*/
				 $this->session->set_flashdata('error', 'Invalid Email,Please user valid Email Id');
			}else{

				/******Sent mail befoe insert data******/
				//send email  to user
				$to = $this->input->post('email');
				$subject = "Welcome to Chefdiner";
				$from = 'chefdiner@gmail.com';

				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

				// Create email headers
				$headers .= 'From:'.$from."\r\n".
				'Reply-To: '.$from."\r\n" .
				'X-Mailer: PHP/' . phpversion();


				$username =$this->input->post('fname');
				$activationlink = base_url() . 'index.php/welcome/Confirm_Activation/'.base64_encode($this->input->post('email'));
				$message = 'Hello '.ucfirst($username).',Thank you for signing up. We will be using your email to send you information about bookings and other important events.';
				$message .='<button><a href="'.$activationlink.'">Click Here</a></button>';

				$res = $this->welcome_model->userRegister($data);
				
				if(is_string($res) && $res == 'NR'){
					 $this->session->set_flashdata('error', 'Something went wrong please try again.');
					/*$arr = array('txt'=>'NR','msg'=>'Something went wrong please try again.');
					echo json_encode($arr);*/
				}elseif(is_string($res) && $res == 'AE'){
					 $this->session->set_flashdata('error', 'Email id already exist,please try with another email.');
					/*$arr = array('txt'=>'AE','msg'=>'Email id already exist,please try with another email.');
					echo json_encode($arr);*/
				}else{
				$mail = mail($to, $subject, $message, $headers);
				if($mail){
					/******Add referal amount********/
				 
						$this->session->set_flashdata('success', 'Activation  link sent to your email id.Please Verify activation link.');
					
					
				/*$arr = array('txt'=>'RS','msg'=>'Activation  link sent to your email id.Please Verify activation link.');
				echo json_encode($arr);*/
				} else{
					$this->session->set_flashdata('error', 'Activation link sent to your email id.Please Verify activation link.');
				 /*  $arr = array('txt'=>'MN','msg'=>'Please use valid email id for registration , mail not sent.');
									echo json_encode($arr);*/
				}
					
				}



				
			}
				redirect('welcome/SignUpR/'.$code,'refresh');

		}

		/********Convert currency code*******/

function convertCurrency($amount, $from, $to) {
     $url = 'http://finance.google.com/finance/converter?a=' . $amount . '&from=' . $from . '&to=' . $to;
     $data = file_get_contents($url);
         preg_match_all("/<span class=bld>(.*)<\/span>/", $data, $converted);
         $final = preg_replace("/[^0-9.]/", "", $converted[1][0]);
        return round($final, 3);
    }

    /*******kitchen details saved*********/

    function saveKitchen(){

    	if (!$this->session->userdata('user_id')) {
    		$this->load->view('front/home');
        }else{
        	// echo "<pre>";
        	// print_r($_POST);die;
        			$chefid = $this->input->post('chefid');
        			$language = $this->session->userdata('site_lang');
        			if($language  == ''){
						$language = 'english';
					}
			    	$serveType1 = $this->input->post('serveType');
                    if(!empty($serveType1)){
                        $serveType = implode(",", $serveType1);
                    }else{
                        $serveType = '';
                    }
			    	


        			$data['kitchen_title'] = $this->input->post('kitchen_title');
		    		$data['service_type'] = $serveType;
		    		$data['currency'] = $this->input->post('NewBuyer_country');
		    		$data['from_guest'] = $this->input->post('from_guest');
		    		$data['upto_guest'] = $this->input->post('upto_guest');
		    		$data['brunch'] = $this->input->post('brunch');
		    		$data['lunch'] = $this->input->post('lunch');
		    		$data['dinner'] = $this->input->post('dinner');
		    		$data['kitchen_descrition'] = $this->input->post('kitchen_descrition');
		    		$data['language'] = $language;
/*echo "<pre>";
		    		print_r($data);die;*/

        	$updateKitchen = $this->welcome_model->updateKitchen($chefid,$data);
        	if(!empty($updateKitchen)){
		    			$this->session->set_flashdata('success', 'You have save your kitchen details');
		    			redirect('welcome/myprofile','refresh');
		    		}else{
		    			$this->session->set_flashdata('error', 'Something went wrong please try again.');
		    			redirect('welcome/mykitchen','refresh');
		    		}
			
        }

    }

    function myprofile(){
    	if (!$this->session->userdata('user_id')) {
    		$this->load->view('front/home');
        }else{
        	$is_chef = $this->session->userdata('is_chef');
        	$chefid = $this->session->userdata('user_id');
        	$data['getchefdata'] = $this->welcome_model->getchefdata($chefid);

        	// echo '<pre>';
        	// print_r($data['getchefdata']);die;
     
			$this->load->view('front/myprofile',$data);
        }

    }

    function saveChefprofile(){

    	if (!$this->session->userdata('user_id')) {
    		$this->load->view('front/home');
        }else{
        	// echo "<pre>";
        	// print_r($_FILES);
        	// print_r($_POST);

        			$chefid = $this->input->post('chefid');
        			$language = $this->session->userdata('site_lang');
        			if($language  == ''){
						$language = 'english';
					}
			    	$language_speak1 = $this->input->post('language_speak');
			    	$language_speak = implode(",", $language_speak1);

        			$data['chef_fname'] = $this->input->post('chef_fname');
		    		$data['language_speak'] = $language_speak;
		    		$data['chef_lname'] = $this->input->post('chef_lname');
		    		$data['chef_bio'] = $this->input->post('chef_bio');
		    		$data['language'] = $this->input->post('prfrSelecter');
		    		$data['other_language'] = $this->input->post('other_language');

		    		if (!empty($_FILES['fileToUpload']['name'])) {
		    		//	echo "string";
			            $config['upload_path']   = 'assets/chefprofile/';
			            $config['allowed_types'] = 'jpg|jpeg|png|gif';
			            $config['file_name']     = $_FILES['fileToUpload']['name'];
			            
			            //Load upload library and initialize configuration
			            $this->load->library('upload', $config);
			            $this->upload->initialize($config);
			            
			            if ($this->upload->do_upload('fileToUpload')) {
			                $uploadData = $this->upload->data();
			                $picture    = $uploadData['file_name'];
			            }
			            
			        }else{
			        	$getchefdata = $this->welcome_model->getchefdata($chefid);
			        	//echo 'fg';
			        	$picture = $getchefdata[0]['profile_pic'];
			        }

			        $data['profile_pic'] = $picture;
		    		
// echo "<pre>";

// 		    		print_r($data);die;

        	$updateProfile = $this->welcome_model->updateProfile($chefid,$data);
        	if(!empty($updateProfile)){
		    			$this->session->set_flashdata('success', 'Your Profile details updated Successfully.');
		    			redirect('welcome/myaddress','refresh');
		    		}else{
		    			$this->session->set_flashdata('error', 'Something went wrong please try again.');
		    			redirect('welcome/myprofile','refresh');
		    		}
			
        }
        

    }

    function myaddress(){
    	if (!$this->session->userdata('user_id')) {
    		$this->load->view('front/home');
        }else{
        	$is_chef = $this->session->userdata('is_chef');
        	$chefid = $this->session->userdata('user_id');
        	$data['getchefdata'] = $this->welcome_model->getchefdata($chefid);
        	$data['country'] = $this->welcome_model->getCountry();

        	// echo '<pre>';
        	// print_r($data['getchefdata']);die;
     
			$this->load->view('front/myaddress',$data);
        }

    }

    function selectCount(){
    	$country_id = $this->input->post('country_id');
    	$chefid = $this->session->userdata('user_id');
    	$getchefdata = $this->welcome_model->getchefdata($chefid);
    	//echo $getchefdata[0]['chef_state'];
    	if(!empty($country_id)){
    		$getstate = $this->welcome_model->getState($country_id);
    		if(!empty($getstate)){
    			foreach ($getstate as $key => $value) { 
    			
    				if($value['name'] == $getchefdata[0]['chef_state']){
    					$selected= 'selected';
    				}else{
    					$selected= '';

    				}

    				echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['name'].'</option>';
    			}

    		}else{
    			echo '<option value="">State not available</option>';
    		}

    	}else{
    			echo '<option value="">State not available</option>';
    		}
    }

     function selectState(){
    	$state_id = $this->input->post('state_id');
    	$chefid = $this->session->userdata('user_id');
    	$getchefdata = $this->welcome_model->getchefdata($chefid);
    	if(!empty($state_id)){
    		$getcity = $this->welcome_model->getCity($state_id);
    		
    		if(!empty($getcity)){
    			foreach ($getcity as $key => $value) { 

    				if($value['name'] == $getchefdata[0]['chef_city']){
    					$selected= 'selected';
    				}else{
    					$selected= '';

    				}

    				echo '<option value="'.$value['name'].'" '.$selected.'>'.$value['name'].'</option>';
    			}

    		}else{
    			echo '<option value="">City not available</option>';
    		}

    	}else{
    		$getcity = $this->welcome_model->getCitywithout();
    		
    		if(!empty($getcity)){
    			foreach ($getcity as $key => $value) { 

    				if($value['name'] == $getchefdata[0]['chef_city']){
    					$selected= 'selected';
    				}else{
    					$selected= '';

    				}

    				echo '<option value="'.$value['name'].'" '.$selected.'>'.$value['name'].'</option>';
    			}

    		}else{
    			echo '<option value="">City not available</option>';
    		}
    		}
    }

   

     function saveMyadd(){

    	if (!$this->session->userdata('user_id')) {
    		$this->load->view('front/home');
        }else{
        	/*echo "<pre>";
        	print_r($_POST);*/
        		$chefid = $this->input->post('chefid');
	        	$address =  $this->input->post('chef_address');
	        	$formattedAddr = str_replace(' ','+',$address);
        		$geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false'); 
		        $output = json_decode($geocodeFromAddr);
		        //Get latitude and longitute from json data
		        if(!empty($output)){
		        $latitude  = $output->results[0]->geometry->location->lat; 
		        $longitude = $output->results[0]->geometry->location->lng;
		      	}else{
		      		$latitude = 0;
		      		$longitude = 0;
		      	}


		      	$data['chef_country'] = $this->input->post('countryname');
	    		$data['chef_state'] =$this->input->post('statesname');
	    		$data['chef_city'] = $this->input->post('city');
	    		$data['phone_number'] = $this->input->post('phonefull');
	    		$data['chef_address'] = $this->input->post('chef_address');
	    		$data['add_direction'] = $this->input->post('add_direction');
	    		$data['house_rules'] = $this->input->post('house_rules');
	    		$data['chef_latitude'] = $latitude;
	    		$data['chef_longitude'] = $longitude;

	    		$updateMyaddress = $this->welcome_model->updateMyaddress($chefid,$data);

	    		    if(!empty($updateMyaddress)){
		    			$this->session->set_flashdata('success', 'Your Location details updated Successfully.');
		    			redirect('welcome/myphotos','refresh');
		    		}else{
		    			$this->session->set_flashdata('error', 'Something went wrong please try again.');
		    			redirect('welcome/myaddress','refresh');
		    		}
 
        }

    }

 function myphotos(){

 	if (!$this->session->userdata('user_id')) {
    		$this->load->view('front/home');
        }else{
        	
        	$chefid = $this->session->userdata('user_id');
        	$data['getchefdata'] = $this->welcome_model->getchefdata($chefid);

        	// echo '<pre>';
        	// print_r($data['getchefdata']);die;
     
			$this->load->view('front/myphotos',$data);
        }

 }

 function saveMyphoto(){
 	if (!$this->session->userdata('user_id')) {
    		$this->load->view('front/home');
        }else{
        	/*echo "<pre>";
       	print_r($_FILES);
       print_r($_FILES['fileToupload']['name']);
       	die;*/
        	$chefid = $this->input->post('chefid');
       if(!empty($_FILES['fileToupload']['name']) && $_FILES['fileToupload']['error'][0] !='4'){
            $filesCount = count($_FILES['fileToupload']['name']);
            $j=1;
            for($i = 0; $i < $filesCount; $i++){
            	//echo "string";
                $_FILES['userFile']['name'] = $_FILES['fileToupload']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['fileToupload']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['fileToupload']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['fileToupload']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['fileToupload']['size'][$i];

                $uploadPath = 'assets/kitchengallery/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i]['modified'] = date("Y-m-d H:i:s");

                    $data['image'.$j] = $uploadData[$i]['file_name'];
                    $j++;
                    if($i==19) break;
                }
            }
        }else{
        	$getchefdata = $this->welcome_model->getchefdata($chefid);
        	$data['image1'] = $getchefdata[0]['image1'];
        	$data['image2'] = $getchefdata[0]['image2'];
        	$data['image3'] = $getchefdata[0]['image3'];
        	$data['image4'] = $getchefdata[0]['image4'];
        	$data['image5'] = $getchefdata[0]['image5'];
        	$data['image6'] = $getchefdata[0]['image6'];
        	$data['image7'] = $getchefdata[0]['image7'];
        	$data['image8'] = $getchefdata[0]['image8'];
        	$data['image9'] = $getchefdata[0]['image9'];
        	$data['image10'] = $getchefdata[0]['image10'];
        	$data['image11'] = $getchefdata[0]['image11'];
        	$data['image12'] = $getchefdata[0]['image12'];
        	$data['image13'] = $getchefdata[0]['image13'];
        	$data['image14'] = $getchefdata[0]['image14'];
        	$data['image15'] = $getchefdata[0]['image15'];
        	$data['image16'] = $getchefdata[0]['image16'];
        	$data['image17'] = $getchefdata[0]['image17'];
        	$data['image18'] = $getchefdata[0]['image18'];
        	$data['image19'] = $getchefdata[0]['image19'];
        	$data['image20'] = $getchefdata[0]['image20'];


        }
        // /echo count($uploadData);
       // print_r($data);die;
        $updateMyphoto = $this->welcome_model->updateMyphoto($chefid,$data);

        			if(!empty($updateMyphoto)){
		    			$this->session->set_flashdata('success', 'Your Kitchen Images updated Successfully.');
		    			redirect('welcome/mymenus','refresh');
		    		}else{
		    			$this->session->set_flashdata('error', 'Something went wrong please try again.');
		    			redirect('welcome/myphotos','refresh');
		    		}
   }
 }

 function mymenus(){
 	if (!$this->session->userdata('user_id')) {
    		$this->load->view('front/home');
        }else{
        	
        	$chefid = $this->session->userdata('user_id');
        	$data['getchefdata'] = $this->welcome_model->getchefdata($chefid);
        	$data['getmenus'] =$this->welcome_model->getmenusforchef($chefid);
        	// echo '<pre>';
        	// print_r($data['getmenus']);die;
			$this->load->view('front/mymenus',$data);
        }
 }

 function chefdetail($id=""){
 	    if (!$this->session->userdata('user_id')) {
            $to_cur ='HKD';
            $getchefprofiledata = $this->welcome_model->getchefprofiledata($id);
            $getfavechef = $this->welcome_model->getfavechef($id);
                if (!empty($getfavechef)) {
                $favcount = count($getfavechef);
                }else{
                    $favcount = 0;
                }

            $getminprice = $this->welcome_model->getminprice($id);
            
                if (!empty($getminprice)) {
                    $minprice = $getminprice[0]->price_per_person;
                
                }else{
                    $minprice = 0 ;
                }
        /*****get max price*****/
            $getmaxpricechef = $this->welcome_model->getmaxpricechef($id);
                if (!empty($getmaxpricechef)) {
                    $maxprice = $getmaxpricechef[0]->price_per_person;
                } else {
                    $maxprice = 0 ;
                }

         /******Get rating********/
            $getchefrating = $this->welcome_model->getRating($id);

                if (!empty($getchefrating)) {
                    $countrat = count($getchefrating);
                } else {
                   $countrat = 0;
                }

            if(!empty($getchefprofiledata)) {
            foreach ($getchefprofiledata as $key => $value) {
               
                    $getchefprofiledata[$key]->favouritecount = $favcount;
                
                /*****get min price*******/
              
                    $getchefprofiledata[$key]->price = $minprice;
               
                /******gte max price*******/

                    $getchefprofiledata[$key]->maxprice = $maxprice;
               
                /********Count total rating******/
                    $getchefprofiledata[$key]->totalreview = $countrat;
              
               }
            }

           // print_r($getchefprofiledata);die;

        /*******get menu sub menus******/
        $getmenus =$this->welcome_model->getmenus($id);
        // echo '<pre>';
        // print_r($getmenus);die;
        if(!empty($getmenus)){
             foreach ($getmenus as $key => $value) {
           $menuid          = $value['id'];
           $MenusSubmenus[] = $this->welcome_model->MenusSubmenus($id, $menuid,$to_cur);
        } if (!empty($MenusSubmenus)) {
            foreach ($MenusSubmenus as $key => $value) {
                
                if (!empty($value)) {
                    $MenusSubmenus1[] = $value;
                }
                // print_r($MenusSubmenus1);
            }
        } else {
            $MenusSubmenus1 = '';
        }
        }else {
            $MenusSubmenus1 = '';
        }
       
       
        
        /********get rating average*********/

        $getReviewRating = $this->welcome_model->getReviewRating($id);
        //print_r($getReviewRating);die;
        if (!empty($getReviewRating)) {
            $n = count($getReviewRating);
        } else {
            $n = 1;
        }
        
        
        $maxtaste = $maxenvironment = $maxservice_quality = $maxcleanliness = $maxprice = $maxavr_rating = $totalreview = 0;
        if (!empty($getReviewRating)) {
            
            foreach ($getReviewRating as $key => $value) {
                
                $maxtaste           = $maxtaste + $value['taste'];
                $maxenvironment     = $maxenvironment + $value['environment'];
                $maxservice_quality = $maxservice_quality + $value['service_quality'];
                $maxcleanliness     = $maxcleanliness + $value['cleanliness'];
                $maxprice           = $maxprice + $value['price'];
                $maxavr_rating      = $maxavr_rating + $value['avr_rating'];
                if (!empty($value['review'])) {
                    $totalreview += count($value['review']);
                }
                
            }
            
        }
        
        $maxtaste           = $maxtaste / $n;
        $maxenvironment     = $maxenvironment / $n;
        $maxservice_quality = $maxservice_quality / $n;
        $maxcleanliness     = $maxcleanliness / $n;
        $maxprice           = $maxprice / $n;
        $maxavr_rating      = $maxavr_rating / $n;
        
        //echo 'Average maxtaste: ', $maxtaste / $n, "\n";
        $taste           = number_format($maxtaste, 1);
        $environment     = number_format($maxenvironment, 1);
        $service_quality = number_format($maxservice_quality, 1);
        $cleanliness     = number_format($maxcleanliness, 1);
        $price           = number_format($maxprice, 1);
        $avr_rating      = number_format($maxavr_rating, 1);
      
        
        $ratingarr = array(
            'chef_id' => $id,
            'avrTaste' => $taste,
            'avr_environment' => $environment,
            'avr_service_quality' => $service_quality,
            'avr_cleanliness' => $cleanliness,
            'avr_price' => $price,
            'avr_review' => $avr_rating,
            'totalreview' => $totalreview
        );

        /*******Review list******/
        $main_user_id   = '';
        
        $getRRlist = $this->welcome_model->getRRlist($id);
        
        $getLikeReview = $this->welcome_model->getLikeReview();

        if (!empty($getLikeReview)) {
            foreach ($getLikeReview as $key => $value) {
                $Reviewarr[] = $value->review_id;
                $Reviewarrq  = array_count_values($Reviewarr);
            }
        } else {
            $Reviewarrq = '';
           
        }


$getLikeYES = $this->welcome_model->getLikeYES($main_user_id);


if (!empty($getLikeYES)) {
            foreach ($getLikeYES as $key => $value) {
                $reviewuser[$value->review_id] = $value->main_user_id;
               // $reviewuser[] = array('main_user_id'=>$value->main_user_id , 'review_id'=>$value->review_id);
                
            }
        } else {
            $reviewuser = '';
           
     }   
   //  print_r($reviewuser);die;

         if (!empty($getRRlist)) {
            foreach ($getRRlist as $key => $value) {
                if ($Reviewarrq != '') {
                    if (array_key_exists($value->rr_id, $Reviewarrq)) {
                        $getRRlist[$key]->likecount = $Reviewarrq[$value->rr_id];
                    } else {
                        $getRRlist[$key]->likecount = 0;
                    }
                } else {
                    $getRRlist[$key]->likecount = 0;
                }

                if ($reviewuser != '') {
                    if (array_key_exists($value->rr_id, $reviewuser)) {
                       // echo 'fg';die;
                         $getRRlist[$key]->likeKey = 'yes';
                    } else {
                       // echo 'fgdg';die;
                        $getRRlist[$key]->likeKey = 'no';
                    }
                } else {
                     $getRRlist[$key]->likeKey = 'no';
                }

                
               
        }
    }else{
            $getRRlist='';
        }
        if(!empty($getchefprofiledata)){
            foreach ($getchefprofiledata as $key => $value) {

               $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                
             
                
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
                }
                $value->currency = $to_cur;
                // $value->price =  round($convertedPrice);
                // $value->maxprice =  round($convertedmaxPrice);
                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice =  round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);
            }

        
}
if(!empty($MenusSubmenus1)){
            foreach ($MenusSubmenus1 as $key => $value) {
                foreach ($value as $key => $value1) {
                   

               $fromChefcurr = $value1->menucurrency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value1->menu_price)){
                    $price = $value1->menu_price;
                }else{
                $price = 0;
                }
                 
                
             
                
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $price;
                }
                $value1->menucurrency = $to_cur;
                // $value->price =  round($convertedPrice);
                // $value->maxprice =  round($convertedmaxPrice);
                $value1->menu_price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
            }
}
        
}


            $data['ratingarr'] = $ratingarr;
            $data['getRRlist'] = $getRRlist;
            $data['getchefdata'] = $getchefprofiledata;
            $data['getmenus'] = $MenusSubmenus1;
            $data['wishlistChefcount'] = 0;
            $data['chef'] = $id;
//             echo '<pre>';
// print_r($data['ratingarr']);die;
            $this->load->view('front/chefdetail',$data);
        }else{
            if($id ==''){
                $id = $this->session->userdata('user_id');
            }
            $user = $this->session->userdata('user_id');
            $wishlistChef = $this->welcome_model->wishlistChef($user,$id);
            if(!empty($wishlistChef)){
                $wishlistChefcount = 1;

            }else{
                $wishlistChefcount = 0;
            }
        	 $getLoggedinusercurrency  = $this->welcome_model->getLoggedinusercurrency($id);
            if(!empty($getLoggedinusercurrency)){
            $to_cur = $getLoggedinusercurrency[0]->currency;
            }else{
                $to_cur ='HKD';
            }
        	$getchefprofiledata = $this->welcome_model->getchefprofiledata($id);
            $getfavechef = $this->welcome_model->getfavechef($id);
            if (!empty($getfavechef)) {
            $favcount = count($getfavechef);
            }else{
                $favcount = 0;
            }

            $getminprice = $this->welcome_model->getminprice($id);
            $getmaxpricechef = $this->welcome_model->getmaxpricechef($id);
            if (!empty($getminprice)) {
                $minprice = $getminprice[0]->price_per_person;
            
            }else{
                $minprice = 0 ;
            }
        /*****get max price*****/
            if (!empty($getmaxpricechef)) {
                $maxprice = $getmaxpricechef[0]->price_per_person;
            } else {
                $maxprice = 0 ;
            }

         /******Get rating********/
            $getchefrating = $this->welcome_model->getRating($id);
            if (!empty($getchefrating)) {
                $countrat = count($getchefrating);
            } else {
                
                $countrat = 0;
            }

            if (!empty($getchefprofiledata)) {
            foreach ($getchefprofiledata as $key => $value) {
               
                    $getchefprofiledata[$key]->favouritecount = $favcount;
                
                /*****get min price*******/
              
                    $getchefprofiledata[$key]->price = $minprice;
               
                /******gte max price*******/

                    $getchefprofiledata[$key]->maxprice = $maxprice;
               
                /********Count total rating******/
                    $getchefprofiledata[$key]->totalreview = $countrat;
              
               
            }
        }
        // echo '<pre>';
        // print_r($getchefprofiledata);die;
         $getmenus =$this->welcome_model->getmenus($id);
        // echo '<pre>';
        // print_r($getmenus);die;
         if(!empty($getmenus)){
            foreach ($getmenus as $key => $value) {
           $menuid          = $value['id'];
           $MenusSubmenus[] = $this->welcome_model->MenusSubmenus($id, $menuid,$to_cur);
        }
         }else{
            $MenusSubmenus= '';
         }
        
        if (!empty($MenusSubmenus)) {
            foreach ($MenusSubmenus as $key => $value) {
                
                if (!empty($value)) {
                    $MenusSubmenus1[] = $value;
                }
                // print_r($MenusSubmenus1);
            }
        } else {
            $MenusSubmenus1 = '';
        }

        
            /********get rating average*********/

        $getReviewRating = $this->welcome_model->getReviewRating($id);
        //print_r($getReviewRating);die;
        if (!empty($getReviewRating)) {
            $n = count($getReviewRating);
        } else {
            $n = 1;
        }
        
        
        $maxtaste = $maxenvironment = $maxservice_quality = $maxcleanliness = $maxprice = $maxavr_rating = $totalreview = 0;
        if (!empty($getReviewRating)) {
            
            foreach ($getReviewRating as $key => $value) {
                
                $maxtaste           = $maxtaste + $value['taste'];
                $maxenvironment     = $maxenvironment + $value['environment'];
                $maxservice_quality = $maxservice_quality + $value['service_quality'];
                $maxcleanliness     = $maxcleanliness + $value['cleanliness'];
                $maxprice           = $maxprice + $value['price'];
                $maxavr_rating      = $maxavr_rating + $value['avr_rating'];
                if (!empty($value['review'])) {
                    $totalreview += count($value['review']);
                }
                
            }
            
        }
        
        $maxtaste           = $maxtaste / $n;
        $maxenvironment     = $maxenvironment / $n;
        $maxservice_quality = $maxservice_quality / $n;
        $maxcleanliness     = $maxcleanliness / $n;
        $maxprice           = $maxprice / $n;
        $maxavr_rating      = $maxavr_rating / $n;
        
        //echo 'Average maxtaste: ', $maxtaste / $n, "\n";
        $taste           = number_format($maxtaste, 1);
        $environment     = number_format($maxenvironment, 1);
        $service_quality = number_format($maxservice_quality, 1);
        $cleanliness     = number_format($maxcleanliness, 1);
        $price           = number_format($maxprice, 1);
        $avr_rating      = number_format($maxavr_rating, 1);
      
        
        $ratingarr = array(
            'chef_id' => $id,
            'avrTaste' => $taste,
            'avr_environment' => $environment,
            'avr_service_quality' => $service_quality,
            'avr_cleanliness' => $cleanliness,
            'avr_price' => $price,
            'avr_review' => $avr_rating,
            'totalreview' => $totalreview
        );
        /*******Review list******/
        $main_user_id   = $this->session->userdata('user_id');
        
        $getRRlist = $this->welcome_model->getRRlist($id);
        
        $getLikeReview = $this->welcome_model->getLikeReview();

        if (!empty($getLikeReview)) {
            foreach ($getLikeReview as $key => $value) {
                $Reviewarr[] = $value->review_id;
                $Reviewarrq  = array_count_values($Reviewarr);
            }
        } else {
            $Reviewarrq = '';
           
        }


$getLikeYES = $this->welcome_model->getLikeYES($main_user_id);


if (!empty($getLikeYES)) {
            foreach ($getLikeYES as $key => $value) {
                $reviewuser[$value->review_id] = $value->main_user_id;
               // $reviewuser[] = array('main_user_id'=>$value->main_user_id , 'review_id'=>$value->review_id);
                
            }
        } else {
            $reviewuser = '';
           
     }   
   //  print_r($reviewuser);die;

         if (!empty($getRRlist)) {
            foreach ($getRRlist as $key => $value) {
                if ($Reviewarrq != '') {
                    if (array_key_exists($value->rr_id, $Reviewarrq)) {
                        $getRRlist[$key]->likecount = $Reviewarrq[$value->rr_id];
                    } else {
                        $getRRlist[$key]->likecount = 0;
                    }
                } else {
                    $getRRlist[$key]->likecount = 0;
                }

                if ($reviewuser != '') {
                    if (array_key_exists($value->rr_id, $reviewuser)) {
                       // echo 'fg';die;
                         $getRRlist[$key]->likeKey = 'yes';
                    } else {
                       // echo 'fgdg';die;
                        $getRRlist[$key]->likeKey = 'no';
                    }
                } else {
                     $getRRlist[$key]->likeKey = 'no';
                }

                
               
        }
    }else{
            $getRRlist='';
        }

        if(!empty($getchefprofiledata)){

            foreach ($getchefprofiledata as $key => $value) {

                $fromChefcurr = $value->currency;
               if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                // echo 'dffgvfgdfg'.$price;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
             //  echo $to_cur .'==df='. $fromChefcurr .'===='.$price;
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                  //  echo 'if';
               // echo $fromChefcurr .'if'.$to_cur;
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
               // echo 'else';
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
               // echo $convertedPrice;
                }
                $value->currency = $to_cur;
                // $value->price =  round($convertedPrice);
                // $value->maxprice =  round($convertedmaxPrice);

                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice = round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);
            }

        
}

if(!empty($MenusSubmenus1)){

            foreach ($MenusSubmenus1 as $key => $value) {
                foreach ($value as $key => $value1) {
                 

               $fromChefcurr = $value1->menucurrency;
               if(!empty($value1->menu_price)){
                    $price = $value1->menu_price;
                }else{
                $price = 0;
                }
                
                // echo 'dffgvfgdfg'.$price;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
             //  echo $to_cur .'==df='. $fromChefcurr .'===='.$price;
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                  //  echo 'if';
               // echo $fromChefcurr .'if'.$to_cur;
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                
                }else{
               // echo 'else';
                $convertedPrice = $price;
               
                }
                $value1->menucurrency = $to_cur;
                // $value->price =  round($convertedPrice);
                // $value->maxprice =  round($convertedmaxPrice);

                $value1->menu_price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                
            }

        }
        
}


            $data['ratingarr'] = $ratingarr;
            $data['getRRlist'] = $getRRlist;
            $data['getmenus'] = $MenusSubmenus1;
            $data['getchefdata'] = $getchefprofiledata;
            $data['wishlistChefcount'] = $wishlistChefcount;
            $data['chef'] = $id;
        	// echo '<pre>';
        	// print_r($data['getchefdata']);die;
			$this->load->view('front/chefdetail',$data);
        }
 }

 function getsubmenus(){

    $menuid = $this->input->post('menuid');
    $html='';
    // echo $menuid;
    $getsubmenu = $this->welcome_model->getsubmenu($menuid);
   // print_r($getsubmenu);
    if(!empty($getsubmenu)){
        $html .='<div class="menuItemBlock onlyDish">
        <input type="hidden" name="menu_id" id="menu_id" value='.$menuid.'>';
        foreach ($getsubmenu as $key => $value) {
            if(!empty($value->dish_image)){
                $dish_image = $value->dish_image;
            }else{
                $dish_image = base_url().'front/images/blank.jpg';
            }
            if($value->status != 1){
                $class = 'draftmenu';
                $text = 'Publish';
            }else{
                $class='';
                $text = 'Draft';
            }
          $html .='<div class="itmeDetail">
                                    <h4 class="clr-black ItmName">'. ucfirst($value->dish_name).'</h4>
                                    <p class="itmPrc clr-gray">'. ucfirst($value->dish_category).'</p>
                                </div>  
                                <div class="itemIng">
                                    <div class="img">
                                        <img src='.$dish_image.' alt="">
                                    </div>
                                </div>
                                <div class="menuItemOpt">
                                    <ul>
                                        <li class="up"><a href="javascript:;">Up</a></li>
                                        <li class="down"><a href="javascript:;">Down</a></li>
                                        <li><a href="javascript:;"  class="'.$class.'"
                                        onclick="draftSubmenu('.$value->id.','.$value->status.');">'.$text.'</a></li>
                                        <li><a href="javascript:;" onclick="editSubmenu('.$value->id.');">Edit</a></li>
                                        <li><a href="javascript:;" onclick="deleteSubmenu('.$value->id.');">Delete</a></li>
                                    </ul>
                                </div>';
        }
        $html .='</div>';
    }else{
        $html .='<div class="menuItemBlock onlyDish">
        <input type="hidden" name="menu_id" id="menu_id" value='.$menuid.'>';

        $html .='</div>';
    }

    echo $html;
 }

function addMenu(){

  //    print_r($_POST);
  // die;
    $menuid = $this->input->post('menuid');
    $menuimage = $this->input->post('menuimage');
    if($menuimage == ''){
        $menuimage = $this->input->post('menuimg');
    }
    $price_per_person = $this->input->post('personprice');
    $data['menu_title'] = $this->input->post('menutitle');
    $data['service_type'] = $this->input->post('serType');
    $data['actualprice'] = $this->input->post('personprice');
    $getadmincharge = $this->welcome_model->getadmincharge();
    $charge = $getadmincharge[0]->per_amt;
    $percent = $charge / 100;
    $percentamt = $price_per_person * $percent;
    $totalamt = $price_per_person + $percentamt;
    // /echo $totalamt;die;
    $data['price_per_person'] = round($totalamt, 2, PHP_ROUND_HALF_UP);
    $data['currency'] = $this->input->post('select_nenu_currency');
    $data['cusine'] = $this->input->post('cuisine');
    $data['subcusine'] = $this->input->post('subcuisnes');
    $data['kitchen_tool'] = $this->input->post('kitchenTool');
    $data['user_id'] = $this->input->post('chefid');
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['status'] = 1;
    $data['menu_image'] = $menuimage;
   

    
    
    $addMenu = $this->welcome_model->addMenu($data,$menuid);
    if($addMenu){
        echo '1';
    }else{
        echo '0';
    }

}

function uploadImage(){
    // echo $_FILES['file']['name'];
    // print_r($_FILES);
    if (!empty($_FILES['file']['name'])) {
            $config['upload_path']   = 'assets/menuImgae/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['file_name']     = $_FILES['file']['name'];
            
            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if ($this->upload->do_upload('file')) {
                $uploadData = $this->upload->data();
                $picture    = $uploadData['file_name'];
            }else{
                $$picture = '';
            }
           
        } 
        echo $picture;

}

function uploadishImage(){
    if (!empty($_FILES['file']['name'])) {
            $config['upload_path']   = 'assets/submenuImage/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['file_name']     = $_FILES['file']['name'];
            
            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if ($this->upload->do_upload('file')) {
                $uploadData = $this->upload->data();
                $picture    = $uploadData['file_name'];
            }else{
                $$picture = '';
            }
           
        } 
        echo $picture;
}

function addsubMenu(){
  //  print_r($_POST);die;
    $menuid =  $this->input->post('menuids');
    /***getdetails of menu****/
    // $getmenudata = $this->welcome_model->getmenudata($menuid);
    $dishimage = $this->input->post('dishimage');
    if($dishimage == ''){
        $dishimage = $this->input->post('submenuimg');
    }
    $data['menu_id'] = $menuid;
    $data['user_id']       = $this->input->post('chefids');
    // $data['menu_title']    = $getmenudata[0]->menu_title;
    // $data['menu_price']    = $getmenudata[0]->price_per_person;
    // $data['currency']      = $getmenudata[0]->currency;
    $data['dish_name'] = $this->input->post('dishtitle');
    $data['dish_image'] = $dishimage;
    $data['dish_category'] = $this->input->post('mainCat1');
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['status'] = 1;
    $data['id'] = $this->input->post('submenuid');


    $addsubMenu = $this->welcome_model->addSubMenu($data);
    if($addsubMenu){
        echo '1';
    }else{
        echo '0';
    }
}

function draftMenu(){
    $menuid = $this->input->post('menuid');
    $draftstatus = $this->input->post('draftstatus');
    // echo $menuid . $draftstatus; 
    $data['status'] = $draftstatus;
    $updateDraft = $this->welcome_model->updateDraft($menuid,$data);
    if($updateDraft){
        echo '1';
    }else{
        echo '0';
    }
}

function draftSubmenu(){
    $submenuid = $this->input->post('submenuid');
    $draftsubstatus = $this->input->post('draftsubstatus');
    // echo $menuid . $draftstatus; 
    $data['status'] = $draftsubstatus;
    $updateSubDraft = $this->welcome_model->updateSubDraft($submenuid,$data);
    if($updateSubDraft){
        echo '1';
    }else{
        echo '0';
    }
}

function deleteMenu(){
    $menuid = $this->input->post('menuid');
    $deleteM = $this->welcome_model->deleteMenus($menuid);
    if(!empty($deleteM) && $deleteM == 1){
              echo '1';
    }else{
            echo '0';
    }
}

function deleteSubmenu(){
    $submenuid = $this->input->post('submenuid');
    $deleteSM = $this->welcome_model->deleteSubmenu($submenuid);
    if(!empty($deleteSM) && $deleteSM == 1){
              echo '1';
    }else{
            echo '0';
    }
}

function getsubmenuarray(){
    $submenuid = $this->input->post('submenuid');
    $getsubmenuarray = $this->welcome_model->getsubmenuarray($submenuid);
    if(!empty($getsubmenuarray)){
              //echo json_encode(array('getsubmenuarray'=>$getsubmenuarray));
        echo json_encode($getsubmenuarray[0]);
    }else{
            echo '0';
    }
}
function editChefmenus(){
    $menuid = $this->input->post('menuid');
    $getmenus =$this->welcome_model->getmenusforedit($menuid);
    
    if(!empty($getmenus)){
              //echo json_encode(array('getsubmenuarray'=>$getsubmenuarray));
        echo json_encode($getmenus[0]);
    }else{
            echo '0';
    }
    }

function editpofile(){
         if (!$this->session->userdata('user_id')) {
            $this->load->view('front/home');
        }else{
            
            $chefid = $this->session->userdata('user_id');
            $data['getchefdata'] = $this->welcome_model->getchefdata($chefid);
           
            // print_r($data['getmenus']);die;
            $this->load->view('front/editprofile',$data);
        }
}
 function EditChefprofile(){

        if (!$this->session->userdata('user_id')) {
            $this->load->view('front/home');
        }else{
            // echo "<pre>";
            // print_r($_FILES);
            // print_r($_POST);

                    $chefid = $this->input->post('chefid');
                    $language = $this->session->userdata('site_lang');
                    if($language  == ''){
                        $language = 'english';
                    }
                    $language_speak1 = $this->input->post('language_speak');
                    $language_speak = implode(",", $language_speak1);

                    $data['chef_fname'] = $this->input->post('chef_fname');
                    $data['language_speak'] = $language_speak;
                    $data['chef_lname'] = $this->input->post('chef_lname');
                    $data['chef_bio'] = $this->input->post('chef_bio');
                    $data['language'] = $this->input->post('prfrSelecter');
                    $data['other_language'] = $this->input->post('other_language');
                    $data['phone_number'] = $this->input->post('phonefull1');
                    if (!empty($_FILES['fileToUpload']['name'])) {
                     //  echo "string";die;
                        $config['upload_path']   = 'assets/chefprofile/';
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        $config['file_name']     = $_FILES['fileToUpload']['name'];
                        
                        //Load upload library and initialize configuration
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        
                        if ($this->upload->do_upload('fileToUpload')) {
                            $uploadData = $this->upload->data();
                            $picture    = $uploadData['file_name'];
                        }
                        
                    }else{
                        $getchefdata = $this->welcome_model->getchefdata($chefid);
                        //echo 'fg';
                        $picture = $getchefdata[0]['profile_pic'];
                    }

                    $data['profile_pic'] = $picture;
                    
// echo "<pre>";

//                  print_r($data);die;

            $updateProfile = $this->welcome_model->updateProfile($chefid,$data);
            if(!empty($updateProfile)){
                        $this->session->set_flashdata('success', 'Your Profile details updated Successfully.');
                        redirect('welcome/changepassword','refresh');
                    }else{
                        $this->session->set_flashdata('error', 'Something went wrong please try again.');
                        redirect('welcome/editpofile','refresh');
                    }
            
        }
        

    }

    function changepassword(){
        if (!$this->session->userdata('user_id')) {
            $this->load->view('front/home');
        }else{
            
            $chefid = $this->session->userdata('user_id');
            $data['getchefdata'] = $this->welcome_model->getchefdata($chefid);
           
            // print_r($data['getmenus']);die;
            $this->load->view('front/changepassword',$data);
        }
    }

    function socialnetwork(){
        if (!$this->session->userdata('user_id')) {
            $this->load->view('front/home');
        }else{
            
            $chefid = $this->session->userdata('user_id');
            $data['getchefdata'] = $this->welcome_model->getchefdata($chefid);
            $data['getpermission'] = $this->welcome_model->getpermission($chefid);
            // print_r($data['getmenus']);die;
            $this->load->view('front/socialnetwork',$data);
        }
    }

    function forgetPass(){
        if (!$this->session->userdata('user_id')) {
            $this->load->view('front/forgetpass');
        }else{
            $lang = $this->session->userdata('site_lang');
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('first_name');
            $this->session->unset_userdata('last_name');
            $this->session->unset_userdata('is_chef');
            $this->session->unset_userdata('is_verify_chef');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('referal_code');
            $this->session->set_userdata('site_lang', $lang);
       // $this->session->sess_destroy();
 //$this->session->set_userdata('site_lang', $lang);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
         
        
            $this->load->view('front/forgetpass');
        }
    }

    function checkPass(){
        $chefid = $this->input->post('chefid');
        $oldpassword =md5($this->input->post('oldpassword'));
        $chefpass  = $this->welcome_model->checkchefpass($chefid,$oldpassword);
        if(!empty($chefpass)){
            echo '1';

        }else{
            echo '0';
        }
    }

    function changePass(){
         if (!$this->session->userdata('user_id')) {
            $this->load->view('front/home');
        }else{
            // echo '<pre>';
            // print_r($_POST);
            $chefid = $this->input->post('chefid');
            $newpass = md5($this->input->post('newpassword'));
            $changepass = $this->welcome_model->changePass($chefid,$newpass);
            if(!$changepass){
                $this->session->set_flashdata('success', 'Your Password update Successfully.');
                redirect('welcome/changepassword','refresh');
            }else{
                $this->session->set_flashdata('error', 'Something went wrong please try again.');
                        redirect('welcome/changepassword','refresh');
            }
        }

    }

    function forget(){
        if(isset($_POST['submit'])){
            $email  = $this->input->post('email');
            $checkemail = $this->welcome_model->checkemail($email);
            if(!empty($checkemail)){
                                $to = $this->input->post('email');
                                $subject = "Forget Password Link";
                                $from = 'chefdiner@gmail.com';

                                // To send HTML mail, the Content-type header must be set
                                $headers  = 'MIME-Version: 1.0' . "\r\n";
                                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                                // Create email headers
                                $headers .= 'From:'.$from."\r\n".
                                'Reply-To: '.$from."\r\n" .
                                'X-Mailer: PHP/' . phpversion();
                               
                                $activationlink = base_url() . 'index.php/welcome/forget_Activation/'.base64_encode($this->input->post('email'));
                                
                                $message = 'Hi,'.$checkemail[0]->first_name;
                                $message .= 'Sorry to hear that your password has gone missing!';
                                $message .='Please click below button to reset your Chefdiner Password.';
                                $message .='<a href='.$activationlink.'>Reset Your Password</a>';
                                $message .= 'Once you reset your password you would able to sign in with new password to avail the chef diner services again.';
                                
                                
                                $mail=mail($to,$subject,$message ,$headers);
                            
                                $this->session->set_flashdata('success', 'Password reset link send to your email');
                                redirect('welcome/forgetPass','refresh');
                        }
                     else{
                        $this->session->set_flashdata('error', 'Your Email address is incorrect, Please enter correct Email address');
                        redirect('welcome/forgetPass','refresh');
                    }
            }
    }

    function forget_Activation(){
        $url = $this->uri->segment_array();
        $url_param = end($url);
        
        // and later:
        $base_64 = $url_param . str_repeat('=', strlen($url_param) % 4);
        $email = base64_decode($base_64);
        $checkemail = $this->welcome_model->checkemail($email);
        if(!empty($checkemail)){
            //echo $getforget[0]['email'];die;
            $data['email'] = $checkemail[0]->email;
            $data['userid'] = $checkemail[0]->id;
            $this->load->view('front/updatePass',$data);
        }
    }

    function updateforget(){
       // if(isset($_POST['submit'])){
            $id = $this->input->post('userid');
            $email = $this->input->post('email');
            $password = md5($this->input->post('newpass'));
            $isSaved = $this->welcome_model->UpdatePass($password,$id,$email);
            if($isSaved){
             //   print_r($isSaved);die;
                $data['email'] = $this->input->post('email');
                $data['password'] =  $this->input->post('newpass');
                       $res = $this->welcome_model->userLogin($data);
                      // print_r($res);die;
                       if(is_string($res) && $res == 'SL'){
                       // echo '1';die;
                         $this->session->set_flashdata('success', 'Password change Successfully.');
                                redirect(base_url(),'refresh');
                       
                        }else{
                            //echo '2';die;
                        $this->session->set_flashdata('error', 'Something went worng please try again');
                        redirect('welcome/forgetPass','refresh');

                        }

        } 
       
    }

     function instagramlogin(){
        if (!$this->session->userdata('user_id')) {
            $this->load->view('front/home');
        }else{

      $userid = $this->session->userdata('user_id');
        if(isset($_GET['code'])){
            //echo $_GET['code'];
            // echo "string".$_GET['token'];
                $uri = 'https://api.instagram.com/oauth/access_token'; 
                $instagram_parameter = array(
                    'client_id' => 'd385960b553c4b7aa0d3c508b5a6c710', 
                    'client_secret' => '6471e4bedaa7428cbff28327388bbc04', 
                    'grant_type' => 'authorization_code', 
                    'redirect_uri' => 'http://vervedemos.in/chefdiner/welcome/instagramlogin', 
                    'code' => $_GET['code']
                );
    // echo '<pre>'; // preformatted view
    // print_r($instagram_parameter);
    $curl = curl_init('https://api.instagram.com/oauth/access_token');
    // curl_setopt($ch, CURLOPT_URL, $uri); // uri
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // RETURN RESULT true
    curl_setopt($curl, CURLOPT_POSTFIELDS, $instagram_parameter); // POST DATA
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // VERIFY SSL PEER false
    $result = curl_exec($curl);
    // echo "<pre>";
    
   // curl_close($curl);
    $finalresult = json_decode($result); // execute curl
   //print_r($finalresult);

    $access_token = $finalresult->access_token;
        if(!empty($access_token)){
            $this->welcome_model->updateinstaToken($access_token,$userid);

        }
    
          $this->session->set_flashdata('success', 'Connected with instagram Successfully');
                redirect('welcome/socialnetwork','refresh');


        }else{
        $this->session->set_flashdata('error', 'Something Went wrong');
                redirect('welcome/socialnetwork','refresh');
    }

        }
    }
    function saveSetting(){
        if (!$this->session->userdata('user_id')) {
            $this->load->view('front/home');
        }else{
           
            $permissionid = $this->input->post('permissionid');
            $chefid = $this->session->userdata('user_id');
            $weeklyNewsletter = $this->input->post('weeklyNewsletter');
            $messageNotification = $this->input->post('messageNotification');
            $smsaboutnewmessages = $this->input->post('smsaboutnewmessages');
            $pushnotification = $this->input->post('pushnotification');
            $instaLogn = $this->input->post('instaLogn');
            if($weeklyNewsletter ==''){
                $weeklyNewsletter= 0;
            }else{
                $weeklyNewsletter = 1;
            }
            if($messageNotification ==''){
                $messageNotification= 0;
            }else{
                $messageNotification = 1;
            }
            if($smsaboutnewmessages ==''){
                $smsaboutnewmessages= 0;
            }else{
                $smsaboutnewmessages = 1;
            }
            if($pushnotification ==''){
                $pushnotification= 0;
            }else{
                $pushnotification = 1;
            }
            if($instaLogn ==''){
                $instaLogn= 0;
            }else{
                $instaLogn = 1;
            }

            $data['email_pro'] = $weeklyNewsletter;
            $data['email_msg'] = $messageNotification;
            $data['sms_msg'] = $smsaboutnewmessages;
            $data['mobile_msg'] = $pushnotification;
            $data['instagram_login'] = $instaLogn;

            // echo '<pre>';
            // print_r($data);die;
            $saveSetting = $this->welcome_model->saveSetting($chefid,$permissionid,$data);
         //   print_r($saveSetting);die;
            if(!empty($saveSetting)){
                $this->session->set_flashdata('success', 'Account permission saved Successfully.');
                redirect('welcome/socialnetwork','refresh');
            }else{
                $this->session->set_flashdata('error', 'Something went wrong please try again.');
                        redirect('welcome/socialnetwork','refresh');
            }


        }

    }

    function likeDislike(){
        if (!$this->session->userdata('user_id')) {
            $this->load->view('front/hostLoginreg');
        }else{
        //print_r($_POST);
        $data['main_user_id'] = $this->input->post('mainuserid');
        $data['like_rev'] = $this->input->post('like_rev');
        $data['review_id'] = $this->input->post('review_id');
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
         if ($data['like_rev'] == '1') { // like the chef
            //insert data
            $likeReview = $this->welcome_model->likeReview($data);
        } else { //delete records
            $likeReview = $this->welcome_model->deletelikeReview($data);
            
        }
        if (!empty($likeReview)) {
            echo '1';
        }else{
            echo '0';
        }
    }
    }

    function termscondition(){
       

        if($this->session->userdata('site_lang') !=''){
        $lan =$this->session->userdata('site_lang');
       
        }else{
        $lan ='english';
        }
      // /  echo $lan;
        $data['termscondition'] = $this->welcome_model->termscondition($lan);
            $this->load->view('front/termscondition',$data);

    }

    function privacypolicy(){
       
        if($this->session->userdata('site_lang') !=''){
        $lan =$this->session->userdata('site_lang');
       
        }else{
        $lan ='english';
        }
      // /  echo $lan;
        $data['privacypolicy'] = $this->welcome_model->privacypolicy($lan);
            $this->load->view('front/privacy',$data);
        

    }

    function saveWishlist(){
        
        //print_r($_POST);
        $user_id = $this->input->post('mainuserid');
        if($user_id == ''){
            echo '0';

        }else{
             $data['user_id'] = $user_id;
        $data['favourite'] = $this->input->post('favroit');
        $data['chef_id'] = $this->input->post('chef');
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        //print_r($data);die;
         if ($data['favourite'] == '1') { // like the chef
            //insert data
            $saveWishlist = $this->welcome_model->favouriteChef($data);
        } else { //delete records
            $saveWishlist = $this->welcome_model->deletefavouriteChef($data);
            
        }
        if (!empty($saveWishlist)) {
            echo '1';
        }else{
            echo '0';
        }
        }
       
    
    }

    function dailymeals()
    {
      //    echo $this->session->userdata('site_lang');die;
        if (!$this->session->userdata('user_id')) {
            $to_cur ='HKD';
            $dailymealchef = $this->welcome_model->dailymealchef();
         
            $getfave = $this->welcome_model->getfave();
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }


            if (!empty($dailymealchef)) {
            foreach ($dailymealchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $dailymealchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $dailymealchef[$key]->favouritecount = 0;
                    }
                } else {
                    $dailymealchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $dailymealchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $dailymealchef[$key]->price = 0;
                    }
                } else {
                    $dailymealchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $dailymealchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $dailymealchef[$key]->maxprice = 0;
                    }
                } else {
                    $dailymealchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $dailymealchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $dailymealchef[$key]->totalreview = 0;
                    }
                } else {
                    $dailymealchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $dailymealchef='';
        }

       
        /*if(!empty($dailymealchef)){

      //      echo 'if';
            foreach ($dailymealchef as $key => $value) {

                $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
              if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $value->price;
                $convertedmaxPrice= $value->maxprice;
                }
                $value->currency = $to_cur;
                $value->price =  round($convertedPrice);
                $value->maxprice =  round($convertedmaxPrice);
            }

        
}*/
 

            $data['dailymealchef'] =$dailymealchef;
          
            $this->load->view('front/dailymeal',$data);


        }else{
            /*$data['page']='clinicdash';
            $this->load->view('front/dashboard',$data);*/
            $chefid = $this->session->userdata('user_id');
            $getLoggedinusercurrency  = $this->welcome_model->getLoggedinusercurrency($chefid);
            if(!empty($getLoggedinusercurrency)){
            $to_cur = $getLoggedinusercurrency[0]->currency;
            }else{
                $to_cur ='HKD';
            }
            $dailymealchef = $this->welcome_model->dailymealchef($chefid);
           
            $getfave = $this->welcome_model->getfave();
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }


            if (!empty($dailymealchef)) {
            foreach ($dailymealchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $dailymealchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $dailymealchef[$key]->favouritecount = 0;
                    }
                } else {
                    $dailymealchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $dailymealchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $dailymealchef[$key]->price = 0;
                    }
                } else {
                    $dailymealchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $dailymealchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $dailymealchef[$key]->maxprice = 0;
                    }
                } else {
                    $dailymealchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $dailymealchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $dailymealchef[$key]->totalreview = 0;
                    }
                } else {
                    $dailymealchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $dailymealchef='';
        }

       
        /*if(!empty($dailymealchef)){

      //      echo 'if';
            foreach ($dailymealchef as $key => $value) {

                $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
              if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $value->price;
                $convertedmaxPrice= $value->maxprice;
                }
                $value->currency = $to_cur;
                $value->price =  round($convertedPrice);
                $value->maxprice =  round($convertedmaxPrice);
            }

        
}*/
     

       

            $data['dailymealchef'] =$dailymealchef;
         
            $this->load->view('front/dailymeal',$data);
        }
    }
    

     function newchef()
    {
     if (!$this->session->userdata('user_id')) {
            $to_cur ='HKD';
            $newchef = $this->welcome_model->newchef();
          //  print_r($newchef);die;
            $getfave = $this->welcome_model->getfave();
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }



        /*******new chef*******/
         if (!empty($newchef)) {
            foreach ($newchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $newchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $newchef[$key]->favouritecount = 0;
                    }
                } else {
                    $newchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $newchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $newchef[$key]->price = 0;
                    }
                } else {
                    $newchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $newchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $newchef[$key]->maxprice = 0;
                    }
                } else {
                    $newchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $newchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $newchef[$key]->totalreview = 0;
                    }
                } else {
                    $newchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $newchef='';
        }
        /*if(!empty($dailymealchef)){

      //      echo 'if';
            foreach ($dailymealchef as $key => $value) {

                $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
              if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $value->price;
                $convertedmaxPrice= $value->maxprice;
                }
                $value->currency = $to_cur;
                $value->price =  round($convertedPrice);
                $value->maxprice =  round($convertedmaxPrice);
            }

        
}*/
            // echo '<pre>';
            // print_r($dailymealchef);die;

      
            $data['newchef'] =$newchef;
            

            $this->load->view('front/newchef',$data);


        }else{
            
            $chefid = $this->session->userdata('user_id');
            $getLoggedinusercurrency  = $this->welcome_model->getLoggedinusercurrency($chefid);
            if(!empty($getLoggedinusercurrency)){
            $to_cur = $getLoggedinusercurrency[0]->currency;
            }else{
                $to_cur ='HKD';
            }
            $newchef = $this->welcome_model->newchef($chefid);
          //  print_r($newchef);die;
            $getfave = $this->welcome_model->getfave();
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }



        /*******new chef*******/
         if (!empty($newchef)) {
            foreach ($newchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $newchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $newchef[$key]->favouritecount = 0;
                    }
                } else {
                    $newchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $newchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $newchef[$key]->price = 0;
                    }
                } else {
                    $newchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $newchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $newchef[$key]->maxprice = 0;
                    }
                } else {
                    $newchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $newchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $newchef[$key]->totalreview = 0;
                    }
                } else {
                    $newchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $newchef='';
        }
        /*if(!empty($dailymealchef)){

      //      echo 'if';
            foreach ($dailymealchef as $key => $value) {

                $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
              if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $value->price;
                $convertedmaxPrice= $value->maxprice;
                }
                $value->currency = $to_cur;
                $value->price =  round($convertedPrice);
                $value->maxprice =  round($convertedmaxPrice);
            }

        
}*/
      
     
            $data['newchef'] =$newchef;
            $this->load->view('front/newchef',$data);
        }
    }

    function ourpicks(){

        if (!$this->session->userdata('user_id')) {
            $to_cur ='HKD';
           
        
            $getfave = $this->welcome_model->getfave();
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }


 

        /***********Our picks chef start************/
        $ourpickchef = $this->welcome_model->ourpickchef();

        if (!empty($ourpickchef)) {
            foreach ($ourpickchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $ourpickchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $ourpickchef[$key]->favouritecount = 0;
                    }
                } else {
                    $ourpickchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $ourpickchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $ourpickchef[$key]->price = 0;
                    }
                } else {
                    $ourpickchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $ourpickchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $ourpickchef[$key]->maxprice = 0;
                    }
                } else {
                    $ourpickchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $ourpickchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $ourpickchef[$key]->totalreview = 0;
                    }
                } else {
                    $ourpickchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $ourpickchef='';
        }
           /*if(!empty($dailymealchef)){

      //      echo 'if';
            foreach ($dailymealchef as $key => $value) {

                $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
              if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $value->price;
                $convertedmaxPrice= $value->maxprice;
                }
                $value->currency = $to_cur;
                $value->price =  round($convertedPrice);
                $value->maxprice =  round($convertedmaxPrice);
            }

        
}*/
     
            $data['ourpickchef'] =$ourpickchef;

            $this->load->view('front/ourpicks',$data);


        }else{
           
            $chefid = $this->session->userdata('user_id');
            
            $getLoggedinusercurrency  = $this->welcome_model->getLoggedinusercurrency($chefid);
            if(!empty($getLoggedinusercurrency)){
            $to_cur = $getLoggedinusercurrency[0]->currency;
            }else{
                $to_cur ='HKD';
            }
           
            $getfave = $this->welcome_model->getfave();
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }


            /***********Our picks chef start************/
        $ourpickchef = $this->welcome_model->ourpickchef($chefid);

        if (!empty($ourpickchef)) {
            foreach ($ourpickchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $ourpickchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $ourpickchef[$key]->favouritecount = 0;
                    }
                } else {
                    $ourpickchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $ourpickchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $ourpickchef[$key]->price = 0;
                    }
                } else {
                    $ourpickchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $ourpickchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $ourpickchef[$key]->maxprice = 0;
                    }
                } else {
                    $ourpickchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $ourpickchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $ourpickchef[$key]->totalreview = 0;
                    }
                } else {
                    $ourpickchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $ourpickchef='';
        }

          /*if(!empty($dailymealchef)){

      //      echo 'if';
            foreach ($dailymealchef as $key => $value) {

                $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
              if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $value->price;
                $convertedmaxPrice= $value->maxprice;
                }
                $value->currency = $to_cur;
                $value->price =  round($convertedPrice);
                $value->maxprice =  round($convertedmaxPrice);
            }

        
}*/

       
            $data['ourpickchef'] =$ourpickchef;
            $this->load->view('front/ourpicks',$data);
        }
        

    }

    function exploreworld()
    {
        if (!$this->session->userdata('user_id')) {

            $to_cur ='HKD';
            $getfave = $this->welcome_model->getfave();
            $language = $this->session->userdata('site_lang');
            if($language  == ''){
                $language = 'english';
            }
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }


 

        /***********Our picks chef start************/
        $whatshostchef = $this->welcome_model->whatshostchef();

        if (!empty($whatshostchef)) {
            foreach ($whatshostchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $whatshostchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $whatshostchef[$key]->favouritecount = 0;
                    }
                } else {
                    $whatshostchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $whatshostchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $whatshostchef[$key]->price = 0;
                    }
                } else {
                    $whatshostchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $whatshostchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $whatshostchef[$key]->maxprice = 0;
                    }
                } else {
                    $whatshostchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $whatshostchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $whatshostchef[$key]->totalreview = 0;
                    }
                } else {
                    $whatshostchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $whatshostchef='';
        }
            if(!empty($whatshostchef)){
            foreach ($whatshostchef as $key => $value) {

               $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                
             
                
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
                }
                $value->currency = $to_cur;
                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice =  round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);
            }

        
}
             /********Service type counter start*******/
             $chefid='';
             $DailyMeal = $this->welcome_model->DailyMeal($chefid,$language);
             $CookingWorkshop = $this->welcome_model->CookingWorkshop($chefid,$language);
             $FoodDelivery = $this->welcome_model->FoodDelivery($chefid,$language);
             $OnsiteCooking = $this->welcome_model->OnsiteCooking($chefid,$language);
             $HomePrivateMeal = $this->welcome_model->HomePrivateMeal($chefid,$language);
             $RestaurantPrivate = $this->welcome_model->RestaurantPrivate($chefid,$language);

            /*********Service type counter end*********/
            $data['DailyMeal'] =$DailyMeal;
            $data['CookingWorkshop'] =$CookingWorkshop;
            $data['FoodDelivery'] =$FoodDelivery;
            $data['OnsiteCooking'] =$OnsiteCooking;
            $data['HomePrivateMeal'] =$HomePrivateMeal;
            $data['RestaurantPrivate'] =$RestaurantPrivate;
            $data['whatshostchef'] =$whatshostchef;

            $this->load->view('front/exploreworld',$data);


        }else{
            $language = $this->session->userdata('site_lang');
            if($language  == ''){
                $language = 'english';
            }
           
            $chefid = $this->session->userdata('user_id');
            
            $getLoggedinusercurrency  = $this->welcome_model->getLoggedinusercurrency($chefid);
            if(!empty($getLoggedinusercurrency)){
            $to_cur = $getLoggedinusercurrency[0]->currency;
            }else{
                $to_cur ='HKD';
            }
           
            $getfave = $this->welcome_model->getfave();
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }


            /***********Our picks chef start************/
        $whatshostchef = $this->welcome_model->whatshostchef($chefid);

        if (!empty($whatshostchef)) {
            foreach ($whatshostchef as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $whatshostchef[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $whatshostchef[$key]->favouritecount = 0;
                    }
                } else {
                    $whatshostchef[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $whatshostchef[$key]->price = $getless[$value->user_id];
                    } else {
                        $whatshostchef[$key]->price = 0;
                    }
                } else {
                    $whatshostchef[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $whatshostchef[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $whatshostchef[$key]->maxprice = 0;
                    }
                } else {
                    $whatshostchef[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $whatshostchef[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $whatshostchef[$key]->totalreview = 0;
                    }
                } else {
                    $whatshostchef[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $whatshostchef='';
        }

           if(!empty($whatshostchef)){
            foreach ($whatshostchef as $key => $value) {

               $fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                
             
                
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
                }
                $value->currency = $to_cur;
                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice =  round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);
            }

        
}
           /********Service type counter start*******/
             $DailyMeal = $this->welcome_model->DailyMeal($chefid,$language);
             $CookingWorkshop = $this->welcome_model->CookingWorkshop($chefid,$language);
             $FoodDelivery = $this->welcome_model->FoodDelivery($chefid,$language);
             $OnsiteCooking = $this->welcome_model->OnsiteCooking($chefid,$language);
             $HomePrivateMeal = $this->welcome_model->HomePrivateMeal($chefid,$language);
             $RestaurantPrivate = $this->welcome_model->RestaurantPrivate($chefid,$language);

            /*********Service type counter end*********/
            $data['DailyMeal'] =$DailyMeal;
            $data['CookingWorkshop'] =$CookingWorkshop;
            $data['FoodDelivery'] =$FoodDelivery;
            $data['OnsiteCooking'] =$OnsiteCooking;
            $data['HomePrivateMeal'] =$HomePrivateMeal;
            $data['RestaurantPrivate'] =$RestaurantPrivate;
       
            $data['whatshostchef'] =$whatshostchef;
            $this->load->view('front/exploreworld',$data);
        }
   }

   function getfilters(){
   $userid = $this->input->post('userid');
   $filtertype = $this->input->post('filter');
   $language = $this->session->userdata('site_lang');
            if($language  == ''){
                $language = 'english';
            }
   if(!empty($userid)){ //when user id logged in

    if($filtertype == 'highestRec'){ // Highest Recommended
        $getfilters = $this->welcome_model->gethighestRec($userid);

    }elseif($filtertype == 'highestBook'){ // Highest Bookmarked
        $getfilters = $this->welcome_model->gethighestBook($userid);

    }elseif($filtertype == 'hightolow'){ // Price : High to Low
        $getfilters = $this->welcome_model->gethightolow($userid);
    }elseif($filtertype == 'lowtohigh'){ // Price : Low to High
        $getfilters = $this->welcome_model->getlowtohigh($userid);
    }elseif($filtertype == 'ServiceType'){ // Price : Low to High
      
        $service_type = $this->input->post('service_type');
        if(!empty($service_type)){
        $arr= explode(",", $service_type);
        $arr = array_values(array_unique($arr,SORT_REGULAR));
        }else{
        $arr= array();
        }
        
       //print_r($arr);die;
        $getfilters = $this->welcome_model->getServiceType($userid,$arr);
    }

            $getLoggedinusercurrency  = $this->welcome_model->getLoggedinusercurrency($userid);
            if(!empty($getLoggedinusercurrency)){
            $to_cur = $getLoggedinusercurrency[0]->currency;
            }else{
                $to_cur ='HKD';
            }
            $getfave = $this->welcome_model->getfave();
            
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }
            if (!empty($getfilters)) {
            foreach ($getfilters as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $getfilters[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $getfilters[$key]->favouritecount = 0;
                    }
                } else {
                    $getfilters[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $getfilters[$key]->price = $getless[$value->user_id];
                    } else {
                        $getfilters[$key]->price = 0;
                    }
                } else {
                    $getfilters[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $getfilters[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $getfilters[$key]->maxprice = 0;
                    }
                } else {
                    $getfilters[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $getfilters[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $getfilters[$key]->totalreview = 0;
                    }
                } else {
                    $getfilters[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $getfilters = '';
        }
        // echo '<pre>';
        // print_r($getfilters);
        $html = '';

        if(!empty($getfilters)){
             foreach ($getfilters as $key => $value) {

$fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                
             
                
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
                }
                $value->currency = $to_cur;
                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice =  round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);


                    $imgs = $value->allimage;
                    $allimg  = explode("|", $imgs);
                    $allimg  = array_values(array_filter($allimg));
                    $count = count($allimg);
                 //   echo $count;
                    if($count > 4){
                      $remain = $count - 4 .'+';
                      $image1 = $allimg[0];
                      $image2 = $allimg[1];
                      $image3 = $allimg[2];
                      $image4 = $allimg[3];
                     
                  }else{
                      $remain = '';
                      $image1 = base_url().'front/images/blank.jpg';
                      $image2 = base_url().'front/images/blank.jpg';
                      $image3 = base_url().'front/images/blank.jpg';
                      $image4 = base_url().'front/images/blank.jpg';
                      
                  }

                  if(!empty($value->profile_pic)){
                    $profile_pic = $value->profile_pic;
                  }else{
                    $profile_pic = base_url().'front/images/blank.jpg';
                  }

              if(is_float($value->average_rating)){
                $avr_rating = $value->average_rating;
                }else{
                $avr_rating  = number_format((float)$value->average_rating, 1, '.', '');
                }

                $rountRat = round($value->average_rating);
                                           //echo $rountRat;
                                           if($rountRat > 0 && $rountRat <=0.5){
                                          
                                           $valhalf5 = '0.5';
                                           $chkhalf5 = 'checked';

                                           }else{
                                            $valhalf5 = '0.5';
                                            $chkhalf5 = '';
                                           }

                                           if($rountRat > 0.5 && $rountRat <=1){
                                          
                                           $valfull1 = '1.0';
                                           $chkfull1 = 'checked';

                                           }else{
                                            $valfull1 = '1.0';
                                            $chkfull1 = '';
                                           }

                                           if($rountRat > 1 && $rountRat <=1.5){
                                          
                                           $valhalf1 = '1.5';
                                           $chkhalf1 = 'checked';

                                           }else{
                                            $valhalf1 = '1.5';
                                            $chkhalf1 = '';
                                           }
                                           if($rountRat > 1.5 && $rountRat <=2){
                                          
                                           $valfull2 = '2';
                                           $chkfull2 = 'checked';

                                           }else{
                                            $valfull2 = '2';
                                            $chkfull2 = '';
                                           }

                                           if($rountRat > 2 && $rountRat <=2.5){
                                          
                                           $valhalf2 = '2.5';
                                           $chkhalf2 = 'checked';

                                           }else{
                                            $valhalf2 = '2.5';
                                            $chkhalf2 = '';
                                           }

                                           if($rountRat > 2.5 && $rountRat <=3){
                                          
                                           $valfull3 = '3';
                                           $chkfull3 = 'checked';

                                           }else{
                                            $valfull3 = '3';
                                            $chkfull3 = '';
                                           }

                                          if($rountRat > 3 && $rountRat <=3.5){
                                          
                                           $valhalf3 = '3.5';
                                           $chkhalf3 = 'checked';

                                           }else{
                                            $valhalf3 = '3.5';
                                            $chkhalf3 = '';
                                           }
                                           if($rountRat > 3.5 && $rountRat <=4){
                                          
                                           $valfull4 = '4';
                                           $chkfull4 = 'checked';

                                           }else{
                                            $valfull4= '4';
                                            $chkfull4 = '';
                                           }
                                           
                                           if($rountRat > 4 && $rountRat <=4.5){
                                          
                                           $valhalf4 = '4.5';
                                           $chkhalf4 = 'checked';

                                           }else{
                                            $valhalf4= '4.5';
                                            $chkhalf4 = '';
                                           }

                                           if($rountRat > 4.5 && $rountRat <=5){
                                          
                                           $valfull5 = '5';
                                           $chkfull5 = 'checked';

                                           }else{
                                            $valfull5= '5';
                                            $chkfull5 = '';
                                           }

                $html .='<div class="flterDataBox">
                    <div class="imgBox">
                        <div class="fullWdth">
                            <img src="'.$image1.'" alt="">
                        </div>
                        <div class="smlwdth">
                            <img src="'.$image2.'">
                        </div>
                        <div class="smlwdth">
                            <img src="'.$image3.'">
                        </div>
                        <div class="smlwdth mrRight">
                            <img src="'.$image4.'">
                            <div class="shadowBox">
                                <a href="'.base_url().'chefdetail/'.$value->user_id.'" class="text-center">'.$remain.'<br>More</a>
                            </div>
                        </div>
                    </div>
                    <div class="float-left KtchnDetaisl">
                        <div class="chfImg">
                            <div class="img">
                                <img src="'.$profile_pic.'" alt="">
                            </div>
                        </div>
                        <div class="Kitchn">
                            <h3 class="clr-black wishListed">'.ucfirst($value->kitchen_title).'</h3>
                            
                            <div class="float-left ktchnUsrName">
                                <a href="" class="clr-red">'.ucfirst($value->chef_fname).'</a> from <a href="#" class="clr-red">'.ucfirst($value->chef_country).'</a>
                            </div>
                            <div class="clearfix reactions">
                                <div class="reactionBox clr-gray">
                                    <span class="imogy yummy float-left"></span>
                                    <span class="counts float-left">'.$value->recommended.'</span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy avg float-left"></span>
                                    <span class="counts float-left">'.$value->ok.'</span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy bad float-left"></span>
                                    <span class="counts float-left">'.$value->no_recommended.'</span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy bkmark float-left"></span>
                                    <span class="counts float-left">'.$value->favouritecount.'</span>
                                </div>
                            </div>
                        </div>
                        <div class="lcCat">
                            <div class="float-left ktchLoc clr-gray">
                                <span class="float-left img"><img src="'.base_url().'front/images/loc-red.png" alt=""></span>
                                <span class="float-left">'.ucfirst($value->chef_address).'</span>
                            </div>
                            <div class="float-left ktchLoc itmcusine clr-gray">
                                <span class="float-left"><img src="'.base_url().'front/images/cusine.png" alt=""></span>
                                <span class="float-left no-brdr mrRight">'.$value->cuisine.'</span>
                            </div>
                        </div>
                        <div class="srvcType">
                            <span class="float-left img">
                                <img src="'.base_url().'front/images/platehand.png" alt="">
                            </span>
                            <span class="float-left clr-gray">'.$value->service_type.'</span>
                        </div>
                    </div> 
                    <div class="float-left rvwSec">
                        <div class="reviewsBox">
                            <div class="starBox">
                                <span class="rateScore">'.$avr_rating.'</span>
                                <span class="star">
                                    <div class="ratingBox">
                                        <ul>
                                                <li class="rateStar">
                                                <div class="cstmr-reviews">
                                                        <fieldset class="rating">
                                                            <input type="radio" id="stara'.$value->id.'" name="rating'.$value->id.'" value="'.$valfull5.'" '.$chkfull5.' disabled="disable" />
                                                            <label class = "full" for="stara'.$value->id.'" title="Awesome - 5 stars"></label>

                                                            <input type="radio" id="starb'.$value->id.'" name="rating'.$value->id.'" value="'.$valhalf4.'" '.$chkhalf4.'disabled="disable" />
                                                            <label class="half" for="starb'.$value->id.'" title="Pretty good - 4.5 stars"></label>

                                                            <input type="radio" id="starc'.$value->id.'" name="rating'.$value->id.'" value="'.$valfull4.'" '.$chkfull4.' disabled="disable"  />
                                                            <label class = "full" for="starc'.$value->id.'" title="Pretty good - 4 stars"></label>

                                                            <input type="radio" id="stard'.$value->id.'" name="rating'.$value->id.'" value="'.$valhalf3.'" '.$chkhalf3.' disabled="disable" />
                                                            <label class="half" for="stard'.$value->id.'" title="Meh - 3.5 stars"></label>

                                                            <input type="radio" id="stare'.$value->id.'" name="rating'.$value->id.'" value="'.$valfull3.'" '.$chkfull3.' disabled="disable"/>
                                                            <label class = "full" for="stare'.$value->id.'" title="Meh - 3 stars"></label>

                                                            <input type="radio" id="starf'.$value->id.'" name="rating'.$value->id.'" value="'.$valhalf2.'" '.$chkhalf2.' disabled="disable" />
                                                            <label class="half" for="starf'.$value->id.'" title="Kinda bad - 2.5 stars"></label>

                                                            <input type="radio" id="starg'.$value->id.'" name="rating'.$value->id.'" value="'.$valfull2.'" '.$chkfull2.' disabled="disable" />
                                                            <label class = "full" for="starg'.$value->id.'" title="Kinda bad - 2 stars"></label>

                                                            <input type="radio" id="starh'.$value->id.'" name="rating'.$value->id.'" value="'.$valhalf1.'" '.$chkhalf1.' disabled="disable"  />
                                                            <label class="half" for="starh'.$value->id.'" title="Meh - 1.5 stars"></label>

                                                            <input type="radio" id="stari'.$value->id.'" name="rating'.$value->id.'" value="'.$valfull1.'" '.$chkfull1.' disabled="disable" />
                                                            <label class = "full" for="stari'.$value->id.'" title="Sucks big time - 1 star"></label>

                                                            <input type="radio" id="starj'.$value->id.'" name="rating'.$value->id.'" value="'.$valhalf5.'" '.$chkhalf5.' disabled="disable" />
                                                            <label class="half" for="starj'.$value->id.'" title="Sucks big time - 0.5 stars"></label>
                                                        </fieldset>
                                                    </div>
                                           
                                                </li>
                                        </ul>
                                    </div>
                                    <p>'.$value->totalreview.' Reviews</p>
                                </span>
                            </div>
                        </div>
                        <div class="wishBox">
                            <span class="text-center">
                                <img src="'.base_url().'front/images/pricelabel.png" alt=""><br>
                                <strong class="clr-black">'.$value->currency.''.$value->price.' to '.$value->currency.''.$value->maxprice.'</strong><br> Per Guest
                            </span>
                        </div>
                    </div>
                </div>';
            }

        }
        echo $html;


   }else{
$userid = '';
     $language = $this->session->userdata('site_lang');
            if($language  == ''){
                $language = 'english';
            }

    if($filtertype == 'highestRec'){ // Highest Recommended
        $getfilters = $this->welcome_model->gethighestRec();

    }elseif($filtertype == 'highestBook'){ // Highest Bookmarked
        $getfilters = $this->welcome_model->gethighestBook();

    }elseif($filtertype == 'hightolow'){ // Price : High to Low
        $getfilters = $this->welcome_model->gethightolow();
    }elseif($filtertype == 'lowtohigh'){ // Price : Low to High
        $getfilters = $this->welcome_model->getlowtohigh();
    }elseif($filtertype == 'ServiceType'){ // Price : Low to High
      
        $service_type = $this->input->post('service_type');
        if(!empty($service_type)){
        $arr= explode(",", $service_type);
        $arr = array_values(array_unique($arr,SORT_REGULAR));
        }else{
        $arr= array();
        }
        
       //print_r($arr);die;
        $getfilters = $this->welcome_model->getServiceType($userid,$arr);
    }

            $to_cur ='HKD';
            $getfave = $this->welcome_model->getfave();
            $language = $this->input->post('language');
            if($language  == ''){
                $language = 'english';
            }
            if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
                    }
                } else {
                    $chefarrq = '';
                }
            $getprice = $this->welcome_model->getprice();
            $getmaxprice = $this->welcome_model->getmaxprice();
            if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
                }
            } else {
                $getless = '';
            }
        /*****get max price*****/
            if (!empty($getmaxprice)) {
                foreach ($getmaxprice as $key => $value) {
                    $getmax[$value->user_id] = $value->price_per_person;
                    
                }
            } else {
                $getmax = '';
            }

         /******Get rating********/
            $getrating = $this->welcome_model->getchefrating();
            if (!empty($getrating)) {
                foreach ($getrating as $key => $value) {
                    
                    $count[]   = $value['chef_id'];
                    $chefcount = array_count_values($count);
                }
            } else {
                
                $chefcount = '';
            }
            if (!empty($getfilters)) {
            foreach ($getfilters as $key => $value) {
                if ($chefarrq != '') {
                    if (array_key_exists($value->id, $chefarrq)) {
                        $getfilters[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $getfilters[$key]->favouritecount = 0;
                    }
                } else {
                    $getfilters[$key]->favouritecount = 0;
                }
                /*****get min price*******/
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $getfilters[$key]->price = $getless[$value->user_id];
                    } else {
                        $getfilters[$key]->price = 0;
                    }
                } else {
                    $getfilters[$key]->price = 0;
                }

                /******gte max price*******/

                if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $getfilters[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $getfilters[$key]->maxprice = 0;
                    }
                } else {
                    $getfilters[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $getfilters[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $getfilters[$key]->totalreview = 0;
                    }
                } else {
                    $getfilters[$key]->totalreview = 0;
                }
                
               
            }
        }else{
            $getfilters = '';
        }
        // echo '<pre>';
        // print_r($getfilters);
        $html = '';

        if(!empty($getfilters)){
             foreach ($getfilters as $key => $value) {
$fromChefcurr = $value->currency;
                if($fromChefcurr == ''){
                    $fromChefcurr='HKD';
                }
                
                if(!empty($value->price)){
                    $price = $value->price;
                }else{
                $price = 0;
                }
                 if(!empty($value->maxprice)){
                    $maxprice = $value->maxprice;
                }else{
                $maxprice = 0;
                }
                
             
                
                if($to_cur != $fromChefcurr && $price != '0' && $maxprice != '0'){ //HKD
                 //   echo "string";
                $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
                $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
                }else{
                $convertedPrice = $price;
                $convertedmaxPrice= $maxprice;
                }
                $value->currency = $to_cur;
                $value->price =  round($convertedPrice, 2, PHP_ROUND_HALF_UP);
                $value->maxprice =  round($convertedmaxPrice, 2, PHP_ROUND_HALF_UP);


                    $imgs = $value->allimage;
                    $allimg  = explode("|", $imgs);
                    $allimg  = array_values(array_filter($allimg));
                    $count = count($allimg);
                 //   echo $count;
                    if($count > 4){
                      $remain = $count - 4 .'+';
                      $image1 = $allimg[0];
                      $image2 = $allimg[1];
                      $image3 = $allimg[2];
                      $image4 = $allimg[3];
                     
                  }else{
                      $remain = '';
                      $image1 = base_url().'front/images/blank.jpg';
                      $image2 = base_url().'front/images/blank.jpg';
                      $image3 = base_url().'front/images/blank.jpg';
                      $image4 = base_url().'front/images/blank.jpg';
                      
                  }

                  if(!empty($value->profile_pic)){
                    $profile_pic = $value->profile_pic;
                  }else{
                    $profile_pic = base_url().'front/images/blank.jpg';
                  }

              if(is_float($value->average_rating)){
                $avr_rating = $value->average_rating;
                }else{
                $avr_rating  = number_format((float)$value->average_rating, 1, '.', '');
                }

                $rountRat = round($value->average_rating);
                                           //echo $rountRat;
                                           if($rountRat > 0 && $rountRat <=0.5){
                                          
                                           $valhalf5 = '0.5';
                                           $chkhalf5 = 'checked';

                                           }else{
                                            $valhalf5 = '0.5';
                                            $chkhalf5 = '';
                                           }

                                           if($rountRat > 0.5 && $rountRat <=1){
                                          
                                           $valfull1 = '1.0';
                                           $chkfull1 = 'checked';

                                           }else{
                                            $valfull1 = '1.0';
                                            $chkfull1 = '';
                                           }

                                           if($rountRat > 1 && $rountRat <=1.5){
                                          
                                           $valhalf1 = '1.5';
                                           $chkhalf1 = 'checked';

                                           }else{
                                            $valhalf1 = '1.5';
                                            $chkhalf1 = '';
                                           }
                                           if($rountRat > 1.5 && $rountRat <=2){
                                          
                                           $valfull2 = '2';
                                           $chkfull2 = 'checked';

                                           }else{
                                            $valfull2 = '2';
                                            $chkfull2 = '';
                                           }

                                           if($rountRat > 2 && $rountRat <=2.5){
                                          
                                           $valhalf2 = '2.5';
                                           $chkhalf2 = 'checked';

                                           }else{
                                            $valhalf2 = '2.5';
                                            $chkhalf2 = '';
                                           }

                                           if($rountRat > 2.5 && $rountRat <=3){
                                          
                                           $valfull3 = '3';
                                           $chkfull3 = 'checked';

                                           }else{
                                            $valfull3 = '3';
                                            $chkfull3 = '';
                                           }

                                          if($rountRat > 3 && $rountRat <=3.5){
                                          
                                           $valhalf3 = '3.5';
                                           $chkhalf3 = 'checked';

                                           }else{
                                            $valhalf3 = '3.5';
                                            $chkhalf3 = '';
                                           }
                                           if($rountRat > 3.5 && $rountRat <=4){
                                          
                                           $valfull4 = '4';
                                           $chkfull4 = 'checked';

                                           }else{
                                            $valfull4= '4';
                                            $chkfull4 = '';
                                           }
                                           
                                           if($rountRat > 4 && $rountRat <=4.5){
                                          
                                           $valhalf4 = '4.5';
                                           $chkhalf4 = 'checked';

                                           }else{
                                            $valhalf4= '4.5';
                                            $chkhalf4 = '';
                                           }

                                           if($rountRat > 4.5 && $rountRat <=5){
                                          
                                           $valfull5 = '5';
                                           $chkfull5 = 'checked';

                                           }else{
                                            $valfull5= '5';
                                            $chkfull5 = '';
                                           }

                $html .='<div class="flterDataBox">
                    <div class="imgBox">
                        <div class="fullWdth">
                            <img src="'.$image1.'" alt="">
                        </div>
                        <div class="smlwdth">
                            <img src="'.$image2.'">
                        </div>
                        <div class="smlwdth">
                            <img src="'.$image3.'">
                        </div>
                        <div class="smlwdth mrRight">
                            <img src="'.$image4.'">
                            <div class="shadowBox">
                                <a href="'.base_url().'chefdetail/'.$value->user_id.'" class="text-center">'.$remain.'<br>More</a>
                            </div>
                        </div>
                    </div>
                    <div class="float-left KtchnDetaisl">
                        <div class="chfImg">
                            <div class="img">
                                <img src="'.$profile_pic.'" alt="">
                            </div>
                        </div>
                        <div class="Kitchn">
                            <h3 class="clr-black wishListed">'.ucfirst($value->kitchen_title).'</h3>
                            
                            <div class="float-left ktchnUsrName">
                                <a href="" class="clr-red">'.ucfirst($value->chef_fname).'</a> from <a href="#" class="clr-red">'.ucfirst($value->chef_country).'</a>
                            </div>
                            <div class="clearfix reactions">
                                <div class="reactionBox clr-gray">
                                    <span class="imogy yummy float-left"></span>
                                    <span class="counts float-left">'.$value->recommended.'</span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy avg float-left"></span>
                                    <span class="counts float-left">'.$value->ok.'</span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy bad float-left"></span>
                                    <span class="counts float-left">'.$value->no_recommended.'</span>
                                </div>
                                <div class="reactionBox clr-gray">
                                    <span class="imogy bkmark float-left"></span>
                                    <span class="counts float-left">'.$value->favouritecount.'</span>
                                </div>
                            </div>
                        </div>
                        <div class="lcCat">
                            <div class="float-left ktchLoc clr-gray">
                                <span class="float-left img"><img src="'.base_url().'front/images/loc-red.png" alt=""></span>
                                <span class="float-left">'.ucfirst($value->chef_address).'</span>
                            </div>
                            <div class="float-left ktchLoc itmcusine clr-gray">
                                <span class="float-left"><img src="'.base_url().'front/images/cusine.png" alt=""></span>
                                <span class="float-left no-brdr mrRight">'.$value->cuisine.'</span>
                            </div>
                        </div>
                        <div class="srvcType">
                            <span class="float-left img">
                                <img src="'.base_url().'front/images/platehand.png" alt="">
                            </span>
                            <span class="float-left clr-gray">'.$value->service_type.'</span>
                        </div>
                    </div> 
                    <div class="float-left rvwSec">
                        <div class="reviewsBox">
                            <div class="starBox">
                                <span class="rateScore">'.$avr_rating.'</span>
                                <span class="star">
                                    <div class="ratingBox">
                                        <ul>
                                                <li class="rateStar">
                                                <div class="cstmr-reviews">
                                                        <fieldset class="rating">
                                                            <input type="radio" id="stara'.$value->id.'" name="rating'.$value->id.'" value="'.$valfull5.'" '.$chkfull5.' disabled="disable" />
                                                            <label class = "full" for="stara'.$value->id.'" title="Awesome - 5 stars"></label>

                                                            <input type="radio" id="starb'.$value->id.'" name="rating'.$value->id.'" value="'.$valhalf4.'" '.$chkhalf4.'disabled="disable" />
                                                            <label class="half" for="starb'.$value->id.'" title="Pretty good - 4.5 stars"></label>

                                                            <input type="radio" id="starc'.$value->id.'" name="rating'.$value->id.'" value="'.$valfull4.'" '.$chkfull4.' disabled="disable"  />
                                                            <label class = "full" for="starc'.$value->id.'" title="Pretty good - 4 stars"></label>

                                                            <input type="radio" id="stard'.$value->id.'" name="rating'.$value->id.'" value="'.$valhalf3.'" '.$chkhalf3.' disabled="disable" />
                                                            <label class="half" for="stard'.$value->id.'" title="Meh - 3.5 stars"></label>

                                                            <input type="radio" id="stare'.$value->id.'" name="rating'.$value->id.'" value="'.$valfull3.'" '.$chkfull3.' disabled="disable"/>
                                                            <label class = "full" for="stare'.$value->id.'" title="Meh - 3 stars"></label>

                                                            <input type="radio" id="starf'.$value->id.'" name="rating'.$value->id.'" value="'.$valhalf2.'" '.$chkhalf2.' disabled="disable" />
                                                            <label class="half" for="starf'.$value->id.'" title="Kinda bad - 2.5 stars"></label>

                                                            <input type="radio" id="starg'.$value->id.'" name="rating'.$value->id.'" value="'.$valfull2.'" '.$chkfull2.' disabled="disable" />
                                                            <label class = "full" for="starg'.$value->id.'" title="Kinda bad - 2 stars"></label>

                                                            <input type="radio" id="starh'.$value->id.'" name="rating'.$value->id.'" value="'.$valhalf1.'" '.$chkhalf1.' disabled="disable"  />
                                                            <label class="half" for="starh'.$value->id.'" title="Meh - 1.5 stars"></label>

                                                            <input type="radio" id="stari'.$value->id.'" name="rating'.$value->id.'" value="'.$valfull1.'" '.$chkfull1.' disabled="disable" />
                                                            <label class = "full" for="stari'.$value->id.'" title="Sucks big time - 1 star"></label>

                                                            <input type="radio" id="starj'.$value->id.'" name="rating'.$value->id.'" value="'.$valhalf5.'" '.$chkhalf5.' disabled="disable" />
                                                            <label class="half" for="starj'.$value->id.'" title="Sucks big time - 0.5 stars"></label>
                                                        </fieldset>
                                                    </div>
                                           
                                                </li>
                                        </ul>
                                    </div>
                                    <p>'.$value->totalreview.' Reviews</p>
                                </span>
                            </div>
                        </div>
                        <div class="wishBox">
                            <span class="text-center">
                                <img src="'.base_url().'front/images/pricelabel.png" alt=""><br>
                                <strong class="clr-black">'.$value->currency.''.$value->price.' to '.$value->currency.''.$value->maxprice.'</strong><br> Per Guest
                            </span>
                        </div>
                    </div>
                </div>';
            }

        }
        echo $html;



   }

   }

   function wishlist(){

    if (!$this->session->userdata('user_id')) {
        $this->load->view('front/home');
    }else{
        $user_id = $this->session->userdata('user_id');
        $getLoggedinusercurrency  = $this->welcome_model->getLoggedinusercurrency($user_id);

        if(!empty($getLoggedinusercurrency)){
            $to_cur = $getLoggedinusercurrency[0]->currency;
        }else{
            $to_cur ='HKD';
        }

        $getfavroite = $this->welcome_model->getfavroite($user_id);
        
        $chefidarr = array();
        foreach ($getfavroite as $key => $value) {
            //print_r($value);
            $chefidarr[] = $value->chef_id;
        }
        // / print_r(array_unique($chefidarr));
        
        $chefids = implode(",", array_unique($chefidarr));
        // echo $chefids;
        
        $getchefprofile = $this->welcome_model->getchefprofile($chefids);
        
        
        $getfave = $this->welcome_model->getfave();
        
        if (!empty($getfave)) {
            foreach ($getfave as $key => $value) {
                $chefarr[] = $value->chef_id;
                $chefarrq  = array_count_values($chefarr);
            }
        } else {
            $chefarrq = '';
        }
        //print_r($chefarrq);die;
        $getprice = $this->welcome_model->getprice();
        if (!empty($getprice)) {
            foreach ($getprice as $key => $value) {
                $getless[$value->user_id] = $value->price_per_person;
                
            }
        } else {
            $getless = '';
        }

        $getmaxprice = $this->welcome_model->getmaxprice();
        
        /*****get max price*****/
        if (!empty($getmaxprice)) {
            foreach ($getmaxprice as $key => $value) {
                $getmax[$value->user_id] = $value->price_per_person;
                
            }
        } else {
            $getmax = '';
        }

        /******Get rating********/
        $getrating = $this->welcome_model->getchefrating();
        //print_r($getrating);die;
        if (!empty($getrating)) {
            foreach ($getrating as $key => $value) {
                //if(!empty($value['review'])){
                
                $count[]   = $value['chef_id'];
                $chefcount = array_count_values($count);
                
                
            }
        } else {
            
            $chefcount = '';
        }
        if (!empty($getchefprofile)) {
            foreach ($getchefprofile as $key => $value) {
                if ($chefarrq != '') {
                    // /    echo $value->id;die;
                    if (array_key_exists($value->id, $chefarrq)) {
                        $getchefprofile[$key]->favouritecount = $chefarrq[$value->id];
                    } else {
                        $getchefprofile[$key]->favouritecount = 0;
                    }
                } else {
                    $getchefprofile[$key]->favouritecount = 0;
                }
                
                if ($getless != '') {
                    if (array_key_exists($value->user_id, $getless)) {
                        $getchefprofile[$key]->price = $getless[$value->user_id];
                    } else {
                        $getchefprofile[$key]->price = 0;
                    }
                } else {
                    $getchefprofile[$key]->price = 0;
                }

                 if ($getmax != '') {
                    if (array_key_exists($value->user_id, $getmax)) {
                        $getchefprofile[$key]->maxprice = $getmax[$value->user_id];
                    } else {
                        $getchefprofile[$key]->maxprice = 0;
                    }
                } else {
                    $getchefprofile[$key]->maxprice = 0;
                }
                
                if ($chefcount != '') {
                    if (array_key_exists($value->user_id, $chefcount)) {
                        $getchefprofile[$key]->totalreview = $chefcount[$value->user_id];
                    } else {
                        $getchefprofile[$key]->totalreview = 0;
                    }
                } else {
                    $getchefprofile[$key]->totalreview = 0;
                }
            }
            
        }

        // foreach ($getchefprofile as $key => $value) {
        //         $fromChefcurr = $value->currency;
        //         $price = $value->price;
        //         $maxprice = $value->maxprice;
        //       //  echo $to_cur .'==='. $fromChefcurr .$price;
        //         if($to_cur != $fromChefcurr){ //HKD
        //         $convertedPrice = $this->convertCurrency($price, $fromChefcurr, $to_cur);
        //         $convertedmaxPrice = $this->convertCurrency($maxprice, $fromChefcurr, $to_cur);
        //         }else{
        //         $convertedPrice = $value->price;
        //         $convertedmaxPrice = $value->maxprice;
        //         }
        //         $value->currency = $to_cur;
        //         $value->price =  round($convertedPrice);
        //         $value->maxprice =  round($convertedmaxPrice);
        //     }
       
       $data['wishlist'] = $getchefprofile;
        $this->load->view('front/wishlist',$data);
        
    }

   }



}
