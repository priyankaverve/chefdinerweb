<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// Load library and url helper
		$this->load->library('facebook');
		$this->load->helper('url');
        $this->load->model('users_model'); 

	}

	// ------------------------------------------------------------------------

	/**
	 * Index page
	 */
	public function index()
	{
		$this->load->view('login');
	}

	// ------------------------------------------------------------------------

	/**
	 * Web redirect login example page
	 */
	public function web_login()
	{
		
		//echo "dfjsfjk";
		$data['user'] = array();

		// Check if user is logged in
		if ($this->facebook->is_authenticated())
		{
			//print_r($data['user']);die;
			// User logged in, get user details
			$user = $this->facebook->request('get', '/me?fields=id,name,email,picture');
			//print_r($user);die;
			if (!isset($user['error']))
			{
				//echo "string";die;

				$data['user'] = $user;
				$image = "https://graph.facebook.com/".$user['id']."/picture?type=large";
				  $user_information  = array(
				    'username'    => $user["name"],
				    'email'   => $user["email"],
				    'profile_pic'=>$image,
				    'created_at' =>date("Y-m-d H:i:s"),
				    'updated_at' =>date("Y-m-d H:i:s"),
				    'user_type' =>1,
				    'status'=>1,
				    //'gender'  => $user["gender"],
				   // 'birthday' => date("Y-m-d",strtotime($user['birthday'])),
				    'log_mode' => 'facebook'
				);
				$log_users = $this->users_model->check_auth($user['id'],'facebook');
			//print_r($log_users);die;
	            if(is_object($log_users)){
	            	//echo "if";
		            $session_data=array('user_id'=>$log_users->user_id,
				'username'=>$log_users->username,
		            	'log_mode'=>$log_users->log_mode,
		            	 'email'=>$log_users->email,
             			 'user_type'=>$log_users->user_type
		            	);
	             $this->session->set_userdata($session_data);
	             if($this->session->userdata('user_id')!='' && $this->session->userdata('log_mode') == 'facebook'){
	             	//echo "dffdff". $log_users->user_id;die;
	            	$this->db->where('auth_id',$user['id']);
    				$this->db->update('users', $user_information);
	                //$this->session->set_flashdata('message', 'Welcome Back '.$log_users->name);
	                //redirect('user/dashboard');
	                redirect('template');
	             }
	          
	            }
	            else {
	            	//echo "else";
	            	$user_information['auth_id'] = $user['id'];
	            	$this->db->insert('users',$user_information);
	            	$id = $this->db->insert_id();
	            	//echo "dff". $this->db->insert_id();die;
		            // $session_data=array('session_id'=>$id);
		            $session_data=array('user_id'=>$id,
				'username'=>$user["name"],
		            	'log_mode'=>'facebook',
		            	'email'=>$user["email"],
		            	'user_type'=>1);
		            // echo "<pre>";
		            // print_r($session_data);
		            $this->session->set_userdata($session_data);
		             if($this->session->userdata('user_id')!='' && $this->session->userdata('log_mode') == 'facebook'){
		             	//redirect('user/dashboard');
		             	redirect('template');
		             }
	               // $this->session->set_flashdata('message', 'Thank for registration with us.');
	            }
	            //endif;
	            
		    //    $this->session->set_userdata('user_session_info', $session_data);
		    //    print_r( $this->session->userdata('user_session_info')['user_id'] );
		    //    //print_r($user_information);
		    //   // die;
		    //    if($this->session->userdata('user_session_info')['user_id'] !=''  &&  $this->session->userdata('user_session_info')['user_type'] == 1){
		    //   // 	echo 'hello';die;
		    //    redirect('user/dashboard');
		    // }
		  //  echo "string";die;
			}

		}
		else 
		{
			echo 'We are unable fetch your facebook information.'; exit;
		}

		// display view
	}

	// ------------------------------------------------------------------------

	
	public function logout()
	{
		//$this->facebook->setAccessToken('');
		// $this->session->unset_userdata('user_id');
		// $this->session->unset_userdata('log_mode');
		// $this->session->unset_userdata('email');
		// $this->session->unset_userdata('user_type');
		$this->facebook->destroy_session();
		redirect('welcome/web_login');
	}
}
